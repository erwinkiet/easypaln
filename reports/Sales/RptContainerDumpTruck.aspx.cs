﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Text;

public partial class reports_Sales_RptContainerDumpTruck : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtReport = new DataTable("dtReport"); // datatable DO

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2, filter, fieldName1, fieldName2, filterValue;
        tanggal = "";
        tanggal2 = "";
        filter = "";
        fieldName1 = "";
        fieldName2 = "";
        filterValue = "";

        if (Page.IsPostBack)
        {
            dtReport = Session["dtReport"] as DataTable;
        }
        if (!Page.IsPostBack)
        {
            FillSession();
        }

        SqlDataAdapter da = new SqlDataAdapter();

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();
        filter = Request.QueryString["f"].ToString().Trim();
        fieldName1 = Request.QueryString["f1"].ToString().Trim();
        fieldName2 = Request.QueryString["f2"].ToString().Trim();
        filterValue = Request.QueryString["fv"].ToString().Trim();

        if(fieldName1 == "alldriver" )
        {
            CrystalReportSource1.Report.FileName = "RptContainerDumpTruckAllDriver.rpt";
        }
        if (fieldName1 != "alldriver")
        {
            CrystalReportSource1.Report.FileName = "RptContainerDumpTruck.rpt";
        }


        if (fieldName2 == "InputDate" && fieldName1 == "alldriver" && filterValue == "")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.Additional, data.TransactionNo ,data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut,td.TransactionNo, CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.InputDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.InputDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }
        if (fieldName2 == "TransactionDate" && fieldName1 == "alldriver" && filterValue == "")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.Additional, data.TransactionNo ,data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove * Netto ELSE PriceBelow * Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut,td.TransactionNo, CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id

                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.TransactionDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.TransactionDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }
        if (fieldName2 == "CrossDate" && fieldName1 == "alldriver" && filterValue == "")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.Additional, data.TransactionNo ,data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut,td.TransactionNo, CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.CrossDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.CrossDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }
        if (fieldName2 == "InputDate" && fieldName1 == "all" && filterValue == "" )
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.TransactionNo,data.Additional, data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut, td.TransactionNo,CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.InputDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.InputDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }
        if (fieldName2 == "TransactionDate" && fieldName1 == "all" && filterValue == "")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.TransactionNo ,data.Additional, data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut, td.TransactionNo,CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.TransactionDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.TransactionDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }

        if (fieldName2 == "CrossDate" && fieldName1 == "all" && filterValue == "")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.Additional, data.TransactionNo ,data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut,td.TransactionNo, CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.CrossDate,103) >=CONVERT(DATE, '" + tanggal +@"', 103) AND 
                                    CONVERT(DATE,td.CrossDate,103)<=CONVERT(DATE, '" + tanggal2 +@"', 103) ) data
                                    order by data.drivername asc", con);
        }

        if (fieldName2 == "InputDate" && fieldName1 == "driver")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.TravelFeeDate,data.TransactionNo,data.Additional, data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut, td.TransactionNo,CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE d.Name = '" + filterValue + @"' AND td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.InputDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.InputDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }
        if (fieldName2 == "TransactionDate" && fieldName1 == "driver")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.TransactionNo ,data.Additional, data.TravelFeeDate,data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut, td.TransactionNo,CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE d.Name = '" + filterValue + @"' AND td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.TransactionDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.TransactionDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }

        if (fieldName2 == "CrossDate" && fieldName1 == "driver")
        {
            da = new SqlDataAdapter(@"select data.Id, data.Cut, data.TransactionNo,data.TravelFeeDate,data.Additional, data.VehicleId ,data.DriverId, data.DriverName, data.VehicleNo, data.ItemName,
                                    data.Destination, data.TransactionDate, data.Netto, data.Net, data.Ton, data.TravelFee,
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) AS Price,

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee from (select td.Cut, td.TransactionNo,CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate,td.Additional, td.VehicleId ,td.DriverId, d.Name as DriverName, v.VehicleNo, td.ItemId, it.Name as ItemName,
                                    Rtrim(te.Origin) +' - '+ te.Destination as Destination, CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate, te.HasBase,
                                    te.PriceAbove, te.PriceBelow, te.TonBase, te.Price, te.Base, td.TravelFee , 

                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    td.OriginNetto/1000 ELSE 
                                    td.DestinationNetto/1000 END AS Net,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto/1000 ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto/1000 ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Ton,
                                    CASE WHEN td.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN te.TonBase = 0 THEN td.OriginNetto ELSE 
                                    CASE WHEN td.OriginNetto/1000 >= 27 THEN 27 ELSE td.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN te.TonBase = 0 THEN td.DestinationNetto ELSE 
                                    CASE WHEN td.DestinationNetto/1000 >= 27 THEN 27 ELSE td.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto, td.Id


                                    from TravelDocuments td
                                    inner join Driver d
                                    on d.Id = td.DriverId
                                    inner join Vehicle v
                                    on v.Id = td.VehicleId
                                    inner join Items it
                                    on it.Id = td.ItemId
                                    inner join TravelExpenses te
                                    on te.Id = td.TravelExpenseId

                                    WHERE d.Name = '" + filterValue + @"' AND td.IsExternal = 0 and td.IsContractWork = 0 and td.Status<>'Cancel' AND CONVERT(DATE,td.CrossDate,103) >=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    CONVERT(DATE,td.CrossDate,103)<=CONVERT(DATE, '" + tanggal2 + @"', 103) ) data
                                    order by data.drivername asc", con);
        }


        dtReport.Clear();
        da.Fill(dtReport);



        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtReport);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());
    }
        protected void Page_OnUnload(object sender, EventArgs e) 
    {
        // remove all session for this page
        if (!Page.IsPostBack) {
            Session.Remove("dtReport");
        }
    }
    protected void FillSession() 
    {
        Session.Add("dtReport", dtReport);
    }

    public string allrecord1 { get; set; }
}