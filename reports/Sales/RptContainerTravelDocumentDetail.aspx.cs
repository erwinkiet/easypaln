﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class reports_Sales_RptContainerTravelDocumentDetail : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtTravelDocumentDetail = new DataTable("dtTravelDocumentDetail");

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtTravelDocumentDetail");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtTravelDocumentDetail", dtTravelDocumentDetail);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2, filter, fieldName, optr;
        tanggal = "";
        tanggal2 = "";
        optr = "";

        if (Page.IsPostBack)
        {
            dtTravelDocumentDetail = Session["dtTravelDocumentDetail"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        SqlDataAdapter da = new SqlDataAdapter();

        filter = Request.QueryString["f"].ToString().Trim();
        fieldName = Request.QueryString["fn"].ToString().Trim();
        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();

        if (filter == "y" && fieldName == "External")
        {
            optr = "1";
        }

        if (filter == "y" && fieldName == "NonExternal")
        {
            optr = "0";
        }

        if (filter == "y" && fieldName == "Default")
        {
            da = new SqlDataAdapter(@"SELECT tds.Id, tds.TransactionNo, CONVERT(varchar,tds.TransactionDate,103) as Date, tds.TravelExpenseId,
            vc.VehicleNo, tds.TravelFee, dr.Name as Driver, tds.ShipName, tds.CustomerId, cst.Name as CustomerName,
            tds.Bruto, tds.Tare, tds.OriginNetto, tds.DestinationNetto, tds.UseDestinationNetto,  
            itm.ItemId, itm.Name as ItemName
            FROM TravelDocuments tds
            INNER JOIN Customer cst
            ON tds.CustomerId = cst.CustomerId
            INNER JOIN Items itm
            ON tds.ItemId = itm.Id
            INNER JOIN Vehicle vc
            ON tds.VehicleId = vc.Id
            INNER JOIN Driver dr
            ON tds.DriverId = dr.Id
            WHERE tds.Status<>'Cancel' AND tds.TransactionDate>=convert(date,'" + tanggal + @"',103) AND 
            tds.TransactionDate<=convert(date,'" + tanggal2 + @"',103)
            order by tds.TransactionDate asc, tds.TransactionNo asc", con);
        }

        else {
        da = new SqlDataAdapter(@"SELECT tds.Id, tds.TransactionNo, CONVERT(varchar,tds.TransactionDate,103) as Date, tds.TravelExpenseId,
            vc.VehicleNo, tds.TravelFee, dr.Name as Driver, tds.ShipName, tds.CustomerId, cst.Name as CustomerName,
            tds.Bruto, tds.Tare, tds.OriginNetto, tds.DestinationNetto, tds.UseDestinationNetto,  
            itm.ItemId, itm.Name as ItemName
            FROM TravelDocuments tds
            INNER JOIN Customer cst
            ON tds.CustomerId = cst.CustomerId
            INNER JOIN Items itm
            ON tds.ItemId = itm.Id
            INNER JOIN Vehicle vc
            ON tds.VehicleId = vc.Id
            INNER JOIN Driver dr
            ON tds.DriverId = dr.Id
            WHERE tds.Status<>'Cancel' AND tds.IsExternal = " + optr + @" AND tds.TransactionDate>=convert(date,'" + tanggal + @"',103) AND 
            tds.TransactionDate<=convert(date,'" + tanggal2 + @"',103)
            order by tds.TransactionDate asc, tds.TransactionNo asc", con);
       }
        dtTravelDocumentDetail.Clear();
        da.Fill(dtTravelDocumentDetail);
        
        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtTravelDocumentDetail);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}