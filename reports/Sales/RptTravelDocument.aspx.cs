﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptTravelDocument : BasePage
{
    DataTable dtTD = new DataTable("dtTD"); // datatable DO
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtApplicationReports = new DataTable();
    
    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtTD");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtTD", dtTD);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            dtTD = Session["dtTD"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            if (!dtTD.Columns.Contains("TDID"))
            {
                DataColumn tmbh_column = dtTD.Columns.Add("TDID", typeof(int));
                dtTD.Columns.Add("TransactionNo", typeof(string));
                dtTD.Columns.Add("Date", typeof(string));
                dtTD.Columns.Add("TravelExpenseId", typeof(int));
                dtTD.Columns.Add("VehicleNo", typeof(string));
                dtTD.Columns.Add("TravelFee", typeof(decimal));
                dtTD.Columns.Add("Driver", typeof(string));
                dtTD.Columns.Add("ShipName", typeof(string));
                dtTD.Columns.Add("CustomerId", typeof(string));
                dtTD.Columns.Add("CustomerName", typeof(string));
                dtTD.Columns.Add("ItemId", typeof(int));
                dtTD.Columns.Add("Bruto", typeof(decimal));
                dtTD.Columns.Add("Tare", typeof(decimal));
                dtTD.Columns.Add("OriginNetto", typeof(decimal));
                dtTD.Columns.Add("DestinationNetto", typeof(decimal));
                dtTD.Columns.Add("UseDestinationNetto", typeof(int));
                dtTD.Columns.Add("Remark", typeof(string));
            }

            FillSession();
        }

        string TDID = Request.QueryString["nota"].Trim();
        string query = @"select td.Id, td.TransactionNo, CONVERT(varchar(10), td.TransactionDate, 120) as Date, 
                         td.TravelExpenseId, td.VehicleId,v.VehicleNo , td.TravelFee, td.DriverId, d.Name as Driver ,td.ShipName, 
                         td.CustomerId, cus.Name as CustomerName,
                         td.ItemId, td.Bruto, td.Tare, td.OriginNetto, td.DestinationNetto, td.UseDestinationNetto, 
                         td.Remark from TravelDocuments td 
                         inner join Customer cus on cus.CustomerID = td.CustomerId
                         inner join Vehicle v on td.VehicleId = v.Id
                         inner join Driver d on d.Id = td.DriverId where td.Id = " + TDID + @" ";

        SqlDataAdapter da = new SqlDataAdapter(query, con);
        dtTD.Clear();
        da.Fill(dtTD);



        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtTD);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

    }

    public string allrecord1 { get; set; }
}