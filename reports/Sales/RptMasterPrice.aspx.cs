﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptMasterPrice : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtMasterPrice = new DataTable("dtMasterPrice"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtMasterPrice");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtMasterPrice", dtMasterPrice);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.IsPostBack)
        {
            dtMasterPrice = Session["dtMasterPrice"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }



        SqlDataAdapter da = new SqlDataAdapter(@"SELECT data.Base, data.Destination, data.HasBase, data.Item, data.CustomerName, data.FPrice,
                            data.ItemId, data.Price,data.PriceAbove, data.PriceBelow, data.TonBase,
                            data.total, data.grand
                            FROM (SELECT te.Base, Rtrim(te.Origin) + '-' + te.Destination as Destination, te.HasBase, 
                            te.Id, te.ItemId as Item, c.Name as CustomerName, fpds.Price as FPrice,
                            it.Name as ItemId, te.Price, te.PriceAbove, te.PriceBelow, te.TonBase,
                            (fpds.Price * te.Base) as grand,
                            (fpds.Price * te.Base) - te.PriceAbove as total
                            FROM TravelExpenses te
                            INNER JOIN Items it
                            ON it.Id = te.ItemId
                            INNER JOIN FreightPriceDetails fpds
                            ON fpds.ItemId = te.ItemId
                            INNER JOIN FreightPrices fps
                            ON fps.Id = fpds.FreightPriceId
                            INNER JOIN Customer c
                            ON c.CustomerID = fps.CustomerId
                            where te.TonBase = 0 and te.HasBase = 1 and fpds.Price <> '0'
                            ) data order by data.ItemId asc, data.destination asc, data.CustomerName, data.Fprice asc", con);

        dtMasterPrice.Clear();
        da.Fill(dtMasterPrice);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtMasterPrice);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

    }
}