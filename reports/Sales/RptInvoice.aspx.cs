﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptInvoice : System.Web.UI.Page
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtInvoiceNote = new DataTable("dtInvoiceNote");

    string say, fv;

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtInvoiceNote");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtInvoiceNote", dtInvoiceNote);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string filter, filter1, f, fn;

        if (Page.IsPostBack)
        {
            dtInvoiceNote = Session["dtInvoiceNote"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }


        string bcadarno = "NB : Pembayaran melalui cek / transfer mohon ditujukan ke BANK CENTRAL ASIA CB. DIPONEGORO MEDAN A/N. DARNO HARTONO No. A/C 0222-59-1919 , dan harap dilakukan 30 hari setelah invoice ini diterima.";
        string bcapjp = "NB : Pembayaran melalui cek / transfer mohon ditujukan ke BANK CENTRAL ASIA CB. DIPONEGORO MEDAN A/N. PT. PRIMA JAYA PERKASA No. A/C 0221-83-8598 , dan harap dilakukan 30 hari setelah invoice ini diterima.";
        string danamonpjp = "NB : Pembayaran melalui cek / transfer mohon ditujukan ke BANK DANAMON A/N. PT. PRIMA JAYA PERKASA CB. DIPONEGORO MEDAN No. A/C 359-723-7423 , dan harap dilakukan 30 hari setelah invoice ini diterima.";



        filter = Request.QueryString["IsPpn"].ToString().Trim();
        filter1 = Request.QueryString["IsPph"].ToString().Trim();
        f = Request.QueryString["f"].ToString().Trim();
        fn = Request.QueryString["fn"].ToString().Trim();


        if (filter == "0" && filter1 == "1" || filter1 == "0")
        {
            CrystalReportSource1.Report.FileName = "InvoiceNoteNON.rpt";
        }
        if (filter == "1" && filter1 == "0")
        {
            CrystalReportSource1.Report.FileName = "InvoiceNotePPN.rpt";
        }
        if (filter == "1" && filter1 == "1")
        {
            CrystalReportSource1.Report.FileName = "InvoiceNotePPNPPH.rpt";
        }

        if (f == "y" && fn == "bcadarno")
        {
            fv = bcadarno;
        }

        if (f == "y" && fn == "bcapjp")
        {
            fv = bcapjp;
        }

        if (f == "y" && fn == "danamonpjp")
        {
            fv = danamonpjp;
        }

        


        string invoiceId = Request.QueryString["Id"].Trim();

        string query = @"SELECT inv.Id, inv.TransactionNo, inv.InputDate, CONVERT(VARCHAR(10),inv.LoadDate,103) as LoadDate, CONVERT(VARCHAR(10),inv.UnloadDate,103) as UnLoadDate,inv.CustomerId, cst.Name as CustomerName,
                         inv.IsPpn, inv.BaseNumber, inv.Remark, inv.Price, inv.Total, cst.City, inv.Pph, inv.GrandTotal, tvd.Id as TravelDocumentId, inv.Total + inv.EtcPrice as TotalGrand,
                         CONVERT(DATE,tvd.TransactionDate,103) as TravelDocumentDate, inv.EtcPrice, inv.RemarkEtc, tvd.VehicleId, v.VehicleNo,tvd.UseDestinationNetto, tvd.OriginNetto, 
                         tvd.DestinationNetto, tvd.Bruto, tvd.Tare, tvd.ItemId, itm.Name as ItemName, RTRIM(tve.Origin) +' - ' + tve.Destination as Destination,
                         CASE WHEN tvd.UseDestinationNetto = 0 THEN tvd.OriginNetto ELSE tvd.DestinationNetto END AS netto
                         FROM Invoices inv
                         INNER JOIN InvoiceDetails invd
                         ON inv.Id = invd.InvoiceId
                         INNER JOIN TravelDocuments tvd
                         ON invd.TravelDocumentId = tvd.Id
                         INNER JOIN Customer cst
                         ON tvd.CustomerId = cst.CustomerID
                         INNER JOIN Items itm
                         ON tvd.ItemId = itm.Id
                         INNER JOIN TravelExpenses tve
                         ON tvd.TravelExpenseId = tve.Id
                         INNER JOIN Vehicle v
                         ON v.Id = tvd.VehicleId
                         WHERE inv.Id = '" + invoiceId + "' ";
        SqlDataAdapter da = new SqlDataAdapter(query, con);
        dtInvoiceNote.Clear();
        da.Fill(dtInvoiceNote);



        string tanggal = Helper.getIndonesianDate(dtInvoiceNote.Rows[0]["InputDate"].ToString());

        if (filter1 == "1")
        {
            say = '#' + Helper.Say(Convert.ToInt64(dtInvoiceNote.Rows[0]["GrandTotal"])) + '#';
        }
        if (filter1 == "0")
        {
            say = '#' + Helper.Say(Convert.ToInt64(dtInvoiceNote.Rows[0]["TotalGrand"])) + '#';
        }

        



        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = Helper.getServerName();
        crConnectionInfo.DatabaseName = Helper.getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtInvoiceNote);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", Helper.getServerName(), Helper.getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Date"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Date"].CurrentValues.Add(paramDV);

        paramDV.Value = say;

        CrystalReportSource1.ReportDocument.ParameterFields["Say"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Say"].CurrentValues.Add(paramDV);

        paramDV.Value = fv;

        CrystalReportSource1.ReportDocument.ParameterFields["filtername"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["filtername"].CurrentValues.Add(paramDV);
    }
}