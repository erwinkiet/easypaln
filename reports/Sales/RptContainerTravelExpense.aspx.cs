﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptContainerTravelExpense : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtTravelExpense = new DataTable("dtTravelExpense"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtTravelExpense");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtTravelExpense", dtTravelExpense);
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        SqlDataAdapter da = new SqlDataAdapter(@"SELECT te.Base, Rtrim(te.Origin) + '-' + te.Destination as Destination, te.HasBase, te.Id, te.ItemId as Item,
                                                 it.Name as ItemId, te.Price, te.PriceAbove, te.PriceBelow, te.TonBase, te.Honor
                                                 FROM TravelExpenses te
                                                 INNER JOIN Items it
                                                 ON it.Id = te.ItemId
                                                 ORDER BY it.Name asc, destination asc", con);
        dtTravelExpense.Clear();
        da.Fill(dtTravelExpense);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtTravelExpense);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        //paramDV.Value = tanggal;

        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        //paramDV.Value = tanggal2;

        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}