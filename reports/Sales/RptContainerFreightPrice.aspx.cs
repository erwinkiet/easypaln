﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptContainerFreightPrice : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtFreightPrice = new DataTable("dtFreightPrice"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtFreightPrice");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtFreightPrice", dtFreightPrice);
    }

    SqlDataAdapter da = new SqlDataAdapter();

    


    protected void Page_Load(object sender, EventArgs e)
    {
        string fieldName;

        fieldName = Request.QueryString["fn"].ToString().Trim();

        if(fieldName == "item")
        {
            da = new SqlDataAdapter(@"SELECT c.Name as CustomerName, it.Name as ItemName, 
                                                RTRIM(fpd.Destination) +' - ' + fpd.Origin as Destination, fpd.Price
                                                FROM FreightPrices fp
                                                INNER JOIN FreightPriceDetails fpd 
                                                ON fpd.FreightPriceId = fp.Id
                                                INNER JOIN Customer c
                                                ON c.CustomerID = fp.CustomerId
                                                INNER JOIN Items it
                                                ON it.Id = fpd.ItemId
                                                order by it.Name asc, Destination asc, c.Name asc", con);
        }
        else
        {
        da = new SqlDataAdapter(@"SELECT c.Name as CustomerName, it.Name as ItemName, 
                                                RTRIM(fpd.Destination) +' - ' + fpd.Origin as Destination, fpd.Price
                                                FROM FreightPrices fp
                                                INNER JOIN FreightPriceDetails fpd 
                                                ON fpd.FreightPriceId = fp.Id
                                                INNER JOIN Customer c
                                                ON c.CustomerID = fp.CustomerId
                                                INNER JOIN Items it
                                                ON it.Id = fpd.ItemId
                                                order by c.Name asc, it.Name asc, Destination asc ", con);
        }
        dtFreightPrice.Clear();
        da.Fill(dtFreightPrice);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtFreightPrice);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        //paramDV.Value = tanggal;

        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        //paramDV.Value = tanggal2;

        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}