﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptOmset : System.Web.UI.Page
{
    DataTable dtOmset = new DataTable("dtOmset");
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtApplicationReports = new DataTable();

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtOmset");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtOmset", dtOmset);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        string filter, fieldName, packinglistId;

        if (Page.IsPostBack)
        {
            dtOmset = Session["dtOmset"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }


        SqlDataAdapter da = new SqlDataAdapter();

        filter = Request.QueryString["f"].ToString().Trim();
        fieldName = Request.QueryString["fn"].ToString().Trim();
        packinglistId = Request.QueryString["Id"].Trim();

        if (filter == "y" && fieldName == "Default")
        {
            CrystalReportSource1.Report.FileName = "PackingListDefaultOmset.rpt";
        }
        if (filter == "y" && fieldName == "Destination")
        {
            CrystalReportSource1.Report.FileName = "PackingListDestinationOmset.rpt";
        }
        if (filter == "y" && fieldName == "Tonnase")
        {
            CrystalReportSource1.Report.FileName = "PackingListTonaseOmset.rpt";
        }
        if (filter == "y" && fieldName == "TransactionNo")
        {
            CrystalReportSource1.Report.FileName = "PackingListTransactionNoOmset.rpt";
        }


        string query = @"SELECT pl.Id, pl.TransactionNo as plTransactionNo, tvd.TransactionNo as tvTransactionNo, pl.InputDate, CONVERT(Varchar(10),pl.TransactionDate,103) as TransactionDate, pl.CustomerId, cst.Name as CustomerName,
                         tvd.Id as TravelDocumentId, tvd.Remark, '-  '+ tvd.ShipName as ShipName,
                         CONVERT(Varchar(10),tvd.TransactionDate,103) as TravelDocumentDate, tvd.VehicleId, v.VehicleNo,tvd.UseDestinationNetto, tvd.OriginNetto, 
                         tvd.DestinationNetto, tvd.Bruto, tvd.Tare, tvd.Bruto - tvd.Tare as Netto, DestinationNetto - (tvd.Bruto - tvd.Tare) as plusminus, tvd.ItemId, itm.Name as ItemName, tve.Origin, tve.Destination,
                         
                         CASE WHEN tvd.UseDestinationNetto = 1
                         THEN (CASE WHEN tve.TonBase = 1 
                         THEN tvd.DestinationNetto ELSE tvd.OriginNetto
						 END) 
						 ELSE (CASE WHEN tve.TonBase = 0 THEN tvd.OriginNetto ELSE tvd.DestinationNetto END)
						 END AS tonnase,

						 DestinationNetto - OriginNetto as plusminusdestination,
						 inv.Price
                         
                         FROM PackingLists pl
                         INNER JOIN PackingListDetails pld
                         ON pl.Id = pld.PackingListId
                         INNER JOIN TravelDocuments tvd
                         ON pld.TravelDocumentId = tvd.Id
                         INNER JOIN Customer cst
                         ON tvd.CustomerId = cst.CustomerID
                         INNER JOIN Items itm
                         ON tvd.ItemId = itm.Id
                         INNER JOIN TravelExpenses tve
                         ON tvd.TravelExpenseId = tve.Id
                         INNER JOIN Vehicle v
                         ON v.Id = tvd.VehicleId
                         INNER JOIN Invoices inv
                         ON inv.PackingListId = pl.Id
                         WHERE pl.Id = " + packinglistId + " order by pl.TransactionDate asc, tvd.TransactionNo asc";
        da = new SqlDataAdapter(query, con);
        dtOmset.Clear();
        da.Fill(dtOmset);

        string tanggal = Helper.getIndonesianDate(dtOmset.Rows[0]["TransactionDate"].ToString());

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = Helper.getServerName();
        crConnectionInfo.DatabaseName = Helper.getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtOmset);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", Helper.getServerName(), Helper.getDatabaseName());


    }
}