﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class reports_Sales_RptContainerFreightCost : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtRptFreightCost = new DataTable("dtRptFreightCost");

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtRptFreightCost");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtRptFreightCost", dtRptFreightCost);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2, filter, filtername;
        tanggal = "";
        tanggal2 = "";
       
        if (Page.IsPostBack)
        {
            dtRptFreightCost = Session["dtRptFreightCost"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }



        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();
        filtername = Request.QueryString["fn"].ToString().Trim();
        filter = Request.QueryString["f"].ToString().Trim();


        SqlDataAdapter da = new SqlDataAdapter();

        
            da = new SqlDataAdapter(@"select td.Id, td.DriverId,td.Additional, CONVERT(varchar(10),td.UnloadDate,103) as UnloadDate, d.Name as DriverName, td.VehicleId, v.VehicleNo, 
                                CONVERT(varchar(10),td.TransactionDate,103) as TransactionDate, i.Name as ItemName, te.Origin, 
                                te.Destination, td.DestinationNetto, td.OriginNetto, td.DestinationNetto - td.OriginNetto 
                                as plusminus, td.TravelFee, te.Price, CONVERT(varchar(10),td.TravelFeeDate,103) as TravelFeeDate
                                from TravelDocuments td 
                                inner join Driver d on d.Id = td.DriverId
                                inner join Vehicle v on v.Id = td.VehicleId
                                inner join Items i on i.Id = td.ItemId
                                LEFT OUTER join TravelExpenses te on te.Id = td.TravelExpenseId
                                where td.Status<>'Cancel' AND 
                                CONVERT(DATE,td.TransactionDate,103) >=convert(date,'" + tanggal + @"',103) AND 
                                CONVERT(DATE,td.TransactionDate,103) <=convert(date,'" + tanggal2 + @"',103) AND
                                (td.isContractWork = 1 OR td.isExternal = 1) 
                                order by td.TravelFeeDate asc, d.Name asc ", con);
        
        
        dtRptFreightCost.Clear();
        da.Fill(dtRptFreightCost);


        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtRptFreightCost);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}
