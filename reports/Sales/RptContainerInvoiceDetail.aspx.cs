﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptContainerInvoiceDetail : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtInvoiceDetail = new DataTable("dtInvoiceDetail"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtInvoiceDetail");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtInvoiceDetail", dtInvoiceDetail);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2, filtervalue, filter;
        tanggal = "";
        tanggal2 = "";
        filtervalue = "";
        filter = "";

        if (Page.IsPostBack)
        {
            dtInvoiceDetail = Session["dtInvoiceDetail"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();
        filtervalue = Request.QueryString["fv"].ToString().Trim();
        filter = Request.QueryString["f"].ToString().Trim();

        if (filter == "customer")
        {
            CrystalReportSource1.Report.FileName = "InvoiceDetail.rpt";
        }
        if (filter == "allcustomer")
        {
            CrystalReportSource1.Report.FileName = "InvoiceDetailAllCustomer.rpt";
        }


        SqlDataAdapter da = new SqlDataAdapter();
        if ( filter == "customer")
        {
            da = new SqlDataAdapter(@"select data.invid, data.InvoiceNo, data.EtcPrice, data.LoadDate, data.unLoadDate, data.plid, data.plTransactionNo, data.ItemName, data.Price, data.Truck,
                                                    data.Netto, data.Destination, data.CustomerName, data.netto * data.Price + data.EtcPrice
                                                    AS GrandTotal,data.PPN
                                                    from(select inv.id as invid , inv.TransactionNo as InvoiceNo , CONVERT(varchar(10),inv.LoadDate,103) as LoadDate,  CONVERT(varchar(10),inv.UnloadDate,103) as unLoadDate,pl.Id as plid,pl.TransactionNo as plTransactionNo, it.Name as ItemName,inv.Price, COUNT(td.Id)as truck,
                                                    SUM(CASE WHEN td.UseDestinationNetto = 0 THEN td.OriginNetto ELSE td.DestinationNetto END) AS netto 
                                                    , Rtrim(te.Origin) +' - '+ te.Destination as Destination, inv.IsPpn, cus.Name as CustomerName , inv.EtcPrice,
                                                    CASE WHEN inv.IsPpn = 0 THEN 'T' ELSE 'P' END AS PPN
                                                    from PackingLists pl
                                                    INNER JOIN PackingListDetails pld
                                                    ON pl.Id = pld.PackingListId
                                                    INNER JOIN TravelDocuments td
                                                    ON td.Id = pld.TravelDocumentId
                                                    INNER JOIN Invoices inv
                                                    ON inv.PackingListId = pl.Id
                                                    INNER JOIN Items it
                                                    ON td.ItemId = it.Id
                                                    INNER JOIN TravelExpenses te
                                                    ON te.Id = td.TravelExpenseId
                                                    INNER JOIN Customer cus
                                                    ON inv.CustomerId = cus.CustomerID 
                                                    
                                                where inv.LoadDate >= convert(date, '" + tanggal + @"', 103) and 
                                                inv.LoadDate <= convert(date, '" + tanggal2 + @"', 103) and 
                                                cus.Name = '"+ filtervalue + @"'
                                                group by cus.Name, inv.id, inv.LoadDate, inv.UnloadDate, te.Destination, te.Origin ,it.Name ,
                                                inv.TransactionNo, inv.EtcPrice, pl.id, pl.TransactionNo, inv.Price, inv.IsPpn, inv.Total, inv.GrandTotal ) data
                                                order by data.LoadDate asc, data.InvoiceNo desc", con);
        }
         else if ( filter == "allcustomer")
        {
            da = new SqlDataAdapter(@"select data.invid, data.InvoiceNo, data.EtcPrice, data.LoadDate, data.unLoadDate, data.plid, data.plTransactionNo, data.ItemName, data.Price, data.Truck,
                                                    data.Netto, data.Destination, data.CustomerName, data.netto * data.Price + data.EtcPrice
                                                    AS GrandTotal,data.PPN
                                                    from(select inv.id as invid , inv.TransactionNo as InvoiceNo , CONVERT(varchar(10),inv.LoadDate,103) as LoadDate,  CONVERT(varchar(10),inv.UnloadDate,103) as unLoadDate,pl.Id as plid,pl.TransactionNo as plTransactionNo, it.Name as ItemName,inv.Price, COUNT(td.Id)as truck,
                                                    SUM(CASE WHEN td.UseDestinationNetto = 0 THEN td.OriginNetto ELSE td.DestinationNetto END) AS netto 
                                                    , Rtrim(te.Origin) +' - '+ te.Destination as Destination, inv.IsPpn, cus.Name as CustomerName , inv.EtcPrice,
                                                    CASE WHEN inv.IsPpn = 0 THEN 'T' ELSE 'P' END AS PPN
                                                    from PackingLists pl
                                                    INNER JOIN PackingListDetails pld
                                                    ON pl.Id = pld.PackingListId
                                                    INNER JOIN TravelDocuments td
                                                    ON td.Id = pld.TravelDocumentId
                                                    INNER JOIN Invoices inv
                                                    ON inv.PackingListId = pl.Id
                                                    INNER JOIN Items it
                                                    ON td.ItemId = it.Id
                                                    INNER JOIN TravelExpenses te
                                                    ON te.Id = td.TravelExpenseId
                                                    INNER JOIN Customer cus
                                                    ON inv.CustomerId = cus.CustomerID 
                                                    
                                                where inv.LoadDate >= convert(date, '" + tanggal + @"', 103) and 
                                                inv.LoadDate <= convert(date, '" + tanggal2 + @"', 103) and
                                                inv.status <> 'Cancel' and td.Status <> 'Cancel'
                                                group by inv.EtcPrice, cus.Name, inv.id, inv.LoadDate, inv.UnloadDate, te.Destination, te.Origin ,it.Name ,
                                                inv.TransactionNo ,pl.id, pl.TransactionNo, inv.Price, inv.IsPpn, inv.Total, inv.GrandTotal ) data
                                                order by data.InvoiceNo asc", con);
            
            
        }
            else
        {
            da = new SqlDataAdapter(@"select data.invid, data.EtcPrice,data.InvoiceNo, data.LoadDate, data.unLoadDate, data.plid, data.plTransactionNo, data.ItemName, data.Price, data.Truck,
                                                    data.Netto, data.Destination, data.CustomerName, data.netto * data.Price + data.EtcPrice
                                                    AS GrandTotal,data.PPN
                                                    from(select inv.id as invid,inv.TransactionNo as InvoiceNo , CONVERT(varchar(10),inv.LoadDate,103) as LoadDate,  CONVERT(varchar(10),inv.UnloadDate,103) as unLoadDate,pl.Id as plid,pl.TransactionNo as plTransactionNo, it.Name as ItemName,inv.Price, COUNT(td.Id)as truck,
                                                    SUM(CASE WHEN td.UseDestinationNetto = 0 THEN td.OriginNetto ELSE td.DestinationNetto END) AS netto 
                                                    , Rtrim(te.Origin) +' - '+ te.Destination as Destination, inv.IsPpn, cus.Name as CustomerName , inv.EtcPrice,
                                                    CASE WHEN inv.IsPpn = 0 THEN 'T' ELSE 'P' END AS PPN
                                                    from PackingLists pl
                                                    INNER JOIN PackingListDetails pld
                                                    ON pl.Id = pld.PackingListId
                                                    INNER JOIN TravelDocuments td
                                                    ON td.Id = pld.TravelDocumentId
                                                    INNER JOIN Invoices inv
                                                    ON inv.PackingListId = pl.Id
                                                    INNER JOIN Items it
                                                    ON td.ItemId = it.Id
                                                    INNER JOIN TravelExpenses te
                                                    ON te.Id = td.TravelExpenseId
                                                    INNER JOIN Customer cus
                                                    ON inv.CustomerId = cus.CustomerID 
                                                    
                                                where inv.LoadDate >= convert(date, '" + tanggal + @"', 103) and 
                                                inv.LoadDate <= convert(date, '" + tanggal2 + @"', 103) and
                                                inv.status <> 'Cancel' and td.Status <> 'Cancel'
                                                group by inv.EtcPrice,  cus.Name, inv.id, inv.LoadDate, inv.UnloadDate, te.Destination, te.Origin ,it.Name ,
                                                inv.TransactionNo ,pl.id, pl.TransactionNo, inv.Price, inv.IsPpn, inv.Total, inv.GrandTotal ) data
                                                order by data.LoadDate asc, data.InvoiceNo desc", con);
            
            
    }
        dtInvoiceDetail.Clear();
        da.Fill(dtInvoiceDetail);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtInvoiceDetail);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}