﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptContainerOmsetDetail : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtOmsetDetail = new DataTable("dtOmsetDetail"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtOmsetDetail");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtOmsetDetail", dtOmsetDetail);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2;
        tanggal = "";
        tanggal2 = "";

        if (Page.IsPostBack)
        {
            dtOmsetDetail = Session["dtOmsetDetail"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();

        SqlDataAdapter da = new SqlDataAdapter(@"select data.tvTransactionNo, data.TravelDocumentDate,data.invid, data.VehicleNo ,data.InvoiceNo, data.LoadDate, data.unLoadDate, data.plid, data.plTransactionNo, data.ItemName, data.Price, data.Truck,
                                                data.tonnase, data.Destination, data.CustomerName, data.tonnase * data.Price 
                                                AS Total, data.PPN
                                                from(select td.TransactionNo as tvTransactionNo, CONVERT(varchar(10),td.TransactionDate,103) as TravelDocumentDate , v.VehicleNo ,inv.id as invid ,inv.TransactionNo as InvoiceNo , CONVERT(varchar(10),inv.LoadDate,103) as LoadDate, CONVERT(varchar(10),inv.UnloadDate,103) as unLoadDate,pl.Id as plid,pl.TransactionNo as plTransactionNo, it.Name as ItemName,inv.Price, COUNT(td.Id)as truck,
                                                SUM(CASE WHEN td.UseDestinationNetto = 0 THEN td.OriginNetto ELSE td.DestinationNetto END) AS tonnase 
                                                , Rtrim(te.Origin) +' - '+ te.Destination as Destination, inv.IsPpn, cus.Name as CustomerName ,
                                                CASE WHEN inv.IsPpn = 0 THEN 'T' ELSE 'P' END AS PPN
                                                from PackingLists pl
                                                INNER JOIN PackingListDetails pld
                                                ON pl.Id = pld.PackingListId
                                                INNER JOIN TravelDocuments td
                                                ON td.Id = pld.TravelDocumentId
                                                INNER JOIN Invoices inv
                                                ON inv.PackingListId = pl.Id
                                                INNER JOIN Items it
                                                ON td.ItemId = it.Id
                                                INNER JOIN TravelExpenses te
                                                ON te.Id = td.TravelExpenseId
                                                INNER JOIN Customer cus
                                                ON inv.CustomerId = cus.CustomerID 
                                                INNER JOIN Vehicle v
                                                ON v.Id = td.VehicleId
                                                where td.TransactionDate >= convert(date, '" + tanggal + @"', 103) and 
                                                td.TransactionDate <= convert(date, '" + tanggal2 + @"', 103) and
                                                inv.status <> 'Cancel' and td.Status <> 'Cancel'
                                                group by v.VehicleNo ,td.TransactionDate ,td.TransactionNo,cus.Name, inv.id, inv.LoadDate, inv.UnloadDate, te.Destination, te.Origin ,it.Name ,
                                                inv.TransactionNo, pl.id, pl.TransactionNo, inv.Price, inv.IsPpn, inv.Total, inv.GrandTotal ) data

                                                order by data.TravelDocumentDate asc, data.tvTransactionNo asc", con);
        dtOmsetDetail.Clear();
        da.Fill(dtOmsetDetail);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtOmsetDetail);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}