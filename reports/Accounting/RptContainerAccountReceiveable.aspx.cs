﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Accounting_RptContainerAccountReceiveable : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtAccountReceiveable = new DataTable("dtAccountReceiveable"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtAccountReceiveable");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtAccountReceiveable", dtAccountReceiveable);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2;
        tanggal = "";
        tanggal2 = "";

        if (Page.IsPostBack)
        {
            dtAccountReceiveable = Session["dtAccountReceiveable"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();

        SqlDataAdapter da = new SqlDataAdapter(@"SELECT csd.*, cst.Name as CustomerName  
        INTO #tmpCustomer 
        FROM Customer cst
        INNER JOIN CustomerDetail csd
        ON cst.CustomerID = csd.CustomerID

        SELECT inv.CustomerId, SUM(inv.GrandTotal) as Sales  
        INTO #tmpInvoiceAll 
        FROM Invoices inv
        WHERE inv.Status<>'Cancel' AND inv.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103)  
        GROUP BY inv.CustomerId

        SELECT cbr.CustomerId, SUM(cbd.PayAmount) as Pay  
        INTO #tmpReceiveAll
        FROM CashBankReceives cbr
        INNER JOIN CashBankReceiveDetails cbd
        ON cbr.Id = cbd.CashBankReceiveId
        WHERE cbr.Status<>'Cancel' AND cbr.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) 
        GROUP BY cbr.CustomerId

        SELECT inv.CustomerId, SUM(inv.GrandTotal) as Sales  
        INTO #tmpInvoice 
        FROM Invoices inv
        WHERE inv.Status<>'Cancel' AND inv.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
        inv.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) 
        GROUP BY inv.CustomerId

        SELECT cbr.CustomerId, SUM(cbd.PayAmount) as Pay  
        INTO #tmpReceive
        FROM CashBankReceives cbr
        INNER JOIN CashBankReceiveDetails cbd
        ON cbr.Id = cbd.CashBankReceiveId
        WHERE cbr.Status<>'Cancel' AND cbr.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
        cbr.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) 
        GROUP BY cbr.CustomerId

        SELECT cst.CustomerId, cst.CustomerName, cst.Amount,  
        CASE WHEN iva.Sales is null THEN 0 ELSE iva.Sales END as SalesAll, 
        CASE WHEN rca.Pay is null THEN 0 ELSE rca.Pay END as PayAll, 
        CASE WHEN inv.Sales is null THEN 0 ELSE inv.Sales END as Sales, 
        CASE WHEN rcv.Pay is null THEN 0 ELSE rcv.Pay END as Pay
        INTO #tmpAR
        FROM #tmpCustomer cst
        LEFT OUTER JOIN #tmpInvoiceAll iva
        ON cst.CustomerID = iva.CustomerId 
        LEFT OUTER JOIN #tmpReceiveAll rca 
        ON cst.CustomerID = rca.CustomerId 
        LEFT OUTER JOIN #tmpInvoice inv 
        ON cst.CustomerID = inv.CustomerId 
        LEFT OUTER JOIN #tmpReceive rcv
        ON cst.CustomerID = rcv.CustomerId

        SELECT CustomerID, CustomerName, (Amount-SalesAll+PayAll) as OpenBalance, Sales, Pay, 
        (Amount-SalesAll+PayAll)+Sales-Pay as EndBalance 
        FROM #tmpAR

        DROP TABLE #tmpCustomer
        DROP TABLE #tmpInvoiceAll
        DROP TABLE #tmpReceiveAll
        DROP TABLE #tmpInvoice
        DROP TABLE #tmpReceive
        DROP TABLE #tmpAR", con);

        dtAccountReceiveable.Clear();
        da.Fill(dtAccountReceiveable);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtAccountReceiveable);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}