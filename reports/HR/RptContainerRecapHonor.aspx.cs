﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_HR_RptContainerRecapHonor : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtRecapHonor = new DataTable("dtRecapHonor"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtRecapHonor");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtRecapHonor", dtRecapHonor);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2;
        tanggal = "";
        tanggal2 = "";

        if (Page.IsPostBack)
        {
            dtRecapHonor = Session["dtRecapHonor"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();

        SqlDataAdapter da = new SqlDataAdapter(@"SELECT td.TransactionNo, CONVERT(DATE,td.TravelFeeDate,103) as TravelFeeDate, 
                                                CONVERT(VARCHAR(10),td.TransactionDate,103) as TransactionDate,v.VehicleNo, 
                                                d.Name as DriverName, td.TravelFee, ISNULL(te.Honor,0) as TravelExpense, ISNULL(ddc.Amount,0) as Deduction, 
                                                ISNULL(td.Additional,0) as Additional
                                                FROM TravelDocuments td
                                                LEFT OUTER JOIN Vehicle v
                                                ON td.VehicleId = v.Id
                                                LEFT OUTER JOIN TravelExpenses te
                                                ON te.Id = td.TravelExpenseId
                                                LEFT OUTER JOIN Driver d
                                                ON td.DriverId = d.Id
                                                LEFT OUTER JOIN DriverHonors dh
                                                ON td.DriverHonorId = dh.Id
                                                LEFT OUTER JOIN DriverDeduction ddc
                                                ON td.DriverId = ddc.DriverId
                                                WHERE td.IsExternal = 1  AND td.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                                td.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103)
                                                ORDER BY td.TransactionDate asc", con);
        dtRecapHonor.Clear();
        da.Fill(dtRecapHonor);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtRecapHonor);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}