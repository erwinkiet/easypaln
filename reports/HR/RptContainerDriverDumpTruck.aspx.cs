﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_HR_RptContainerDriverDumpTruck : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtDriverDumpTruck = new DataTable("dtDriverDumpTruck");

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtDriverDumpTruck");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtDriverDumpTruck", dtDriverDumpTruck);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2, filter, fieldName, fieldName1,optr;
        tanggal = "";
        tanggal2 = "";
        optr = "";


        if (Page.IsPostBack)
        {
            dtDriverDumpTruck = Session["dtDriverDumpTruck"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        filter = Request.QueryString["f"].ToString().Trim();
        fieldName = Request.QueryString["fn"].ToString().Trim();
        fieldName1 = Request.QueryString["fn1"].ToString().Trim();
        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();
        

        if (filter == "y" && fieldName == "Default" && fieldName1 == "internal" )
        {
            CrystalReportSource1.Report.FileName = "DriverDumpTruck.rpt";
        }
        if (filter == "y" && fieldName == "PLUS" || fieldName == "MINUS" && fieldName1 == "internal")
        {
            CrystalReportSource1.Report.FileName = "DriverDumpTruckPlusminusInternal.rpt";
        }
        if (filter == "y" && fieldName == "Default" && fieldName1 == "external")
        {
            CrystalReportSource1.Report.FileName = "DriverDumpTruckExternal.rpt";
        }
        if (filter == "y" && fieldName == "PLUS" || fieldName == "MINUS" && fieldName1 == "external")
        {
            CrystalReportSource1.Report.FileName = "DriverDumpTruckPlusminusExternal.rpt";
        }

        if (filter == "y" && fieldName == "PLUS")
        {
            optr = ">";
        }

        if (filter == "y" && fieldName == "MINUS")
        {
            optr = "<";
        }

        SqlDataAdapter da = new SqlDataAdapter();

        if (filter == "y" && fieldName == "Default" && fieldName1 == "internal")
        {
            da = new SqlDataAdapter(@"SELECT tds.DriverId, dr.Name as DriverName, '' as ddremark,tds.VehicleId, vc.VehicleNo, tds.TravelFee, 
                                        tes.HasBase, tes.TonBase, tes.Base, tes.Price, tes.PriceAbove, tes.PriceBelow, 

                                        CASE WHEN tds.UseDestinationNetto = 0 THEN 
                                        (CASE WHEN tes.TonBase = 0 THEN tds.OriginNetto ELSE 
                                              CASE WHEN tds.OriginNetto/1000 >= 27 THEN 27 ELSE tds.OriginNetto/1000 END 
                                        END) 
                                        ELSE 
                                        (CASE WHEN tes.TonBase = 0 THEN tds.DestinationNetto ELSE 
                                              CASE WHEN tds.DestinationNetto/1000 >= 27 THEN 27 ELSE tds.DestinationNetto/1000 END 
                                        END)
                                        END AS Netto

                                        INTO #tmpTravel 
                                        FROM TravelDocuments tds 
                                        INNER JOIN TravelExpenses tes
                                        ON tds.TravelExpenseId = tes.Id
                                        INNER JOIN Driver dr
                                        ON tds.DriverId = dr.Id
                                        INNER JOIN Vehicle vc
                                        ON tds.VehicleId = vc.Id 
                                        WHERE tds.Status<>'Cancel' AND tds.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                        tds.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) AND tds.IsExternal = 0 and tds.IsContractWork = 0

                                        SELECT ddc.DriverId, dr.Name as DriverName, ddc.VehicleId, '' as VehicleNo,
                                        0 as TravelFee, 0 as TravelExpenseFee, ddc.Amount as Deduction, ddc.Amount*-1 as EndBalance, 
                                        ddc.Remark  
                                        INTO #tmpDeduction 
                                        FROM DriverDeduction ddc 
                                        INNER JOIN Driver dr 
                                        ON ddc.DriverId = dr.Id 
                                        WHERE ddc.Status<>'Cancel' AND ddc.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                        ddc.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) AND ddc.IsExternal = 0

                                        SELECT DriverId, DriverName, VehicleId, VehicleNo, TravelFee, 

                                        CASE WHEN TonBase = 0 THEN 
                                            (CASE WHEN HasBase = 0 THEN Price ELSE 
                                                CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                            END) 
                                        ELSE 
                                            (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                                CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                            END)
                                        END AS TravelExpenseFee  

                                        INTO #tmpHonor
                                        FROM #tmpTravel

                                        SELECT * 
                                        INTO #tmpTD
                                        FROM (
	                                        SELECT DriverId, DriverName, VehicleId, VehicleNo, SUM(TravelFee) as TravelFee, 
	                                        SUM(TravelExpenseFee) as TravelExpenseFee, 0 as Deduction, 
	                                        SUM(TravelExpenseFee-TravelFee) as EndBalance, 
	                                        'HONOR BULAN SEKARANG' as Remark FROM #tmpHonor 
	                                        GROUP BY DriverId, DriverName, VehicleId, VehicleNo
		                                        UNION
	                                        SELECT * 
	                                        FROM #tmpDeduction
                                        ) a

                                        SELECT DriverId, DriverName, ROW_NUMBER()OVER (ORDER BY DriverName, DriverId ) ROW INTO #tmpROW
                                        FROM #tmpTD
                                        GROUP BY DriverId, DriverName

                                        SELECT travel.*, row.ROW,  ROW_NUMBER()OVER (PARTITION BY travel.DriverId, travel.DriverName ORDER BY travel.DriverName, travel.DriverId) ROW1
                                        INTO #tmpNumber
                                        FROM #tmpTD travel INNER JOIN #tmpROW row
	                                        ON travel.DriverId = row.DriverId

                                        UPDATE #tmpNumber SET ROW = null 
                                        WHERE ROW1 > 1

                                        SELECT * from #tmpNumber
                                        ORDER BY DriverName, DriverId

                                        DROP TABLE #tmpTravel
                                        DROP TABLE #tmpDeduction
                                        DROP TABLE #tmpHonor
                                        DROP TABLE #tmpTD
                                        DROP TABLE #tmpROW
                                        DROP TABLE #tmpNumber", con);
        }


        if (filter == "y" && fieldName1 == "internal" && fieldName == "PLUS" || fieldName == "MINUS")
        {
            CrystalReportSource1.Report.FileName = "DriverDumpTruckPlusminusInternal.rpt";
            
            da = new SqlDataAdapter(@"SELECT tds.DriverId, dr.Name as DriverName, tds.VehicleId, vc.VehicleNo, tds.TravelFee, 
                                    tes.HasBase, tes.TonBase, tes.Base, tes.Price, tes.PriceAbove, tes.PriceBelow, 

                                    CASE WHEN tds.UseDestinationNetto = 0 THEN 
                                    (CASE WHEN tes.TonBase = 0 THEN tds.OriginNetto ELSE 
                                    CASE WHEN tds.OriginNetto/1000 >= 27 THEN 27 ELSE tds.OriginNetto/1000 END 
                                    END) 
                                    ELSE 
                                    (CASE WHEN tes.TonBase = 0 THEN tds.DestinationNetto ELSE 
                                    CASE WHEN tds.DestinationNetto/1000 >= 27 THEN 27 ELSE tds.DestinationNetto/1000 END 
                                    END)
                                    END AS Netto

                                    INTO #tmpTravel 
                                    FROM TravelDocuments tds 
                                    INNER JOIN TravelExpenses tes
                                    ON tds.TravelExpenseId = tes.Id
                                    INNER JOIN Driver dr
                                    ON tds.DriverId = dr.Id
                                    INNER JOIN Vehicle vc
                                    ON tds.VehicleId = vc.Id 
                                    WHERE tds.Status<>'Cancel' AND tds.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                        tds.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) AND tds.IsExternal = 0 and tds.IsContractWork = 0

                                    SELECT ddc.DriverId, dr.Name as DriverName, ddc.VehicleId, '' as VehicleNo,
                                    0 as TravelFee, 0 as TravelExpenseFee, ddc.Amount as Deduction, ddc.Amount*-1 as EndBalance, 
                                    ddc.Remark  
                                    INTO #tmpDeduction 
                                    FROM DriverDeduction ddc 
                                    INNER JOIN Driver dr 
                                    ON ddc.DriverId = dr.Id 
                                    WHERE ddc.Status<>'Cancel' AND ddc.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                        ddc.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) AND ddc.IsExternal <> 1

                                    SELECT DriverId, DriverName, VehicleId, VehicleNo, TravelFee, 

                                    CASE WHEN TonBase = 0 THEN 
                                    (CASE WHEN HasBase = 0 THEN Price ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove ELSE PriceBelow END
                                    END) 
                                    ELSE 
                                    (CASE WHEN HasBase = 0 THEN Price*Netto ELSE 
                                    CASE WHEN Netto>=Base THEN PriceAbove*Netto ELSE PriceBelow*Netto END
                                    END)
                                    END AS TravelExpenseFee  

                                    INTO #tmpHonor
                                    FROM #tmpTravel    

                                    SELECT Honor.DriverId, SUM(EndBalance) AS Summary
                                    INTO #tmpSum 
                                    FROM 
                                    (SELECT DriverId, DriverName, VehicleId, VehicleNo, SUM(TravelFee) as TravelFee, 
                                    SUM(TravelExpenseFee) as TravelExpenseFee, 0 as Deduction, 
                                    SUM(TravelExpenseFee-TravelFee) as EndBalance, 
                                    'HONOR BULAN SEKARANG' as Remark FROM #tmpHonor 
                                    GROUP BY DriverName, DriverId, VehicleId, VehicleNo
                                    UNION
                                    SELECT * 
                                    FROM #tmpDeduction) Honor
                                    GROUP BY Honor.DriverId

                                    SELECT * 
                                    INTO #tmpTD
	                                    FROM (
	                                    SELECT Honor.*
	                                    FROM
	                                    (SELECT DriverId, DriverName, VehicleId, VehicleNo, SUM(TravelFee) as TravelFee, 
	                                    SUM(TravelExpenseFee) as TravelExpenseFee, 0 as Deduction, 
	                                    SUM(TravelExpenseFee-TravelFee) as EndBalance, 
	                                    'HONOR BULAN SEKARANG' as Remark FROM #tmpHonor 
	                                    GROUP BY DriverName, DriverId, VehicleId, VehicleNo
	                                    UNION
	                                    SELECT * 
	                                    FROM #tmpDeduction) Honor
	                                    INNER JOIN #tmpSum tmp
	                                    ON Honor.DriverId = tmp.DriverId 
	                                    WHERE tmp.Summary " + optr + @" 0
	
                                    ) a

                                    SELECT DriverId, DriverName, ROW_NUMBER()OVER (ORDER BY DriverName asc , DriverId ) ROW INTO #tmpROW
                                    FROM #tmpTD
                                    GROUP BY DriverName, DriverId

                                    SELECT travel.*, row.ROW,  ROW_NUMBER()OVER (PARTITION BY travel.DriverId, travel.DriverName ORDER BY travel.DriverName asc, travel.DriverId) ROW1
                                    INTO #tmpNumber
                                    FROM #tmpTD travel INNER JOIN #tmpROW row
	                                    ON travel.DriverId = row.DriverId

                                    UPDATE #tmpNumber SET ROW = null 
                                    WHERE ROW1 > 1
                                    

                                    SELECT * from #tmpNumber
                                    ORDER BY DriverName asc


                                    DROP TABLE #tmpTravel
                                    DROP TABLE #tmpDeduction
                                    DROP TABLE #tmpHonor
                                    DROP TABLE #tmpSum
                                    DROP TABLE #tmpTD
                                    DROP TABLE #tmpROW
                                    DROP TABLE #tmpNumber", con);
        }

        if (filter == "y" && fieldName == "Default" && fieldName1 == "external")
        {
            da = new SqlDataAdapter(@"SELECT tds.DriverId ,d.Name as DriverName, tds.Additional, v.Id as VehicleId, v.VehicleNo, te.Honor + tds.TravelFee  as TravelExpenseFee, tds.TravelFee, 
                                    '' as Remark into #tmpTravel
                                    FROM TravelDocuments tds
                                    INNER JOIN Driver d
                                    ON d.Id = tds.DriverId
                                    INNER JOIN Vehicle v
                                    ON v.Id = tds.VehicleId
                                    INNER JOIN TravelExpenses te
                                    ON te.Id = tds.TravelExpenseId
                                    WHERE tds.Status<>'Cancel' AND (tds.IsExternal = 1 OR tds.IsContractWork = 1) AND tds.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND
                                        tds.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103) 


                                    SELECT ddc.DriverId, d.Name as DriverName, 0 as Additional, v.Id as VehicleId, '' as VehicleNo, 0 as TravelExpenseFee, ddc.Amount as Deduction, 0 as TravelFee, 
                                    ddc.Remark, ddc.Amount*-1 as EndBalance
                                    into #tmpDeduction
                                    FROM DriverDeduction ddc
                                    INNER JOIN Driver d
                                    ON ddc.DriverId = d.Id
                                    INNER JOIN Vehicle v
                                    ON v.Id = ddc.VehicleId
                                    WHERE ddc.Status<>'Cancel' AND ddc.TransactionDate>=CONVERT(DATE,'" + tanggal + @"', 103) AND 
                                        ddc.TransactionDate<=CONVERT(DATE,'" + tanggal2 + @"', 103) AND ddc.IsExternal = 1

                                    SELECT * 
                                    INTO #tmpTD
                                    FROM (
                                    SELECT DriverId, DriverName, sum(Additional) as Additional ,VehicleId, VehicleNo, SUM(TravelExpenseFee) as TravelExpenseFee, 0 as Deduction,
                                    SUM(TravelFee) as TravelFee, 'HONOR BULAN SEKARANG' as Remark, SUM(TravelExpenseFee - TravelFee + Additional) as EndBalance
                                    FROM #tmpTravel
                                    GROUP BY DriverId, DriverName, VehicleId, VehicleNo, Remark
                                    UNION
                                    SELECT * FROM #tmpDeduction
                                    ) a

                                    SELECT DriverId, DriverName, ROW_NUMBER()OVER (ORDER BY DriverName, DriverId ) ROW INTO #tmpROW
                                    FROM #tmpTD
                                    GROUP BY DriverId, DriverName

                                    SELECT travel.*, row.ROW,  ROW_NUMBER()OVER (PARTITION BY travel.DriverId, travel.DriverName ORDER BY travel.DriverName, travel.DriverId) ROW1
                                    INTO #tmpNumber
                                    FROM #tmpTD travel INNER JOIN #tmpROW row
	                                    ON travel.DriverId = row.DriverId

                                    UPDATE #tmpNumber SET ROW = null 
                                    WHERE ROW1 > 1

                                    SELECT * from #tmpNumber
                                    ORDER BY DriverName, DriverId


                                    Drop Table #tmpTravel
                                    Drop Table #tmpDeduction
                                    DROP TABLE #tmpTD
                                    DROP TABLE #tmpROW
                                    DROP TABLE #tmpNumber", con);
        }


        if (filter == "y" && fieldName1 == "external" && fieldName == "PLUS" || fieldName == "MINUS")
        {
            da = new SqlDataAdapter(@"SELECT tds.DriverId ,d.Name as DriverName, tds.Additional, v.Id as VehicleId, v.VehicleNo, te.Honor + tds.TravelFee  as TravelExpenseFee, tds.TravelFee, 
                                    '' as Remark into #tmpTravel
                                    FROM TravelDocuments tds
                                    INNER JOIN Driver d
                                    ON d.Id = tds.DriverId
                                    INNER JOIN Vehicle v
                                    ON v.Id = tds.VehicleId
                                    INNER JOIN TravelExpenses te
                                    ON te.Id = tds.TravelExpenseId
                                    WHERE tds.Status<>'Cancel' AND (tds.IsExternal = 1 OR tds.IsContractWork = 1) AND tds.TransactionDate>=CONVERT(DATE, '" + tanggal + @"', 103) AND 
                                    tds.TransactionDate<=CONVERT(DATE, '" + tanggal2 + @"', 103)

                                    SELECT ddc.DriverId, d.Name as DriverName, 0 as Additional, v.Id as VehicleId, '' as VehicleNo, 0 as TravelExpenseFee, ddc.Amount as Deduction, 0 as TravelFee, 
                                    ddc.Remark, ddc.Amount*-1 as EndBalance
                                    into #tmpDeduction
                                    FROM DriverDeduction ddc
                                    INNER JOIN Driver d
                                    ON ddc.DriverId = d.Id
                                    INNER JOIN Vehicle v
                                    ON v.Id = ddc.VehicleId
                                    WHERE ddc.Status<>'Cancel' AND ddc.TransactionDate>=CONVERT(DATE,'" + tanggal + @"', 103) AND 
                                    ddc.TransactionDate<=CONVERT(DATE,'" + tanggal2 + @"', 103) AND ddc.IsExternal = 1


                                    SELECT DriverId, DriverName, sum(Additional) as Additional ,VehicleId, VehicleNo, SUM(TravelExpenseFee) as TravelExpenseFee, 0 as Deduction,
                                    SUM(TravelFee) as TravelFee, 'HONOR BULAN SEKARANG' as Remark, SUM(TravelExpenseFee - TravelFee + Additional) as EndBalance
                                    INTO #tmpA
                                    FROM #tmpTravel
                                    GROUP BY DriverId, DriverName, VehicleId, VehicleNo, Remark
                                    UNION
                                    SELECT * FROM #tmpDeduction

                                    SELECT DriverId,SUM(EndBalance) as Summary 
                                    INTO #tmpSum
                                    FROM #tmpA
                                    GROUP BY DriverId

                                    SELECT *
                                    INTO #tmpFinal
                                    FROM(
                                    SELECT ta.*, ts.Summary FROM #tmpA ta INNER JOIN #tmpSum ts ON ta.DriverId = ts.DriverId
                                    WHERE ts.Summary "+ optr + @" 0
                                    ) a

                                    SELECT DriverId, DriverName, ROW_NUMBER()OVER (ORDER BY DriverName, DriverId ) ROW INTO #tmpROW
                                    FROM #tmpFinal
                                    GROUP BY DriverId, DriverName

                                    SELECT travel.*, row.ROW,  ROW_NUMBER()OVER (PARTITION BY travel.DriverId, travel.DriverName ORDER BY travel.DriverName, travel.DriverId) ROW1
                                    INTO #tmpNumber
                                    FROM #tmpFinal travel INNER JOIN #tmpROW row
                                    ON travel.DriverId = row.DriverId

                                    UPDATE #tmpNumber SET ROW = null 
                                    WHERE ROW1 > 1

                                    SELECT * from #tmpNumber
                                    ORDER BY DriverName, DriverId


                                    Drop Table #tmpTravel
                                    Drop Table #tmpDeduction
                                    Drop Table #tmpA
                                    Drop Table #tmpSum
                                    Drop Table #tmpFinal
                                    Drop Table #tmpNumber
                                    Drop Table #tmpROW", con);
                                    }
       
        dtDriverDumpTruck.Clear();
        da.Fill(dtDriverDumpTruck);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtDriverDumpTruck);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);

        paramDV.Value = fieldName;

        CrystalReportSource1.ReportDocument.ParameterFields["fieldName"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["fieldName"].CurrentValues.Add(paramDV);
        
    }
}