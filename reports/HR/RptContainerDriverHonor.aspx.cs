﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Sales_RptContainerDriverHonor : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtDriverHonorer = new DataTable("dtDriverHonorer"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtDriverHonorer");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtDriverHonorer", dtDriverHonorer);
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        SqlDataAdapter da = new SqlDataAdapter(@"SELECT Destination, Honor FROM DriverHonors", con);
        dtDriverHonorer.Clear();
        da.Fill(dtDriverHonorer);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtDriverHonorer);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        //paramDV.Value = tanggal;

        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        //paramDV.Value = tanggal2;

        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        //CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}