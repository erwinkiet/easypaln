﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_HR_RptDriverHonor : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtDriverHonor = new DataTable("dtDriverHonor"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtDriverHonor");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtDriverHonor", dtDriverHonor);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        string tanggal, tanggal2, filtervalue, filtervalue1;
        tanggal = "";
        tanggal2 = "";
        filtervalue = "";
        filtervalue1 = "";


        if (Page.IsPostBack)
        {
            dtDriverHonor = Session["dtDriverHonor"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();
        filtervalue = Request.QueryString["fv"].ToString().Trim();
        filtervalue1 = Request.QueryString["fv1"].ToString().Trim();


        SqlDataAdapter da = new SqlDataAdapter(@"SELECT d.Id as DriverId, v.VehicleNo,d.name as DriverName,td.TransactionNo, convert(varchar(10),td.TransactionDate,103) as TransactionDate,
                                                it.Name as ItemName, RTRIM(te.Origin) + ' - ' + te.Destination as Destination,	td.OriginNetto,
                                                td.DestinationNetto, td.DestinationNetto - td.OriginNetto as plusminus, td.TravelFee, td.Additional,
                                                te.Honor, CONVERT(VARCHAR(10),td.UnloadDate,103) as UnloadDate INTO #tmpTravel
                                                FROM TravelDocuments td
                                                INNER JOIN Items it
                                                ON it.Id = td.ItemId
                                                INNER JOIN TravelExpenses te
                                                ON te.Id = td.TravelExpenseId
                                                INNER JOIN Driver d
                                                ON d.Id = td.DriverId
                                                INNER JOIN Vehicle v
                                                ON v.Id = td.VehicleId
                                                WHERE (td.IsExternal = 1 OR td.IsContractWork = 1) AND td.TransactionDate >=convert(date,'" + tanggal + @"',103) AND 
                                                td.TransactionDate <=convert(date,'" + tanggal2 + @"',103) AND 
                                                d.Name = '" + filtervalue + "' AND v.VehicleNo = '" + filtervalue1 + @"'
                                                  

                                                select * 
                                                into #tmpdd
                                                from DriverDeduction 
                                                where TransactionDate >=convert(date,'" + tanggal + @"',103) AND 
	                                                TransactionDate <=convert(date,'" + tanggal2 + @"',103)

                                                SELECT #tmpTravel.*,ISNULL(dd.Amount,0) as Deduction into #tmpDeduction
                                                FROM #tmpTravel
                                                LEFT OUTER JOIN #tmpdd dd
                                                ON #tmpTravel.DriverId = dd.DriverId

                                                SELECT *, SUM(TravelFee) as TotalTravelFee, SUM(Honor) as TotalHonor, SUM(Additional) as TotalAdditional,
                                                SUM(Deduction) as TotalDeduction
                                                FROM #tmpDeduction
                                                GROUP BY DriverId, DriverName, TransactionNo, TransactionDate, ItemName, Destination,
                                                OriginNetto, DestinationNetto, plusminus, TravelFee, Honor, Deduction, VehicleNo, Additional, UnloadDate
                                                ORDER BY TransactionDate asc                                                

                                                DROP TABLE #tmpTravel
                                                DROP TABLE #tmpDeduction
                                                DROP TABLE #tmpdd", con);


        //select td.TransactionNo, d.Name as DriverName, 
        //                convert(varchar(10),td.TransactionDate,103) as TransactionDate, i.Name as ItemName, v.VehicleNo,
        //                te.Origin + ' - ' + te.Destination as Destination, td.OriginNetto, td.DestinationNetto,
        //                td.DestinationNetto - td.OriginNetto as plusminus, td.TravelFee, td.Additional,
        //                SUM(td.TravelFee) as totaltf, te.Honor as Honor, dd.Amount as loan
        //                from TravelDocuments td
        //                inner join Driver d on d.Id = td.DriverId
        //                inner join Items i on i.Id = td.ItemId
        //                inner join TravelExpenses te on te.Id = td.ItemId	
        //                left outer join DriverHonors dh on dh.Id = td.DriverHonorId
        //                inner join DriverDeduction dd on dd.DriverId = td.DriverId
        //                inner join Vehicle v on v.Id = td.VehicleId
        //                where td.TransactionDate >=convert(date,'" + tanggal + @"',103) AND 
        //                td.TransactionDate <=convert(date,'" + tanggal2 + @"',103) AND 
        //                d.Name = '" + filtervalue + "' AND v.VehicleNo = '" + filtervalue1 + @"' AND td.IsExternal = 1
        //                group by td.TransactionNo,d.Name, td.TransactionDate, i.Name, te.Origin, te.Destination, td.OriginNetto, 
        //                td.DestinationNetto,td.TravelFee, te.Honor, dd.Amount, v.VehicleNo, td.Additional
        //                order by td.TransactionDate

        dtDriverHonor.Clear();
        da.Fill(dtDriverHonor);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtDriverHonor);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        
    }
}