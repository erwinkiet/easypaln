﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_HR_RptContainerDriverDeduction : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtContainerDriverDeduction = new DataTable("dtContainerDriverDeduction"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtContainerDriverDeduction");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtContainerDriverDeduction", dtContainerDriverDeduction);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2, filter;
        tanggal = "";
        tanggal2 = "";
        filter = "";

        if (Page.IsPostBack)
        {
            dtContainerDriverDeduction = Session["dtContainerDriverDeduction"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();
        filter = Request.QueryString["f"].ToString().Trim();

        SqlDataAdapter da = new SqlDataAdapter();
        if (filter == "minus")
        {
        da = new SqlDataAdapter(@"select dd.Id, dd.TransactionNo, CONVERT(VARCHAR(10),dd.TransactionDate,103) as TransactionDate,
                                                dd.Amount, dd.DriverId, dd.IsMinus, d.Name as DriverName, dd.VehicleId, v.VehicleNo, dd.Remark
                                                from DriverDeduction dd
                                                INNER JOIN Driver d
                                                ON d.Id = dd.DriverId
                                                INNER JOIN Vehicle v
                                                ON v.Id = dd.VehicleId
                                                WHERE dd.Status <> 'Cancel' and dd.IsExternal = 0 and dd.IsMinus = 1 and
                                                dd.TransactionDate >= convert(date, '" + tanggal + @"', 103) and 
                                                dd.TransactionDate <= convert(date, '" + tanggal2 + @"', 103)
                                                order by d.Name, dd.TransactionDate asc, dd.TransactionNo asc", con);
        }
        else if (filter == "claim")
        {
            da = new SqlDataAdapter(@"select dd.Id, dd.TransactionNo, CONVERT(VARCHAR(10),dd.TransactionDate,103) as TransactionDate,
                                                dd.Amount, dd.DriverId, d.Name as DriverName, dd.VehicleId, v.VehicleNo, dd.Remark
                                                from DriverDeduction dd
                                                INNER JOIN Driver d
                                                ON d.Id = dd.DriverId
                                                INNER JOIN Vehicle v
                                                ON v.Id = dd.VehicleId
                                                WHERE dd.Status <> 'Cancel' and dd.IsExternal = 0 and dd.IsClaim = 1 and
                                                dd.TransactionDate >= convert(date, '" + tanggal + @"', 103) and 
                                                dd.TransactionDate <= convert(date, '" + tanggal2 + @"', 103)
                                                order by d.Name, dd.TransactionDate asc, dd.TransactionNo asc", con);
        }
        else if (filter == "debtcapital")
        {
            da = new SqlDataAdapter(@"select dd.Id, dd.TransactionNo, CONVERT(VARCHAR(10),dd.TransactionDate,103) as TransactionDate,
                                                dd.Amount, dd.DriverId, d.Name as DriverName, dd.VehicleId, v.VehicleNo, dd.Remark
                                                from DriverDeduction dd
                                                INNER JOIN Driver d
                                                ON d.Id = dd.DriverId
                                                INNER JOIN Vehicle v
                                                ON v.Id = dd.VehicleId
                                                WHERE dd.Status <> 'Cancel' and dd.IsExternal = 0 and dd.IsDebtCapital = 1 and
                                                dd.TransactionDate >= convert(date, '" + tanggal + @"', 103) and 
                                                dd.TransactionDate <= convert(date, '" + tanggal2 + @"', 103)
                                                order by d.Name, dd.TransactionDate asc, dd.TransactionNo asc", con);
        }
        else
        {
            da = new SqlDataAdapter(@"select dd.Id, dd.TransactionNo, CONVERT(VARCHAR(10),dd.TransactionDate,103) as TransactionDate,
                                                dd.Amount, dd.DriverId, d.Name as DriverName, dd.VehicleId, v.VehicleNo, dd.Remark
                                                from DriverDeduction dd
                                                INNER JOIN Driver d
                                                ON d.Id = dd.DriverId
                                                INNER JOIN Vehicle v
                                                ON v.Id = dd.VehicleId
                                                WHERE dd.Status <> 'Cancel' and dd.IsExternal = 0 and
                                                dd.TransactionDate >= convert(date, '" + tanggal + @"', 103) and 
                                                dd.TransactionDate <= convert(date, '" + tanggal2 + @"', 103)
                                                order by d.Name asc, dd.TransactionDate asc, dd.TransactionNo asc", con);
        }

        dtContainerDriverDeduction.Clear();
        da.Fill(dtContainerDriverDeduction);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtContainerDriverDeduction);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}