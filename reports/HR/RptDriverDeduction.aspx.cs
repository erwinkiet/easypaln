﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_HR_RptDriverDeduction : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtDriverDeduction = new DataTable("dtDriverDeduction"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtDriverDeduction");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtDriverDeduction", dtDriverDeduction);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
 

        if (Page.IsPostBack)
        {
            dtDriverDeduction = Session["dtDriverDeduction"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            FillSession();
        }

        string DDID = Request.QueryString["Id"].Trim();

        SqlDataAdapter da = new SqlDataAdapter(@"select dd.Id, dd.Amount, dd.Remark, dd.DriverId, 
                            d.Name as DriverName,dd.RowVersion, dd.Status,  CONVERT(Varchar(10),dd.TransactionDate,103) as TransactionDate, 
                            dd.TransactionNo from DriverDeduction dd inner join Driver d on d.Id = 
                            dd.DriverId where dd.Status <> 'Cancel' and dd.Id = " + DDID + " ", con);

        dtDriverDeduction.Clear();
        da.Fill(dtDriverDeduction);

        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtDriverDeduction);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

      
    }
}