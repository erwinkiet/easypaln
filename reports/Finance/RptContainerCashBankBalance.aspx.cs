﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Finance_RptContainerCashBankBalance : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtCashBankBalance = new DataTable("dtCashBankBalance"); // datatable Journal

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtCashBankBalance");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtCashBankBalance", dtCashBankBalance);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string tanggal, tanggal2;
        tanggal = "";
        tanggal2 = "";

        if (Page.IsPostBack)
        {
            dtCashBankBalance = Session["dtCashBankBalance"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            if (!dtCashBankBalance.Columns.Contains("TransactionID"))
            {
                DataColumn tmbh_column = dtCashBankBalance.Columns.Add("SubAccountID", typeof(string));
                dtCashBankBalance.Columns.Add("SubAccountName", typeof(string));
                dtCashBankBalance.Columns.Add("CurrencyID", typeof(string));
                dtCashBankBalance.Columns.Add("Date", typeof(string));
                dtCashBankBalance.Columns.Add("TransactionID", typeof(string));
                dtCashBankBalance.Columns.Add("TransactionType", typeof(string));
                dtCashBankBalance.Columns.Add("Detail", typeof(string));
                dtCashBankBalance.Columns.Add("Receipt", typeof(decimal));
                dtCashBankBalance.Columns.Add("Pay", typeof(decimal));
                dtCashBankBalance.Columns.Add("EndBalance", typeof(decimal));
                dtCashBankBalance.Columns.Add("Remark", typeof(string));
            }

            FillSession();
        }

        tanggal = Request.QueryString["d1"].ToString().Trim();
        tanggal2 = Request.QueryString["d2"].ToString().Trim();

        string filter;
        if (Request.QueryString["c"].ToString().Trim() == "n")
        {
            filter = " and a.Status<>'Cancel'";
        }
        else
        {
            filter = "";
        }

        SqlDataAdapter da = new SqlDataAdapter(@"
            select a.SubAccountID as SubAccountID, a.Name as SubAccountName, a.CurrencyID, 
                '' as Date, '' as TransactionID,
                '' as TransactionType, 'Saldo Awal' as Detail, 0.00 as Receipt, 0.00 as Pay, 
                a.Amount - isnull(d.Tot,0) + isnull(e.Tot,0) -
                isnull(br1.Tot,0) - isnull(gr1.Tot,0) as EndBalance, '' as Remark
            from CashBank a left outer join    
                (select a.SubAccountCash, SUM(a.ReceiveCash) as Tot
                from CashBankReceives a inner join 
                    CashBank b on a.SubAccountCash=b.SubAccountID
                where a.TransactionDate >= convert(date, '" + tanggal + @"', 103)" + filter + @" 
                group by a.SubAccountCash) d on d.SubAccountCash=a.SubAccountID left outer join
    
                (select c.SubAccountId, SUM(c.PayAmount) as Tot
                from CashBankReceiveDetails c inner join CashBankReceives a on c.CashBankReceiveId=a.Id inner join 
                    CashBank b on c.SubAccountId=b.SubAccountID
                where a.TransactionDate >= convert(date, '" + tanggal + @"', 103)" + filter + @" 
                group by c.SubAccountId) e on e.SubAccountId=a.SubAccountID	left outer join 
    
                (select a.SubAccountBank, SUM(a.ReceiveBank) as Tot
                from CashBankReceives a 
                where a.TransactionDate >= convert(date, '" + tanggal + @"', 103)" + filter + @" 
                group by a.SubAccountBank) br1 on br1.SubAccountBank=a.SubAccountID left outer join 
    
                (select a.SubAccountGiro, SUM(a.ReceiveGiro) as Tot
                from CashBankReceives a 
                where a.TransactionDate >= convert(date, '" + tanggal + @"', 103)" + filter + @" 
                group by a.SubAccountGiro) gr1 on gr1.SubAccountGiro=a.SubAccountID
            union
            select a.SubAccountCash as SubAccountID, b.Name as SubAccountName, b.CurrencyID, 
                Convert(varchar, a.TransactionDate, 103) as Date, a.TransactionNo as TransactionID,
                a.TransactionType, a.CustomerName as Detail, a.ReceiveCash as Receipt, 0.00 as Pay, 0 as EndBalance, a.Remark
            from CashBankReceives a inner join CashBank b on a.SubAccountCash=b.SubAccountID 
            where a.TransactionDate >= convert(date, '" + tanggal + @"', 103) and a.TransactionDate <= convert(date, '" + tanggal2 + @"', 103) 
	            and a.Status<>'Cancel' 
            union
            select a.SubAccountBank as SubAccountID, b.Name as SubAccountName, a.CurrencyID, 
                Convert(varchar, a.TransactionDate, 103) as Date, a.TransactionNo as TransactionID,
                a.TransactionType, a.CustomerName as Detail, a.ReceiveBank as Receipt, 0.00 as Pay, 0 as EndBalance, a.Remark
            from CashBankReceives a inner join CashBank b on a.SubAccountBank=b.SubAccountID 
            where a.TransactionDate >= convert(date, '" + tanggal + @"', 103) and a.TransactionDate <= convert(date, '" + tanggal2 + @"', 103) 
                 " + filter + @" 
            union
            select a.SubAccountGiro as SubAccountID, b.Name as SubAccountName, a.CurrencyID, 
                Convert(varchar, a.TransactionDate, 103) as Date, a.TransactionNo as TransactionID,
                a.TransactionType, a.CustomerName as Detail, a.ReceiveGiro as Receipt, 0.00 as Pay, 0 as EndBalance, a.Remark
            from CashBankReceives a inner join CashBank b on a.SubAccountGiro=b.SubAccountID 
            where a.TransactionDate >= convert(date, '" + tanggal + @"', 103) and a.TransactionDate <= convert(date, '" + tanggal2 + @"', 103) 
                 " + filter + @" 
            union
            select d.SubAccountId as SubAccountID, b.Name as SubAccountName, b.CurrencyID, 
                Convert(varchar, a.TransactionDate, 103) as Date, a.TransactionNo as TransactionID,
                a.TransactionType, a.CustomerName as Detail, 0.00 as Receipt, d.PayAmount as Pay, 0 as EndBalance, a.Remark
            from CashBankReceiveDetails d inner join CashBankReceives a on d.CashBankReceiveId=a.Id inner join 
                CashBank b on d.SubAccountId=b.SubAccountID
            where a.TransactionDate >= convert(date, '" + tanggal + @"', 103) and a.TransactionDate <= convert(date, '" + tanggal2 + @"', 103) and 
                b.Type<>'PettyCash' " + filter + @"", con);
        dtCashBankBalance.Clear();
        da.Fill(dtCashBankBalance);

        //filterAccount = Request.QueryString["f"].ToString().Trim();
        //fieldName = Request.QueryString["fn"].ToString().Trim();
        //filterValue = Request.QueryString["fv"].ToString().Trim();
        //if (filterAccount == "y")
        //{
        //    dtCashBankBalance.DefaultView.RowFilter = fieldName + "='" + filterValue + "'";
        //    dtCashBankBalance = dtCashBankBalance.DefaultView.ToTable();
        //}
        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtCashBankBalance);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

        paramDV.Value = tanggal;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal"].CurrentValues.Add(paramDV);

        paramDV.Value = tanggal2;

        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Clear();
        CrystalReportSource1.ReportDocument.ParameterFields["Tanggal2"].CurrentValues.Add(paramDV);
    }
}