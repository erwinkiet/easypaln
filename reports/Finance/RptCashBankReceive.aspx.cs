﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CrystalDecisions.Shared;
using System.Diagnostics;

public partial class reports_Accounting_RptCashBankReceive : BasePage
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    DataTable dtCBR = new DataTable("dtCBR"); // datatable DO

    protected void Page_OnUnload(object sender, EventArgs e)
    {
        // remove all session for this page
        if (!Page.IsPostBack)
        {
            Session.Remove("dtCBR");
        }
    }

    protected void FillSession()
    {
        Session.Add("dtCBR", dtCBR);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            dtCBR = Session["dtCBR"] as DataTable;
        }

        if (!Page.IsPostBack)
        {
            if (!dtCBR.Columns.Contains("CBRID"))
            {
                DataColumn tmbh_column = dtCBR.Columns.Add("CBRID", typeof(string));
                dtCBR.Columns.Add("TransactionNo", typeof(string));
                dtCBR.Columns.Add("TransactionDate", typeof(string));
                dtCBR.Columns.Add("TransactionType", typeof(string));
                dtCBR.Columns.Add("CustomerID", typeof(string));
                dtCBR.Columns.Add("Conversion", typeof(decimal));
                dtCBR.Columns.Add("CustomerName", typeof(string));
                dtCBR.Columns.Add("CurrencyId", typeof(string));
                dtCBR.Columns.Add("RemainingDebt", typeof(decimal));
                dtCBR.Columns.Add("Remark", typeof(string));
                dtCBR.Columns.Add("PayAmount", typeof(decimal));
                dtCBR.Columns.Add("EtcAmount", typeof(decimal));
                dtCBR.Columns.Add("total", typeof(decimal));
                dtCBR.Columns.Add("Price", typeof(decimal));
            }

            FillSession();
        }

        string CBRID = Request.QueryString["nota"].Trim();
        string query = @"select case when SUBSTRING(CBR.TransactionNo,15,1)='/' then LEFT(CBR.TransactionNo,14) when SUBSTRING(CBR.TransactionNo,14,1)='/' then LEFT(CBR.TransactionNo,13) end TransactionNo, CBR.Id,inv.GrandTotal - ISNULL(mix.Tot,0) as RemainingDebt, CONVERT(varchar(10), CBR.TransactionDate, 120) TransactionDate, 
                                        CBR.CustomerId, CBR.CustomerName, CBR.CurrencyId, CBR.Conversion, CBR.ReceiveCash, 
                                        CBR.SubAccountCash, CBR.ReceiveGiro, CBRD.PayAmount, CBRD.EtcAmount,
                                        CBR.ReceiveBank,CBR.TransactionType, CBR.Remark, CBR.GrandTotal from CashBankReceives CBR inner join CashBankReceiveDetails cbrd on cbr.Id=cbrd.CashBankReceiveId  inner join 
                                       CustomerDetail cus on cus.CustomerID = CBR.CustomerId  inner join customer cus1 on cus.CustomerID = cus1.CustomerID left outer join 
                                       (select SUM(cbrd1.PayAmount+cbrd1.EtcAmount) as Tot, cbrd1.InvoiceId
                                       from  CashBankReceives cbr1 inner join CashBankReceiveDetails cbrd1 on cbr1.Id = cbrd1.CashBankReceiveId
                                       where cbr1.Status<>'Cancel' and cbr1.Status<>'Closed'
                                       group by cbrd1.InvoiceId) mix on cbrd.InvoiceId = mix.InvoiceId inner join invoices inv on  inv.Id = mix.InvoiceId where CBR.Id = " + CBRID + " ";
            
        SqlDataAdapter da = new SqlDataAdapter(query, con);
        da.Fill(dtCBR);



        ConnectionInfo crConnectionInfo = new ConnectionInfo();

        crConnectionInfo.ServerName = getServerName();
        crConnectionInfo.DatabaseName = getDatabaseName();
        crConnectionInfo.UserID = "jollyroger";
        crConnectionInfo.Password = "greyhound";
        TableLogOnInfo CrystalRepor = new TableLogOnInfo();
        CrystalDecisions.CrystalReports.Engine.Tables CrTables = CrystalReportSource1.ReportDocument.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            CrystalRepor = CrTable.LogOnInfo;
            CrystalRepor.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(CrystalRepor);
        }

        ParameterDiscreteValue paramDV = new ParameterDiscreteValue();

        CrystalReportSource1.ReportDocument.SetDataSource(dtCBR);
        CrystalReportSource1.ReportDocument.SetDatabaseLogon("jollyroger", "greyhound", getServerName(), getDatabaseName());

       
    }




    public string allrecord1 { get; set; }

     
}