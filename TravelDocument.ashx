﻿<%@ WebHandler Language="C#" Class="TravelDocumentHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class TravelDocumentDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public int IsExternal { get; set; }
    public int DriverHonorId { get; set; }
    public int IsContractWork { get; set; }
    public int TravelExpenseId { get; set; }
    public int ItemId { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public int VehicleId { get; set; }
    public int DriverId { get; set; }
    public decimal TravelFee { get; set; }
    public int UseDestinationNetto { get; set; }
    public decimal Bruto { get; set; }
    public decimal Tare { get; set; }
    public decimal OriginNetto { get; set; }
    public decimal DestinationNetto { get; set; }
    public string subAccountId { get; set; }
    public string Destination { get; set; }
    public string ShipName { get; set; }
    public string Remark { get; set; }
    public string DONo { get; set; }
    public string Status { get; set; }
    public string TravelFeeDate { get; set; }
    public string UnloadDate { get; set; }
    public string CrossDate { get; set; }
    public byte[] RowVersion { get; set; } 
}

class TravelDocumentHandler : BaseHandler<TravelDocumentDTO>
{
    protected override void Insert(TravelDocumentDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string crossdate = input.CrossDate;
        string unloaddate = input.UnloadDate;
        string status = "Open";
        DateTime? crossDate = null;
        DateTime? unloadDate = null;
        
        if (crossdate == null || crossdate == "null" || crossdate == "" || crossdate == "undefined")
        {
            crossDate = null;
        }
        else
        {
            crossDate = DateTime.Parse(input.CrossDate);
        }

        
        if (unloaddate == null || unloaddate == "null" || unloaddate == "" || unloaddate == "undefined")
        {
            unloadDate = null;
        }
        else
        {
            unloadDate = DateTime.Parse(input.UnloadDate);
        } 
        
        ctx.TravelDocuments.AddObject(new TravelDocuments()
        {
            TransactionNo = input.TransactionNo,
            CrossDate = crossDate,
            UnloadDate = unloadDate,
            TransactionDate = DateTime.Parse(input.TransactionDate),
            IsExternal = input.IsExternal,
            DriverHonorId = input.DriverHonorId,
            IsContractWork = input.IsContractWork,
            TravelExpenseId = input.TravelExpenseId,
            VehicleId = input.VehicleId,
            CustomerId = input.CustomerId,
            TravelFeeDate = DateTime.Parse(input.TravelFeeDate),
            TravelFee = input.TravelFee,
            DriverId = input.DriverId,
            ShipName = input.ShipName,
            ItemId = input.ItemId,
            Bruto = input.Bruto,
            Tare = input.Tare,
            DestinationNetto = input.DestinationNetto,
            OriginNetto = input.OriginNetto,
            UseDestinationNetto = input.UseDestinationNetto,
            Remark = input.Remark,
            DONo = input.DONo,
            CreatedBy = user.UserID,
            Status = status,
            InputDate = DateTime.Now
        });
              
        Helper.InsertLog("SAL-Transaction-TravelDocument-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(TravelDocumentDTO input, TravelDocumentDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int transactionid = oldInput.Id;

        EntityKey key = new EntityKey("EasyEntities.TravelDocuments", "Id", transactionid);

        TravelDocuments td = (TravelDocuments)ctx.GetObjectByKey(key);

        string transactionno = input.TransactionNo;
        string crossdate = input.CrossDate;
        string unloaddate = input.UnloadDate;
        string status = "Open";
        DateTime? crossDate = null;
        DateTime? unloadDate = null;



        if (crossdate == null || crossdate == "null" || crossdate == "" || crossdate == "undefined")
        {
            crossDate = null;
        }
        else
        {
            crossDate = DateTime.Parse(input.CrossDate);
        }

        if (unloaddate == null || unloaddate == "null" || unloaddate == "" || unloaddate == "undefined")
        {
            unloadDate = null;
        }
        else
        {
            unloadDate = DateTime.Parse(input.UnloadDate);
        } 
        
        if (td != null)
        {
            td.TransactionNo = transactionno;
            td.TransactionDate = DateTime.Parse(input.TransactionDate);
            td.TravelFeeDate = DateTime.Parse(input.TravelFeeDate);
            td.CrossDate = crossDate;
            td.UnloadDate = unloadDate;
            td.TravelExpenseId = input.TravelExpenseId;
            td.IsExternal = input.IsExternal;
            td.DriverHonorId = input.DriverHonorId;
            td.IsContractWork = input.IsContractWork;
            td.CustomerId = input.CustomerId;
            td.VehicleId = input.VehicleId;
            td.TravelFee = input.TravelFee;
            td.DriverId = input.DriverId;
            td.ShipName = input.ShipName;
            td.ItemId = input.ItemId;
            td.Bruto = input.Bruto;
            td.Tare = input.Tare;
            td.DestinationNetto = input.DestinationNetto;
            td.OriginNetto = input.OriginNetto;
            td.UseDestinationNetto = input.UseDestinationNetto;
            td.Remark = input.Remark;
            td.DONo = input.DONo;
            td.ModifiedBy = user.UserID;
            td.ModifiedDate = DateTime.Now;
            td.Status = status;
        }

        Helper.InsertLog("SAL-Transaction-TravelDocument-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SAL-Transaction-TravelDocument-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Cancel(TravelDocumentDTO input, TravelDocumentDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        string transactionno = input.TransactionNo;

        EntityKey key = new EntityKey("EasyEntities.TravelDocuments", "Id", oldInput.Id);

        TravelDocuments td = (TravelDocuments)ctx.GetObjectByKey(key);

        td.Status = "Cancel";

        Helper.InsertLog("SAL-Transaction-TravelDocument-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"select td.Id, td.TransactionNo, CONVERT(varchar(10), td.TransactionDate, 120) as Date, CONVERT(varchar(10), td.TransactionDate, 103) as TDate, 
                                    CONVERT(varchar(10), td.CrossDate, 120) as CrossDate, CONVERT(varchar(10), td.TravelFeeDate, 120) as TravelFeeDate, 
                                    CONVERT(varchar(10), td.UnloadDate, 120) as UnloadDate, td.RowVersion, td.IsExternal, td.DriverHonorId, 
                                    CASE WHEN dh.Honor IS NULL THEN 0 ELSE dh.Honor END as Honor, 
                                    td.IsContractWork, td.TravelExpenseId, 
                                    td.VehicleId, vc.VehicleNo, td.TravelFee, td.DriverId, dr.Name as DriverName, td.ShipName, td.CustomerId, 
                                    cst.Name as CustomerName, td.ItemId, td.Bruto, td.Tare, td.OriginNetto, td.DestinationNetto, 
                                    td.UseDestinationNetto, td.Remark, td.Status, td.DONo, it.ItemId as ItemCode, it.Name as ItemName, 
                                    us.Name as CreatedBy, td.ModifiedBy as mb, te.Destination into #temptable
                                    from TravelDocuments td inner join [User] us 
                                    on td.CreatedBy = us.UserID
                                    INNER JOIN Customer cst
                                    on cst.CustomerId = td.CustomerId
                                    INNER JOIN Items it
                                    ON it.Id = td.ItemId
                                    INNER JOIN TravelExpenses te
                                    ON td.TravelExpenseId = te.Id
                                    INNER JOIN Driver dr
                                    ON td.DriverId = dr.Id
                                    INNER JOIN Vehicle vc
                                    ON td.VehicleId = vc.Id
                                    LEFT OUTER JOIN DriverHonors dh
                                    ON td.DriverHonorId = dh.Id

                                    " + Helper.PagingQuery (@"SELECT data.* FROM (select cbp.*, us.Name as ModifiedBy, ROW_NUMBER() OVER (ORDER BY Date DESC, Id DESC) as row
                                    from #temptable cbp left outer join
                                    [User] us 
                                    on cbp.mb = us.UserID
                                    WHERE " + Helper.FilterQuery(option, "cbp.Status", "TransactionNo",
                                     "Tdate", "CustomerName", "CustomerId", "DriverName","VehicleNo", "OriginNetto", "DestinationNetto") + @") data
                                    ", option) + @"
                                    drop table #temptable", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT CustomerId, Name as CustomerName  
	                    FROM Customer 
                        WHERE CustomerId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, TravelDocumentDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            
            if (list == "travelexpense")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT te.Id as TravelExpenseId, te.Origin, te.Destination, te.HasBase, te.Base, te.Price, 
                                       te.PriceBelow, te.PriceAbove, it.Id as ItemId, it.ItemId as ItemCode, it.Name as ItemName,
                                       ROW_NUMBER() OVER (ORDER BY te.Id) as row                                         
                                       FROM TravelExpenses te 
                                       INNER JOIN Items it 
                                       ON te.ItemId=it.Id
                                       WHERE " + Helper.FieldFilterQuery(option, "it.ItemId", "it.Name", "te.Origin", "te.Destination", "te.Price"), option), con);
            }
            else if (list == "customer")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
                    FROM Customer  
                    WHERE " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
            }
            else if (list == "vehicle")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT vhc.Id as VehicleId, vhc.VehicleNo, vhd.DriverId, dr.Name as DriverName,  
                    ROW_NUMBER() OVER (ORDER BY VehicleNo) as row                 
                    FROM Vehicle vhc
                    INNER JOIN VehicleDetails vhd
                    ON vhc.Id = vhd.VehicleId
                    INNER JOIN Driver dr
                    ON vhd.DriverId = dr.Id                                                                                  
                    WHERE vhd.Status='Open' AND dr.Status = 'Open' AND " + Helper.FieldFilterQuery(option, "vhc.Id", "vhc.VehicleNo", "dr.Name"), option), con);
            }
            else if (list == "driverhonor")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT Id, Destination, Honor, 
                    ROW_NUMBER() OVER (ORDER BY Destination) as row                 
                    FROM DriverHonors  
                    WHERE " + Helper.FieldFilterQuery(option, "Id", "Destination", "Honor"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, TravelDocumentDTO input, TravelDocumentDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.TravelDocuments where a.Id == oldInput.Id select a).FirstOrDefault();

        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }
    

    protected override void Validate(BaseHandler<TravelDocumentDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, TravelDocumentDTO input, TravelDocumentDTO oldInput)
    {
        string transactionNo = input.TransactionNo;
        
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert)
        {
            var travelNo = (from a in ctx.TravelDocuments where a.TransactionNo == transactionNo select a).FirstOrDefault();

            if (travelNo != null)
            {
                Helper.AddError(err, "transactionNo", "Transaction No. " + transactionNo + " has been registered!");
            }
        }    
        
        
        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
           // if (input.UnloadDate == "" && input.IsContractWork == 1 || input.IsExternal == 1)
                
           // {
           //     Helper.AddError(err, "UnloadDate", "Unload date must not be empty!");
           // }
             
           //if (input.CrossDate == "" && input.IsContractWork == 0 || input.IsExternal == 0)
           // {
           //     Helper.AddError(err, "CrossDate", "Cross date must not be empty!");
           // }

            if (input.TravelFeeDate == "")
            {
                Helper.AddError(err, "travelFeeDate", "Travel Fee Date Must Not be empty!");
            }
            
            if (input.TransactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. is required!");
            }
            if (input.DriverId == 0)
            {
                Helper.AddError(err, "driver", "Driver is required!");
            }
            if (input.VehicleId == 0)
            {
                Helper.AddError(err, "vehicle", "Vehicle No is required!");
            }
            if (input.CustomerId == "" && input.CustomerName == "")
            {
                Helper.AddError(err, "customer", "Customer is required!");
            }
            if (input.ItemCode == "" && input.ItemName == "")
            {
                Helper.AddError(err, "item", "Item is required!");
            }
            if (input.CustomerId != "" && input.CustomerName == "")
            {
                Helper.AddError(err, "customer", "Customer does not exist!");
            }
            if (input.ItemCode != "" && input.ItemName == "")
            {
                Helper.AddError(err, "item", "Item does not exist!");
            }
         
            //if (input.Tare == 0)
            //{
            //    Helper.AddError(err, "tare", "Tare must have value!");
            //}
           
            //if (input.OriginNetto == 0)
            //{
            //    Helper.AddError(err, "originnetto", "Origin Netto must have value!");
            //}
            //DateTime dateposting = Convert.ToDateTime(input.TransactionDate);
            //Helper.CheckPosting(ctx, dateposting, "SAL", err);
        }
        

        if (mode == ValidateMode.Update || mode == ValidateMode.Cancel)
        {
            int transactionid = oldInput.Id;

            var invoice = (from a in ctx.InvoiceDetails where a.TravelDocumentId == transactionid select a).FirstOrDefault();

            

            if (input.Status == "Cancel" || input.Status == "Closed")
            {
                Helper.AddError(err, "status", "Travel Document has been Canceled/Closed!");
            }
        }
    }
}