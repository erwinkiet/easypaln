using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

//public class LoggedUserPermission
//{
//    public int PermissionID { get; set; }
//    public string Module { get; set; }
//    public string Name { get ; set ; }
//    public string Type { get; set; }
//    public string Form { get; set; }
//    public string Action { get; set; }
//}

public class LoggedUser
{
    public int UserID { get; set; }
    public string Username { get; set; }
    public string Name { get; set; }
    public string Initial { get; set; }
    public string DepartmentID { get; set; }
    public string Role { get; set; }
    public string Privilege { get; set; }
}

public class ListOption
{
    public string Filter { get; set; }
    public string[] Filters { get; set; }
    public string[] StatusFilters { get; set; }
    public int CurrentPage { get; set; }
    public int PageSize { get; set; }
    public SortOption[] Sortings { get; set; }
}

public class SortOption
{
    public string Field { get; set; }
    public string Direction { get; set; }
}

/// <summary>
/// Summary description for BaseHandler
/// </summary>
public abstract class BaseHandler<T> : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    protected string conString;
    protected JavaScriptSerializer json;
    protected HttpContext http;
    protected LoggedUser user;

    protected enum ValidateMode { Insert, Update, Cancel, Delete, Approve, Reject, Open, Close }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public BaseHandler()
    {
        conString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
        json = new JavaScriptSerializer();
    }

    protected virtual void GetDefaultData() { }
    protected virtual void GetMasterData(ListOption option) { }
    protected virtual void GetDetailData(string id, string detail) { }
    protected virtual void GetListData(string list, string id) { }
    protected virtual void GetList(string list, T input, ListOption option, int currentDetailIndex) { }
    protected virtual void Insert(T input, EasyEntities ctx) { }
    protected virtual void Update(T input, T oldInput, EasyEntities ctx) { }
    protected virtual void Delete(T input, T oldInput, EasyEntities ctx) { }
    protected virtual void Cancel(T input, T oldInput, EasyEntities ctx) { }
    protected virtual void Approve(T input, T oldInput, EasyEntities ctx) { }
    protected virtual void Reject(T input, T oldInput, EasyEntities ctx) { }
    protected virtual void Open(T input, T oldInput, EasyEntities ctx) { }
    protected virtual void Close(T input, T oldInput, EasyEntities ctx) { }

    protected virtual void CheckConcurrency(Dictionary<string, List<string>> err, 
        EasyEntities ctx, T input, T oldInput) { }
    
    protected virtual void Validate(ValidateMode mode,
        Dictionary<string, List<string>> err, EasyEntities ctx, T input, T oldInput)
    { }

    protected virtual void GetCurrentUserAndPermission(string formName)
    {
        string userJSON = json.Serialize(user);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"SELECT a.* FROM Permission a, UserPermission b 
                WHERE b.UserID=" + user.UserID + @" AND 
                    a.ID=b.PermissionID AND 
                    a.Form='" + formName + "'", con);

            List<string> tmp2 = new List<string>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tmp2.Add("\"" + dt.Rows[i]["Action"].ToString().ToLower() + "\": true");
            }

            string tmp3 = "{ " + String.Join(", ", tmp2) + " }";

            http.Response.Write("{ \"user\": " + userJSON + ", \"permission\": " + tmp3 + " }");
        }
    }

    public virtual void BeginProcess(Dictionary<string, List<string>> err)
    {
        if (http.Request.RequestType == "GET" && http.Request.QueryString["id"] == null &&
            http.Request.QueryString["formName"] == null &&
            http.Request.QueryString["action"] == null)
        {
            ListOption option = json.Deserialize<ListOption>(http.Request["option"]);
            GetMasterData(option);
        }
        else if (http.Request.RequestType == "GET" && http.Request.QueryString["formName"] != null)
        {
            GetCurrentUserAndPermission(http.Request.QueryString["formName"]);
        }
        else if (http.Request.RequestType == "GET" && http.Request.QueryString["list"] != null)
        {
            GetListData(http.Request.QueryString["list"], http.Request.QueryString["id"]);
        }
        else if (http.Request.RequestType == "GET" && http.Request.QueryString["id"] != null)
        {
            string detail = http.Request.QueryString["detail"];
            GetDetailData(http.Request.QueryString["id"], detail != null ? detail : "");
        }
        else if (http.Request.RequestType == "GET" && http.Request.QueryString["action"] != null &&
            http.Request.QueryString["action"] == "getDefaults")
        {
            GetDefaultData();
        }
        else if (http.Request.RequestType == "POST" && http.Request["list"] != null)
        {
            T input = json.Deserialize<T>(http.Request["input"]);
            ListOption option = json.Deserialize<ListOption>(http.Request["option"]);

            GetList(http.Request["list"], input, option, Convert.ToInt32(http.Request["detailIndex"]));
        }
        else if (http.Request.RequestType == "POST")
        {
            T input = json.Deserialize<T>(http.Request["input"]);
            T oldInput = json.Deserialize<T>(http.Request["oldInput"]);

            using (EasyEntities ctx = new EasyEntities())
            {
                string action = http.Request["action"];
                ValidateMode mode = ValidateMode.Insert;

                switch (action)
                {
                    case "insert":
                        mode = ValidateMode.Insert;
                        break;
                    case "update":
                        mode = ValidateMode.Update;
                        break;
                    case "cancel":
                        mode = ValidateMode.Cancel;
                        break;
                    case "delete":
                        mode = ValidateMode.Delete;
                        break;
                    case "open":
                        mode = ValidateMode.Open;
                        break;
                    case "close":
                        mode = ValidateMode.Close;
                        break;
                    case "approve":
                        mode = ValidateMode.Approve;
                        break;
                    case "reject":
                        mode = ValidateMode.Reject;
                        break;
                }

                if (mode != ValidateMode.Insert)
                {
                    CheckConcurrency(err, ctx, input, oldInput);
                    if (err.Count > 0)
                    {
                        // Concurrency error
                        http.Response.StatusCode = 400;
                        http.Response.Write(json.Serialize(err));
                        return;
                    }
                }

                Validate(mode, err, ctx, input, oldInput);
                if (err.Count == 0)
                {
                    switch (action)
                    {
                        case "insert":
                            Insert(input, ctx);
                            break;
                        case "update":
                            Update(input, oldInput, ctx);
                            break;
                        case "cancel":
                            Cancel(input, oldInput, ctx);
                            break;
                        case "delete":
                            Delete(input, oldInput, ctx);
                            break;
                        case "open":
                            Open(input, oldInput, ctx);
                            break;
                        case "close":
                            Close(input, oldInput, ctx);
                            break;
                        case "approve":
                            Approve(input, oldInput, ctx);
                            break;
                        case "reject":
                            Reject(input, oldInput, ctx);
                            break;
                    }
                    http.Response.StatusCode = 200;
                }
                else
                {
                    // Validation error
                    http.Response.StatusCode = 400;
                    http.Response.Write(json.Serialize(err));
                }
            }
        }        
    }

    public virtual void ProcessRequest(HttpContext context)
    {
        this.http = context;

        context.Response.ContentType = "application/json";
        context.Response.TrySkipIisCustomErrors = true;

        Dictionary<string, List<string>> err = new Dictionary<string, List<string>>();

        try
        {
            user = Helper.GetCurrentUser(context);
            BeginProcess(err);
        }
        catch (Exception e)
        {
            context.Response.StatusCode = 500;
            Helper.ServerError(err, e);
            context.Response.Write(json.Serialize(err));
        }
    }
        
}