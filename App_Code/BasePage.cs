﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

public class BasePage : System.Web.UI.Page
{
    static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);
    string[] dateFormat = new string[] { "dd/MM/yyyy HH:mm:ss" };

    /// <summary>
    /// Cek apakah string merupakan tanggal dengan format dd/MM/yyyy.
    /// </summary>
    /// <param name="txt">String yang ingin dicek.</param>
    /// <returns>True bila ya, false bila tidak.</returns>
    public bool isDate(string txt)
    {
        string[] dateFormat = new string[] { "dd/MM/yyyy" };
        DateTime tmp;

        return DateTime.TryParseExact(txt, dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out tmp);
    }

    /// <summary>
    /// Hapus semua record di semua tabel.
    /// </summary>
    /// <param name="con">Koneksi database.</param>
    protected void DeleteAllRowsFromAllTables(SqlConnection con)
    {
        SqlCommand cmd;
        string query;

        query = @"EXEC sp_MSForEachTable 'DISABLE TRIGGER ALL ON ?';
            EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL';
            EXEC sp_MSForEachTable 'DELETE FROM ?';
            EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL';
            EXEC sp_MSForEachTable 'ENABLE TRIGGER ALL ON ?';";

        cmd = new SqlCommand(query, con);
        cmd.ExecuteNonQuery();
    }
    /// <summary>
    /// Insert data dari DataTable ke tabel database.
    /// </summary>
    /// <param name="dt">DataTable.</param>
    /// <param name="con">Koneksi database.</param>
    /// <param name="tableName">Nama tabel.</param>
    protected void InsertTable(DataTable dt, SqlConnection con, string tableName, int start = 0)
    {
        string query;
        SqlCommand cmd;

        if (dt.Rows.Count == 0)
        {
            return;
        }

        query = "INSERT INTO [" + tableName + "] VALUES";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query += " (";
            for (int j = start; j < dt.Columns.Count; j++)
            {
                string tmp = dt.Rows[i][j].ToString();
                DateTime tmp4;

                if (DateTime.TryParseExact(tmp, dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out tmp4 ))
                {
                    tmp = tmp.Substring(0, 10).Replace('/', '-');
                    string[] tmp2 = tmp.Split('-');
                    string tmp3 = tmp2[0];
                    tmp2[0] = tmp2[2];
                    tmp2[2] = tmp3;
                    tmp = string.Join("-", tmp2);
                }

                query += "'" + tmp + "'";
                if (j < dt.Columns.Count - 1)
                {
                    query += ", ";
                }
            }
            query += ")";
            if ((i + 1) % 1000 == 0)
            {
                cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                
                if (i < dt.Rows.Count - 1)
                {
                    query = "INSERT INTO [" + tableName + "] VALUES";
                }
                else
                {
                    query = "";
                }
            }
            else if (i < dt.Rows.Count - 1)
            {
                query += ",";
            }
        }
        if (query != "")
        {
            cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// Copy DataTable ke tabel database.
    /// </summary>
    /// <param name="dt">DataTable.</param>
    /// <param name="con">Koneksi database tujuan.</param>
    /// <param name="tableName">Nama tabel.</param>
    /// <param name="isDetail">Apakah table yang dicopy adalah tabel detail.</param>
    protected void CopyTable(DataTable dt, SqlConnection con, string tableName, bool isDetail = false)
    {
        string query;
        SqlCommand cmd;

        if (isDetail)
        {
            query = "DELETE FROM [" + tableName + "]";
        }
        else
        {
            query = "DELETE FROM [" + tableName + "]; DBCC CHECKIDENT('[" + tableName + "]', RESEED, 0)";
        }
        cmd = new SqlCommand(query, con);
        cmd.ExecuteNonQuery();

        if (dt.Rows.Count == 0)
        {
            return;
        }

        query = "INSERT INTO [" + tableName + "] VALUES";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query += " (";
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                query += "'" + dt.Rows[i][j].ToString() + "'";
                if (j < dt.Columns.Count - 1)
                {
                    query += ", ";
                }
            }
            query += ")";
            if (i < dt.Rows.Count - 1)
            {
                query += ",";
            }
        }
        //Debug.WriteLine(dt.Rows.Count);
        cmd = new SqlCommand(query, con);
        cmd.ExecuteNonQuery();
    }

    /// <summary>
    /// Untuk mengcopy table dari 1 database ke database lain.
    /// </summary>
    /// <param name="con1">Koneksi 1.</param>
    /// <param name="con2">Koneksi 2.</param>
    /// <param name="tableName">Nama table.</param>
    protected void CopyTable(SqlConnection con1, SqlConnection con2, string tableName)
    {
        SqlDataAdapter da;
        DataTable dt = new DataTable();
        string query;
        SqlCommand cmd;

        query = "TRUNCATE TABLE [" +  tableName + "]";
        cmd = new SqlCommand(query, con2);
        cmd.ExecuteNonQuery();
        query = "SELECT * FROM [" + tableName + "]";
        da = new SqlDataAdapter(query, con1);
        da.Fill(dt);

        if (dt.Rows.Count == 0)
        {
            return;
        }

        query = "INSERT INTO [" + tableName + "] VALUES";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query += " (";
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                query += "'" + dt.Rows[i][j].ToString() + "'";
                if (j < dt.Columns.Count - 1)
                {
                    query += ", ";
                }
            }
            query += ")";
            if (i < dt.Rows.Count - 1)
            {
                query += ",";
            }
        }
        cmd = new SqlCommand(query, con2);
        cmd.ExecuteNonQuery();
    }

    /// <summary>
    /// Untuk mengaktifkan atau menonaktifkan semua kontrol.
    /// </summary>
    /// <param name="status">False untuk menonaktifkan, kosong untuk mengaktifkan.</param>
    protected void EnableControls(bool status = true)
    {

        foreach (Control c in Page.Controls)
            foreach (Control ctrl in c.Controls)

                if (ctrl is TextBox)

                    ((TextBox)ctrl).Enabled = status;

                else if (ctrl is Button)

                    ((Button)ctrl).Enabled = status;

                else if (ctrl is RadioButton)

                    ((RadioButton)ctrl).Enabled = status;

                else if (ctrl is ImageButton)

                    ((ImageButton)ctrl).Enabled = status;

                else if (ctrl is CheckBox)

                    ((CheckBox)ctrl).Enabled = status;

                else if (ctrl is DropDownList)

                    ((DropDownList)ctrl).Enabled = status;

                else if (ctrl is HyperLink)

                    ((HyperLink)ctrl).Enabled = status;

    }

    /// <summary>
    /// Untuk fokus ke kontrol.
    /// </summary>
    /// <param name="focusControl">Kontrol yang ingin difokus.</param>
    /// <param name="clearControl">Kontrol yang ingin dihapus isinya.</param>
    protected void FocusControl(Control focusControl, Control clearControl = null)
    {
        if (clearControl == null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "f", "document.getElementById('" + focusControl.ClientID + "').focus();", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "f", "document.getElementById('" + focusControl.ClientID + "').focus();document.getElementById('" + clearControl.ClientID + "').value='';", true); 
        }
    }

    private void Page_PreInit(object sender, EventArgs e)
    {
        HttpCookie preferredTheme = Request.Cookies.Get("PreferredTheme");
        
        if (preferredTheme != null)
        {
            Page.Theme = preferredTheme.Value;
        }
    }

    private void Page_PreRender(object sender, EventArgs e)
    {
        if (this.Title == "Untitled Page" || string.IsNullOrEmpty(this.Title))
        {
            throw new Exception("Page title cannot be \"Untitled Page\" or an empty string.");
        }
    }

    public BasePage()
    {
        this.PreRender += new EventHandler(Page_PreRender);
        this.PreInit += new EventHandler(Page_PreInit);
    }

    // method untuk popup messagebox
    public void MessageBox(string pesan)
    {
        string message = pesan.Trim();
        System.Text.StringBuilder sb = new System.Text.StringBuilder("");
        //sb.Append("<script type = 'text/javascript'>");
        //sb.Append("window.onload=function(){");
        sb.Append("alert('");
        sb.Append(message);
        sb.Append("');");
        //sb.Append("</script>");
      
        ScriptManager.RegisterStartupScript(this,GetType(), "alert", sb.ToString(),true);
        
    }

    /// <summary>
    /// Untuk update ApplicationLog.
    /// </summary>
    /// <param name="module"></param>
    /// <param name="action"></param>
    public void InsertLog(string module, string action)
    {
        string sql = "insert into ApplicationLog (UserID, Module, ActionTime, Action) values (@User, @Module, @ActionTime, @Action)";

        SqlCommand cmd = new SqlCommand(sql, con);

        cmd.Parameters.Add("@User", SqlDbType.Int);
        cmd.Parameters["@User"].Value = Session["UserID"].ToString();

        cmd.Parameters.Add("@Module", SqlDbType.NVarChar);
        cmd.Parameters["@Module"].Value = module.Trim();

        cmd.Parameters.Add("@ActionTime", SqlDbType.DateTime);
        cmd.Parameters["@ActionTime"].Value = DateTime.Now.ToString();

        cmd.Parameters.Add("@Action", SqlDbType.Text);
        cmd.Parameters["@Action"].Value = action.Trim();

        try
        {
            con.Open();
            cmd.ExecuteScalar();
        }
        finally
        {
            con.Close();
        }
    }

    /// <summary>
    /// Method untuk custom query SQL.
    /// </summary>
    /// <param name="query">Query yang ingin dijalankan.</param>
    public void CustomQuery(string query)
    {
        SqlCommand cmd = new SqlCommand(query, con);

        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
        catch
        {
            con.Close();
        }
    }

    // Method untuk mengambil connection properties server name di crystal report
    public string getServerName()
    {
        return "ERWINCHANDRA-PC\\SQLEXPRESS";
    }

    // Method untuk mengambil connection properties database name di crystal report
    public string getDatabaseName()
    {
        return "easyPJP";
    }

    public string getDay(string hari)
    {
        hari = DateTime.Parse(hari).DayOfWeek.ToString();
        if (hari == "Monday")
        {
            hari = "Senin";
        }
        else if (hari == "Tuesday")
        {
            hari = "Selasa";
        }
        else if (hari == "Wednesday")
        {
            hari = "Rabu";
        }
        else if (hari == "Thursday")
        {
            hari = "Kamis";
        }
        else if (hari == "Friday")
        {
            hari = "Jumat";
        }
        else if (hari == "Saturday")
        {
            hari = "Sabtu";
        }
        else if (hari == "Sunday")
        {
            hari = "Minggu";
        }
        return hari;
    }

    public string getDate(string tanggal)
    {
        string bulan, tgl, tahun;
        tgl = tanggal.Substring(0, 2);
        bulan = tanggal.Substring(3, 2);
        tahun = tanggal.Substring(6, 4);
        if (bulan == "01")
        {
            bulan = "Januari";
        }
        else if (bulan == "02")
        {
            bulan = "Februari";
        }
        else if (bulan == "03")
        {
            bulan = "Maret";
        }
        else if (bulan == "04")
        {
            bulan = "April";
        }
        else if (bulan == "05")
        {
            bulan = "Mei";
        }
        else if (bulan == "06")
        {
            bulan = "Juni";
        }
        else if (bulan == "07")
        {
            bulan = "Juli";
        }
        else if (bulan == "08")
        {
            bulan = "Agustus";
        }
        else if (bulan == "09")
        {
            bulan = "September";
        }
        else if (bulan == "10")
        {
            bulan = "Oktober";
        }
        else if (bulan == "11")
        {
            bulan = "November";
        }
        else if (bulan == "12")
        {
            bulan = "Desember";
        }
        tanggal = tgl + " " + bulan + " " + tahun;
        return tanggal;
    }

    public string Terbilang(long number)
    {
        string strTerbilang = "";
        string[] angka = { "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas" };
        if (number < 12)
        {
            strTerbilang = " " + angka[number];
        }
        else if (number < 20)
        {
            strTerbilang = this.Terbilang(number - 10) + " Belas ";
        }
        else if (number < 100)
        {
            strTerbilang = this.Terbilang(number / 10) + " Puluh " + this.Terbilang(number % 10);
        }
        else if (number < 200)
        {
            strTerbilang = " Seratus " + this.Terbilang(number - 100);
        }
        else if (number < 1000)
        {
            strTerbilang = this.Terbilang(number / 100) + " Ratus " + this.Terbilang(number % 100);
        }
        else if (number < 2000)
        {
            strTerbilang = " Seribu " + this.Terbilang(number - 1000);
        }
        else if (number < 1000000)
        {
            strTerbilang = this.Terbilang(number / 1000) + " Ribu " + this.Terbilang(number % 1000);
        }
        else if (number < 1000000000)
        {
            strTerbilang = this.Terbilang(number / 1000000) + " Juta " + this.Terbilang(number % 1000000);
        }
        else if (number < 1000000000000)
        {
            strTerbilang = this.Terbilang(number / 1000000000) + " Milyar " + this.Terbilang(number % 1000000000);
        }
        // menghilangkan multiple spasi
        strTerbilang = System.Text.RegularExpressions.Regex.Replace(strTerbilang, @"^\s+|\s+$", " ");
        return strTerbilang;
    }

    // Mengecek Posting
    public int CheckPosting(string date, string module, string action, string href)
    {
        int bulan = Convert.ToDateTime(date).Month;

        int tahun = Convert.ToDateTime(date).Year;

        SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Posting WHERE Module='" + module + "' ORDER BY Periode DESC", con);
        DataTable dt = new DataTable();
        da.Fill(dt);

        if (dt.Rows.Count > 0)
        {
            string period = dt.Rows[0]["Periode"].ToString();
            int bulan_posting = Convert.ToInt32(period.Substring(5, 2)) + 1;
            int tahun_posting = Convert.ToInt32(period.Substring(0, 4));
            
            if (((tahun == tahun_posting) && (bulan < bulan_posting + 3)) || ((tahun == (tahun_posting + 1)) && (12 - bulan_posting + bulan) < 3))
            {
                return 1;
            }
               
            else
            {
                if (action != "insert" && action != "update")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('Data has been posted! Can not " + action + "!'); window.location.href = '" + href + "';", true);
                    //MessageBox("Data has been posted! Can not " + action + "!");
                }
                else if (action == "update")
                {
                    MessageBox("Data has been Posted! Can not " + action + "!");
                }
                else
                {
                    MessageBox("Date Only can three month greater than last posting time : " + period);
                }

                return 0;
            }
        }
        else
        {
            MessageBox("Last Posting not found!");
            return 0;
        }
    }
    public string getservertime()
    {
        SqlDataAdapter da = new SqlDataAdapter("select YEAR(getdate()) as thn,MONTH(getdate())as bln,DAY(getdate())as tgl", con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        string tanggal = dt.Rows[0]["tgl"].ToString().PadLeft(2,'0') + "/" + dt.Rows[0]["bln"].ToString().PadLeft(2,'0') +"/"+dt.Rows[0]["thn"].ToString();
        return tanggal;
    }

    /// <summary>
    /// Untuk mengkonversi nama bulan dalam bentuk bahasa indonesia
    /// </summary>
    /// <param name="bulan">Bulan yang ingin dikonversi</param>
    public string IndonesiaMonth(string bulan)
    {
        if(bulan == "1")
            return "Januari";

        if (bulan == "2")
            return "Februari";

        if (bulan == "3")
            return "Maret";

        if (bulan == "4")
            return "April";

        if (bulan == "5")
            return "Mei";

        if (bulan == "6")
            return "Juni";

        if (bulan == "7")
            return "Juli";

        if (bulan == "8")
            return "Agustus";

        if (bulan == "9")
            return "September";

        if (bulan == "10")
            return "Oktober";

        if (bulan == "11")
            return "November";
        else
            return "Desember";
    }
}