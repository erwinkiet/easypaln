﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Diagnostics;
using EasyModel;
using System.Text;
using System.Data.SqlTypes;
using JWT;

/// <summary>
/// Summary description for Helper
/// </summary>
public static class Helper
{
    public static string conString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
    public static string appName = ConfigurationManager.AppSettings["SiteName"];
    public static string tokenKey = ConfigurationManager.AppSettings["TokenKey"];

    public static LoggedUser GetCurrentUser(HttpContext context)
    {
        LoggedUser userDic = new LoggedUser();
        string token = context.Request.Headers.Get("userToken");

        userDic = JsonWebToken.DecodeToObject<LoggedUser>(token, tokenKey);

        return userDic;
    }

    public static bool IsDate(string txt)
    {
        string[] dateFormat = new string[] { "dd/MM/yyyy", "yyyy-MM-dd", "MM/dd/yyyy" };
        DateTime tmp;

        return DateTime.TryParseExact(txt, dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out tmp);
    }

    public static string EscapeSQLString(string value)
    {
        StringBuilder sb = new StringBuilder(value.Length);
        for (int i = 0; i < value.Length; i++)
        {
            char c = value[i];
            switch (c)
            {
                case ']':
                case '[':
                case '%':
                case '*':
                    sb.Append("[").Append(c).Append("]");
                    break;
                case '\'':
                    sb.Append("''");
                    break;
                default:
                    sb.Append(c);
                    break;
            }
        }
        return sb.ToString();
    }

    /// <summary>
    /// Insert data dari DataTable ke tabel database.
    /// </summary>
    /// <param name="dt">DataTable.</param>
    /// <param name="con">Koneksi database.</param>
    /// <param name="tableName">Nama tabel.</param>
    public static void InsertTable(DataTable dt, SqlConnection con, string tableName, int start = 0)
    {
        string query;
        SqlCommand cmd;
        string[] dateFormat = new string[] { "dd/MM/yyyy HH:mm:ss" };

        if (dt.Rows.Count == 0)
        {
            return;
        }

        query = "INSERT INTO [" + tableName + "] (";
        for (int j = start; j < dt.Columns.Count; j++)
        { 
            DataColumn c = dt.Columns[j];
            query += "[" + c.ColumnName + "]";

            if (j < dt.Columns.Count - 1)
            {
                query += ", ";
            }
        }
        query += ") VALUES";
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query += " (";
            for (int j = start; j < dt.Columns.Count; j++)
            {
                string tmp = dt.Rows[i][j].ToString();
                DateTime tmp4;

                if (DateTime.TryParseExact(tmp, dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out tmp4))
                {
                    tmp = tmp.Substring(0, 10).Replace('/', '-');
                    string[] tmp2 = tmp.Split('-');
                    string tmp3 = tmp2[0];
                    tmp2[0] = tmp2[2];
                    tmp2[2] = tmp3;
                    tmp = string.Join("-", tmp2);
                }

                query += "'" + tmp + "'";
                if (j < dt.Columns.Count - 1)
                {
                    query += ", ";
                }
            }
            query += ")";
            if ((i + 1) % 1000 == 0)
            {
                cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();

                if (i < dt.Rows.Count - 1)
                {
                    query = "INSERT INTO [" + tableName + "] VALUES";
                }
                else
                {
                    query = "";
                }
            }
            else if (i < dt.Rows.Count - 1)
            {
                query += ",";
            }
        }
        if (query != "")
        {
            cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
        }
    }

    public static void InsertTableIfNotExists(DataTable dt, SqlConnection con, string tableName, string idName, int start = 0)
    {
        string query = "";
        SqlCommand cmd;
        string[] dateFormat = new string[] { "dd/MM/yyyy HH:mm:ss" };

        if (dt.Rows.Count == 0)
        {
            return;
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            query += "if not exists(select * from " + tableName + " where " + idName + " = '" + dt.Rows[i][0] + "') insert into " + tableName + " (";
            for (int j = start; j < dt.Columns.Count; j++)
            {
                DataColumn c = dt.Columns[j];
                query += "[" + c.ColumnName + "]";

                if (j < dt.Columns.Count - 1)
                {
                    query += ", ";
                }
            }
            query +=  ") values (";
            for (int j = start; j < dt.Columns.Count; j++)
            {
                string tmp = dt.Rows[i][j].ToString();
                DateTime tmp4;

                if (DateTime.TryParseExact(tmp, dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out tmp4))
                {
                    tmp = tmp.Substring(0, 10).Replace('/', '-');
                    string[] tmp2 = tmp.Split('-');
                    string tmp3 = tmp2[0];
                    tmp2[0] = tmp2[2];
                    tmp2[2] = tmp3;
                    tmp = string.Join("-", tmp2);
                }

                query += "'" + tmp + "'";
                if (j < dt.Columns.Count - 1)
                {
                    query += ", ";
                }
            }
            query += ");";
        }
        if (query != "")
        {
            cmd = new SqlCommand(query, con);
            cmd.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// Convert DataTable ke JSON
    /// </summary>
    /// <param name="dt">DataTable yang ingin di-convert.</param>
    /// <returns>JSON</returns>
    public static string DataTabletoJSON(DataTable dt)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        serializer.MaxJsonLength = Int32.MaxValue;
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;

        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
                //Debug.WriteLine(dr[col]);
            }
            rows.Add(row);
        }

        return serializer.Serialize(rows);
    }

    public static string DataTablestoJSON(List<DataTable> dts)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer(); 
        serializer.MaxJsonLength = Int32.MaxValue;
        List<List<Dictionary<string, object>>> rowsList = new List<List<Dictionary<string, object>>>();

        foreach (DataTable dt in dts)
        {
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                    //Debug.WriteLine(dr[col]);
                }
                rows.Add(row);
            }

            rowsList.Add(rows);
        }

        return serializer.Serialize(rowsList);
    }

    /// <summary>
    /// Generate nomor ID
    /// </summary>
    /// <param name="tableName">Nama tabel di database.</param>
    /// <param name="idName">Nama field ID di tabel.</param>
    /// <param name="generatedName">Nama yang diinginkan.</param>
    /// <param name="dateString">Tanggal dalam string.</param>
    /// <param name="pic">PIC.</param>
    /// <param name="isPPN">PPN atau bukan.</param>
    /// <returns>ID hasil generate.</returns>
    public static string GenerateID(string tableName, string idName, string generatedName, string dateString, string pic, bool isPPN = true)
    {
        string id;
        int no;
        string month;
        DateTime date = DateTime.Parse(dateString);
        string year = date.Year.ToString().Substring(2, 2);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (isPPN)
            {
                month = date.Month.ToString().PadLeft(2, '0');
            }
            else
            {
                month = Convert.ToChar(date.Month + 64).ToString();
            }

            string dateCheck = year + "/" + month + "/";

            SqlDataAdapter da = new SqlDataAdapter("select " + idName + " from " + tableName +
                " where " + idName + " like '%" + dateCheck + "%' order by " + idName + " desc;", con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                string tmp = dt.Rows[0][idName].ToString().Trim();

                if (isPPN)
                {
                    no = Convert.ToInt32(tmp.Substring(generatedName.Length + 7, 4)) + 1;
                }
                else
                {
                    no = Convert.ToInt32(tmp.Substring(generatedName.Length + 6, 4)) + 1;
                }
            }
            else
            {
                no = 1;
            }

            id = generatedName + "/" + dateCheck + no.ToString().PadLeft(4, '0') + "/" + pic;
        }

        return id;
    }

    public static int GetNextID(string tablename)
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            SqlDataAdapter da = new SqlDataAdapter(@"declare @identcurrent int
                                                    declare @columncount int
                                                    declare @nextid int
                                                    select @identcurrent = IDENT_CURRENT('" + tablename + @"')
                                                    select @columncount = COUNT(*) from " + tablename + @"

                                                    select case when @columncount = 0 AND @identcurrent = 1 then 1 else 
                                                    @identcurrent + 1 end nextid", con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return Convert.ToInt32(dt.Rows[0]["nextid"]);
        }
    }

    public static void InsertLog(string module, string type, EasyEntities ctx, object dto, int userid)
    {
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        string logAction = (type == "" ? type : (type + ": ")) + serializer.Serialize(dto);

        ctx.ApplicationLog.AddObject(new ApplicationLog()
        {
            ActionTime = DateTime.Now,
            Action = logAction,
            Module = module,
            UserID = userid
        });
    }

    /// <summary>
    /// Memasukkan  ApplicationLog.
    /// </summary>
    /// <param name="module">modul-jenis transaksi</param>
    /// <param name="action">data-data transaksi</param>
    public static void InsertLog(string module, string type, EasyEntities ctx, Dictionary<string, string> input, List<Dictionary<string, string>> detail, int userid)
    {
        string logAction = type + " Master:";

        int i = 0;
        foreach (KeyValuePair<string, string> entry in input)
        {
            if(i == 0)
                logAction = logAction + " " + entry.Key + "=" + entry.Value;
            else
                logAction = logAction + ", " + entry.Key + "=" + entry.Value;

            i++;
        }

        if (detail != null)
        {
            int j = 0;
            foreach (Dictionary<string, string> d in detail)
            {
                if (j == 0)
                    logAction = logAction + "; " + type + " Detail " + j.ToString() + ": ";
                else
                    logAction = logAction + ", " + type + " Detail " + j.ToString() + ": ";

                foreach (KeyValuePair<string, string> entry in d)
                {
                    logAction = logAction + entry.Key + "=" + entry.Value;
                }

                j++;
            }
        }

        ctx.ApplicationLog.AddObject(new ApplicationLog()
        {
            ActionTime = DateTime.Now,
            Action = logAction,
            Module = module,
            UserID = userid
        });
    }

    public static void InsertLog(string module, string type, EasyEntities ctx, Dictionary<string, string> input, string[] label, List<Dictionary<string, string>>[] other, int userid)
    {
        string logAction = type + " Master:";

        int i = 0;
        foreach (KeyValuePair<string, string> entry in input)
        {
            if (i == 0)
                logAction = logAction + " " + entry.Key + "=" + entry.Value;
            else
                logAction = logAction + ", " + entry.Key + "=" + entry.Value;

            i++;
        }

        if (other != null)
        {
            int j = 0;
            int k = 0;
            foreach (List<Dictionary<string, string>> detail in other)
            {
                foreach (Dictionary<string, string> d in detail)
                {
                    if (j == 0)
                        logAction = logAction + "; " + type + " " + label[k] + " " + j.ToString() + ": ";
                    else
                        logAction = logAction + ", " + type + " " + label[k] + " " + j.ToString() + ": ";

                    foreach (KeyValuePair<string, string> entry in d)
                    {
                        logAction = logAction + entry.Key + "=" + entry.Value;
                    }

                    j++;
                }
                k++;
            }
        }

        ctx.ApplicationLog.AddObject(new ApplicationLog()
        {
            ActionTime = DateTime.Now,
            Action = logAction,
            Module = module,
            UserID = userid
        });
    }

    /// <summary>
    /// Validasi tanggal transaksi berdasarkan periode posting
    /// </summary>
    /// <param name="date">tanggal transaksi</param>
    /// <param name="module">modul transaksi</param>
    /// <param name="action">insert/update/cancel</param>
    /// <param name="href"></param>
    public static bool CheckPosting(EasyEntities ctx, DateTime date, string module, Dictionary<string, List<string>> err, int offset = 1)
    {
        Posting posting = (from a in ctx.Posting where a.Module == module orderby a.Periode descending select a).FirstOrDefault();

        if (posting == null)
        {
            Helper.AddError(err, "posting", "Posting not found!");
            return false;
        }
        else
        {
            int m1 = date.Month;
            int y1 = date.Year;
            int m2 = Convert.ToInt32(posting.Periode.Substring(5, 2));
            int y2 = Convert.ToInt32(posting.Periode.Substring(0, 4));

            string inputperiode = y1 + "-" + m1.ToString().PadLeft(2, '0');
            Debug.WriteLine(inputperiode + "," + module);
            Posting posting2 = (from a in ctx.Posting where a.Module == module && a.Periode == inputperiode select a).FirstOrDefault();
            int isopen = posting2 == null ? 2 : posting2.IsOpen;
            
            if (y1 == y2)
            {
                Debug.WriteLine(m1 + "," + m2 + "," + isopen);
                if (m1 <= m2)
                {
                    if (isopen == 0 || isopen == 2)
                    {
                        Helper.AddError(err, "posting", "Transaction date can not be less than the last posting (" + posting.Periode + ")!");
                        return false;
                    }
                }
                else if (m1 > m2 + offset)
                {
                    Helper.AddError(err, "posting", "Transaction date can not be " + offset + " months greater than the last posting (" + posting.Periode + ")!");
                    return false;
                }
            }
            else
            {
                if (y1 < y2)
                {
                    if (isopen == 0 || isopen == 2)
                    {
                        Helper.AddError(err, "posting", "Transaction date can not be less than the last posting (" + posting.Periode + ")!");
                        return false;
                    }
                }
                else if ((y1 - 1 == y2) && 12 - m2 + m1 > offset)
                {
                    Helper.AddError(err, "posting", "Transaction date can not be " + offset + " months greater than the last posting (" + posting.Periode + ")!");
                    return false;
                }

                else if (y1 > y2 + 1)
                {
                    Helper.AddError(err, "posting", "Transaction date can not be " + offset + " months greater than the last posting (" + posting.Periode + ")!");
                    return false;
                }
            }
        }

        return true;
    }

    /// <summary>
    /// Validasi tanggal transaksi berdasarkan periode posting
    /// </summary>
    /// <param name="date">tanggal transaksi</param>
    /// <param name="module">modul transaksi</param>
    /// <param name="action">insert/update/cancel</param>
    /// <param name="href"></param>
    public static bool CheckPosting(string date, string module, out string errorMessage)
    {
        int bulan = Convert.ToDateTime(date).Month;

        int tahun = Convert.ToDateTime(date).Year;

        using (SqlConnection con = new SqlConnection(conString))
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Posting WHERE Module='" + module + "' ORDER BY Periode DESC", con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                string period = dt.Rows[0]["Periode"].ToString();
                int bulan_posting = Convert.ToInt32(period.Substring(5, 2)) + 1;
                int tahun_posting = Convert.ToInt32(period.Substring(0, 4));

                if (((tahun == tahun_posting) && (bulan < bulan_posting + 3) && (bulan > bulan_posting)) || ((tahun == (tahun_posting + 1)) && (12 - bulan_posting + bulan) < 3))
                {
                    errorMessage = "";
                    return true;
                }
                else
                {
                    errorMessage = "Date can only three month greater than last posting time : " + period;
                    return false;
                }
            }
            else
            {
                errorMessage = "Last posting not found!";
                return false;
            }
        }
    }

    public static void AddError(Dictionary<string, List<string>> errors, string id, string message)
    {
        if (!errors.ContainsKey(id))
        {
            errors.Add(id, new List<string>());
        }
        errors[id].Add(message);
    }

    public static void ServerError(Dictionary<string, List<string>> errors)
    {
        errors.Clear();
        errors.Add("exception", new List<string>());
        errors["exception"].Add("Failed to connect to database. Please try again later");
    }

    public static void ServerError(Dictionary<string, List<string>> errors, Exception e)
    {
        errors.Clear();
        errors.Add("exception", new List<string>());
        errors["exception"].Add(e.Message);
        errors["exception"].Add(e.StackTrace);
        if (e.InnerException != null)
        {
            errors["exception"].Add(e.InnerException.Message);
        }
    }

    public static void ConcurrencyError(Dictionary<string, List<string>> errors)
    {
        errors.Clear();
        errors.Add("concurrency", new List<string>());
        errors["concurrency"].Add("This transaction has been modified when you are editing. Please reload this page!");
    }

    public static void ConcurrencyError(Dictionary<string, List<string>> errors, Exception e)
    {
        errors.Clear();
        errors.Add("exception", new List<string>());
        errors["exception"].Add(e.Message);
        errors["exception"].Add(e.StackTrace);
    }

    public static DataTable NewDataTable(string query, SqlConnection con)
    {
        DataTable dt;

        using (SqlDataAdapter da = new SqlDataAdapter(query, con))
        {
            dt = new DataTable();
            da.SelectCommand.CommandTimeout = 0;
            da.Fill(dt);
        }

        return dt;
    }

    public static bool CheckConcurrency(Dictionary<string, List<string>> errors, byte[] stamp1, byte[] stamp2)
    {
        SqlBinary sqb1 = new SqlBinary(stamp1);
        SqlBinary sqb2 = new SqlBinary(stamp2);

        if (sqb1.CompareTo(sqb2) == 0)
        {
            return false;
        }
        else
        {
            ConcurrencyError(errors);
            return true;
        }
    }

    public static bool CheckConcurrency(Dictionary<string, List<string>> errors, string stamp1, string stamp2)
    {
        if (stamp1 == stamp2)
        {
            return false;
        }
        else
        {
            ConcurrencyError(errors);
            return true;
        }
    }

 /*    public static void UpdateAccountAmount(string action, EasyEntities ctx, List<Dictionary<string, string>> detail)
    {
        for (int i = 0; i < detail.Count; i++)
        {
            string accountID = detail[i]["SubAccountID"];

            // Untuk mengupdate Nilai parent account
           for (; ; )
            {
                //var accountds = from a in ctx.SubAccount where a.SubAccountID == accountID select a;

                if (accountds != null && accountds.Count() > 0)
                {
                    decimal debitsNow, creditsNow, debits, credits = 0;

                    foreach (var accountd in accountds)
                    {
                        EntityKey key = new EntityKey("EasyEntities.SubAccount", "SubAccountID", accountID);
                        SubAccount account = (SubAccount)ctx.GetObjectByKey(key);

                        debitsNow = accountd.Debits;
                        creditsNow = accountd.Credits;
                        debits = Convert.ToDecimal(detail[i]["Debits"]);
                        credits = Convert.ToDecimal(detail[i]["Credits"]);

                        if (action == "insert")
                        {
                            decimal tmp = debitsNow - creditsNow + debits - credits;

                            account.Debits = tmp > 0 ? tmp : 0;
                            account.Credits = tmp < 0 ? -tmp : 0;
                        }
                        else // kondisi update atau cancel
                        {
                            decimal tmp = creditsNow - debitsNow + credits - debits;

                            account.Debits = tmp > 0 ? tmp : 0;
                            account.Credits = tmp < 0 ? -tmp : 0;
                        }
                        accountID = accountd.ParentAccountID;
                    }
                }  
                else
                {
                    break;
                } 
            }
        }
    } */

    public static string PagingQuery(string query, ListOption option)
    {
        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;

        string sortQuery = "";

        if (option.Sortings.Length > 0)
        {
            sortQuery = "ORDER BY ";

            List<string> tmp = new List<string>();

            foreach (var sorting in option.Sortings)
            {
                tmp.Add(sorting.Field + " " + sorting.Direction);
            }

            sortQuery += String.Join(", ", tmp);
        }
        else
        {
            sortQuery = "ORDER BY (SELECT NULL)";
        }

        string r = @"select data.* from (
                select *, (SUM(1) OVER()) as dataCount,
                    ROW_NUMBER() OVER (" + sortQuery + @") as row2
                from (" + query + @") tmp
            ) data 
            where data.row2 >= " + fromRow + " and data.row2 <= " + toRow + ";";

        return r;
    } 

    public static string StatusFilterQuery(ListOption option, string statusField)
    {
        return statusField +
            " IN ('" + string.Join("','", option.StatusFilters) + "')";
    }

    public static string FieldFilterQuery(ListOption option, params string[] filterFields)
    {
        List<string> r = new List<string>();

        foreach (string filter in option.Filters)
        {
            List<string> r2 = new List<string>();

            foreach (string field in filterFields)
            {
                string tmp = field + " like '%" + filter + "%'";
                r2.Add(tmp);
            }

            r.Add("(" + String.Join(" or ", r2) + ")");
        }

        return String.Join(" and ", r);
    }

    public static string FilterQuery(ListOption option, string statusField, params string[] filterFields)
    {
        return FieldFilterQuery(option, filterFields) + " and " + StatusFilterQuery(option, statusField);
    }

  /*  public static void UpdateConfig(EasyEntities ctx, string name, string value)
    {
        var tmp = (from a in ctx.AppConfigs where a.Name == name select a).FirstOrDefault();

        if (tmp == null)
        {
            ctx.AppConfigs.AddObject(new AppConfigs()
            {
                Name = name,
                Value = value
            });
        }
        else
        {
            tmp.Value = value;
        }
    } */
   
    
    public static string getDay(string hari)
    {
        hari = DateTime.Parse(hari).DayOfWeek.ToString();
        if (hari == "Monday")
        {
            hari = "Senin";
        }
        else if (hari == "Tuesday")
        {
            hari = "Selasa";
        }
        else if (hari == "Wednesday")
        {
            hari = "Rabu";
        }
        else if (hari == "Thursday")
        {
            hari = "Kamis";
        }
        else if (hari == "Friday")
        {
            hari = "Jumat";
        }
        else if (hari == "Saturday")
        {
            hari = "Sabtu";
        }
        else if (hari == "Sunday")
        {
            hari = "Minggu";
        }
        return hari;
    }

    public static string getDate(string tanggal)
    {
        string bulan, tgl, tahun;
        tgl = tanggal.Substring(0, 2);
        bulan = tanggal.Substring(3, 2);
        tahun = tanggal.Substring(6, 4);
        if (bulan == "01")
        {
            bulan = "Januari";
        }
        else if (bulan == "02")
        {
            bulan = "Februari";
        }
        else if (bulan == "03")
        {
            bulan = "Maret";
        }
        else if (bulan == "04")
        {
            bulan = "April";
        }
        else if (bulan == "05")
        {
            bulan = "Mei";
        }
        else if (bulan == "06")
        {
            bulan = "Juni";
        }
        else if (bulan == "07")
        {
            bulan = "Juli";
        }
        else if (bulan == "08")
        {
            bulan = "Agustus";
        }
        else if (bulan == "09")
        {
            bulan = "September";
        }
        else if (bulan == "10")
        {
            bulan = "Oktober";
        }
        else if (bulan == "11")
        {
            bulan = "November";
        }
        else if (bulan == "12")
        {
            bulan = "Desember";
        }
        tanggal = tgl + " " + bulan + " " + tahun;
        return tanggal;
    }

    /// <summary>
    /// Untuk mengkonversi nama bulan dalam bentuk bahasa indonesia
    /// </summary>
    /// <param name="bulan">Bulan yang ingin dikonversi</param>
    public static string IndonesiaMonth(string bulan)
    {
        if (bulan == "1")
            return "Januari";

        if (bulan == "2")
            return "Februari";

        if (bulan == "3")
            return "Maret";

        if (bulan == "4")
            return "April";

        if (bulan == "5")
            return "Mei";

        if (bulan == "6")
            return "Juni";

        if (bulan == "7")
            return "Juli";

        if (bulan == "8")
            return "Agustus";

        if (bulan == "9")
            return "September";

        if (bulan == "10")
            return "Oktober";

        if (bulan == "11")
            return "November";
        else
            return "Desember";
    }

    public static string getIndonesianDate(string tanggal)
    {
        string bulan, tgl, tahun;
        tgl = tanggal.Substring(0, 2);
        bulan = tanggal.Substring(3, 2);
        tahun = tanggal.Substring(6, 4);
        if (bulan == "01")
        {
            bulan = "Januari";
        }
        else if (bulan == "02")
        {
            bulan = "Februari";
        }
        else if (bulan == "03")
        {
            bulan = "Maret";
        }
        else if (bulan == "04")
        {
            bulan = "April";
        }
        else if (bulan == "05")
        {
            bulan = "Mei";
        }
        else if (bulan == "06")
        {
            bulan = "Juni";
        }
        else if (bulan == "07")
        {
            bulan = "Juli";
        }
        else if (bulan == "08")
        {
            bulan = "Agustus";
        }
        else if (bulan == "09")
        {
            bulan = "September";
        }
        else if (bulan == "10")
        {
            bulan = "Oktober";
        }
        else if (bulan == "11")
        {
            bulan = "November";
        }
        else if (bulan == "12")
        {
            bulan = "Desember";
        }
        tanggal = tgl + " " + bulan + " " + tahun;
        return tanggal;
    }

    public static string Say(long number)
    {
        string strTerbilang = "";
        string[] angka = { "", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas" };

        if (number < 12)
        {
            strTerbilang = " " + angka[number];
        }
        else if (number < 20)
        {
            strTerbilang = Say(number - 10) + " Belas ";
        }
        else if (number < 100)
        {
            strTerbilang = Say(number / 10) + " Puluh " + Say(number % 10);
        }
        else if (number < 200)
        {
            strTerbilang = " Seratus " + Say(number - 100);
        }
        else if (number < 1000)
        {
            strTerbilang = Say(number / 100) + " Ratus " + Say(number % 100);
        }
        else if (number < 2000)
        {
            strTerbilang = " Seribu " + Say(number - 1000);
        }
        else if (number < 1000000)
        {
            strTerbilang = Say(number / 1000) + " Ribu " + Say(number % 1000);
        }
        else if (number < 1000000000)
        {
            strTerbilang = Say(number / 1000000) + " Juta " + Say(number % 1000000);
        }
        else if (number < 1000000000000)
        {
            strTerbilang = Say(number / 1000000000) + " Milyar " + Say(number % 1000000000);
        }
        // menghilangkan multiple spasi
        strTerbilang = System.Text.RegularExpressions.Regex.Replace(strTerbilang, @"^\s+|\s+$", " ");
        return strTerbilang;
    }

    public static string getServerName()
    {
        return "192.168.100.4";
    }

    /// <summary>
    /// Mengambil database name untuk reporting
    /// </summary>
    public static string getDatabaseName()
    {
        return "easyPJP";
    }


}