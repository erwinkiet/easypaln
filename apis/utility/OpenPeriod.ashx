﻿<%@ WebHandler Language="C#" Class="OpenPeriodHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class OpenPeriodDTO
{
    public int PostingId { get; set; }
    public string Year { get; set; }
    public string Month { get; set; }
    public string Module { get; set; }
    public string Status { get; set; }
}

class OpenPeriodHandler : BaseHandler<OpenPeriodDTO>
{
    protected override void Open(OpenPeriodDTO input, OpenPeriodDTO oldInput, EasyEntities ctx)
    {
        base.Open(input, oldInput, ctx);

        var p = (from a in ctx.Posting where a.PostingID == input.PostingId select a).FirstOrDefault();
        p.IsOpen = 1;
        
        Helper.InsertLog("General-Utility-OpenPeriod-Open", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Close(OpenPeriodDTO input, OpenPeriodDTO oldInput, EasyEntities ctx)
    {
        base.Close(input, oldInput, ctx);

        var p = (from a in ctx.Posting where a.PostingID == input.PostingId select a).FirstOrDefault();
        p.IsOpen = 0;

        Helper.InsertLog("General-Utility-OpenPeriod-Close", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                SELECT a.* FROM (
                    SELECT 
	                    PostingID, 
	                    LEFT(periode,4) as [Year], 
	                    DATENAME(month, '2015-'+ RIGHT(periode,2) +'-21') as [Month], 
	                    Module,
	                    Case when IsOpen=0 then 'Closed' else 'Open' end as Status 
                    FROM posting
                ) a
                WHERE " + Helper.FilterQuery(option, "a.Status", "a.PostingID",
                    "a.[Year]", "a.[Month]", "a.Module"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(BaseHandler<OpenPeriodDTO>.ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, OpenPeriodDTO input, OpenPeriodDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        
    }
}