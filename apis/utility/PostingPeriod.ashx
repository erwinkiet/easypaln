﻿<%@ WebHandler Language="C#" Class="PostingPeriodHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class PostingPeriodDTO
{
    public string DepartmentId { get; set; }
    public string Month { get; set; }
    public string Year { get; set; }
    public string SubAccountId { get; set; }
}

class PostingPeriodHandler : BaseHandler<PostingPeriodDTO>
{
    protected override void Update(PostingPeriodDTO input, PostingPeriodDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        string bulan = input.Month.PadLeft(2, '0');
        string tahun = input.Year;
        string period = tahun + "-" + bulan;
        
        ctx.Posting.AddObject(new Posting()
        {
            PostingID = Convert.ToByte(4),
            Periode = period,
            Module = user.DepartmentID,
            UserID = user.UserID
        });

        //reset accounting saldo
        //if (user.DepartmentID == "ACC")
        //{
        //    string subAccountID = input.SubAccountId;
        //    string type = "R";
        //    var acc = from a in ctx.SubAccount where a.Type == type select a;
        //    foreach (var acc1 in acc)
        //    {
        //        acc1.Debits = 0;
        //        acc1.Credits = 0;
        //    }

        //    acc = from a in ctx.SubAccount where a.Type == type && a.HasChild == "0" select a;
        //    foreach (var acc1 in acc)
        //    {
        //        //    ctx.ACCPosting.AddObject(new ACCPosting
        //        //    {
        //        //        SubAccountID = acc1.SubAccountID,
        //        //        Debits = acc1.Credits,
        //        //        Credits = acc1.Debits,
        //        //        Month = Convert.ToByte(bulan),
        //        //        Year = Convert.ToInt32(tahun)
        //        //    });
        //        int tahuni = Convert.ToInt32(tahun);
        //        int bulani = Convert.ToInt32(bulan);
        //        DateTime date = new DateTime(tahuni, bulani, DateTime.DaysInMonth(tahuni, bulani));
        //        var jdts = from a in ctx.JournalDetailTransaction
        //                   join b in ctx.JournalTransaction on a.TransactionID equals b.TransactionID
        //                   where b.Date > date && a.SubAccountID == acc1.SubAccountID
        //                   select a;

        //        if (jdts.Count() <= 0)
        //        {
        //            continue;
        //        }

        //        decimal credits = jdts.Sum(x => x.Credits);
        //        decimal debits = jdts.Sum(x => x.Debits);
        //        decimal tot = credits - debits;

        //        if (tot > 0)
        //        {
        //            acc1.Credits = tot;
        //        }
        //        else if (tot < 0)
        //        {
        //            acc1.Debits = -tot;
        //        }

        //        string csa = acc1.ParentAccountID;
        //        for (int i = 0; i < acc1.ChildNo - 1; i++)
        //        {
        //            SubAccount sat = (from a in ctx.SubAccount where a.SubAccountID == csa select a).FirstOrDefault();

        //            decimal tmp = tot + sat.Credits - sat.Debits;

        //            if (tmp > 0)
        //            {
        //                sat.Credits = tmp;
        //                sat.Debits = 0;
        //            }
        //            else if (tmp < 0)
        //            {
        //                sat.Credits = 0;
        //                sat.Debits = -tmp;
        //            }
        //            else if (tmp == 0)
        //            {
        //                sat.Credits = 0;
        //                sat.Debits = 0;
        //            }

        //            csa = sat.ParentAccountID;
        //        }
        //    }

        //    UpdateAccount(ctx, subAccountID, bulan, tahun);
        //}
        
        Helper.InsertLog("General-Utility-PostingPeriod-Update", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

//    protected override void GetListData(string list, string id)
//    {
//        base.GetListData(list, id);

//        using (SqlConnection con = new SqlConnection(conString))
//        {
//            DataTable dt = null;

//            if (list == "account")
//            {
//                dt = Helper.NewDataTable(@"
//                    SELECT SubAccountID as AccountID,Name as AccountName,ROW_NUMBER() OVER (ORDER BY SubAccountID) 
//                    as row FROM SubAccount WHERE SubAccountID='" + Helper.EscapeSQLString(id) + "'", con);
//            }

//            http.Response.Write(Helper.DataTabletoJSON(dt));
//        }
//    }

//    protected override void GetList(string list, PostingPeriodDTO input, ListOption option, int detailIndex)
//    {
//        base.GetList(list, input, option, detailIndex);

//        using (SqlConnection con = new SqlConnection(conString))
//        {
//            DataTable dt = null;

//            if (list == "account")
//            {
//                dt = Helper.NewDataTable(Helper.PagingQuery(@"
//                    SELECT SubAccountID as AccountID,Name as AccountName FROM SubAccount WHERE Type='N' and " + Helper.FieldFilterQuery(option, "SubAccountID", "Name"), option), con);
//            }

//            http.Response.Write(Helper.DataTabletoJSON(dt));
//        }
//    }

    protected override void GetDefaultData()
    {   
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select '" + user.DepartmentID + "' as DepartmentID", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(BaseHandler<PostingPeriodDTO>.ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, PostingPeriodDTO input, PostingPeriodDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        int allowcheck = 1;
        string bulan = input.Month.PadLeft(2, '0');
        string tahun = input.Year;
        string period = tahun + "-" + bulan;
        Debug.WriteLine(tahun);
        Debug.WriteLine("");
        string departmentid = user.DepartmentID;
        string periodelalu;
        if (input.Month == "")
        {
            Helper.AddError(err, "type", "Month must be selected!");
            allowcheck = 0;
        }

        if (input.Year == "")
        {
            Helper.AddError(err, "type", "Year must be selected!");
            allowcheck = 0;
        }

        if (allowcheck == 1)
        {
            //SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Posting WHERE Periode='" + period + "' AND Module='" + Session["Department"].ToString() + "'", con);
            //DataTable dt = new DataTable();
            //da.Fill(dt);

            var dt = from a in ctx.Posting where a.Periode == period && a.Module == departmentid select a;

            if (dt.Count() > 0)
            {
                Helper.AddError(err, "period", "This Period already Posted!");
            }
            else
            {

                if (bulan == "01")
                {
                    periodelalu = Convert.ToString(Convert.ToDecimal(tahun) - 1) + "-12";
                }
                else
                {
                    periodelalu = tahun + "-" + Convert.ToString(Convert.ToDecimal(bulan) - 1).PadLeft(2, '0');
                }

                var dt1 = from a in ctx.Posting where a.Periode == periodelalu && a.Module == departmentid select a;

                if (dt1.Count() == 0)
                {
                    var dtlast = from a in ctx.Posting where a.Module == departmentid orderby a.Periode descending select new { a.Periode };
                    var dtlastfirst = dtlast.FirstOrDefault();
                    if (dtlast.Count() > 0)
                    {
                        Helper.AddError(err, "Period", "Previous Period has not Posted yet! Please Post it first! Last Posting found is : " + dtlastfirst.Periode);
                    }
                    //else
                    //{
                    //    Helper.AddError(err, "Period", "Previous Period has not Posted yet! Please Post it first!");
                    //}
                }

                //if (user.DepartmentID.ToUpper() == "PUR")
                //{
                //    string bulan1 = period.ToString().Substring(5, 2);
                //    string tahun1 = period.ToString().Substring(0, 4);
                //    int bulanlalu = Convert.ToInt32(periodelalu.ToString().Substring(5, 2));
                //    int tahunlalu = Convert.ToInt32(periodelalu.ToString().Substring(0, 4));

                //    // Purchasing

                //    string typepo = "Destination Point";
                //    string status = "Open";
                //    var dt2 = from a in ctx.Receive where a.POType == typepo && a.Date.Month == bulanlalu && a.Date.Year == tahunlalu && a.Status == status select a;

                //    if (dt2.Count() > 0)
                //    {
                //        Helper.AddError(err, "Purchase", "Can not Posting Period : " + period + " because there are product receive not made into purchase invoice yet!");
                //    }
                //}

                //if (user.DepartmentID.ToUpper() == "ACC")
                //{
                //    if (input.SubAccountId == "")
                //    {
                //        Helper.AddError(err, "Account", "Account ID for transfer profit and loss balance must be filled!");
                //    }
                //    else
                //    {
                //        SubAccount sa = (from a in ctx.SubAccount where a.SubAccountID == input.SubAccountId select a).FirstOrDefault();

                //        if (sa == null)
                //        {
                //            Helper.AddError(err, "subAccountId", "Account does not exist!");
                //        }
                //        else if (sa.HasChild != "0")
                //        {
                //            Helper.AddError(err, "subAccountId", "Account must be the lowest child!");
                //        }
                //        else if (sa.Status == "Closed")
                //        {
                //            Helper.AddError(err, "subAccountId", "Account is already closed!");
                //        }
                //        else if (sa.Type != "N")
                //        {
                //            Helper.AddError(err, "subAccountId", "Account must be of type Neraca");
                //        }
                //    }
                //}
            }
        }
    }

//    void UpdateAccount(EasyEntities ctx, string subAccountID, string bulan, string tahun)
//    {
//        using (SqlConnection con = new SqlConnection(conString))
//        {
//            SqlDataAdapter da = new SqlDataAdapter();
//            SqlDataAdapter da2 = new SqlDataAdapter();

//            DataTable dtBalancingSheet = new DataTable();

//            da = new SqlDataAdapter(@"SELECT DISTINCT SubAccount.SubAccountID, SubAccount.TopParentAccountID, SubAccount.ChildNo, SubAccount.HasChild, SubAccount.Name, SubAccount.TopParentAccountID, 'N' as Type, 0 as OB, case when TotalJournal.TotalDebits is null then 0 else TotalJournal.TotalDebits end as TotalDebits, 
//                    case when TotalJournal.TotalCredits is null then 0 else TotalJournal.TotalCredits end as TotalCredits, 
//                    case when Journal.TotalDebits is null then 0 else Journal.TotalDebits end as Debits,
//                    case when Journal.TotalCredits is null then 0 else Journal.TotalCredits end as Credits, 
//                    SubAccount.Debits as EndDebits, SubAccount.Credits as EndCredits, case when JournalAdjust.TotalDebits is null then 0 else JournalAdjust.TotalDebits end as AdjustDebits,
//                                    case when JournalAdjust.TotalCredits is null then 0 else JournalAdjust.TotalCredits end as AdjustCredits FROM SubAccount SubAccount LEFT OUTER JOIN 
//                    (SELECT b.SubAccountID, SUM(b.Debits) as TotalDebits, SUM(b.Credits) as TotalCredits FROM JournalTransaction a, JournalDetailTransaction b WHERE a.TransactionID=b.TransactionID AND a.Status<>'Cancel' AND MONTH(a.Date)>='" + bulan + @"' and YEAR(a.Date)>='" + tahun + @"'
//                    GROUP BY b.SubAccountID) as TotalJournal on TotalJournal.SubAccountID=SubAccount.SubAccountID LEFT OUTER JOIN 
//                    (SELECT b.SubAccountID, SUM(b.Debits) as TotalDebits, SUM(b.Credits) as TotalCredits FROM JournalTransaction a, JournalDetailTransaction b, JournalMaster c WHERE a.TransactionID=b.TransactionID and a.JournalID=c.JournalID AND c.[Double]='N' AND a.Status<>'Cancel' AND MONTH(a.Date)='" + bulan + @"' and YEAR(a.Date)='" + tahun + @"' 
//                    GROUP BY b.SubAccountID) as Journal on Journal.SubAccountID=SubAccount.SubAccountID LEFT OUTER JOIN 
//                    (SELECT b.SubAccountID, SUM(b.Debits) as TotalDebits, SUM(b.Credits) as TotalCredits FROM JournalTransaction a, JournalDetailTransaction b, JournalMaster c WHERE a.TransactionID=b.TransactionID and a.JournalID=c.JournalID AND c.[Double]='Y' AND a.Status<>'Cancel' AND MONTH(a.Date)='" + bulan + @"' and YEAR(a.Date)='" + tahun + @"' 
//                    GROUP BY b.SubAccountID) as JournalAdjust on JournalAdjust.SubAccountID=SubAccount.SubAccountID WHERE SubAccount.Type='N'", con);

//            da2 = new SqlDataAdapter(@"SELECT DISTINCT SubAccount.SubAccountID, SubAccount.TopParentAccountID, SubAccount.ChildNo, SubAccount.HasChild, SubAccount.Name, SubAccount.TopParentAccountID, 'R' as Type, 0 as OB, 0 as TotalDebits, 
//                    0 as TotalCredits, case when Journal.TotalDebits is null then 0 else Journal.TotalDebits end as Debits,
//                    case when Journal.TotalCredits is null then 0 else Journal.TotalCredits end as Credits, 
//                    SubAccount.Debits as EndDebits, SubAccount.Credits as EndCredits, case when JournalAdjust.TotalDebits is null then 0 else JournalAdjust.TotalDebits end as AdjustDebits,
//                    case when JournalAdjust.TotalCredits is null then 0 else JournalAdjust.TotalCredits end as AdjustCredits FROM SubAccount SubAccount LEFT OUTER JOIN 
//                    (SELECT b.SubAccountID, SUM(b.Debits) as TotalDebits, SUM(b.Credits) as TotalCredits FROM JournalTransaction a, JournalDetailTransaction b, JournalMaster c WHERE a.TransactionID=b.TransactionID and a.JournalID=c.JournalID AND c.[Double]='N' AND a.Status<>'Cancel' AND MONTH(a.Date)='" + bulan + @"' and YEAR(a.Date)='" + tahun + @"'
//                    GROUP BY b.SubAccountID) as Journal on Journal.SubAccountID=SubAccount.SubAccountID LEFT OUTER JOIN 
//                    (SELECT b.SubAccountID, SUM(b.Debits) as TotalDebits, SUM(b.Credits) as TotalCredits FROM JournalTransaction a, JournalDetailTransaction b, JournalMaster c WHERE a.TransactionID=b.TransactionID and a.JournalID=c.JournalID AND c.[Double]='Y' AND a.Status<>'Cancel' AND MONTH(a.Date)='" + bulan + @"' and YEAR(a.Date)='" + tahun + @"'
//                    GROUP BY b.SubAccountID) as JournalAdjust on JournalAdjust.SubAccountID=SubAccount.SubAccountID
//                        WHERE SubAccount.Type='R'", con);

//            DataTable dtTemp = new DataTable();
//            dtTemp.Clear();
//            dtBalancingSheet.Clear();
//            da2.Fill(dtTemp);
//            da.Fill(dtBalancingSheet);

//            for (int i = 0; i < dtTemp.Rows.Count; i++)
//            {
//                dtBalancingSheet.ImportRow(dtTemp.Rows[i]);
//            }

//            decimal openBalance;
//            decimal endNeraca;
//            decimal neracaDebits;
//            decimal neracaCredits;
//            decimal endBalance;
//            decimal endDebits;
//            decimal endCredits;
//            decimal rDebits;
//            decimal rCredits;
//            decimal nettoDebits;
//            decimal nettoCredits;
//            //decimal nettoNeracaDebits;
//            decimal nettoNeracaCredits;
//            decimal sumRDebits = 0;
//            decimal sumRCredits = 0;

//            foreach (DataRow dr in dtBalancingSheet.Rows)
//            {
//                decimal bsDebits = Convert.ToDecimal(dr["Debits"]);
//                decimal bsCredits = Convert.ToDecimal(dr["Credits"]);
//                decimal bsEndDebits = Convert.ToDecimal(dr["EndDebits"]);
//                decimal bsEndCredits = Convert.ToDecimal(dr["EndCredits"]);
//                decimal bsTotalDebits = Convert.ToDecimal(dr["TotalDebits"]);
//                decimal bsTotalCredits = Convert.ToDecimal(dr["TotalCredits"]);
//                decimal bsAdjustDebits = Convert.ToDecimal(dr["AdjustDebits"]);
//                decimal bsAdjustCredits = Convert.ToDecimal(dr["AdjustCredits"]);
//                int bsHasChild = Convert.ToInt32(dr["HasChild"]);
//                string bsType = dr["Type"].ToString();

//                if (bsType == "N")
//                {
//                    openBalance = bsEndDebits - bsEndCredits + bsTotalCredits - bsTotalDebits;
//                }
//                else if (bsHasChild == 0)
//                {
//                    openBalance = bsTotalDebits - bsTotalCredits;
//                }
//                else
//                {
//                    openBalance = bsEndDebits - bsEndCredits + bsTotalCredits - bsTotalDebits;
//                }

//                endNeraca = openBalance + bsDebits - bsCredits;
//                neracaDebits = endNeraca > 0 ? endNeraca : 0;
//                neracaCredits = endNeraca < 0 ? -endNeraca : 0;
//                endBalance = endNeraca + bsAdjustDebits - bsAdjustCredits;
//                endDebits = endBalance > 0 ? endBalance : 0;
//                endCredits = endBalance < 0 ? -endBalance : 0;
//                rDebits = bsType == "R" ? endDebits : 0;
//                rCredits = bsType == "R" ? endCredits : 0;

//                if (bsHasChild == 0)
//                {
//                    sumRDebits += rDebits;
//                    sumRCredits += rCredits;
//                }
//            }

//            nettoDebits = sumRDebits - sumRCredits < 0 ? sumRCredits - sumRDebits : 0;
//            nettoCredits = sumRDebits - sumRCredits > 0 ? sumRDebits - sumRCredits : 0;
//            //nettoNeracaDebits = nettoCredits > 0 ? nettoCredits : 0;
//            //nettoNeracaCredits = nettoDebits > 0 ? nettoDebits : 0;
//            nettoNeracaCredits = nettoDebits > 0 ? nettoDebits : (nettoCredits > 0 ? -nettoCredits : 0);

//            SubAccount sa = (from a in ctx.SubAccount where a.SubAccountID == subAccountID select a).FirstOrDefault();
//            //decimal tmp = sa.Debits + nettoNeracaDebits - sa.Credits - nettoNeracaCredits;
//            //sa.Debits = tmp > 0 ? tmp : 0;
//            //sa.Credits = tmp < 0 ? -tmp : 0;
//            sa.Debits = sa.Debits > 0 ? sa.Debits - nettoNeracaCredits : 0;
//            sa.Credits = sa.Credits > 0 ? sa.Credits + nettoNeracaCredits : 0;

//            if (sa.Debits == 0 && sa.Credits == 0)
//            {
//                if (nettoNeracaCredits > 0)
//                {
//                    sa.Credits = nettoNeracaCredits;
//                }
//                else
//                {
//                    sa.Debits = -nettoNeracaCredits;
//                }
//            }

//            string csa = sa.ParentAccountID;

//            ctx.ACCPosting.AddObject(new ACCPosting
//            {
//                SubAccountID = sa.SubAccountID,
//                Debits = nettoNeracaCredits < 0 ? -nettoNeracaCredits : 0,
//                Credits = nettoNeracaCredits > 0 ? nettoNeracaCredits : 0,
//                Month = Convert.ToByte(bulan),
//                Year = Convert.ToInt32(tahun)
//            });

//            for (int i = 0; i < sa.ChildNo - 1; i++)
//            {
//                SubAccount sat = (from a in ctx.SubAccount where a.SubAccountID == csa select a).FirstOrDefault();
//                //decimal tmpt = sat.Debits + nettoNeracaDebits - sat.Credits - nettoNeracaCredits;
//                //sat.Debits = tmpt > 0 ? tmpt : 0;
//                //sat.Credits = tmpt < 0 ? -tmpt : 0;
//                sat.Debits = sat.Debits > 0 ? sat.Debits - nettoNeracaCredits : 0;
//                sat.Credits = sat.Credits > 0 ? sat.Credits + nettoNeracaCredits : 0;
//                if (sat.Debits == 0 && sat.Credits == 0)
//                {
//                    if (nettoNeracaCredits > 0)
//                    {
//                        sat.Credits = nettoNeracaCredits;
//                    }
//                    else
//                    {
//                        sat.Debits = -nettoNeracaCredits;
//                    }
//                }

//                csa = sat.ParentAccountID;

//                ctx.ACCPosting.AddObject(new ACCPosting
//                {
//                    SubAccountID = sat.SubAccountID,
//                    Debits = nettoNeracaCredits < 0 ? -nettoNeracaCredits : 0,
//                    Credits = nettoNeracaCredits > 0 ? nettoNeracaCredits : 0,
//                    Month = Convert.ToByte(bulan),
//                    Year = Convert.ToInt32(tahun)
//                });
//            }
//        }
//    }
}