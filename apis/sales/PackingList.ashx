﻿<%@ WebHandler Language="C#" Class="PackingListHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class PackingListDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public string Origin { get; set; }
    public string Destination { get; set; }
    public string Remark { get; set; }
    public string Status { get; set; }
    public byte[] RowVersion { get; set; }
    public List<ItemDTO> Items { get; set; } 
}

class ItemDTO
{
    public int DetailId { get; set; }
    public int TravelDocumentId { get; set; }
    public string TravelDocumentNo { get; set; }
    public int PackingListId { get; set; }
}

class PackingListHandler : BaseHandler<PackingListDTO>
{
    protected override void Insert(PackingListDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string status = "Open";

        ctx.PackingLists.AddObject(new PackingLists()
        {
            TransactionNo = input.TransactionNo,
            TransactionDate = DateTime.Parse(input.TransactionDate),
            CustomerId = input.CustomerId,
            Remark = input.Remark,
            CreatedBy = user.UserID,
            Status = status,
            InputDate = DateTime.Now
        });

        for (int i = 0; i < input.Items.Count; i++)
        {
            int PackingListId = Helper.GetNextID("PackingLists");
            ctx.PackingListDetails.AddObject(new PackingListDetails()
            {
                PackingListId = PackingListId,
                TravelDocumentId = input.Items[i].TravelDocumentId,
            });
        }

        UpdateStatusTravelDocument(input, null, "insert", ctx);


        Helper.InsertLog("SALES-Transaction-PackingLists-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();

    }
    
    protected override void Update(PackingListDTO input, PackingListDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.PackingLists", "Id", oldInput.Id);

        PackingLists pl = (PackingLists)ctx.GetObjectByKey(key);
        var plds = from a in ctx.PackingListDetails where a.PackingListId == oldInput.Id orderby a.Id descending select a;

        int transactionid = oldInput.Id;
        string oldCustomerId = oldInput.CustomerId;
        string Remark = oldInput.Remark;
        string status = "Open";

        CustomerDetail cst = (from a in ctx.CustomerDetail where a.CustomerID == oldCustomerId select a).FirstOrDefault();

        if (pl != null && plds != null)
        {
            pl.TransactionNo = input.TransactionNo;
            pl.TransactionDate = DateTime.Parse(input.TransactionDate);
            pl.CustomerId = input.CustomerId;
            pl.Remark = input.Remark;
            pl.ModifiedBy = user.UserID;
            pl.Status = status;
            pl.ModifiedDate = DateTime.Now;

            for (int i = 0; i < oldInput.Items.Count(); i++)
            {
                int detailid = Convert.ToInt32(oldInput.Items[i].DetailId);
                var pldetails = (from a in ctx.PackingListDetails where a.PackingListId == transactionid & a.Id == detailid orderby a.Id select a).FirstOrDefault();
                int pldetailid = Convert.ToInt32(oldInput.Items[i].DetailId);

                int ada = 0;
                for (int j = 0; j < input.Items.Count(); j++)
                {
                    if (pldetailid.ToString().Trim() == input.Items[j].DetailId.ToString().Trim())
                    {
                        ada = 1;
                        break;
                    }
                }
                if (ada == 0)
                {

                    ctx.DeleteObject(pldetails);
                }
            }
            
            
            
            for (int i = 0; i < input.Items.Count; i++)
            {
                int dID = input.Items[i].DetailId;
                PackingListDetails pld = (from a in ctx.PackingListDetails where a.PackingListId == oldInput.Id && a.Id == dID select a).FirstOrDefault();

                if (dID == 0)
                {
                    ctx.PackingListDetails.AddObject(new PackingListDetails()
                    {
                        PackingListId = oldInput.Id,
                        TravelDocumentId = input.Items[i].TravelDocumentId,
                    });
                }
                else
                {
                        pld.PackingListId = input.Items[i].PackingListId;
                        pld.TravelDocumentId = input.Items[i].TravelDocumentId;
                }
            }
        }


        UpdateStatusTravelDocument(input, oldInput, "update", ctx);

        Helper.InsertLog("SALES-Transaction-PackingLists-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SALES-Transaction-PackingLists-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }
    
    protected override void Cancel(PackingListDTO input, PackingListDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.PackingLists", "Id", oldInput.Id);

        PackingLists pl = (PackingLists)ctx.GetObjectByKey(key);

        pl.Status = "Cancel";

        
        UpdateStatusTravelDocument(input, oldInput, "cancel", ctx);

        Helper.InsertLog("SALES-Transaction-PackingLists-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"select pl.Id, pl.CustomerId, cus.Name as CustomerName, 
                                                 pl.InputDate, pl.Remark, pl.RowVersion, pl.Status, pl.ModifiedBy as mb,
                                                 CONVERT(varchar(10), pl.TransactionDate, 120) as TransactionDate, 
                                                 pl.TransactionNo, u.Name as CreatedBy into #temptable
                                                 from PackingLists pl
                                                 inner join 
                                                 [User] u on pl.CreatedBy = u.UserID
                                                 inner join
                                                 Customer cus on cus.CustomerID = pl.CustomerId
                                                 " + Helper.PagingQuery (@"SELECT data.* FROM (select pl.*, us.Name as ModifiedBy,
                                                 ROW_NUMBER() OVER (ORDER BY TransactionDate DESC, Id DESC) as row
                                                 from #temptable pl left outer join
                                                 [User] us on pl.mb = us.UserID 

                                                 WHERE " + Helper.FilterQuery(option, "pl.Status", "TransactionNo", "CustomerId", "CustomerName", "CONVERT(VARCHAR,TransactionDate,120)") + @") data 
                                                                                        ", option) + @"

                                                 
                                                 drop table #temptable", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (detail == "items")
            {
                DataTable dt = Helper.NewDataTable(@"select pld.Id as detailId, 
                                                     td.Id as travelDocumentId, pld.PackingListId, pl.TransactionNo, 
                                                     td.TransactionNo as travelDocumentNo, 
                                                     CONVERT(varchar(10), td.TransactionDate, 120) as travelDocumentDate, 
                                                     v.vehicleNo, td.destinationNetto, td.OriginNetto as netto
                                                     from PackingLists pl, PackingListDetails pld, TravelDocuments td inner join 
                                                     Vehicle v on v.Id = td.VehicleId where 
                                                     pl.Id = pld.PackingListId  and td.Id=pld.TravelDocumentId
                                                     and pld.PackingListId ='" + Helper.EscapeSQLString(id) + @"'", con);

                http.Response.Write(Helper.DataTabletoJSON(dt));
            }
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT CustomerId, Name as CustomerName  
	                    FROM Customer 
                        WHERE CustomerId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, PackingListDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            string customerId = input.CustomerId;
            string origin = input.Origin;
            
            if (list == "customer")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
                    FROM Customer  
                    WHERE " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
            }

            if (list == "origin")
            {

                dt = Helper.NewDataTable(Helper.PagingQuery(@"select td.Id, td.TransactionNo, CONVERT(varchar(10), td.TransactionDate,120) 
                as Date,te.Origin,te.Destination, td.Status, ROW_NUMBER() OVER (ORDER BY td.Id) as row 
                from TravelDocuments  td inner join TravelExpenses te on te.Id = td.TravelExpenseId
                WHERE td.Status<>'Cancel' AND td.Status<>'Closed' AND td.CustomerId = '" + customerId + @"' AND " + Helper.FieldFilterQuery(option, "Origin", "Destination"), option), con);
            }

            if (list == "traveldocument")
            {
                
                dt = Helper.NewDataTable(Helper.PagingQuery(@"select td.Id, td.TransactionNo, CONVERT(varchar(10), td.TransactionDate,120) 
                as Date,td.Remark ,te.Origin,te.Destination,td.VehicleId, td.ShipName,v.VehicleNo, td.DestinationNetto,
                td.OriginNetto AS Netto, td.Status, td.DONo, ROW_NUMBER() OVER (ORDER BY td.Id) as row 
                from TravelDocuments  td LEFT OUTER JOIN TravelExpenses te on te.Id = td.TravelExpenseId inner join Vehicle v on v.Id = td.VehicleId
                WHERE td.Status<>'Cancel' AND td.Status<>'Closed' AND td.CustomerId = '" + customerId + @"' AND " 
                + Helper.FieldFilterQuery(option, "td.Id", "td.TransactionNo", "CONVERT(varchar,td.TransactionDate,103)", "v.VehicleNo", "td.Remark",
                "td.OriginNetto", "te.Origin", "te.Destination", "td.DestinationNetto", "td.Status","td.ShipName"), option), con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, PackingListDTO input, PackingListDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.PackingLists where a.Id == oldInput.Id select a).FirstOrDefault();

        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }

    protected override void Validate(BaseHandler<PackingListDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, PackingListDTO input, PackingListDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        int Id = input.Id;
        string transactionNo = input.TransactionNo;
        string date = input.TransactionDate;
        string customerID = input.CustomerId;

        if (mode == ValidateMode.Insert)
        {
            var travelNo = (from a in ctx.TravelDocuments where a.TransactionNo == transactionNo select a).FirstOrDefault();

            if (travelNo != null)
            {
                Helper.AddError(err, "transactionNo", "Transaction No. " + transactionNo + " has been registered!");
            }
        }
        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (transactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. must not be empty!");
            }

            var packinglistNo = (from a in ctx.PackingLists where a.TransactionNo == transactionNo select a).FirstOrDefault();

            if (customerID == "")
            {
                Helper.AddError(err, "customerID", "Customer ID must not be empty!");
            }
        }


        if (input.Items.Count == 0)
        {
            Helper.AddError(err, "detail", "Transaction detail must not be empty!");
        }

        for (int i = 0; i < input.Items.Count; i++)
        {
            string transactionDetail = input.Items[i].TravelDocumentNo;
            int transactionDetailId = input.Items[i].DetailId;
            if (transactionDetail == "")
            {
                Helper.AddError(err, "detail", "Transaction No. detail on row : " + (i + 1) + " must not be empty!");
            }
            else
            {
                if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
                {
                   

                    var getDetail = from a in input.Items where a.TravelDocumentId == transactionDetailId select a;

                    if (getDetail.Count() > 1)
                    {
                        Helper.AddError(err, "detail" + i, "Travel Document Id no " + transactionDetail + " is more than one!");
                    }
                }
            }
        }
    }

    private void UpdateStatusTravelDocument(PackingListDTO input, PackingListDTO oldInput, string action, EasyEntities ctx)
    {
        if (action != "insert")
        {
            for (int i = 0; i < oldInput.Items.Count(); i++)
            {
                int transactionDetailId = oldInput.Items[i].TravelDocumentId;
                var travelDocument = (from a in ctx.TravelDocuments where a.Id == transactionDetailId select a).FirstOrDefault();
                travelDocument.Status = "Open";
            }
        }

        if (action != "cancel")
        {
            for (int i = 0; i < input.Items.Count(); i++)
            {
                int transactionDetailId = input.Items[i].TravelDocumentId;
                var travelDocument = (from a in ctx.TravelDocuments where a.Id == transactionDetailId select a).FirstOrDefault();
                travelDocument.Status = "Closed";
            }
        }
    }
}