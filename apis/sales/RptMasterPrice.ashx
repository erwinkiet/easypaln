﻿<%@ WebHandler Language="C#" Class="RptMasterPriceHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class RptMasterPriceDTO
{ }


class RptMasterPriceHandler : BaseHandler<RptMasterPriceDTO>
{
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as fromDate, convert(varchar(10),GETDATE(),120) as toDate", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
}