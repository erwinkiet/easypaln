﻿<%@ WebHandler Language="C#" Class="RptInvoiceDetailHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class RptInvoiceDetailDTO
{ }

class RptInvoiceDetailHandler : BaseHandler<RptInvoiceDetailDTO>
{
    protected override void GetList(string list, RptInvoiceDetailDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "driver")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"
                    SELECT Id as DriverId, Name as DriverName,
                                       ROW_NUMBER() OVER (ORDER BY Id) as row from Driver 
                    WHERE " + Helper.FieldFilterQuery(option, "Id", "Name"), option), con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as fromDate, convert(varchar(10),GETDATE(),120) as toDate", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
}