﻿<%@ WebHandler Language="C#" Class="InvoiceHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class InvoiceDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string LoadDate { get; set; }
    public string UnLoadDate { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public int FreightPriceDetailId { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public int PackingListId { get; set; }
    public decimal Price { get; set; }
    public decimal EtcPrice { get; set; }
    public int IsPpn { get; set; }
    public int BaseNumber { get; set; }
    public string Destination { get; set; }
    public string Remark { get; set; }
    public string RemarkEtc { get; set; }
    public string Status { get; set; }
    public decimal Dpp { get; set; }
    public decimal Pph { get; set; }
    public decimal GrandTotal { get; set; }
    public byte[] RowVersion { get; set; }
    public List<InvoiceItemDTO> Items { get; set; } 
}

class InvoiceItemDTO
{
    public int DetailId { get; set; }
    public int TravelDocumentId { get; set; }
    public string TravelDocumentNo { get; set; }
    public string TravelDocumentDate { get; set; }
    public string VehicleNo { get; set; }
    public decimal Netto { get; set; }
}

class InvoiceHandler : BaseHandler<InvoiceDTO>
{
    protected override void Insert(InvoiceDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string status = "Open";

        ctx.Invoices.AddObject(new Invoices()
        {
            TransactionNo = input.TransactionNo,
            LoadDate = DateTime.Parse(input.LoadDate),
            UnloadDate = DateTime.Parse(input.UnLoadDate),
            CustomerId = input.CustomerId,
            FreightPriceDetailId = input.FreightPriceDetailId,
            PackingListId = input.PackingListId,
            Price = input.Price,
            EtcPrice = input.EtcPrice,
            Remark = input.Remark,
            RemarkEtc = input.RemarkEtc,
            Total = input.Dpp,
            GrandTotal = input.GrandTotal,
            Pph = input.Pph,
            IsPpn = input.IsPpn,
            BaseNumber = input.BaseNumber,
            CreatedBy = user.UserID,
            Status = status,
            InputDate = DateTime.Now
        });

        for (int i = 0; i < input.Items.Count; i++)
        {
            int TravelDocumentId = input.Items[i].TravelDocumentId;
            ctx.InvoiceDetails.AddObject(new InvoiceDetails()
            {
                InvoiceId = Helper.GetNextID("Invoices"),
                TravelDocumentId = TravelDocumentId,
            });
        }

        UpdateStatusPackingList(input, null, "insert", ctx);

        string customerid = input.CustomerId;
        CustomerDetail cst = (from a in ctx.CustomerDetail where a.CustomerID == customerid select a).FirstOrDefault();

        if (cst != null)
        {
            cst.Amount += input.GrandTotal;
        }

        Helper.InsertLog("SALES-Transaction-Invoices-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(InvoiceDTO input, InvoiceDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.Invoices", "Id", oldInput.Id);

        Invoices inv = (Invoices)ctx.GetObjectByKey(key);
        var invds = from a in ctx.InvoiceDetails where a.InvoiceId == oldInput.Id orderby a.Id descending select a;

        string oldCustomerId = oldInput.CustomerId;
        int FreightPriceDetailId = oldInput.FreightPriceDetailId;
        decimal Price = oldInput.Price;
        string Remark = oldInput.Remark;
        string RemarkNote = oldInput.RemarkEtc;
        decimal EtcPrice = oldInput.EtcPrice;
        decimal Pph = oldInput.Pph;
        int IsPpn = oldInput.IsPpn;
        int BaseNumber = oldInput.BaseNumber;
        string status = "Open";

        CustomerDetail cst = (from a in ctx.CustomerDetail where a.CustomerID == oldCustomerId select a).FirstOrDefault();

        if (cst != null)
        {
            cst.Amount -= oldInput.GrandTotal;
        }

        if (inv != null && invds != null)
        {
            inv.TransactionNo = input.TransactionNo;
            inv.LoadDate = DateTime.Parse(input.LoadDate);
            inv.UnloadDate = DateTime.Parse(input.UnLoadDate);
            inv.CustomerId = input.CustomerId;
            inv.FreightPriceDetailId = input.FreightPriceDetailId;
            inv.PackingListId = input.PackingListId;
            inv.Price = input.Price;
            inv.Remark = input.Remark;
            inv.EtcPrice = input.EtcPrice;
            inv.RemarkEtc = input.RemarkEtc;
            inv.Total = input.Dpp;
            inv.GrandTotal = input.GrandTotal;
            inv.Pph = input.Pph;
            inv.IsPpn = input.IsPpn;
            inv.BaseNumber = input.BaseNumber;
            inv.ModifiedBy = user.UserID;
            inv.Status = status;
            inv.ModifiedDate = DateTime.Now;

            for (int i = 0; i < input.Items.Count; i++)
            {
                int dID = input.Items[i].DetailId;

                if (dID == 0)
                {
                    ctx.InvoiceDetails.AddObject(new InvoiceDetails()
                    {
                        InvoiceId = oldInput.Id,
                        TravelDocumentId = input.Items[i].TravelDocumentId,
                    });
                }
                else
                {
                    InvoiceDetails invd = (from a in ctx.InvoiceDetails where a.InvoiceId == oldInput.Id && a.Id == dID select a).FirstOrDefault();
                    if (invd != null)
                    {
                        invd.TravelDocumentId = input.Items[i].TravelDocumentId;
                    }
                }
            }
        }

        string customerid = input.CustomerId;
        CustomerDetail cust = (from a in ctx.CustomerDetail where a.CustomerID == customerid select a).FirstOrDefault();

        if (cust != null)
        {
            cust.Amount += input.GrandTotal;
        }

        UpdateStatusPackingList(input, oldInput, "update", ctx);

        Helper.InsertLog("SALES-Transaction-Invoice-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SALES-Transaction-Invoice-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Cancel(InvoiceDTO input, InvoiceDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.Invoices", "Id", oldInput.Id);

        Invoices inv = (Invoices)ctx.GetObjectByKey(key);

        inv.Status = "Cancel";

        string customerid = oldInput.CustomerId;
        CustomerDetail cst = (from a in ctx.CustomerDetail where a.CustomerID == customerid select a).FirstOrDefault();

        if (cst != null)
        {
            cst.Amount -= oldInput.GrandTotal;
        }

        UpdateStatusPackingList(input, oldInput, "cancel", ctx);

        Helper.InsertLog("SALES-Transaction-Invoices-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"select inv.Id, inv.TransactionNo, CONVERT(varchar(10), inv.LoadDate, 120) as LoadDate, 
                                    CONVERT(varchar(10), inv.UnloadDate, 120) as UnLoadDate,
                                    inv.CustomerId, cst.Name as CustomerName, inv.RowVersion, inv.FreightPriceDetailId, inv.IsPpn, inv.BaseNumber, inv.Remark, 
                                    inv.RemarkEtc, inv.PackingListId, pls.TransactionNo as PackingListNo, inv.Price, inv.EtcPrice, inv.Total, inv.Pph, 
                                    inv.GrandTotal, inv.ModifiedBy as mb, inv.Status, u.Name as CreatedBy, itm.Name as ItemName, fpd.Destination, 
                                    itm.ItemId as ItemCode into #temptable
                                    from Invoices inv inner join [User] u 
                                    on inv.CreatedBy = u.UserID
                                    INNER JOIN Customer cst
                                    ON cst.CustomerId = inv.CustomerId
                                    INNER JOIN FreightPriceDetails fpd
                                    ON inv.FreightPriceDetailId = fpd.Id
                                    INNER JOIN Items itm
                                    ON fpd.ItemId = itm.Id
                                    INNER JOIN PackingLists pls
                                    ON inv.PackingListId = pls.Id
                                    

                                    " + Helper.PagingQuery(@"SELECT data.* FROM (select inv.*, us.Name as ModifiedBy,
                                    ROW_NUMBER() OVER (ORDER BY LoadDate DESC, UnLoadDate DESC, Id DESC) as row
                                    from #temptable inv left outer join
                                    [User] us 
                                    on inv.mb = us.UserID 

                                    WHERE " + Helper.FilterQuery(option, "inv.Status", "TransactionNo",
                                    "CustomerId", "Name", "CONVERT(varchar,LoadDate,103)", "CONVERT(varchar,UnLoadDate,103)") + @") data
                                    ", option) + @"

                                    drop table #temptable", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            
            if (detail == "invoiceDetail")
            {
                dt = Helper.NewDataTable(@"
                    select 0 as detailId, pld.travelDocumentId, tds.TransactionNo as travelDocumentNo, 
                    CONVERT(VARCHAR(10),tds.TransactionDate,120) as travelDocumentDate, vhc.vehicleNo,
                    CASE WHEN tds.UseDestinationNetto = 0 THEN tds.OriginNetto ELSE tds.DestinationNetto END AS netto 
                    FROM PackingListDetails pld
                    INNER JOIN TravelDocuments tds
                    ON pld.TravelDocumentId = tds.Id
                    INNER JOIN Vehicle vhc
                    ON tds.VehicleId = vhc.Id 
                    WHERE pld.PackingListId = '" + Helper.EscapeSQLString(id) + "' order by pld.Id", con);
            }
            else if (detail == "items")
            {
                dt = Helper.NewDataTable(@"select invDet.Id as detailId, td.Id as travelDocumentId, td.TransactionNo as travelDocumentNo, 
                CONVERT(varchar(10), td.TransactionDate, 120) as travelDocumentDate, v.vehicleNo,CASE WHEN td.UseDestinationNetto = 1 THEN 
                td.DestinationNetto ELSE td.OriginNetto END AS netto from Invoices inv, InvoiceDetails invDet, TravelDocuments td inner join Vehicle v on v.Id = td.VehicleId where 
                invDet.InvoiceId=inv.Id and td.Id=invDet.TravelDocumentId and inv.Id='" + Helper.EscapeSQLString(id) + @"' 
                order by td.Id", con);               
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT CustomerId, Name as CustomerName  
	                    FROM Customer 
                        WHERE CustomerId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, InvoiceDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "packinglist")
            {
                string customerId = input.CustomerId;

                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT pls.Id, pls.TransactionNo, CONVERT(VARCHAR,pls.TransactionDate,103) as Date,
                cst.Name as CustomerName, ROW_NUMBER() OVER (ORDER BY pls.Id) as row
                FROM PackingLists pls
                INNER JOIN Customer cst
                ON pls.CustomerId = cst.CustomerId
                where pls.status = 'Open' AND pls.CustomerId = '" + customerId + @"' AND 
                " + Helper.FieldFilterQuery(option, "pls.Id", "pls.TransactionNo", "CONVERT(VARCHAR,pls.TransactionDate,103)"), option), con);
            }
            else if (list == "freightprice")
            {
                string customerId = input.CustomerId;
                
                dt = Helper.NewDataTable(Helper.PagingQuery(@"select itm.ItemId as ItemCode, itm.name as ItemName, fpd.Origin, fpd.Destination, fpd.Price, fp.CustomerId, 
                fpd.Id as FreightPriceDetailId, ROW_NUMBER() OVER (ORDER BY fp.Id) as row 
                from FreightPrices fp, FreightPriceDetails fpd, Items itm 
                where fp.Id=fpd.FreightPriceId and fpd.ItemId=itm.Id AND fp.CustomerId = '" + customerId + @"' AND 
                " + Helper.FieldFilterQuery(option, "CustomerId", "Name", "Origin",
                "Destination", "Price"), option), con);
            }
            else if (list == "traveldocument")
            {
                string customerId = input.CustomerId;

                dt = Helper.NewDataTable(Helper.PagingQuery(@"select td.Id, td.TransactionNo, CONVERT(varchar(10), td.TransactionDate,120) 
                as Date,td.Remark ,te.Origin,te.Destination,td.VehicleId, v.VehicleNo,CASE WHEN td.UseDestinationNetto = 1 THEN td.DestinationNetto 
                ELSE td.OriginNetto END AS Netto, td.Status, ROW_NUMBER() OVER (ORDER BY td.Id) as row 
                from TravelDocuments  td inner join TravelExpenses te on te.ItemId = td.ItemId inner join Vehicle v on v.Id = td.VehicleId
                WHERE td.Status<>'Cancel' AND td.Status<>'Closed' AND td.CustomerId = '" + customerId + @"' AND " 
                + Helper.FieldFilterQuery(option, "td.Id", "td.TransactionNo", "CONVERT(varchar,td.TransactionDate,103)", "v.VehicleNo", "td.Remark", 
                "td.OriginNetto", "te.Origin","te.Destination","td.DestinationNetto", "td.Status"), option), con);
            }
            else if (list == "customer")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
                    FROM Customer  
                    WHERE Status = 'Open' AND " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, InvoiceDTO input, InvoiceDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.Invoices where a.Id == oldInput.Id select a).FirstOrDefault();
        
        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }

    protected override void Validate(BaseHandler<InvoiceDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, InvoiceDTO input, InvoiceDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        int Id = input.Id;
        string transactionNo = input.TransactionNo;
        string loaddate = input.LoadDate;
        string unloaddate = input.UnLoadDate;
        string customerID = input.CustomerId;
        decimal price = input.Price;

        if (mode == ValidateMode.Insert)
        {
            var travelNo = (from a in ctx.Invoices where a.TransactionNo == transactionNo select a).FirstOrDefault();

            if (travelNo != null)
            {
                Helper.AddError(err, "transactionNo", "Transaction No. " + transactionNo + " has been registered!");
            }
        } 
        
        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {

            if (input.EtcPrice != 0 & input.RemarkEtc == "")
            {
                Helper.AddError(err, "remarketc", "Remark Etc must not be empty!");
            }
            
            if (transactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. must not be empty!");
            }

            var InvoiceNo = (from a in ctx.Invoices where a.TransactionNo == transactionNo select a).FirstOrDefault();

            

            if (customerID == "")
            {
                Helper.AddError(err, "customerID", "Customer ID must not be empty!");
            }

            if (price <= 0)
            {
                Helper.AddError(err, "price", "Price must be greater than 0!");
            }
        }


        if (input.Items.Count == 0)
        {
            Helper.AddError(err, "detail", "Transaction detail must not be empty!");
        }

        for (int i = 0; i < input.Items.Count; i++)
        {
            string transactionDetail = input.Items[i].TravelDocumentNo;
            int transactionDetailId = input.Items[i].DetailId;
            if (transactionDetail == "")
            {
                Helper.AddError(err, "detail", "Transaction No. detail on row : " + (i + 1) + " must not be empty!");
            }
            else
            {
                if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
                {
                    var InvoiceDetail = (from a in ctx.InvoiceDetails where a.TravelDocumentId == transactionDetailId select a).FirstOrDefault();

                   

                    var getDetail = from a in input.Items where a.TravelDocumentId == transactionDetailId select a;

                    if (getDetail.Count() > 1)
                    {
                        Helper.AddError(err, "detail" + i, "Travel Document Id no " + transactionDetail + " is more than one!");
                    }
                }
            }
        }
    }

    private void UpdateStatusPackingList(InvoiceDTO input, InvoiceDTO oldInput, string action, EasyEntities ctx)
    {
        if (action!= "insert")
        {
            int packingListId = oldInput.PackingListId;
            var packingList = (from a in ctx.PackingLists where a.Id == packingListId select a).FirstOrDefault();
            packingList.Status = "Open";
            
            //for (int i = 0; i < oldInput.Items.Count(); i++)
            //{
            //    int transactionDetailId = oldInput.Items[i].TravelDocumentId;
            //    var travelDocument = (from a in ctx.TravelDocuments where a.Id == transactionDetailId select a).FirstOrDefault();
            //    travelDocument.Status = "Open";
            //}
        }

        if (action != "cancel")
        {
            int packingListId = input.PackingListId;
            var packingList = (from a in ctx.PackingLists where a.Id == packingListId select a).FirstOrDefault();
            packingList.Status = "Closed";
            
            //for (int i = 0; i < input.Items.Count(); i++)
            //{
            //    int transactionDetailId = input.Items[i].TravelDocumentId;
            //    var travelDocument = (from a in ctx.TravelDocuments where a.Id == transactionDetailId select a).FirstOrDefault();
            //    travelDocument.Status = "Closed";
            //}
        }
    }
}