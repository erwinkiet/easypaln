﻿<%@ WebHandler Language="C#" Class="FreightPriceHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class FreightPriceDTO
{
    public int Id { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public List<FreightPriceItemDTO> Items { get; set; } 
}

class FreightPriceItemDTO
{
    public int DetailId { get; set; }
    public int FreightPriceId { get; set; }
    public int ItemId { get; set; }
    public string ItemCode { get; set; }
    public string ItemName { get; set; }
    public string Origin { get; set; }
    public string Destination { get; set; }
    public decimal Price { get; set; }
}

class FreightPriceHandler : BaseHandler<FreightPriceDTO>
{
    protected override void Insert(FreightPriceDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        ctx.FreightPrices.AddObject(new FreightPrices()
        {
            CustomerId = input.CustomerId
        });

        for (int i = 0; i < input.Items.Count; i++)
        {
            ctx.FreightPriceDetails.AddObject(new FreightPriceDetails()
            {
                FreightPriceId = Helper.GetNextID("FreightPrices"),
                ItemId = input.Items[i].ItemId,
                Origin = input.Items[i].Origin,
                Destination = input.Items[i].Destination,
                Price = input.Items[i].Price
            });
        }

        Helper.InsertLog("SALES-Master-FreightPrice-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(FreightPriceDTO input, FreightPriceDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);


        int fpriceid = oldInput.Id;
        EntityKey key = new EntityKey("EasyEntities.FreightPrices", "Id", fpriceid);
        
        FreightPrices fp = (FreightPrices)ctx.GetObjectByKey(key);
        var fpd = from a in ctx.FreightPriceDetails where a.FreightPriceId == oldInput.Id orderby a.Id descending select a;
        var fpdetail = from a in ctx.FreightPriceDetails where a.FreightPriceId == fpriceid select a;
        
        if (fp != null && fpd != null)
        {
           
            fp.CustomerId = input.CustomerId;

            for (int i = 0; i < input.Items.Count; i++)
            {
                int dID = input.Items[i].DetailId;
                
                if (dID == 0)
                {
                    ctx.FreightPriceDetails.AddObject(new FreightPriceDetails()
                    {
                        FreightPriceId = oldInput.Id,
                        ItemId = input.Items[i].ItemId,
                        Origin = input.Items[i].Origin,
                        Destination = input.Items[i].Destination,
                        Price = input.Items[i].Price
                    });
                }
                else
                {
                    FreightPriceDetails fpds = (from a in ctx.FreightPriceDetails where a.FreightPriceId == oldInput.Id && a.Id == dID select a).FirstOrDefault();
                    if (fpds != null)
                    {
             
                        
                        fpds.ItemId = input.Items[i].ItemId;
                        fpds.Origin = input.Items[i].Origin;
                        fpds.Destination = input.Items[i].Destination;
                        fpds.Price = input.Items[i].Price;
                    }
                }
            }
        }

        Helper.InsertLog("SALES-Master-FreightPrice-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SALES-Master-FreightPrice-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Delete(FreightPriceDTO input, FreightPriceDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.FreightPrices", "Id", oldInput.Id);

        FreightPrices fps = (FreightPrices)ctx.GetObjectByKey(key);
        var fpds = from a in ctx.FreightPriceDetails where a.Id == oldInput.Id select a;

        if (fps != null && fpds != null)
        {
            foreach (var d in fpds)
            {
                ctx.DeleteObject(d);
            }

            ctx.DeleteObject(fps);
        }
        Helper.InsertLog("SAL-Master-FreightPrices-Delete", "", ctx, input, user.UserID);
        Helper.InsertLog("SAL-Master-FreightPriceDetails-Delete", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT fp.Id, fp.CustomerId , c.Name as CustomerName
                                    FROM FreightPrices fp
                                    INNER JOIN Customer c
                                    ON fp.CustomerId = c.CustomerID
                                    WHERE " + Helper.FieldFilterQuery(option, "fp.CustomerId",
                                    "c.Name") + @"
                                    ", option) + @"", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (detail == "items")
            {
                DataTable dt = Helper.NewDataTable(@"SELECT fpd.Id as detailId, fpd.freightPriceId, fpd.itemId, itm.ItemId as itemCode,
                itm.Name as itemName, fpd.origin, fpd.destination, fpd.price 
                FROM FreightPriceDetails fpd
                INNER JOIN Items itm
                ON fpd.ItemId = itm.Id
                WHERE fpd.FreightPriceId='" + Helper.EscapeSQLString(id) + @"' 
                order by itm.Name, fpd.Origin", con);

                http.Response.Write(Helper.DataTabletoJSON(dt));
            }
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT CustomerId, Name as CustomerName  
	                    FROM Customer 
                        WHERE CustomerId='" + Helper.EscapeSQLString(id) + "'", con);
            }
            else if (list == "item")
            {
                dt = Helper.NewDataTable(@"SELECT te.ItemId as ItemId, it.ItemId as ItemCode, it.Name as ItemName, te.Origin, te.Destination
                        FROM travelExpenses te
                        INNER JOIN Items it
                        ON te.ItemId = it.Id 
                        WHERE ItemId='" + Helper.EscapeSQLString(id) + "' ORDER BY it.Name", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, FreightPriceDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "customer")
            {
                string customerId = input.CustomerId;

                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
                    FROM Customer  
                    WHERE " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
            }
            else if (list == "item")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT te.ItemId as ItemId, it.ItemId as ItemCode, it.Name as ItemName, te.Origin, te.Destination,
                        ROW_NUMBER() OVER (ORDER BY it.Name asc, te.Origin asc) as row
                        FROM travelExpenses te
                        INNER JOIN Items it
                        ON te.ItemId = it.Id   
                        WHERE " + Helper.FieldFilterQuery(option, "it.ItemId", "it.Name", "te.Origin","te.Destination" ), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, FreightPriceDTO input, FreightPriceDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        //var tran = (from a in ctx.Invoices where a.Id == oldInput.Id select a).FirstOrDefault();
        
        //if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        //{
        //    Helper.ConcurrencyError(err);
        //}
    }

    protected override void Validate(BaseHandler<FreightPriceDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, FreightPriceDTO input, FreightPriceDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        string customerid = input.CustomerId;
        int freightpriceid = input.Id;
        
        
        if (mode == ValidateMode.Insert)
        {
            var travelNo = (from a in ctx.FreightPrices where a.CustomerId == customerid select a).FirstOrDefault();

            if (travelNo != null)
            {
                Helper.AddError(err, "CustomerId", "Customer ID " + customerid + " has been registered!");
            }
        }

        //if (mode == ValidateMode.Update)
        //{
        //    var sds = from a in ctx.FreightPriceDetails where a.FreightPriceId == freightpriceid select a;

        //    foreach (FreightPriceDetails sd in sds)
        //    {
        //        var fd = from a in ctx.FreightPrices where a.Id == sd.FreightPriceId && a.Id == freightpriceid select a;

        //        if (fd.Count() > 0)
        //        {
        //            bool edited = false;
        //            bool existed = false;

        //            foreach (var dt in input.Items)
        //            {
        //                if (dt.Origin == sd.Origin && dt.Destination == sd.Destination)
        //                {
        //                    existed = true;

        //                    if (dt.Price != sd.Price)
        //                    {
        //                        edited = true;
        //                        break;
        //                    }
        //                }
        //            }

        //            if (!existed || edited)
        //            {
        //                Helper.AddError(err, "detail", "Destination : " + sd.Origin + " - " + sd.Destination +" is already used, you can not change the price or delete it!");
        //            }
        //        }
        //    }
        //}
    }
}