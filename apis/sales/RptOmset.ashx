﻿<%@ WebHandler Language="C#" Class="RptOmsetHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class RptOmsetDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public string Origin { get; set; }
    public string Destination { get; set; }
    public string Remark { get; set; }
    public string Status { get; set; }
    public byte[] RowVersion { get; set; }
    public List<ItemDTO> Items { get; set; }
}

class ItemDTO
{
    public int DetailId { get; set; }
    public int TravelDocumentId { get; set; }
    public string TravelDocumentNo { get; set; }
    public int PackingListId { get; set; }
}

class RptOmsetHandler : BaseHandler<RptOmsetDTO>
{
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"select pl.Id, pl.CustomerId, cus.Name as CustomerName, 
                                                 pl.InputDate, pl.Remark, pl.RowVersion, pl.Status, pl.ModifiedBy as mb,
                                                 CONVERT(varchar(10), pl.TransactionDate, 120) as TransactionDate, 
                                                 pl.TransactionNo, u.Name as CreatedBy into #temptable
                                                 from PackingLists pl
                                                 inner join 
                                                 [User] u on pl.CreatedBy = u.UserID
                                                 inner join
                                                 Customer cus on cus.CustomerID = pl.CustomerId
                                                 SELECT data.* FROM (select pl.*, us.Name as ModifiedBy,
                                                 ROW_NUMBER() OVER (ORDER BY TransactionDate DESC, Id DESC) as row
                                                 from #temptable pl left outer join
                                                 [User] us on pl.mb = us.UserID 

                                                 WHERE " + Helper.FilterQuery(option, "pl.Status", "TransactionNo", "CustomerId", "CustomerName", "CONVERT(varchar,TransactionDate,103)") + @") data
                                                                                        WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"

                                                 drop table #temptable", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (detail == "items")
            {
                DataTable dt = Helper.NewDataTable(@"select pld.Id as detailId, 
                                                     td.Id as travelDocumentId, pld.PackingListId, pl.TransactionNo, 
                                                     td.TransactionNo as travelDocumentNo, 
                                                     CONVERT(varchar(10), td.TransactionDate, 120) as travelDocumentDate, 
                                                     v.vehicleNo,CASE WHEN td.UseDestinationNetto = 1 THEN 
                                                     td.DestinationNetto ELSE td.OriginNetto END AS netto 
                                                     from PackingLists pl, PackingListDetails pld, TravelDocuments td inner join 
                                                     Vehicle v on v.Id = td.VehicleId where 
                                                     pl.Id = pld.PackingListId  and td.Id=pld.TravelDocumentId
                                                     and pld.PackingListId ='" + Helper.EscapeSQLString(id) + @"'", con);

                http.Response.Write(Helper.DataTabletoJSON(dt));
            }
        }
    }

    

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
}