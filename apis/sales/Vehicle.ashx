﻿<%@ WebHandler Language="C#" Class="VehicleHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class VehicleDTO
{
    public int Id { get; set; }
    public string VehicleNo { get; set; }
    public List<VehicleItemDTO> Items { get; set; } 
}

class VehicleItemDTO
{
    public int DetailId { get; set; }
    public int DriverId { get; set; }
    public string ActiveSince { get; set; }
    public string EndSince { get; set; }
    public string Status { get; set; }
}

class VehicleHandler : BaseHandler<VehicleDTO>
{
    protected override void Insert(VehicleDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        ctx.Vehicle.AddObject(new Vehicle()
        {
            VehicleNo = input.VehicleNo
        });

        for (int i = 0; i < input.Items.Count; i++)
        {
            int DriverId = input.Items[i].DriverId;
            ctx.VehicleDetails.AddObject(new VehicleDetails()
            {
                VehicleId = Helper.GetNextID("Vehicle"),
                DriverId = DriverId,
                ActiveSince = DateTime.Parse(input.Items[i].ActiveSince),
                EndSince = null,
                Status = "Open",
            });
        }
        
        Helper.InsertLog("SAL-Master-Vehicle-Insert", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }

    protected override void Delete(VehicleDTO input, VehicleDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.Vehicle", "Id", oldInput.Id);
        Vehicle Vehicle = (Vehicle)ctx.GetObjectByKey(key);
        var vehicleDetails = from a in ctx.VehicleDetails where a.VehicleId == oldInput.Id select a;
        
        if (Vehicle != null && vehicleDetails != null)
        {
            foreach (var d in vehicleDetails)
            {
                ctx.DeleteObject(d);
            }
            
            ctx.DeleteObject(Vehicle);
        }   

        Helper.InsertLog("SAL-Master-Vehicle-Delete", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }

    protected override void Update(VehicleDTO input, VehicleDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int id = oldInput.Id;
        EntityKey key = new EntityKey("EasyEntities.Vehicle", "Id", oldInput.Id);
        
        Vehicle vehicle = (Vehicle)ctx.GetObjectByKey(key);
        var vehicleDetails = from a in ctx.VehicleDetails where a.VehicleId == id select a;


        if (vehicle != null && vehicleDetails != null)
        {

            vehicle.VehicleNo = input.VehicleNo;
            
            for (int i = 0; i < input.Items.Count; i++)
            {
                int dID = input.Items[i].DetailId;

                if (dID == 0)
                {
                    ctx.VehicleDetails.AddObject(new VehicleDetails()
                    {
                        VehicleId = oldInput.Id,
                        DriverId = input.Items[i].DriverId,
                        ActiveSince = DateTime.Parse(input.Items[i].ActiveSince),
                        EndSince = (DateTime?)null,
                        Status = "Open",
                    });
                }
                else
                {
                    VehicleDetails vcd = (from a in ctx.VehicleDetails where a.VehicleId == oldInput.Id && a.Id == dID select a).FirstOrDefault();
                    if (vcd != null)
                    {
                        
                        vcd.VehicleId = oldInput.Id;
                        vcd.DriverId = input.Items[i].DriverId;
                        vcd.ActiveSince = DateTime.Parse(input.Items[i].ActiveSince);
                        vcd.Status = "Open";

                        if (input.Items[i].EndSince = (DateTime?)null )
                        {
                            vcd.EndSince = DateTime.Parse(input.Items[i].EndSince);
                            vcd.Status = "Closed";
                        }
                    }
                }
                
                // Menghapus baris yang di delete
                //var deletedDetail = oldInput.Items.Where(p => !input.Items.Any(p2 => p2.DetailId == p.DetailId));
                //if (deletedDetail != null)
                //{
                //    foreach (var d in deletedDetail)
                //    {
                //        var deletedRow = from a in ctx.VehicleDetails where a.Id == d.DetailId select a;

                //        if (deletedRow != null)
                //        {
                //            ctx.DeleteObject(deletedRow);
                //        }
                //    }
                //}
            }
        }

        Helper.InsertLog("SAL-Master-Vehicle-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("SAL-Master-Vehicle-Update", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, VehicleDTO input, VehicleDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);
        
        Vehicle vehicle = (from a in ctx.Vehicle where a.Id == oldInput.Id select a).FirstOrDefault();

        if (vehicle == null)
        {
            Helper.ConcurrencyError(err);
        }
        else if (vehicle.VehicleNo != oldInput.VehicleNo)
        {
            Helper.ConcurrencyError(err);
        }
    }
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                select Id, VehicleNo, ROW_NUMBER() OVER (ORDER BY Id) as row
                from Vehicle                   
                where  " + Helper.FieldFilterQuery(
                    option,
                    "VehicleNo"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (detail == "items")
            {
                DataTable dt = Helper.NewDataTable(@"SELECT vcd.Id as detailId, vcd.driverId, drv.Name as driverName, 
                CONVERT(varchar, vcd.ActiveSince,120) as activeSince, CONVERT(varchar, vcd.EndSince,120) as endSince, vcd.status  
                FROM VehicleDetails vcd
                INNER JOIN Driver drv
                ON vcd.DriverId = drv.Id
                WHERE vcd.VehicleId='" + Helper.EscapeSQLString(id) + @"' 
                order by vcd.Id", con);

                http.Response.Write(Helper.DataTabletoJSON(dt));
            }
        }
    }

    protected override void GetList(string list, VehicleDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "driver")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT Id as DriverId, Name as DriverName, 
                    ROW_NUMBER() OVER (ORDER BY Id) as row                 
                    FROM Driver  
                    WHERE " + Helper.FieldFilterQuery(option, "Id", "Name"), option), con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, VehicleDTO input, VehicleDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.VehicleNo == "")
            {
                Helper.AddError(err, "VehicleNo", "Vehicle No must not be empty!");
            }
        }
        
        if (mode == ValidateMode.Update || mode == ValidateMode.Delete)
        {
            var tdCount = (from a in ctx.TravelDocuments where a.VehicleId == oldInput.Id select a).Count();
            
            if (tdCount > 0)
            {
                if (mode == ValidateMode.Delete)
                {
                    Helper.AddError(err, "VehicleNo", "This Vehicle No cannot be deleted because it is already used!");
                }
                else if (mode == ValidateMode.Update)
                {
                    if (input.VehicleNo != oldInput.VehicleNo)
                    {
                        Helper.AddError(err, "VehicleNo", "You cannot change this Vehicle No because it is already used!");
                    }
                }        
            }

            
            
            
            // Driver yang sudah closed tidak boleh dihapus
            var oldClosedDriver = from a in oldInput.Items where a.Status == "Closed" select a;
            var closedDriver = from a in input.Items where a.Status == "Closed" select a;

            if (oldClosedDriver.Count() != closedDriver.Count())
            {
                Helper.AddError(err, "ClosedDriver", "You can not delete driver that has been closed!");
            }
        }
    }        
}