﻿<%@ WebHandler Language="C#" Class="CustomerHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CustomerDTO
{
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public string ParentCustomerId { get; set; }
    public string ParentCustomerName { get; set; }
    public string TermOfPayment { get; set; }
    public string Npwp { get; set; }
    public string Email { get; set; }
    public string FaxNumber { get; set; }
    public string PhoneNumber { get; set; }
    public string Pic { get; set; }
    public string Address { get; set; }
    public string City { get; set; }
    public string Status { get; set; }
    public List<CustomerItemDTO> Items { get; set; } 
}

class CustomerItemDTO
{
    public string CustomerId { get; set; }
    public string CurrencyId { get; set; }
    public decimal PlafondAmount { get; set; }
    public decimal PlafondTolerance { get; set; }
    public decimal Amount { get; set; }
}

class CustomerHandler : BaseHandler<CustomerDTO>
{
    protected override void Insert(CustomerDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        ctx.Customer.AddObject(new Customer()
        {
            CustomerID = input.CustomerId,
            Name = input.CustomerName,
            ParentCustomerID = input.ParentCustomerId,
            NPWP = input.Npwp,
            Email = input.Email,
            Fax = input.FaxNumber,
            Phone = input.PhoneNumber,
            PIC = input.Pic,
            Address = input.Address,
            City = input.City,
            TermofPayment = Convert.ToInt32(input.TermOfPayment),
            HasChild = UpdateHasChild(input, ctx),
            Status = "Open"
        });
        for (int i = 0; i < input.Items.Count; i++)
        {
            ctx.CustomerDetail.AddObject(new CustomerDetail()
            {
                CustomerID = input.CustomerId,
                CurrencyID = input.Items[i].CurrencyId,
                PlafondAmount = input.Items[i].PlafondAmount,
                PlafondTolerance = input.Items[i].PlafondTolerance,
                Amount = input.Items[i].Amount,
                Amounts = 0
            });
        }

        ctx.CustomerOB.AddObject(new CustomerOB()
        {
            CustomerID = input.CustomerId,
            Name = input.CustomerName,
            ParentCustomerID = input.ParentCustomerId,
            NPWP = input.Npwp,
            Email = input.Email,
            Fax = input.FaxNumber,
            Phone = input.PhoneNumber,
            PIC = input.Pic,
            Address = input.Address,
            City = input.City,
            TermofPayment = Convert.ToInt32(input.TermOfPayment),
            HasChild = UpdateHasChild(input, ctx),
            Status = "Open"
        });
        for (int i = 0; i < input.Items.Count; i++)
        {
            //Debug.WriteLine(DateTime.Now.Date);
            ctx.CustomerOBDetail.AddObject(new CustomerOBDetail()
            {
                CustomerID = input.CustomerId,
                CurrencyID = input.Items[i].CurrencyId,
                PlafondAmount = input.Items[i].PlafondAmount,
                PlafondTolerance = input.Items[i].PlafondTolerance,
                Amount = input.Items[i].Amount,
                Amounts = 0,
                DateUpdated = DateTime.Now.Date
            });
        }

        Helper.InsertLog("SAL-Master-Customer-Insert", "", ctx, input, user.UserID);
        Helper.InsertLog("SAL-Master-CustomerOB-Insert", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Update(CustomerDTO input, CustomerDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        string customerID = oldInput.CustomerId;
        EntityKey key = new EntityKey("EasyEntities.Customer", "CustomerID", customerID);

        Customer customer = (Customer)ctx.GetObjectByKey(key);
        var customerDetails = from a in ctx.CustomerDetail where a.CustomerID == customerID select a;
        string parent = input.ParentCustomerId;
        string oldParent = oldInput.ParentCustomerId;
        var child = from c in ctx.Customer where c.ParentCustomerID == oldParent select c;
        var haschild = (from c in ctx.Customer where c.CustomerID == oldParent select c).FirstOrDefault();
        var haschildOB = (from c in ctx.CustomerOB where c.CustomerID == oldParent select c).FirstOrDefault();

        


        if (customer != null)
        {
            foreach (var d in customerDetails)
            {
                ctx.DeleteObject(d);
            }

            customer.Name = input.CustomerName;
            customer.ParentCustomerID = input.ParentCustomerId;
            customer.NPWP = input.Npwp;
            customer.Email = input.Email;
            customer.Fax = input.FaxNumber;
            customer.Phone = input.PhoneNumber;
            customer.PIC = input.Pic;
            customer.Address = input.Address;
            customer.City = input.City;
            customer.TermofPayment = Convert.ToInt32(input.TermOfPayment);
            customer.HasChild = UpdateHasChild(input, ctx);

            for (int i = 0; i < input.Items.Count; i++)
            {
                ctx.CustomerDetail.AddObject(new CustomerDetail()
                {
                    CustomerID = input.CustomerId,
                    CurrencyID = input.Items[i].CurrencyId,
                    PlafondAmount = input.Items[i].PlafondAmount,
                    PlafondTolerance = input.Items[i].PlafondTolerance,
                    Amount = input.Items[i].Amount,
                    Amounts = 0
                });
            }
        }

        key = new EntityKey("EasyEntities.CustomerOB", "CustomerID", customerID);

        CustomerOB customerOB = (CustomerOB)ctx.GetObjectByKey(key);
        var customerOBDetails = from a in ctx.CustomerOBDetail where a.CustomerID == customerID select a;

        if (customerOB != null && customerOBDetails != null)
        {
            customerOB.Name = input.CustomerName;
            customerOB.ParentCustomerID = input.ParentCustomerId;
            customerOB.NPWP = input.Npwp;
            customerOB.Email = input.Email;
            customerOB.Fax = input.FaxNumber;
            customerOB.Phone = input.PhoneNumber;
            customerOB.PIC = input.Pic;
            customerOB.Address = input.Address;
            customerOB.City = input.City;
            customerOB.TermofPayment = Convert.ToInt32(input.TermOfPayment);
            customerOB.HasChild = UpdateHasChild(input, ctx);
            customerOB.HasChild = 0;

            for (int i = 0; i < input.Items.Count; i++)
            {
                string cur = input.Items[i].CurrencyId;
                var tmp = from a in ctx.CustomerOBDetail where a.CustomerID == customerID && a.CurrencyID == cur select a;

                if (tmp.Count() > 0)
                {
                    CustomerOBDetail sd = tmp.FirstOrDefault();
                    decimal amount = input.Items[i].Amount;

                    if (sd.Amount != amount)
                    {

                        ctx.CustomerOBDetail.AddObject(new CustomerOBDetail()
                        {
                            CustomerID = input.CustomerId,
                            CurrencyID = input.Items[i].CurrencyId,
                            PlafondAmount = input.Items[i].PlafondAmount,
                            PlafondTolerance = input.Items[i].PlafondTolerance,
                            Amount = input.Items[i].Amount,
                            Amounts = 0,
                            DateUpdated = DateTime.Now
                        });
                    }
                    else
                    {
                        ctx.CustomerOBDetail.AddObject(new CustomerOBDetail()
                        {
                            CustomerID = input.CustomerId,
                            CurrencyID = input.Items[i].CurrencyId,
                            PlafondAmount = input.Items[i].PlafondAmount,
                            PlafondTolerance = input.Items[i].PlafondTolerance,
                            Amount = input.Items[i].Amount,
                            Amounts = 0,
                            DateUpdated = sd.DateUpdated
                        });
                    }
                }
                else
                {
                    ctx.CustomerOBDetail.AddObject(new CustomerOBDetail()
                    {
                        CustomerID = input.CustomerId,
                        CurrencyID = input.Items[i].CurrencyId,
                        PlafondAmount = input.Items[i].PlafondAmount,
                        PlafondTolerance = input.Items[i].PlafondTolerance,
                        Amount = input.Items[i].Amount,
                        Amounts = 0,
                        IdrAmount = 0,
                        DateUpdated = DateTime.Now.Date
                    });
                }
            }
            foreach (var d in customerOBDetails)
            {
                ctx.DeleteObject(d);
            }
        }

        Helper.InsertLog("SAL-Master-Customer-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SAL-Master-Customer-Update", "", ctx, input, user.UserID);
        Helper.InsertLog("SAL-Master-CustomerOB-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SAL-Master-CustomerOB-Update", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Delete(CustomerDTO input, CustomerDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);

        string customerID = oldInput.CustomerId;
        EntityKey key = new EntityKey("EasyEntities.Customer", "CustomerID", customerID);
        string oldParent = oldInput.ParentCustomerId;
        Customer customer = (Customer)ctx.GetObjectByKey(key);
        var customerDetails = from a in ctx.CustomerDetail where a.CustomerID == customerID select a;
        var child = from c in ctx.Customer where c.ParentCustomerID == oldParent select c;
        var haschild = (from c in ctx.Customer where c.CustomerID == oldParent select c).FirstOrDefault();
        if (child.Count() == 1 && oldParent.Trim() != "")
        {
            haschild.HasChild = 0;
        }
        if (customer != null && customerDetails != null)
        {
            foreach (var d in customerDetails)
            {
                ctx.DeleteObject(d);
            }

            ctx.DeleteObject(customer);
        }

        key = new EntityKey("EasyEntities.CustomerOB", "CustomerID", customerID);

        CustomerOB customerOB = (CustomerOB)ctx.GetObjectByKey(key);
        var customerOBDetails = from a in ctx.CustomerOBDetail where a.CustomerID == customerID select a;

        if (customerOB != null && customerOBDetails != null)
        {
            foreach (var d in customerOBDetails)
            {
                ctx.DeleteObject(d);
            }

            ctx.DeleteObject(customerOB);
        }

        Helper.InsertLog("SAL-Master-Customer-Delete", "", ctx, input, user.UserID);
        Helper.InsertLog("SAL-Master-CustomerOB-Delete", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Open(CustomerDTO input, CustomerDTO oldInput, EasyEntities ctx)
    {
        base.Open(input, oldInput, ctx);

        string customerID = oldInput.CustomerId;
        EntityKey key = new EntityKey("EasyEntities.Customer", "CustomerID", customerID);

        Customer customer = (Customer)ctx.GetObjectByKey(key);

        if (customer != null)
        {
            customer.Status = "Open";
        }

        key = new EntityKey("EasyEntities.CustomerOB", "CustomerID", customerID);

        CustomerOB customerOB = (CustomerOB)ctx.GetObjectByKey(key);

        if (customerOB != null)
        {
            customerOB.Status = "Open";
        }

        Helper.InsertLog("SAL-Master-Customer-Open", "", ctx, input, user.UserID);
        Helper.InsertLog("SAL-Master-CustomerOB-Open", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Close(CustomerDTO input, CustomerDTO oldInput, EasyEntities ctx)
    {
        base.Close(input, oldInput, ctx);

        string customerID = oldInput.CustomerId;
        EntityKey key = new EntityKey("EasyEntities.Customer", "CustomerID", customerID);

        Customer customer = (Customer)ctx.GetObjectByKey(key);

        if (customer != null)
        {
            customer.Status = "Closed";
        }

        key = new EntityKey("EasyEntities.CustomerOB", "CustomerID", customerID);

        CustomerOB customerOB = (CustomerOB)ctx.GetObjectByKey(key);

        if (customerOB != null)
        {
            customerOB.Status = "Closed";
        }

        Helper.InsertLog("SAL-Master-Customer-Close", "", ctx, input, user.UserID);
        Helper.InsertLog("SAL-Master-CustomerOB-Close", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"SELECT data.* FROM (
                                    SELECT cst.CustomerId, cst.Name as CustomerName, cst.Address, cst.City, cst.Phone as PhoneNumber, 
                                    cst.Fax as FaxNumber, cst.Npwp, cst.Email, cst.Pic, cst.TermOfPayment, cst.ParentCustomerId, 
                                    cus.Name as ParentCustomerName, cst.HasChild, cst.Status, 
                                    ROW_NUMBER() OVER (ORDER BY cst.CustomerId) as row 
                                    FROM Customer cst
                                    LEFT OUTER JOIN
                                    Customer cus
                                    ON cst.ParentCustomerId = cus.CustomerId
                                    WHERE " + Helper.FilterQuery(option, "cst.Status", "cst.CustomerId",
                                    "cst.Name", "cst.Address", "cst.Phone", "cst.Pic", "cst.Email", "cst.npwp") + @") data 
                                    WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (detail == "items")
            {
                DataTable dt = Helper.NewDataTable(@"SELECT customerId, currencyId, plafondAmount, plafondTolerance, amount 
                FROM CustomerDetail 
                WHERE CustomerId='" + Helper.EscapeSQLString(id) + @"' 
                ORDER BY CurrencyId", con);

                http.Response.Write(Helper.DataTabletoJSON(dt));
            }
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT CustomerId, Name as CustomerName  
	                    FROM Customer 
                        WHERE CustomerId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, CustomerDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "currency")
            {
                string customerId = input.CustomerId;

                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CurrencyId, Name as CurrencyName, 
                    ROW_NUMBER() OVER (ORDER BY CurrencyId) as row                 
                    FROM Currency  
                    WHERE " + Helper.FieldFilterQuery(option, "CurrencyId", "Name"), option), con);
            }
            else if (list == "customer")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
                    FROM Customer  
                    WHERE " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    //protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, CustomerDTO input, CustomerDTO oldInput)
    //{
    //    base.CheckConcurrency(err, ctx, input, oldInput);

    //    var tran = (from a in ctx.Invoices where a.Id == oldInput.Id select a).FirstOrDefault();
        
    //    if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
    //    {
    //        Helper.ConcurrencyError(err);
    //    }
    //}

    protected override void Validate(BaseHandler<CustomerDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, CustomerDTO input, CustomerDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        string customerID = input.CustomerId;
        string OldcustomerID = oldInput.CustomerId;
        string customerName = input.CustomerName;
        string parentCustomerID = input.ParentCustomerId;
        string city = input.City;

        var freightprice = from a in ctx.FreightPrices where a.CustomerId == customerID select a;
        var traveldocument = from a in ctx.TravelDocuments where a.CustomerId == customerID select a;
        
   
        if (customerID == "")
        {
            Helper.AddError(err, "customerID", "Customer ID must not be empty!");
        }
        else if (mode == ValidateMode.Delete)
        {
            if (freightprice.Count() > 0 || traveldocument.Count() > 0)
            {
                Helper.AddError(err, "customerID", "This customer is already used, you can not delete it!");
            }
        }
        else if (mode == ValidateMode.Update)
        {
            if (input.CustomerId != OldcustomerID)
            {
                Helper.AddError(err, "customerID", "Customer ID Cannot Changes");
            }
        }
        else if (mode == ValidateMode.Close)
        {
            var sds = from a in ctx.CustomerDetail where a.CustomerID == customerID select a;
            foreach (CustomerDetail sd in sds)
            {
                if (sd.Amount > 0)
                {
                    Helper.AddError(err, "customerID", "The currency " + sd.CurrencyID + " still has " + sd.Amount + " amounts remaining, you can not close it!");
                }
            }
        }
        else if (mode == ValidateMode.Insert)
        {
            var customer = from a in ctx.Customer where a.CustomerID == customerID select a;
            if (customer.Count() > 0)
            {
                Helper.AddError(err, "customerID", "Customer ID: " + customerID + " already exists!");
            }
        }

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (city == "")
            {
                Helper.AddError(err, "city", "City must not be empty!");
            }
            
            if (customerName == "")
            {
                Helper.AddError(err, "customerName", "Customer Name must not be empty!");
            }
            else if (mode == ValidateMode.Update)
            {
                var usecustomer = from a in ctx.Customer where a.ParentCustomerID == customerID select a;
                var customer = from a in ctx.Customer where a.CustomerID == customerID select a;

                if ((customer.Count() > 0 && customer.FirstOrDefault().HasChild == 1) || freightprice.Count() > 0 || traveldocument.Count() > 0)
                {
                    
                    if (parentCustomerID != customer.FirstOrDefault().ParentCustomerID)
                    {
                        Helper.AddError(err, "parentID", "Parent Customer ID can not be changed because already used!");
                    }
                }
            }

            if (input.TermOfPayment == "")
            {
                Helper.AddError(err, "termOfPayment", "termOfPayment must be filled and greather than 0 !");
            }
            if (parentCustomerID != "")
            {
                var c = from a in ctx.Customer where a.CustomerID == parentCustomerID select a;
                if (c.Count() <= 0)
                {
                    Helper.AddError(err, "parentCustomer", "The parent customer with ID: " + parentCustomerID + " does not exists!");
                }
                else if (mode == ValidateMode.Update)
                {
                    if (freightprice.Count() > 0 || traveldocument.Count() > 0)
                    {
                        Customer customer = (from b in ctx.Customer where b.CustomerID == customerID select b).FirstOrDefault();
                        if (parentCustomerID != customer.CustomerID)
                        {
                            Helper.AddError(err, "parentCustomerID", "This customer is already used, you can not change the parent!");
                        }
                    }
                }
            }
            
            for (int i = 0; i < input.Items.Count; i++)
            {
                CustomerItemDTO d = input.Items[i];
                string currencyID = d.CurrencyId;
                decimal amount = d.Amount;
                decimal plafondamount = d.PlafondAmount;
                decimal plafondtolerance = d.PlafondTolerance;

                if (currencyID == "")
                {
                    Helper.AddError(err, "detail", "Currency on row: " + (i + 1) + " must not be empty!");
                }
                else
                {
                    var cur = from a in ctx.Currency where a.CurrencyID == currencyID select a;
                    if (cur.Count() <= 0)
                    {
                        Helper.AddError(err, "detail", "Currency on row: " + (i + 1) + " does not exist!");
                    }
                }
                if (plafondamount < 0)
                {
                    Helper.AddError(err, "detail", "Plafond Amount on row: " + (i + 1) + " can not be minus!");
                }
                if (plafondtolerance < 0)
                {
                    Helper.AddError(err, "detail", "Plafond Tolerance on row: " + (i + 1) + " can not be minus!");
                }
                if (amount < 0)
                {
                    Helper.AddError(err, "detail", "Amount on row: " + (i + 1) + " can not be minus!");
                }
                if (plafondamount > 0)
                {
                    if (amount > (plafondamount * (1 + plafondtolerance / Convert.ToDecimal(100.0))))
                    {
                        Helper.AddError(err, "detail", "Amount on row: " + (i + 1) + " can not be grether than Total Plafond!");
                    }
                }
                for (int j = 0; j < i; j++)
                {
                    if (currencyID == input.Items[j].CurrencyId)
                    {
                        Helper.AddError(err, "detail", "Currency on row: " + (i + 1) + " already registered on row " + (j + 1) + "!");
                        break;
                    }
                }
            }

            if (mode == ValidateMode.Update)
            {
                if (parentCustomerID == customerID)
                {
                    Helper.AddError(err, "customerID", "Customer ID can not be same Parent Customer ID!");
                }
            }
        }
    }

    // Untuk mengupdate nilai HasChild : 1 = memiliki child, 0 = tidak memiliki child
    protected byte UpdateHasChild(CustomerDTO input, EasyEntities ctx)
    {
        string customerid = input.CustomerId;
        string parentcustomerid = input.ParentCustomerId;

        if (parentcustomerid != "")
        {
            var parentcustomer = (from a in ctx.Customer where a.CustomerID == parentcustomerid select a).FirstOrDefault();

            if (parentcustomer != null)
            {
                parentcustomer.HasChild = 1;
            }
        }

        var customer = (from a in ctx.Customer where a.ParentCustomerID == customerid select a).FirstOrDefault();

        if (customer != null)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}