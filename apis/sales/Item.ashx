﻿<%@ WebHandler Language="C#" Class="ItemHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class ItemDTO
{
    public int Id { get; set; }
    public string ItemId { get; set; }
    public string ItemName { get; set; }
}

class ItemHandler : BaseHandler<ItemDTO>
{
    protected override void Insert(ItemDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);
        
        ctx.Items.AddObject(new Items()
        {
            ItemId = input.ItemId,
            Name = input.ItemName
        });
        
        Helper.InsertLog("SAL-Master-Item-Insert", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    
    protected override void Delete(ItemDTO input, ItemDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);
        
        EntityKey key = new EntityKey("EasyEntities.Items", "Id", oldInput.Id);
        Items item = (Items)ctx.GetObjectByKey(key);

        if (item != null)
        {
            ctx.DeleteObject(item);
        }
        
        Helper.InsertLog("SAL-Master-Item-Delete", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    
    protected override void Update(ItemDTO input, ItemDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);
        
        EntityKey key = new EntityKey("EasyEntities.Items", "Id", oldInput.Id);
        Items item = (Items)ctx.GetObjectByKey(key);

        item.ItemId = input.ItemId;
        item.Name = input.ItemName;
        
        Helper.InsertLog("SAL-Master-Item-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("SAL-Master-Item-Update", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    
    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, ItemDTO input, ItemDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);
        
        Items item = (from a in ctx.Items where a.Id == oldInput.Id select a).FirstOrDefault();

        if (item == null)
        {
            Helper.ConcurrencyError(err);
        }
        else if (item.ItemId != oldInput.ItemId || item.Name != oldInput.ItemName)
        {
            Helper.ConcurrencyError(err);
        }
    }
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                select Id, ItemId, Name, ROW_NUMBER() OVER (ORDER BY ItemId) as row
                from Items                   
                where  " + Helper.FieldFilterQuery(
                    option,
                    "ItemId", "Name"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, ItemDTO input, ItemDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.ItemId == "")
            {
                Helper.AddError(err, "itemId", "Item ID must not be empty!");
            }
            else
            {
                var itemCount = (from a in ctx.Items where a.ItemId == input.ItemId && a.Id != input.Id select a).Count();

                if (itemCount > 0)                {

                    Helper.AddError(err, "itemId", "Item ID already exist!");
                }
            }
        }
        
        if (mode == ValidateMode.Update || mode == ValidateMode.Delete)
        {
            var teCount = (from a in ctx.TravelExpenses where a.ItemId == oldInput.Id select a).Count();
            var fpCount = (from a in ctx.FreightPriceDetails where a.ItemId == oldInput.Id select a).Count();

            if (teCount > 0 || fpCount > 0)
            {
                if (mode == ValidateMode.Delete)
                {
                    Helper.AddError(err, "itemId", "This item cannot be deleted because it is already used!");
                }
                else if (mode == ValidateMode.Update)
                {
                    if (input.ItemId != oldInput.ItemId)
                    {
                        Helper.AddError(err, "itemId", "You cannot change this item's ID because it is already used!");
                    }
                }        
            }
        }
    }        
}