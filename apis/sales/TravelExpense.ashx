﻿<%@ WebHandler Language="C#" Class="TravelExpenseHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class TravelExpenseDTO
{
    public int Id { get; set; }
    public int ItemId { get; set; }
    public int HasBase { get; set; }
    public int TonBase { get; set; }
    public decimal Base { get; set; }
    public string Origin { get; set; }
    public string Destination { get; set; }
    public decimal Price { get; set; }
    public decimal Honor { get; set; }
    public decimal PriceBelow { get; set; }
    public decimal PriceAbove { get; set; }
}

class TravelExpenseHandler : BaseHandler<TravelExpenseDTO>
{
    protected override void Insert(TravelExpenseDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        ctx.TravelExpenses.AddObject(new TravelExpenses()
        {
            ItemId = input.ItemId,
            Origin = input.Origin,
            Destination = input.Destination,
            HasBase = input.HasBase,
            TonBase = input.TonBase,
            Base = input.Base,
            Price = input.Price,
            Honor = input.Honor,
            PriceBelow = input.PriceBelow,
            PriceAbove = input.PriceAbove
        });

        Helper.InsertLog("SAL-Master-TravelExpense-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(TravelExpenseDTO input, TravelExpenseDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int transactionid = oldInput.Id;

        EntityKey key = new EntityKey("EasyEntities.TravelExpenses", "Id", transactionid);

        TravelExpenses te = (TravelExpenses)ctx.GetObjectByKey(key);

        if (te != null)
        {
            te.ItemId = input.ItemId;
            te.Origin = input.Origin;
            te.Destination = input.Destination;
            te.HasBase = input.HasBase;
            te.TonBase = input.TonBase;
            te.Base = input.Base;
            te.Price = input.Price;
            te.Honor = input.Honor;
            te.PriceBelow = input.PriceBelow;
            te.PriceAbove = input.PriceAbove;
        }

        Helper.InsertLog("SAL-Master-TravelExpense-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("SAL-Master-TravelExpense-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Delete(TravelExpenseDTO input, TravelExpenseDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.TravelExpenses", "Id", oldInput.Id);

        TravelExpenses te = (TravelExpenses)ctx.GetObjectByKey(key);

        if (te != null)
        {
            ctx.DeleteObject(te);
        }

        Helper.InsertLog("SAL-Master-TravelExpense-Delete", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"SELECT data.* FROM (SELECT te.Id, te.ItemId, itm.ItemId as ItemCode, itm.Name as ItemName, 
                                    te.Origin, te.Destination, te.HasBase, te.TonBase, te.Base, te.Price, te.Honor, te.PriceBelow, te.PriceAbove, 
                                    ROW_NUMBER() OVER (ORDER BY itm.ItemId asc, te.Origin asc, te.Destination asc) as row
                                    from TravelExpenses te INNER JOIN
                                    Items itm 
                                    ON te.ItemId = itm.Id 
                                    WHERE " + Helper.FieldFilterQuery(option, "itm.ItemId", "itm.Name", "te.Origin", "te.Destination", 
                                    "te.Price", "te.PriceBelow", "te.PriceAbove") + @") data
                                    WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT ItemId, Name as ItemName  
	                    FROM Items 
                        WHERE ItemId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, TravelExpenseDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            
            if (list == "item")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT Id as ItemId, ItemId as ItemCode, Name as ItemName, 
                    ROW_NUMBER() OVER (ORDER BY ItemId) as row                 
                    FROM Items  
                    WHERE " + Helper.FieldFilterQuery(option, "ItemId", "Name"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, TravelExpenseDTO input, TravelExpenseDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        //var tran = (from a in ctx.TravelDocuments where a.Id == oldInput.Id select a).FirstOrDefault();
        
        //if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        //{
        //    Helper.ConcurrencyError(err);
        //}
    }

    protected override void Validate(BaseHandler<TravelExpenseDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, TravelExpenseDTO input, TravelExpenseDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.Origin == "" && input.Destination == "")
            {
                Helper.AddError(err, "origindestination","Origin / Destination Must Not be empty!");
            }
            if (input.HasBase == 1)
            {
                if (input.PriceAbove == 0 || input.PriceBelow == 0 || input.Base == 0)
                {
                    Helper.AddError(err, "priceAbovebelow", "Price Above / Price Below / Base Must Not be empty!");
                }
            }

            if (input.TonBase == 1 && input.Price == 0)
            {
                Helper.AddError(err, "Price", "Price Must Not be empty!");
            }
        }
    }
}