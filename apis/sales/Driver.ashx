﻿<%@ WebHandler Language="C#" Class="DriverHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class DriverDTO
{
    public int Id { get; set; }
    public string DriverName { get; set; }
    public decimal Amount { get; set; }
    public string DrivingLicenseNo { get; set; }
    public string IdNo { get; set; }
    public string Address { get; set; }
    public string Religion { get; set; }
    public string ActiveDate { get; set; }
    public string Phone { get; set; }
}

class DriverHandler : BaseHandler<DriverDTO>
{
    protected override void Insert(DriverDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);
        
        string status = "Open";
        ctx.Driver.AddObject(new Driver()
        {
            Name = input.DriverName,
            Amount = input.Amount,
            DrivingLicenseNo = input.DrivingLicenseNo,
            IdNo = input.IdNo,
            Address = input.Address,
            Religion = input.Religion,
            ActiveDate = DateTime.Parse(input.ActiveDate),
            Phone = input.Phone,
            Status = status
        });
        
        Helper.InsertLog("SAL-Master-Driver-Insert", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    
    protected override void Delete(DriverDTO input, DriverDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);
        
        EntityKey key = new EntityKey("EasyEntities.Driver", "Id", oldInput.Id);
        Driver driver = (Driver)ctx.GetObjectByKey(key);

        if (driver != null)
        {
            ctx.DeleteObject(driver);
        }
        
        Helper.InsertLog("SAL-Master-Driver-Delete", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }

    protected override void Close (DriverDTO input, DriverDTO oldInput, EasyEntities ctx)
    {
        base.Close(input, oldInput, ctx);

        int DriverID = oldInput.Id;
        EntityKey key = new EntityKey("EasyEntities.Driver", "Id", DriverID);

        Driver driver = (Driver)ctx.GetObjectByKey(key);


        driver.Status = "Closed";
        
 
        Helper.InsertLog("SAL-Master-Driver-Close", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Open(DriverDTO input, DriverDTO oldInput, EasyEntities ctx)
    {
        base.Open(input, oldInput, ctx);

        int DriverID = oldInput.Id;
        EntityKey key = new EntityKey("EasyEntities.Driver", "Id", DriverID);

        Driver driver = (Driver)ctx.GetObjectByKey(key);

        driver.Status = "Open";
       
        
        Helper.InsertLog("SAL-Master-Driver-Open", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }
    
    protected override void Update(DriverDTO input, DriverDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);
        
        EntityKey key = new EntityKey("EasyEntities.Driver", "Id", oldInput.Id);
        Driver driver = (Driver)ctx.GetObjectByKey(key);

        driver.Name = input.DriverName;
        driver.Amount = input.Amount;
        driver.Address = input.Address;
        driver.DrivingLicenseNo = input.DrivingLicenseNo;
        driver.Religion = input.Religion;
        driver.ActiveDate =  DateTime.Parse(input.ActiveDate);
        driver.IdNo = input.IdNo;
        driver.Phone = input.Phone;
        
        Helper.InsertLog("SAL-Master-Driver-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("SAL-Master-Driver-Update", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    
    //ID no = No KTP
    
    
    //protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, DriverDTO input, DriverDTO oldInput)
    //{
    //    base.CheckConcurrency(err, ctx, input, oldInput);
        
    //    Driver driver = (from a in ctx.Driver where a.Id == oldInput.Id select a).FirstOrDefault();

    //    if (driver == null)
    //    {
    //        Helper.ConcurrencyError(err);
    //    }
    //    else if (driver.Name != oldInput.DriverName || driver.Amount != oldInput.Amount || 
    //             driver.IdNo != oldInput.IdNo || driver.Phone != oldInput.Phone || driver.Religion != oldInput.Religion ||
    //             driver.ActiveDate !=  DateTime.Parse(input.ActiveDate) || driver.Address != oldInput.Address || driver.DrivingLicenseNo != oldInput.DrivingLicenseNo)
    //    {
    //        Helper.ConcurrencyError(err);
    //    }
    //}
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                select Id, Name as DriverName, Amount, Address, DrivingLicenseNo, IdNo, CONVERT(VARCHAR(10),ActiveDate,120) as ActiveDate, Religion, Status, Phone,ROW_NUMBER() OVER (ORDER BY Name) as row
                from Driver                  
                where  " + Helper.FieldFilterQuery(
                    option,
                    "Name", "Amount", "Religion", "ActiveDate", "IdNo", "DrivingLicenseNo", "Status"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, DriverDTO input, DriverDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.Address == "")
            {
                Helper.AddError(err, "Address", "Address must not be empty!");
            }
            
            if (input.IdNo == "")
            {
                Helper.AddError(err, "IdNo", "Id No must not be empty!");
            }
            
            if (input.DrivingLicenseNo == "")
            {
                Helper.AddError(err, "DriverLicenseNo", "Driver License No must not be empty!");
            }   
            
            if (input.DriverName == "")
            {
                Helper.AddError(err, "DriverName", "Driver Name must not be empty!");
            }
            
            if (input.Religion == "")
            {
                Helper.AddError(err, "Religion", "Religion must not be empty!");
            }
            
            if (input.ActiveDate == "")
            {
                Helper.AddError(err, "ActiveDate", "Active Date must not be empty!");
            }
            
            if (input.Phone == "")
            {
                Helper.AddError(err, "Phone", "Phone must not be empty!");
            }
            
            //else
            //{
            //    var driverCount = (from a in ctx.Driver where a.Name == input.DriverName select a).Count();

            //    if (driverCount > 0)                {

            //        Helper.AddError(err, "DriverName", "Driver Name already exist!");
            //    }
            //}
            
        }
        
        if (mode == ValidateMode.Update || mode == ValidateMode.Delete)
        {
            var tdCount = (from a in ctx.TravelDocuments where a.DriverId == oldInput.Id select a).Count();

            if (tdCount > 0)
            {
                if (mode == ValidateMode.Delete)
                {
                    Helper.AddError(err, "DriverName", "This Driver Name cannot be deleted because it is already used!");
                }
                        
            }
        }
        if (mode == ValidateMode.Update || mode == ValidateMode.Delete)
        {
            string id = input.DriverName;
            var usecurrency = from a in ctx.Driver where a.Name == id select a;
            var cur = (from a in ctx.Driver where a.Name == id select a).FirstOrDefault();

            if (usecurrency.Count() > 0)
            {
                if (mode == ValidateMode.Update)
                {
                    if (input.DriverName != cur.Name)
                    {
                        Helper.AddError(err, "DriverName", "Driver Name can not be changed because already used!");
                    }
                }

                if (mode == ValidateMode.Delete)
                {
                    Helper.AddError(err, "DriverNameDelete", "Driver Name can not be deleted because already used!");
                }
            }
        }
    }        
}