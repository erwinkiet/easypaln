﻿<%@ WebHandler Language="C#" Class="CashBankPaymentHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CashBankPaymentDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string Toward { get; set; }
    public string CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public decimal Conversion { get; set; }
    public string CashAccountId { get; set; }
    public string CashAccountName { get; set; }
    public decimal PaymentCash { get; set; }
    public string BankAccountId { get; set; }
    public string BankAccountName { get; set; }
    public decimal PaymentBank { get; set; }
    public string GiroAccountId { get; set; }
    public string GiroAccountName { get; set; }
    public decimal PaymentGiro { get; set; }
    public string GiroNo { get; set; }
    public string DueDateGiro { get; set; }
    public decimal PaymentEtc { get; set; }
    public string EtcAccountId { get; set; }
    public string EtcAccountName { get; set; }
    public string RemarkEtc { get; set; }
    public string Remark { get; set; }
    public string CreatedBy { get; set; }
    public string Status { get; set; }
    public decimal GrandTotal { get; set; }
    public decimal GrandTotalInvoice { get; set; }
    public byte[] RowVersion { get; set; }
}

class CashBankPaymentHandler : BaseHandler<CashBankPaymentDTO>
{
    protected override void Insert(CashBankPaymentDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string TransactionNo = input.TransactionNo;
        string duedate = input.DueDateGiro;
        DateTime? dueDate = null;
        string status = "Open";

        //if (TransactionType == "Etc")
        //{
        //    status = "Closed";
        //}

        if (duedate == null || duedate == "null" || duedate == "" || duedate == "undefined")
        {
            dueDate = null;
        }
        else
        {
            dueDate = DateTime.Parse(input.DueDateGiro);
        }

        
            ctx.CashBankPayments.AddObject(new CashBankPayments()
            {
                TransactionNo = TransactionNo,
                TransactionDate = DateTime.Parse(input.TransactionDate),
                Toward = input.Toward,
                PaymentCash = input.PaymentCash,
                SubAccountCash = input.CashAccountId,
                PaymentGiro = input.PaymentGiro,
                DueDateGiro = dueDate,
                GiroNo = input.GiroNo,
                SubAccountGiro = input.GiroAccountId,
                PaymentBank = input.PaymentBank,
                SubAccountBank = input.BankAccountId,
                PaymentEtc = input.PaymentEtc,
                SubAccountEtc = input.EtcAccountId,
                RemarkEtc = input.RemarkEtc,
                CurrencyId = input.CurrencyId,
                Remark = input.Remark,
                CreatedBy = user.UserID,
                Conversion = input.Conversion,
                Status = status,
                GrandTotal = input.GrandTotal,
                InputDate = DateTime.Now
            });

        
            if (input.CashAccountId != null)
            {
                string said = input.CashAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= input.PaymentCash;
                }
                
            }
        
            else if (input.BankAccountId != null)
            {
                string said = input.BankAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= input.PaymentBank;
                }
            }
            else if (input.GiroAccountId != null)
            {
                string said = input.GiroAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= input.PaymentGiro;
                }
            }

            else if (input.EtcAccountId != null)
            {
                string said = input.EtcAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= input.PaymentEtc;
                }
            }
        
           // UpdateStatusInvoice(input, null, "insert", ctx);

        //giro(ctx, input, null, null, null, ValidateMode.Insert, cbpid);
        //CashBank(ctx, input, null, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, ValidateMode.Insert);
        Helper.InsertLog("FIN-Transaction-CashBankPayments-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(CashBankPaymentDTO input, CashBankPaymentDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int transactionid = oldInput.Id;
        EntityKey key = new EntityKey("EasyEntities.CashBankPayments", "Id", transactionid);

        CashBankPayments cbr = (CashBankPayments)ctx.GetObjectByKey(key);


        string status = "Open";
        DateTime? dueDate = null;
        string duedate = input.DueDateGiro;

        if (duedate == null || duedate == "null" || duedate == "" || duedate == "undefined")
        {
            dueDate = null;
        }
        else
        {
            dueDate = DateTime.Parse(input.DueDateGiro);
        }


        if (cbr != null)
        {
            cbr.TransactionNo = input.TransactionNo;
            cbr.TransactionDate = DateTime.Parse(input.TransactionDate);
            cbr.Toward = input.Toward;
            cbr.CurrencyId = input.CurrencyId;
            cbr.Remark = input.Remark;
            cbr.RemarkEtc = input.RemarkEtc;
            cbr.GrandTotal = input.GrandTotal;
            cbr.ModifiedBy = user.UserID;
            cbr.Status = status;
            cbr.ModifiedDate = DateTime.Now;
            cbr.Status = status;
            cbr.Conversion = cbr.Conversion;
            cbr.CurrencyId = cbr.CurrencyId;
            cbr.DueDateGiro = dueDate;
            cbr.GiroNo = input.GiroNo;
            cbr.GrandTotal = input.GrandTotal;
            cbr.PaymentBank = input.PaymentBank;
            cbr.PaymentCash = input.PaymentCash;
            cbr.PaymentEtc = input.PaymentEtc;
            cbr.PaymentGiro = input.PaymentGiro;
            cbr.SubAccountBank = input.BankAccountId;
            cbr.SubAccountCash = input.CashAccountId;
            cbr.SubAccountEtc = input.EtcAccountId;
            cbr.SubAccountGiro = input.GiroAccountId;

            //UpdateStatusInvoice(input, oldInput, "update", ctx);

            if (input.CashAccountId != null)
            {
                string said = input.CashAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += oldInput.PaymentCash;
                    cb.Amount -= input.PaymentCash;
                }
            }
            else if (input.BankAccountId != null)
            {
                string said = input.BankAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += oldInput.PaymentBank;
                    cb.Amount -= input.PaymentBank;
                }
            }
            else if (input.GiroAccountId != null)
            {
                string said = input.GiroAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += oldInput.PaymentGiro;
                    cb.Amount -= input.PaymentGiro;
                }
            }

            else if (input.EtcAccountId != null)
            {
                string said = input.EtcAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += oldInput.PaymentEtc;
                    cb.Amount -= input.PaymentEtc;
                }
            }

            Helper.InsertLog("FIN-Transaction-CashBankPayments-Update", "Old", ctx, oldInput, user.UserID);
            Helper.InsertLog("FIN-Transaction-CashBankPayments-Update", "", ctx, input, user.UserID);

            ctx.SaveChanges();
        }

    }

    protected override void Cancel(CashBankPaymentDTO input, CashBankPaymentDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.CashBankPayments", "Id", oldInput.Id);

        CashBankPayments cb = (CashBankPayments)ctx.GetObjectByKey(key);

        cb.Status = "Cancel";

        if (input.CashAccountId != null)
        {
            string said = input.CashAccountId;
            CashBank cba = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

            if (cb != null)
            {
                cba.Amount += oldInput.PaymentCash;
              
            }
        }
        else if (input.BankAccountId != null)
        {
            string said = input.BankAccountId;
            CashBank cba = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

            if (cb != null)
            {
                cba.Amount += oldInput.PaymentBank;
          
            }
        }
        else if (input.GiroAccountId != null)
        {
            string said = input.GiroAccountId;
            CashBank cba = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

            if (cb != null)
            {
                cba.Amount += oldInput.PaymentGiro;
      
            }
        }

        else if (input.EtcAccountId != null)
        {
            string said = input.EtcAccountId;
            CashBank cba = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

            if (cb != null)
            {
                cba.Amount += oldInput.PaymentEtc;
            }
        }
        
        
        //UpdateStatusInvoice(input, oldInput, "cancel", ctx);

        Helper.InsertLog("FIN-Transaction-CashBankPayments-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {

        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;

        using (SqlConnection con = new SqlConnection(conString))
        {

            DataTable dt = Helper.NewDataTable(@"select cbp.Id, cbp.TransactionNo, CONVERT(varchar(10), cbp.TransactionDate, 120) as TransactionDate, 
                                    cbp.RowVersion, cbp.CurrencyId,cur.Name as CurrencyName, cur.Conversion, cbp.Toward,
                                    ca.Name as CashAccountName, ga.Name as GiroAccountName, ba.Name as BankAccountName, et.Name as EtcAccountName,
                                    cbp.GrandTotal, cbp.Remark, cbp.RemarkEtc, cbp.Status, us.Name as CreatedBy,
                                    cbp.PaymentCash, cbp.PaymentBank, cbp.PaymentGiro, cbp.PaymentEtc, cbp.SubAccountCash as CashAccountId, 
                                    cbp.SubAccountBank as BankAccountId, cbp.SubAccountGiro as GiroAccountId, cbp.SubAccountEtc as EtcAccountId, cbp.GiroNo,
									CONVERT(VARCHAR(10),cbp.DueDateGiro,103) as DueDateGiro,cbp.ModifiedBy as mb
									into #temptable
                                    from cashbankpayments cbp inner join [User] us 
                                    on cbp.CreatedBy = us.UserID
                                    INNER JOIN
									Currency cur ON cur.CurrencyID = cbp.CurrencyId
                                    LEFT OUTER JOIN
                                    CashBank ca ON cbp.SubAccountCash = ca.SubAccountID
                                    LEFT OUTER JOIN
                                    CashBank ba ON cbp.SubAccountBank = ba.SubAccountID
                                    LEFT OUTER JOIN
                                    CashBank ga ON cbp.SubAccountGiro = ga.SubAccountID
                                    LEFT OUTER JOIN
                                    CashBank et ON cbp.SubAccountEtc = et.SubAccountID

                                    " + Helper.PagingQuery(@"SELECT data.* FROM (select cbp.*, us.Name as ModifiedBy, ROW_NUMBER() OVER (ORDER BY Id DESC) as row
                                    from #temptable cbp left outer join
                                    [User] us 
                                    on cbp.mb = us.UserID
                                    WHERE " + Helper.FilterQuery(option, "cbp.Status", "cbp.TransactionNo",
                                     "cbp.TransactionDate") + @") data
                                    ", option) + @"
                                    drop table #temptable

                                    ", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }


    protected override void GetList(string list, CashBankPaymentDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);


        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

//            if (list == "customer")
//            {
//                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
//                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
//                    FROM Customer  
//                    WHERE " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
//            }

//            else if (list == "invoice")
//            {
//                string customerId = input.CustomerId;

//                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT inv.Id as InvoiceId, c.Name as CustomerName,inv.TransactionNo as InvoiceNo, inv.grandTotal as GrandTotalInvoice,
//                    ROW_NUMBER() OVER (ORDER BY Id) as row                 
//                    FROM Invoices inv INNER JOIN Customer c ON c.CustomerId =  inv.CustomerId
//                    WHERE inv.status <> 'Closed' AND inv.CustomerId = '" + customerId + @"' AND " + Helper.FieldFilterQuery(option, "inv.Id", "inv.TransactionNo", "inv.grandTotal"), option), con);
//            }

            if (list == "currency")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CurrencyID as CurrencyId, Name as CurrencyName, Conversion,
                    ROW_NUMBER() OVER (ORDER BY CurrencyID) as row                 
                    FROM Currency
                    WHERE " + Helper.FieldFilterQuery(option, "CurrencyID", "Name", "Conversion"), option), con);
            }

            else if (list == "cashAccount" || list == "giroAccount" || list == "bankAccount" || list == "etcAccount")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"
	                select cb.SubAccountID as AccountId, cb.Name as AccountName, cb.CurrencyID as Currency, 
                    cb.Type, ROW_NUMBER() OVER (ORDER BY cb.SubAccountID) as row from CashBank cb WHERE " + Helper.FieldFilterQuery(option, "cb.SubAccountID", "cb.Name", "cb.CurrencyID", "cb.Type"), option), con);
            }


            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(BaseHandler<CashBankPaymentDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, CashBankPaymentDTO input, CashBankPaymentDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        string transactiono = input.TransactionNo;
        var paymentno = (from a in ctx.CashBankPayments where a.TransactionNo == transactiono select a).FirstOrDefault();

        if (paymentno != null)
        {
            Helper.AddError(err, "cashbankpayment", "Transaction No " + transactiono + " has been registered!");
        }
        
        
        string accountid = input.CashAccountId;
        CashBank am = (from a in ctx.CashBank where a.SubAccountID == accountid select a).FirstOrDefault();

        if (input.PaymentCash - am.Amount < 0)
        {
            Helper.AddError(err, "paymentcash", "Cash amount must not be greater than account's amount");
        }
        

        string transactionNo = input.TransactionNo;
        
        if (mode == ValidateMode.Insert)
        {
            var cbrNo = (from a in ctx.CashBankPayments where a.TransactionNo == transactionNo select a).FirstOrDefault();

            if (cbrNo != null)
            {
                Helper.AddError(err, "transactionNo", "Transaction No. " + transactionNo + " has been registered!");
            }
        }

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {

            if (input.TransactionNo == "")
            {
                Helper.AddError(err, "transactionno", "Transaction No must not be empty");
            }

            if (input.TransactionDate == "")
            {
                Helper.AddError(err, "transactiondate", "Receive Date must not be empty!");
            }

            if (input.Toward == "")
            {
                Helper.AddError(err, "toward", "Toward must not be empty!");
            }

            if (transactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. must not be empty!");
            }

            if (input.CurrencyId == "")
            {
                Helper.AddError(err, "currencyid", "Currency must not be empty!");
            }
        }
    }
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                 select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
}
