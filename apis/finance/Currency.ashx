﻿<%@ WebHandler Language="C#" Class="CurrencyHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CurrencyDTO
{
    public string CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public decimal Conversion { get; set; }
}

class CurrencyHandler : BaseHandler<CurrencyDTO>
{
    protected override void Insert(CurrencyDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);
        ctx.Currency.AddObject(new Currency()
        {
            CurrencyID = input.CurrencyId,
            Name = input.CurrencyName,
            Conversion = Convert.ToDecimal(input.Conversion)

        });
        Helper.InsertLog("FIN-Master-Currency-Insert", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    protected override void Delete(CurrencyDTO input, CurrencyDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);
        string currencyid = oldInput.CurrencyId;
        EntityKey key = new EntityKey("EasyEntities.Currency", "CurrencyID", currencyid);
        Currency currency = (Currency)ctx.GetObjectByKey(key);

        if (currency != null)
        {
            ctx.DeleteObject(currency);
        }
        Helper.InsertLog("FIN-Master-Currency-Delete", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    protected override void Update(CurrencyDTO input, CurrencyDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);
        string currencyid = oldInput.CurrencyId;
        EntityKey key = new EntityKey("EasyEntities.Currency", "CurrencyID", currencyid);
        Currency currency = (Currency)ctx.GetObjectByKey(key);

        currency.Name = input.CurrencyName;
        currency.Conversion = Convert.ToDecimal(input.Conversion);
        Helper.InsertLog("FIN-Master-Currency-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("FIN-Master-Currency-Update", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, CurrencyDTO input, CurrencyDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);
        
        string currencyID = oldInput.CurrencyId;
        Currency currency = (from a in ctx.Currency where a.CurrencyID == currencyID select a).FirstOrDefault();

        if (currency == null)
        {
            Helper.ConcurrencyError(err);
        }
        else if (currency.Name != oldInput.CurrencyName || currency.Conversion != Convert.ToDecimal(oldInput.Conversion))
        {
            Helper.ConcurrencyError(err);
        }
    }
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                        select CurrencyID, Name as CurrencyName, Conversion, 
                        ROW_NUMBER() OVER (ORDER BY CurrencyID) as row from Currency                   
                         WHERE " + Helper.FieldFilterQuery(option,
                        "CurrencyID", "Name"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    
}