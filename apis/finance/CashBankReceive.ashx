﻿<%@ WebHandler Language="C#" Class="CashBankReceiveHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CashBankReceiveDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string TransactionType { get; set; }
    public int InvoiceId { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public string CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public decimal Conversion { get; set; }
    public string CashAccountId { get; set; }
    public string CashAccountName { get; set; }
    public decimal ReceiveCash { get; set; }
    public string BankAccountId { get; set; }
    public string BankAccountName { get; set; }
    public decimal ReceiveBank { get; set; }
    public string GiroAccountId { get; set; }
    public string GiroAccountName { get; set; }
    public decimal ReceiveGiro { get; set; }
    public string GiroNo { get; set; }
    public string DueDateGiro { get; set; }
    public decimal ReceiveEtc { get; set; }
    public string EtcAccountId { get; set;}
    public string EtcAccountName { get; set; }
    public string RemarkEtc { get; set; }
    public string Remark { get; set; }
    public decimal CostReduction { get; set; }
    public string RemarkReduction { get; set; }
    public int CreatedBy { get; set; }
    public string Status { get; set; }
    public decimal Total { get; set; }
    public decimal GrandTotal { get; set; }
    public decimal GrandTotalInvoice { get; set; }
    public byte[] RowVersion { get; set; }
}

class CashBankReceiveHandler : BaseHandler<CashBankReceiveDTO>
{
    protected override void Insert(CashBankReceiveDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string TransactionType = input.TransactionType;
        string TransactionNo = input.TransactionNo;
        string duedate = input.DueDateGiro;
        DateTime? dueDate = null;
        string status = "Open";

        //if (TransactionType == "Etc")
        //{
        //    status = "Closed";
        //}

        if (duedate == null || duedate == "null" || duedate == "" || duedate == "undefined")
        {
            dueDate = null;
        }
        else
        {
            dueDate = DateTime.Parse(input.DueDateGiro);
        }
        
        if( input.TransactionType == "invoice")
        {
            ctx.CashBankReceives.AddObject(new CashBankReceives()
            {
                TransactionNo = TransactionNo,
                TransactionDate = DateTime.Parse(input.TransactionDate),
                CustomerId = input.CustomerId,
                CustomerName = input.CustomerName,
                InvoiceId = input.InvoiceId,
                ReceiveCash = input.ReceiveCash,
                SubAccountCash = input.CashAccountId,
                ReceiveGiro = input.ReceiveGiro,
                DueDateGiro = dueDate,
                GiroNo = input.GiroNo,
                SubAccountGiro = input.GiroAccountId,
                ReceiveBank = input.ReceiveBank,
                SubAccountBank = input.BankAccountId,
                ReceiveEtc = input.ReceiveEtc,
                SubAccountEtc = input.EtcAccountId,
                RemarkEtc = input.RemarkEtc,
                TransactionType = TransactionType,
                CurrencyId = input.CurrencyId,
                Remark = input.Remark,
                CostReduction = input.CostReduction,
                RemarkReduction = input.RemarkReduction,
                CreatedBy = user.UserID,
                Conversion = input.Conversion,
                Status = status,
                Total = input.Total,
                GrandTotal = input.GrandTotal,
                InputDate = DateTime.Now
            });
            
            if(input.CashAccountId != null)
            {
                string said = input.CashAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                    if (cb != null)
                    {
                        cb.Amount += input.ReceiveCash;
                    }
            }
            else if (input.BankAccountId != null)
            {
                string said = input.BankAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                    if (cb != null)
                    {
                        cb.Amount += input.ReceiveBank;
                    }
            }
            else if (input.GiroAccountId != null)
            {
                string said = input.GiroAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                    if (cb != null)
                    {
                        cb.Amount += input.ReceiveGiro;
                    }
            }
           

        UpdateStatusInvoice(input, null, "insert", ctx);
        }

        if (input.TransactionType != "invoice" && input.TransactionType != "etc")
        {
            ctx.CashBankReceives.AddObject(new CashBankReceives()
            {
                TransactionNo = TransactionNo,
                TransactionDate = DateTime.Parse(input.TransactionDate),
                CustomerId = Convert.ToString(input.CustomerId),
                CustomerName = input.CustomerName,
                InvoiceId = input.InvoiceId,
                ReceiveCash = input.ReceiveCash,
                SubAccountCash = input.CashAccountId,
                ReceiveGiro = input.ReceiveGiro,
                DueDateGiro = dueDate,
                GiroNo = input.GiroNo,
                SubAccountGiro = input.GiroAccountId,
                ReceiveBank = input.ReceiveBank,
                SubAccountBank = input.BankAccountId,
                ReceiveEtc = input.ReceiveEtc,
                SubAccountEtc = input.EtcAccountId,
                RemarkEtc = input.RemarkEtc,
                TransactionType = input.TransactionType,
                CurrencyId = input.CurrencyId,
                Remark = input.Remark,
                CostReduction = input.CostReduction,
                RemarkReduction = input.RemarkReduction,
                CreatedBy = user.UserID,
                Conversion = input.Conversion,
                Status = status,
                GrandTotal = input.GrandTotal,
                Total = input.Total,
                InputDate = DateTime.Now
            });

            if (input.CashAccountId != null)
            {
                string said = input.CashAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += input.ReceiveCash;
                }
            }
            else if (input.BankAccountId != null)
            {
                string said = input.BankAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += input.ReceiveBank;
                }
            }
            else if (input.GiroAccountId != null)
            {
                string said = input.GiroAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += input.ReceiveGiro;
                }
            }
        }
        
        if (input.TransactionType == "etc")
        {
            ctx.CashBankReceives.AddObject(new CashBankReceives()
            {
                TransactionNo = TransactionNo,
                TransactionDate = DateTime.Parse(input.TransactionDate),
                CustomerId = Convert.ToString(input.CustomerId),
                CustomerName = input.CustomerName,
                InvoiceId = input.InvoiceId,
                ReceiveCash = input.ReceiveCash,
                SubAccountCash = input.CashAccountId,
                ReceiveGiro = input.ReceiveGiro,
                DueDateGiro = dueDate,
                GiroNo = input.GiroNo,
                SubAccountGiro = input.GiroAccountId,
                ReceiveBank = input.ReceiveBank,
                SubAccountBank = input.BankAccountId,
                ReceiveEtc = input.ReceiveEtc,
                SubAccountEtc = input.EtcAccountId,
                RemarkEtc = input.RemarkEtc,
                TransactionType = input.TransactionType,
                CurrencyId = input.CurrencyId,
                Remark = input.Remark,
                CostReduction = input.CostReduction,
                RemarkReduction = input.RemarkReduction,
                CreatedBy = user.UserID,
                Conversion = input.Conversion,
                Status = status,
                GrandTotal = input.GrandTotal,
                Total = input.Total,
                InputDate = DateTime.Now
            });
            
            if (input.EtcAccountId != null)
            {
                string said = input.EtcAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += input.ReceiveEtc;
                }
            }
        }
        
        //giro(ctx, input, null, null, null, ValidateMode.Insert, cbpid);
        //CashBank(ctx, input, null, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, ValidateMode.Insert);
        Helper.InsertLog("FIN-Transaction-CashBankReceives-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(CashBankReceiveDTO input, CashBankReceiveDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int transactionid = oldInput.Id;
        EntityKey key = new EntityKey("EasyEntities.CashBankReceives", "Id", transactionid);

        CashBankReceives cbr = (CashBankReceives)ctx.GetObjectByKey(key);

       
        string oldCustomerId = oldInput.CustomerId;
        string status = "Open";
        DateTime? dueDate = null;
        string duedate = input.DueDateGiro;

        if (duedate == null || duedate == "null" || duedate == "" || duedate == "undefined")
        {
            dueDate = null;
        }
        else
        {
            dueDate = DateTime.Parse(input.DueDateGiro);
        }
        
        
        if (cbr != null )
        {
            cbr.TransactionNo = input.TransactionNo;
            cbr.TransactionDate = DateTime.Parse(input.TransactionDate);
            cbr.CustomerId = input.CustomerId;
            cbr.CustomerName = input.CustomerName;
            cbr.CurrencyId = input.CurrencyId;
            cbr.Remark = input.Remark;
            cbr.RemarkEtc = input.RemarkEtc;
            cbr.GrandTotal = input.GrandTotal;
            cbr.ModifiedBy = user.UserID;
            cbr.Status = status;
            cbr.ModifiedDate = DateTime.Now;
            cbr.Conversion = input.Conversion;
            cbr.CurrencyId = input.CurrencyId;
            cbr.CustomerName = input.CustomerName;
            cbr.DueDateGiro = dueDate;
            cbr.GiroNo = input.GiroNo;
            cbr.GrandTotal = input.GrandTotal;
            cbr.Total = input.Total;
            cbr.InvoiceId = input.InvoiceId;
            cbr.ReceiveBank = input.ReceiveBank;
            cbr.ReceiveCash = input.ReceiveCash;
            cbr.ReceiveEtc = input.ReceiveEtc;
            cbr.ReceiveGiro = input.ReceiveGiro;
            cbr.SubAccountBank = input.BankAccountId;
            cbr.SubAccountCash = input.CashAccountId;
            cbr.SubAccountEtc = input.EtcAccountId;
            cbr.SubAccountGiro = input.GiroAccountId;
            cbr.CostReduction = input.CostReduction;
            cbr.RemarkReduction = input.RemarkReduction;
            cbr.TransactionType = input.TransactionType;

            if (input.CashAccountId != null)
            {
                string said = input.CashAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= oldInput.ReceiveCash;
                    cb.Amount += input.ReceiveCash;
                }
            }
            else if (input.BankAccountId != null)
            {
                string said = input.BankAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= oldInput.ReceiveBank;
                    cb.Amount += input.ReceiveBank;
                }
            }
            else if (input.GiroAccountId != null)
            {
                string said = input.GiroAccountId;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= oldInput.ReceiveGiro;
                    cb.Amount += input.ReceiveGiro;
                }
            }

            if (input.TransactionType == "etc")
            {
                if (input.EtcAccountId != null)
                {
                    string said = input.EtcAccountId;
                    CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                    if (cb != null)
                    {
                        cb.Amount -= oldInput.ReceiveEtc;
                        cb.Amount += input.ReceiveEtc;
                    }
                }

            }
            UpdateStatusInvoice(input, oldInput, "update", ctx);
            
            Helper.InsertLog("FIN-Transaction-CashBankReceives-Update", "Old", ctx, oldInput, user.UserID);
            Helper.InsertLog("FIN-Transaction-CashBankReceives-Update", "", ctx, input, user.UserID);
            
             ctx.SaveChanges();
        }
        
    }

    protected override void Cancel(CashBankReceiveDTO input, CashBankReceiveDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.CashBankReceives", "Id", oldInput.Id);

        CashBankReceives cb = (CashBankReceives)ctx.GetObjectByKey(key);

        cb.Status = "Cancel";


        UpdateStatusInvoice(input, oldInput, "cancel", ctx);

        Helper.InsertLog("FIN-Transaction-CashBankReceives-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {

        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;

        using (SqlConnection con = new SqlConnection(conString))
        {

            DataTable dt = Helper.NewDataTable(@"select cbr.Id, cbr.CostReduction, cbr.RemarkReduction,cbr.InvoiceId, inv.TransactionNo as InvoiceNo, inv.GrandTotal as GrandTotalInvoice, cbr.TransactionNo, CONVERT(varchar(10), cbr.TransactionDate, 120) as TransactionDate, 
                                    cbr.RowVersion, cbr.CustomerId, cst.Name as CustomerName, cbr.CurrencyId, cur.Name as CurrencyName, cur.Conversion,
                                    cbr.GrandTotal, cbr.TransactionType, cbr.Remark, cbr.RemarkEtc, cbr.Status, us.Name as CreatedBy, cbr.Total,
                                    cbr.ReceiveCash, cbr.ReceiveBank, cbr.ReceiveGiro, cbr.ReceiveEtc, cbr.SubAccountCash as CashAccountId, 
                                    cbr.SubAccountBank as BankAccountId, cbr.SubAccountGiro as GiroAccountId, cbr.SubAccountEtc as EtcAccountId, cbr.GiroNo,
                                    ca.Name as CashAccountName, ga.Name as GiroAccountName, ba.Name as BankAccountName, et.Name as EtcAccountName,
									CONVERT(VARCHAR(10),cbr.DueDateGiro,103) as DueDateGiro,cbr.ModifiedBy as mb 
									into #temptable
                                    from CashBankReceives cbr inner join [User] us 
                                    on cbr.CreatedBy = us.UserID
                                    LEFT OUTER JOIN Customer cst
                                    on cst.CustomerId = cbr.CustomerId
                                    INNER JOIN
									Currency cur ON cur.CurrencyID = cbr.CurrencyId
                                    LEFT OUTER JOIN
                                    Invoices inv ON inv.Id = cbr.InvoiceId
                                    LEFT OUTER JOIN
                                    CashBank ca ON cbr.SubAccountCash = ca.SubAccountID
                                    LEFT OUTER JOIN
                                    CashBank ba ON cbr.SubAccountBank = ba.SubAccountID
                                    LEFT OUTER JOIN
                                    CashBank ga ON cbr.SubAccountGiro = ga.SubAccountID
                                    LEFT OUTER JOIN
                                    CashBank et ON cbr.SubAccountEtc = et.SubAccountID

                                    " + Helper.PagingQuery(@"SELECT data.* FROM (select cbp.*, us.Name as ModifiedBy, ROW_NUMBER() OVER (ORDER BY Id DESC) as row
                                    from #temptable cbp left outer join
                                    [User] us 
                                    on cbp.mb = us.UserID
                                    WHERE " + Helper.FilterQuery(option, "cbp.Status", "TransactionNo",
                                     "TransactionDate", "CustomerName", "CustomerId") + @") data
                                    ", option) + @"
                                    drop table #temptable

                                    ", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "customer")
            {
                dt = Helper.NewDataTable(@"SELECT CustomerId, Name as CustomerName  
	                    FROM Customer 
                        WHERE CustomerId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, CashBankReceiveDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);


        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "customer")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CustomerId, Name as CustomerName, 
                    ROW_NUMBER() OVER (ORDER BY CustomerId) as row                 
                    FROM Customer  
                    WHERE " + Helper.FieldFilterQuery(option, "CustomerId", "Name"), option), con);
            }

            else if (list == "invoice")
            {
                string customerId = input.CustomerId;
                
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT inv.Id as InvoiceId, c.Name as CustomerName,inv.TransactionNo as InvoiceNo, inv.grandTotal as GrandTotalInvoice,
                    ROW_NUMBER() OVER (ORDER BY Id) as row                 
                    FROM Invoices inv INNER JOIN Customer c ON c.CustomerId =  inv.CustomerId
                    WHERE inv.status <> 'Closed' AND inv.CustomerId = '" + customerId + @"' AND " + Helper.FieldFilterQuery(option, "inv.Id", "inv.TransactionNo", "inv.grandTotal"), option), con);
            }
                
            else if (list == "currency")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT CurrencyID as CurrencyId, Name as CurrencyName, Conversion,
                    ROW_NUMBER() OVER (ORDER BY CurrencyID) as row                 
                    FROM Currency
                    WHERE " + Helper.FieldFilterQuery(option, "CurrencyID", "Name", "Conversion"), option), con);
            }

            else if (list == "cashAccount" || list == "giroAccount" || list == "bankAccount" || list == "etcAccount")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"
	                select cb.SubAccountID as AccountId, cb.Name as AccountName, 
                    cb.Type, ROW_NUMBER() OVER (ORDER BY cb.SubAccountID) as row from CashBank cb WHERE " + Helper.FieldFilterQuery(option, "cb.SubAccountID", "cb.Name", "cb.Type"), option), con);
            }
            
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    
                                        
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                 select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, CashBankReceiveDTO input, CashBankReceiveDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.CashBankReceives where a.Id == oldInput.Id select a).FirstOrDefault();

        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }

    protected override void Validate(BaseHandler<CashBankReceiveDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, CashBankReceiveDTO input, CashBankReceiveDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        string transactionNo = input.TransactionNo;
        
        if (mode == ValidateMode.Insert)
        {
            var cbrNo = (from a in ctx.CashBankReceives where a.TransactionNo == transactionNo select a).FirstOrDefault();

            if (cbrNo != null)
            {
                Helper.AddError(err, "transactionNo", "Transaction No. " + transactionNo + " has been registered!");
            }
        }

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if(input.TransactionType == "invoice")
            {
                if (input.GrandTotal != (input.GrandTotalInvoice - input.CostReduction))
                {
                    Helper.AddError(err, "grandtotal", "Grand Total invoice and grand total cash bank receive must same!");
                }
            }

            if (input.TransactionType == "etc")
            {
                if (input.EtcAccountId == "" || input.ReceiveEtc == 0)
                {
                    Helper.AddError(err, "etc", "Etc Account ID or Receive Etc must not be empty!");
                }
            }
            
            if(input.TransactionNo == "")
            {
                Helper.AddError(err,"transactionno","Transaction No must not be empty");
            }
            
            if(input.TransactionDate == "")
            {
                Helper.AddError(err,"transactiondate","Receive Date must not be empty!");
            }
            
            if (input.CustomerId == "")
            {
                Helper.AddError(err, "customerID", "Customer ID must not be empty!");
            }

            if (input.ReceiveEtc != 0 & input.RemarkEtc == "")
            {
                Helper.AddError(err, "remarketc", "Remark Etc must not be empty!");
            }

            if (transactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. must not be empty!");
            }

            if (input.CurrencyId == "")
            {
                Helper.AddError(err, "currencyid", "Currency must not be empty!");
            }
        }
    }
    
    
    private void UpdateStatusInvoice(CashBankReceiveDTO input, CashBankReceiveDTO oldInput, string action, EasyEntities ctx)
    {
        if (action != "insert")
        {
            int invoiceId = oldInput.InvoiceId;
            var invoice = (from a in ctx.Invoices where a.Id == invoiceId select a).FirstOrDefault();
            invoice.Status = "Open";

            //for (int i = 0; i < oldInput.Items.Count(); i++)
            //{
            //    int transactionDetailId = oldInput.Items[i].TravelDocumentId;
            //    var travelDocument = (from a in ctx.TravelDocuments where a.Id == transactionDetailId select a).FirstOrDefault();
            //    travelDocument.Status = "Open";
            //}
        }

        if (action != "cancel")
        {
            int invoiceId = input.InvoiceId;
            var invoice = (from a in ctx.Invoices where a.Id == invoiceId select a).FirstOrDefault();
            invoice.Status = "Closed";

            //for (int i = 0; i < input.Items.Count(); i++)
            //{
            //    int transactionDetailId = input.Items[i].TravelDocumentId;
            //    var travelDocument = (from a in ctx.TravelDocuments where a.Id == transactionDetailId select a).FirstOrDefault();
            //    travelDocument.Status = "Closed";
            //}
        }
    }
}

