﻿<%@ WebHandler Language="C#" Class="GiroClearingHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class GiroClearingDTO
{
    public int Id { get; set; }
    public string ClearingId { get; set; }
    public string Type { get; set; }
    public string GiroNo { get; set; }
    public string DueDate { get; set; }
    public string SubAccountID { get; set; }
    public string BankName { get; set; }
    public string Date { get; set; }
    public decimal Amount { get; set; }
    
}


class GiroClearingHandler : BaseHandler<GiroClearingDTO>
{
    protected override void Insert(GiroClearingDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);
        //string transactionid = Helper.GenerateID("GiroClearing", "ClearingID", "GC", input.Date, user.Initial.ToString());
        //input.ClearingId = transactionid;
        //DateTime? nulldate = null;
        //var girodata = (from a in ctx.Giro where a.Type == input.Type && a.GiroNo == input.GiroNo && a.Status != "Closed" select a).FirstOrDefault();
        ctx.GiroClearing.AddObject(new GiroClearing()
        {
            ClearingID = input.ClearingId,
            TransactionType = input.Type,
            GiroNo = input.GiroNo,
            UserID = Convert.ToByte(user.UserID),
            SubAccountID = input.SubAccountID,
            BankName = input.BankName,
            Amount = Convert.ToDecimal(input.Amount),
            Date = DateTime.Parse(input.Date),
            Status = "Open"
        });
        //if (input.Type == "Payment")
        //{
        //    UpdateAmount(input.SubAccountID, -girodata.Amount, ctx);
        //    UpdateAmount(girodata.SubAccountID, girodata.Amount, ctx);
        //    //UpdateAmountSupplier(input["giroNo"], -girodata.Amount,ctx);
        //}
        //else if (input.Type == "Receipt")
        //{
        //    UpdateAmount(input.SubAccountID, girodata.Amount, ctx);
        //    UpdateAmount(girodata.SubAccountID, -girodata.Amount, ctx);
        //}

        //UpdateGiroStatus(girodata.GiroNo, "Closed", ctx);
        //InsertJournalTransaction(ctx, http, input);

        if (input.Type == "Payment")
        {
            if (input.SubAccountID != null)
            {
                string said = input.SubAccountID;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= input.Amount;
                }

            }
        }
        else if (input.Type == "Receive")
        {
            if (input.SubAccountID != null)
            {
                string said = input.SubAccountID;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += input.Amount;
                }

            }
        }
        
        

        Helper.InsertLog("FIN-Transaction-GiroClearing-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Cancel(GiroClearingDTO input, GiroClearingDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        if (input.Type == "Payment")
        {
            if (input.SubAccountID != null)
            {
                string said = input.SubAccountID;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount += input.Amount;
                }

            }
        }
        else if (input.Type == "Receive")
        {
            if (input.SubAccountID != null)
            {
                string said = input.SubAccountID;
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                if (cb != null)
                {
                    cb.Amount -= input.Amount;
                }

            }
        }
          
//        string transactionid = oldInput.ClearingId;
//        EntityKey key = new EntityKey("EasyEntities.GiroClearing", "ClearingID", transactionid);


//        GiroClearing giroclearing = (GiroClearing)ctx.GetObjectByKey(key);
//        var girodata = (from a in ctx.Giro where a.Type == input.Type && a.GiroNo == input.GiroNo select a).FirstOrDefault();
//        if (giroclearing != null)
//        {
//            giroclearing.Status = "Cancel";
//        }
        
//        if (input.Type == "Payment")
//        {
//            //UpdateAmount(input.SubAccountID, -input.Amount, ctx);
//            UpdateAmount(oldInput.SubAccountID, oldInput.Amount, ctx);
//            UpdateAmount(girodata.SubAccountID, -oldInput.Amount, ctx);
//            //UpdateAmountSupplier(input["giroNo"], -girodata.Amount,ctx);
//        }
//        else if (input.Type == "Receipt")
//        {
//            //UpdateAmount(input.SubAccountID, input.Amount, ctx);
//            UpdateAmount(oldInput.SubAccountID, -oldInput.Amount, ctx);
//            UpdateAmount(girodata.SubAccountID, oldInput.Amount, ctx);
//        }

//        UpdateGiroStatus(oldInput.GiroNo, "Open", ctx);
//        //UpdateGiroStatus(input.GiroNo, "Closed", ctx);

//        CancelJournalTransaction(ctx, oldInput);
//        Helper.InsertLog("FIN-Transaction-GiroClearing-Update", "Old", ctx, oldInput, user.UserID);
//        Helper.InsertLog("FIN-Transaction-GiroClearing-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();

    }
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                SELECT gc.Id, gc.ClearingID, gc.TransactionType as Type, gc.GiroNo, gc.UserID, gc.SubAccountID, gc.BankName, gc.Amount, gc.[Status], 
                CONVERT(varchar(10), gc.[Date], 120) as [Date], 
                (CASE WHEN gc.TransactionType = 'Receive' THEN CONVERT(varchar(10), cbr.DueDateGiro, 120)
                ELSE CASE WHEN gc.TransactionType = 'Payment' THEN CONVERT(varchar(10), cbp.DueDateGiro,120) END 
                END) as DueDate,
                Row_number() over(order by gc.Date desc, gc.GiroNo desc) as row FROM GiroClearing gc
                LEFT OUTER JOIN CashBankReceives cbr ON cbr.GiroNo = gc.GiroNo
                LEFT OUTER JOIN CashBankPayments cbp ON cbp.GiroNo = gc.GiroNo 
                WHERE " + Helper.FieldFilterQuery(
                        option, "gc.Status", "gc.ClearingID", "gc.TransactionType", "gc.SubAccountID", "gc.Amount", "gc.Date"
                    ), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
//    private void GetDataSelected(HttpContext context)
//    {
//        //Dictionary<string, string> input = json.Deserialize<Dictionary<string, string>>(context.Request["input"]);
//        string transactionid = context.Request.QueryString["id"].Trim();
//        using (SqlConnection con = new SqlConnection(conString))
//        {
//            DataTable dt = Helper.NewDataTable("SELECT a.ClearingID, a.[Type], a.GiroNo, a.UserID, a.SubAccountID, a.BankName, a.Amount, CONVERT(varchar(10), a.[Date], 120) as [Date], CONVERT(varchar(10), b.DueDate, 120) as DueDate FROM [GiroClearing] a, Giro b where a.ClearingID = '" + transactionid + "' and a.GiroNo = b.GiroNo;", con);
//            context.Response.Write(Helper.DataTabletoJSON(dt));
//        }
//    }
    protected override void GetList(string list, GiroClearingDTO input, ListOption option, int detailIndex)
    {
        base.GetList(list, input, option, detailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "girono")
            {
                if (input.Type == "Receive")
                {
                    dt = Helper.NewDataTable(Helper.PagingQuery(@"
	                    SELECT GiroNo, CONVERT(VARCHAR,DueDateGiro,120) as DueDate, TransactionType, ReceiveGiro as Amount, SubAccountGiro as SubAccountID,
                        ROW_NUMBER() OVER (ORDER BY GiroNo) as row from CashBankReceives where GiroNo <> 0
                        and " + Helper.FieldFilterQuery(option, "GiroNo", "SubAccountGiro", "DueDateGiro", "TransactionType", "ReceiveGiro"), option), con);
                }

                if (input.Type == "Payment")
                {
                    dt = Helper.NewDataTable(Helper.PagingQuery(@"
	                    SELECT GiroNo, CONVERT(VARCHAR,DueDateGiro,120) as DueDate, PaymentGiro as Amount, SubAccountGiro as SubAccountID,
                        ROW_NUMBER() OVER (ORDER BY GiroNo) as row from CashBankPayments where GiroNo <> 0
                        and " + Helper.FieldFilterQuery(option, "GiroNo", "SubAccountGiro", "DueDateGiro", "PaymentGiro"), option), con);
                }
            }
            else if (list == "account")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"
                        select a.SubAccountID, a.Name as SubAccountName, a.CurrencyID, a.Amount,
                        ROW_NUMBER() OVER (ORDER BY a.SubAccountID) as row                        
                        from CashBank a where "+
                        Helper.FieldFilterQuery(option, "a.SubAccountID", "a.Name", "a.CurrencyID", "a.Amount"), option), con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
//            if (list == "girono")
//            {

//                dt = Helper.NewDataTable(@"
//	                SELECT GiroNo, SubAccountID, BankName, CONVERT(VARCHAR,DueDate,120) as DueDate, Type, Amount from Giro
//                    where GiroNo='" + Helper.EscapeSQLString(id) + "'", con);
//            }
            if (list == "account")
            {
                dt = Helper.NewDataTable(@"
                        select a.SubAccountID, a.Name as SubAccountName, a.CurrencyID, a.Amount from CashBank a where 
                        a.SubAccountID='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    //protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, GiroClearingDTO input, GiroClearingDTO oldInput)
    //{
    //    base.CheckConcurrency(err, ctx, input, oldInput);

    //    string transactionID = oldInput.ClearingId;
    //    GiroClearing giroclearing = (from a in ctx.GiroClearing where a.ClearingID == transactionID select a).FirstOrDefault();

    //    if (giroclearing.Type != oldInput.Type || giroclearing.Date != DateTime.Parse(oldInput.Date) ||
    //        giroclearing.SubAccountID != oldInput.SubAccountID || giroclearing.GiroNo != oldInput.GiroNo)
    //    {
    //        Helper.ConcurrencyError(err);
    //    }
    //}

    protected override void Validate(ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, GiroClearingDTO input, GiroClearingDTO oldInput)
    {

        string transactiono = input.ClearingId;
        var girono = (from a in ctx.GiroClearing where a.ClearingID == transactiono select a).FirstOrDefault();

        if (girono != null)
        {
            Helper.AddError(err, "cleringid", "Clearing ID " + transactiono + " has been registered!");
        }
        
        
        base.Validate(mode, err, ctx, input, oldInput);
        if (input.GiroNo == "")
        {
            Helper.AddError(err, "girono", "Giro No is required!");
        }

    //    else
    //    {
    //        string giro = input.GiroNo;
    //        var datagiro = from a in ctx.Giro where a.GiroNo == giro select a;
    //        var firstdatagiro = datagiro.FirstOrDefault();

    //        if (datagiro.Count() <= 0)
    //        {
    //            Helper.AddError(err, "girono", "Giro does not exist!");
    //        }
    //        else
    //        {
    //            if (firstdatagiro.Type != input.Type)
    //            {
    //                Helper.AddError(err, "type", "The giro you selected is not of type " + input.Type + "!");
    //            }
    //            if (firstdatagiro.Status == "Closed" && firstdatagiro.GiroNo != input.GiroNo)
    //            {
    //                Helper.AddError(err, "status", "The giro you selected is already closed!");
    //            }
    //            if (firstdatagiro.Status == "Cancel")
    //            {
    //                Helper.AddError(err, "status", "The giro you selected is already canceled!");
    //            }
    //        }

    //        if (input.Type == "Select")
    //        {
    //            Helper.AddError(err, "type", "Type must be selected");
    //        }
    //        else
    //        {
        if (input.SubAccountID == "")
        {
            Helper.AddError(err, "subaccount", "Sub Account is required");
        }
    //            else
    //            {
    //                string subacc = input.SubAccountID;
    //                var subaccount = from a in ctx.CashBank
    //                                 join b in ctx.SubAccount on a.SubAccountID equals b.SubAccountID
    //                                 where a.SubAccountID == subacc
    //                                 select new
    //                                 {
    //                                     a,
    //                                     b.Name
    //                                 };

    //                var firstsubaccount = subaccount.FirstOrDefault();

    //                if (subaccount.Count() <= 0)
    //                {
    //                    Helper.AddError(err, "subaccount", "Sub Account does not exist!");
    //                }
    //                else
    //                {
    //                    if (firstsubaccount.a.Type != "Bank")
    //                    {
    //                        Helper.AddError(err, "subaccount", "The Sub Account you choose is not of type Bank!");
    //                    }
    //                    else if (input.Type == "Payment" && mode == ValidateMode.Insert)
    //                    {
    //                        if (Convert.ToDecimal(input.Amount) > firstsubaccount.a.Amount && firstsubaccount.a.BankLoans != 1)
    //                        {
    //                            //Helper.AddError(err, "subaccount", firstsubaccount.a.Amount.ToString());
    //                            Helper.AddError(err, "subaccount", "The amount of giro payment is greater than your Sub Account amount!");
    //                        }
    //                    }
    //                    else if (input.Type == "Receipt" && mode == ValidateMode.Insert)
    //                    {
    //                        var sar = (from a in ctx.CashBank where a.SubAccountID == firstdatagiro.SubAccountID select a).FirstOrDefault();
    //                        if (sar != null && Convert.ToDecimal(input.Amount) > sar.Amount && sar.BankLoans != 1)
    //                        {
    //                            Helper.AddError(err, "subaccount", "The amount of subaccount " + sar.SubAccountID + " of your giro is less than " + Convert.ToDecimal(input.Amount).ToString("#,##0.00") + "!");
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //    }

    //    string girono = input.GiroNo;
    //    DateTime date = Convert.ToDateTime(input.Date);
    //    if (input.Date == "")
    //    {
    //        Helper.AddError(err, "date", "Date must be filled!");
    //    }
    //    else
    //    {
    //        Helper.CheckPosting(ctx, date, "FIN", err);
    //    }
    //    var giro1 = from a in ctx.Giro where a.GiroNo == girono && a.DueDate > date select a;

    //    if (giro1.Count() > 0)
    //    {
    //        Helper.AddError(err, "date", "Date must be greater than or equal to the Giro Due Date!");
    //    }
    }
    //protected void UpdateAmount(string subAccountID, decimal amountChange, EasyEntities ctx)
    //{
    //    var dt = (from a in ctx.CashBank where a.SubAccountID == subAccountID select a).FirstOrDefault();
    //    decimal newAmount = dt.Amount + amountChange;
    //    var updatecashbank = from a in ctx.CashBank where a.SubAccountID == subAccountID select a;

    //    foreach (var updatecash in updatecashbank)
    //    {
    //        updatecash.Amount = newAmount;
    //    }
    //}

    //protected void UpdateAmountSupplier(string giroid, decimal amount, EasyEntities ctx)
    //{
    //    var cashbankpayment = (from a in ctx.Giro join b in ctx.CashBankPayments on a.TransactionID equals b.TransactionNo where a.GiroNo == giroid select b).FirstOrDefault();
    //    //if (cashbankpayment != null)
    //    //{
    //    if (cashbankpayment.TransactionType.ToUpper() != "ETC")
    //    {
    //        string supplierid = cashbankpayment.SupplierId;
    //        string currencyid = cashbankpayment.CurrencyId;
    //        var supplier = (from a in ctx.SupplierDetail where a.SupplierID == supplierid && a.CurrencyID == currencyid select a).FirstOrDefault();
    //        supplier.Amount = supplier.Amount + amount;
    //    }
    //    //}
    //}

    //protected void UpdateGiroStatus(string giroNo, string status, EasyEntities ctx)
    //{
    //    var giros = from a in ctx.Giro where a.GiroNo == giroNo select a;

    //    foreach (var giro in giros)
    //    {
    //        giro.Status = status;
    //    }
    //}

    //void InsertJournalTransaction(EasyEntities ctx, HttpContext context, GiroClearingDTO input)
    //{
    //    string transactionid = Helper.GenerateID("JournalTransaction", "TransactionID", "JRL", input.Date, user.Initial.ToString());
    //    //JournalMaster jm = (from a in ctx.JournalMaster where a.Status != "Closed" select a).FirstOrDefault();
    //    List<Dictionary<string, string>> jnd = new List<Dictionary<string, string>>();
    //    Dictionary<string, string> tmp;

    //    string gn = input.GiroNo;

    //    if (input.Type == "Receipt")
    //    {
    //        CashBankReceives cbr = (from a in ctx.CashBankReceives where a.Giro == gn || a.Giro2 == gn || a.Giro3 == gn select a).FirstOrDefault();

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", input.SubAccountID);
    //        tmp.Add("Remark", "Giro Clearing No: " + input.ClearingId + ", Date: " + input.Date);

    //        decimal ga = 0;
    //        if (cbr.Giro == gn)
    //        {
    //            ga = cbr.ReceiveGiro;
    //        }
    //        else if (cbr.Giro2 == gn)
    //        {
    //            ga = cbr.ReceiveGiro2;
    //        }
    //        else if (cbr.Giro3 == gn)
    //        {
    //            ga = cbr.ReceiveGiro3;
    //        }
    //        tmp.Add("Debits", ga.ToString());
    //        tmp.Add("Credits", "0");
    //        tmp.Add("DetailID", "1");
    //        jnd.Add(tmp);

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", cbr.SubAccountGiro);
    //        tmp.Add("Remark", "Giro Clearing No: " + input.ClearingId + ", Date: " + input.Date);
    //        tmp.Add("Debits", "0");
    //        tmp.Add("Credits", ga.ToString());
    //        tmp.Add("DetailID", "2");
    //        jnd.Add(tmp);

    //        ctx.JournalTransaction.AddObject(new JournalTransaction()
    //        {
    //            TransactionID = transactionid,
    //            JournalID = "",
    //            FromTransactionID = input.ClearingId,
    //            Date = DateTime.Parse(input.Date),
    //            UserID = Convert.ToInt32(user.UserID),
    //            PICID = Convert.ToInt32(user.UserID),
    //            Status = "Open"
    //        });

    //        for (int i = 0; i < jnd.Count; i++)
    //        {
    //            ctx.JournalDetailTransaction.AddObject(new JournalDetailTransaction()
    //            {
    //                TransactionID = transactionid,
    //                ID = Convert.ToByte(i + 1),
    //                TransactionNo = input.ClearingId,
    //                FromTransactionDetailID = jnd[i]["DetailID"],
    //                SubAccountID = jnd[i]["SubAccountID"],
    //                Remark = jnd[i].ContainsKey("Remark") ? jnd[i]["Remark"] : "",
    //                Debits = Convert.ToDecimal(jnd[i]["Debits"]),
    //                Credits = Convert.ToDecimal(jnd[i]["Credits"])
    //            });
    //        }

    //        Helper.UpdateAccountAmount("insert", ctx, jnd);
    //    }
    //    else if (input.Type == "Payment")
    //    {
    //        CashBankPayments cbp = (from a in ctx.CashBankPayments where a.Giro == gn || a.Giro2 == gn || a.Giro3 == gn select a).FirstOrDefault();

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", input.SubAccountID);
    //        tmp.Add("Remark", "Giro Clearing No: " + input.ClearingId + ", Date: " + input.Date);

    //        decimal ga = 0;
    //        string sa = "";
    //        if (cbp.Giro == gn)
    //        {
    //            ga = cbp.PaymentGiro;
    //            sa = cbp.SubAccountGiro;
    //        }
    //        else if (cbp.Giro2 == gn)
    //        {
    //            ga = cbp.PaymentGiro2;
    //            sa = cbp.SubAccountGiro2;
    //        }
    //        else if (cbp.Giro3 == gn)
    //        {
    //            ga = cbp.PaymentGiro3;
    //            sa = cbp.SubAccountGiro3;
    //        }
    //        tmp.Add("Debits", "0");
    //        tmp.Add("Credits", ga.ToString());
    //        tmp.Add("DetailID", "1");
    //        jnd.Add(tmp);

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", sa);
    //        tmp.Add("Remark", "Giro Clearing No: " + input.ClearingId + ", Date: " + input.Date);
    //        tmp.Add("Debits", ga.ToString());
    //        tmp.Add("Credits", "0");
    //        tmp.Add("DetailID", "2");
    //        jnd.Add(tmp);

    //        ctx.JournalTransaction.AddObject(new JournalTransaction()
    //        {
    //            TransactionID = transactionid,
    //            JournalID = "",
    //            FromTransactionID = input.ClearingId,
    //            Date = DateTime.Parse(input.Date),
    //            UserID = Convert.ToInt32(user.UserID),
    //            PICID = Convert.ToInt32(user.UserID),
    //            Status = "Open"
    //        });

    //        for (int i = 0; i < jnd.Count; i++)
    //        {
    //            ctx.JournalDetailTransaction.AddObject(new JournalDetailTransaction()
    //            {
    //                TransactionID = transactionid,
    //                ID = Convert.ToByte(i + 1),
    //                TransactionNo = input.ClearingId,
    //                FromTransactionDetailID = jnd[i]["DetailID"],
    //                SubAccountID = jnd[i]["SubAccountID"],
    //                Remark = jnd[i].ContainsKey("Remark") ? jnd[i]["Remark"] : "",
    //                Debits = Convert.ToDecimal(jnd[i]["Debits"]),
    //                Credits = Convert.ToDecimal(jnd[i]["Credits"])
    //            });
    //        }

    //        Helper.UpdateAccountAmount("insert", ctx, jnd);
    //    }
    //}

    //void CancelJournalTransaction(EasyEntities ctx, GiroClearingDTO input)
    //{
    //    string transactionid = input.ClearingId;

    //    JournalTransaction journalTransaction = (from a in ctx.JournalTransaction
    //                                             join b in ctx.JournalDetailTransaction on a.TransactionID equals b.TransactionID
    //                                             where b.TransactionNo == transactionid
    //                                             select a).FirstOrDefault();
    //    var sods = from a in ctx.JournalDetailTransaction where a.TransactionNo == transactionid select a;
    //    GiroClearing gc = (from a in ctx.GiroClearing where a.ClearingID == transactionid select a).FirstOrDefault();

    //    if (journalTransaction == null || journalTransaction.Status == "Closed" || journalTransaction.Status == "Cancel")
    //    {
    //        return;
    //    }

    //    List<Dictionary<string, string>> jnd = new List<Dictionary<string, string>>();
    //    //List<Dictionary<string, string>> ojnd = new List<Dictionary<string, string>>();

    //    Dictionary<string, string> tmp;

    //    if (input.Type == "Receipt")
    //    {
    //        CashBankReceives cbr = (from a in ctx.CashBankReceives where a.Giro == gc.GiroNo || a.Giro2 == gc.GiroNo || a.Giro3 == gc.GiroNo select a).FirstOrDefault();

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", gc.SubAccountID);
            
    //        decimal ga = 0;
    //        if (cbr.Giro == gc.GiroNo)
    //        {
    //            ga = cbr.ReceiveGiro;
    //        }
    //        else if (cbr.Giro2 == gc.GiroNo)
    //        {
    //            ga = cbr.ReceiveGiro2;
    //        }
    //        else if (cbr.Giro3 == gc.GiroNo)
    //        {
    //            ga = cbr.ReceiveGiro3;
    //        }
    //        tmp.Add("Debits", "0");
    //        tmp.Add("Credits", ga.ToString());
    //        tmp.Add("DetailID", "1");
    //        jnd.Add(tmp);

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", cbr.SubAccountGiro);
    //        tmp.Add("Debits", ga.ToString());
    //        tmp.Add("Credits", "0");
    //        tmp.Add("DetailID", "2");
    //        jnd.Add(tmp);
            
    //        journalTransaction.Status = "Cancel";
    //        Helper.UpdateAccountAmount("cancel", ctx, jnd);
    //    }
    //    else if (input.Type == "Payment")
    //    {
    //        CashBankPayments cbp = (from a in ctx.CashBankPayments where a.Giro == gc.GiroNo || a.Giro2 == gc.GiroNo || a.Giro3 == gc.GiroNo select a).FirstOrDefault();

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", gc.SubAccountID);

    //        decimal ga = 0;
    //        string sa = "";
    //        if (cbp.Giro == gc.GiroNo)
    //        {
    //            ga = cbp.PaymentGiro;
    //            sa = cbp.SubAccountGiro;
    //        }
    //        else if (cbp.Giro2 == gc.GiroNo)
    //        {
    //            ga = cbp.PaymentGiro2;
    //            sa = cbp.SubAccountGiro2;
    //        }
    //        else if (cbp.Giro3 == gc.GiroNo)
    //        {
    //            ga = cbp.PaymentGiro3;
    //            sa = cbp.SubAccountGiro3;
    //        }
    //        tmp.Add("Debits", ga.ToString());
    //        tmp.Add("Credits", "0");
    //        tmp.Add("DetailID", "1");
    //        jnd.Add(tmp);

    //        tmp = new Dictionary<string, string>();
    //        tmp.Add("SubAccountID", sa);
    //        tmp.Add("Debits", "0");
    //        tmp.Add("Credits", ga.ToString());
    //        tmp.Add("DetailID", "2");
    //        jnd.Add(tmp);
            
    //        journalTransaction.Status = "Cancel";
    //        Helper.UpdateAccountAmount("cancel", ctx, jnd);
    //    }
    //}

}