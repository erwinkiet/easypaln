﻿<%@ WebHandler Language="C#" Class="CashBankReceiveHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CashBankReceiveDTO
{
    public int Id {get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string Type { get; set; }
    public string CustomerId { get; set; }
    public string CustomerName { get; set; }
    public string CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public decimal Conversion { get; set; }
    public string CashAccountId { get; set; }
    public string CashAccountName { get; set; }
    public decimal CashAmount { get; set; }
    public string BankAccountId { get; set; }
    public string BankAccountName { get; set; }
    public decimal BankAmount { get; set; }
    public string GiroAccountId { get; set; }
    public string GiroAccountName { get; set; }
    public decimal GiroAmount { get; set; }
    public string GiroNo { get; set; }
    public string GiroDueDate { get; set; }
    public string Remark { get; set; }
    public string LastModifiedBy { get; set; }
    public string CreatedBy { get; set; }
    public string Status { get; set; }
    public decimal GrandTotal { get; set; }
    public byte[] RowVersion { get; set; } 
    public List<CashBankReceiveItemDTO> Items { get; set; } 
}

class CashBankReceiveItemDTO
{
    public string DetailId { get; set; }
    public string SubAccountId { get; set; }
    public string SubAccountName {get; set; }
    public string CurrencyId { get; set; }
    public decimal PayAmount { get; set; }
    public string EtcAccountId { get; set; }
    public decimal Conversion { get; set; }
    public string TypeDetail { get; set; }
    public decimal EtcAmount { get; set; }
    public string status { get; set; }
    public string Remark { get; set; }
    public decimal SubTotal { get; set; }
}

class CashBankReceiveHandler : BaseHandler<CashBankReceiveDTO>
{
    protected override void Insert(CashBankReceiveDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string transactionNo = input.TransactionNo;
        string duedate = input.GiroDueDate;
        string currencyID = input.CurrencyId;
        string customerName = input.CustomerName;
        string typetransac = input.Type;
        DateTime? dueDate = null;
        string status = "Open";

        if (duedate == null || duedate == "null" || duedate == "" || duedate == "undefined")
        {
            dueDate = null;
        }
        else
        {
            dueDate = DateTime.Parse(input.GiroDueDate);
        }

        ctx.CashBankReceives.AddObject(new CashBankReceives()
        {
            TransactionType = typetransac,
            TransactionNo = input.TransactionNo,
            TransactionDate = DateTime.Parse(input.TransactionDate),
            CustomerId = input.CustomerId,
            CustomerName = input.CustomerName,
            CurrencyId = input.CurrencyId,
            Conversion = Convert.ToDecimal(input.Conversion),
            ReceiveCash = Convert.ToDecimal(input.CashAmount),
            SubAccountCash = input.CashAccountId,
            ReceiveGiro = Convert.ToDecimal(input.GiroAmount),
            SubAccountGiro = input.GiroAccountId,
            ReceiveBank = Convert.ToDecimal(input.BankAmount),
            SubAccountBank = input.BankAccountId,
            Giro = input.GiroNo,
            DueDateGiro = DateTime.Parse(duedate),
            Remark = input.Remark,
            CreatedBy = Convert.ToInt32(user.UserID),
            Status = status,
            GrandTotal = Convert.ToDecimal(input.GrandTotal),
            InputDate = DateTime.Now
        });

        for (int i = 0; i < input.Items.Count; i++)
        {
            ctx.CashBankReceiveDetail.AddObject(new CashBankReceiveDetail()
            {
                CashBankReceiveId = Helper.GetNextID("CashBankReceive"),
                SubAccountId = input.Items[i].SubAccountId,
                CurrencyId = input.Items[i].CurrencyId,
                PayAmount = Convert.ToDecimal(input.Items[i].PayAmount),
                Conversion = Convert.ToDecimal(input.Items[i].Conversion),
                //EtcAccountId = input.Items[i].EtcAccountId,
                EtcAmount = Convert.ToDecimal(input.Items[i].EtcAmount),
                Remark = input.Items[i].Remark,
                //Status = status,
                //Type = input.Items[i].TypeDetail
            });

            string said = input.Items[i].SubAccountId.ToString();
            CashBank cbr = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

            if (cbr != null)
            {
                cbr.Amount += Convert.ToDecimal(input.Items[i].PayAmount);
            }

            string etcid = input.Items[i].EtcAccountId;
            CashBank cbr2 = (from a in ctx.CashBank where a.SubAccountID == etcid select a).FirstOrDefault();

            if (cbr2 != null)
            {
                cbr2.Amount += Convert.ToDecimal(input.Items[i].EtcAmount);
            }


            //string cus = input.CustomerId;
            //CustomerDetail cus2 = (from a in ctx.CustomerDetail where a.CustomerID == cus select a).FirstOrDefault();

            //if (cus != null)
            //{
            //    cus2.Amount  -= input.Items[i].PayAmount;
            //    cus2.IdrAmount -= input.Items[i].PayAmount * Convert.ToDecimal(input.Items[i].Conversion);
            //}

        }

        cashbank(ctx, input, null, null, 0, null, 0, null, 0, "Insert");
        Helper.InsertLog("FIN-Transaction-CashBankPayment-Insert", "", ctx, input, Convert.ToInt32(user.UserID));

        ctx.SaveChanges();
    }

    protected override void Update(CashBankReceiveDTO input, CashBankReceiveDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        EntityKey key = new EntityKey("EasyEntities.CashBankReceives", "Id", input.Id);

        CashBankReceives cbp = (CashBankReceives)ctx.GetObjectByKey(key);
        var cbrds = from a in ctx.CashBankReceiveDetail where a.CashBankReceiveId == input.Id orderby a.Id descending select a;

        int cbrid = input.Id;
        string cashAccountID = input.CashAccountId;
        string currencyID = input.CurrencyId;
        string oldGiro = oldInput.GiroNo;
        string oldCurrencyID = oldInput.CurrencyId;
        string oldCustomerID = oldInput.CurrencyId;
        string oldCashAccountID = oldInput.CashAccountId;
        decimal oldCashAmount = Convert.ToDecimal(oldInput.CashAmount);
        string oldBankAccountID = oldInput.BankAccountId;
        decimal oldBankAmount = Convert.ToDecimal(oldInput.BankAmount);
        string oldGiroAccountID = oldInput.GiroAccountId;
        decimal oldGiroAmount = Convert.ToDecimal(oldInput.GiroAmount);
        string duedate = input.GiroDueDate;
        string status = "Open";

        DateTime? dueDate = null;

        if (duedate == null || duedate == "null" || duedate == "" || duedate == "undefined")
        {
            dueDate = null;
        }
        else
        {
            dueDate = DateTime.Parse(input.GiroDueDate);
        }

        if (cbp != null && cbrds != null)
        {
            cbp.TransactionNo = input.TransactionNo;
            cbp.TransactionDate = DateTime.Parse(input.TransactionDate);
            cbp.CustomerId = input.CustomerId;
            cbp.ReceiveCash = Convert.ToDecimal(input.CashAmount);
            cbp.SubAccountCash = input.CashAccountId;
            cbp.ReceiveGiro = Convert.ToDecimal(input.GiroAmount);
            cbp.SubAccountGiro = input.GiroAccountId;
            cbp.ReceiveBank = Convert.ToDecimal(input.BankAmount);
            cbp.SubAccountBank = input.BankAccountId;
            cbp.Giro = input.GiroNo;
            cbp.DueDateGiro = Convert.ToDateTime(dueDate);
            cbp.TransactionType = input.Type;
            cbp.CurrencyId = input.CurrencyId;
            cbp.Remark = input.Remark;
            cbp.GrandTotal = Convert.ToDecimal(input.GrandTotal);
            cbp.ModifiedBy = Convert.ToInt32(user.UserID);
            cbp.ModifiedDate = DateTime.Now;
            cbp.Status = status;

            foreach (var cbpd in cbrds)
            {
                var tmp = from a in input.Items where (a.DetailId) == cbpd.Id.ToString() select a;

                if (tmp.Count() <= 0)
                {
                    CashBank cbd = (from a in ctx.CashBank where a.SubAccountID == cbpd.SubAccountId select a).FirstOrDefault();

                    if (cbd != null)
                    {
                        cbd.Amount += cbpd.PayAmount;
                    }

                    CashBank cbd2 = (from a in ctx.CashBank where a.SubAccountID == cbpd.SubAccountId select a).FirstOrDefault();

                    if (cbd2 != null)
                    {
                        cbd2.Amount += cbpd.EtcAmount;
                    }

                    ctx.DeleteObject(cbpd);
                }
            }

            for (int i = 0; i < input.Items.Count; i++)
            {
                int dID = Convert.ToInt32(input.Items[i].DetailId);

                if (dID == 0)
                {
                    ctx.CashBankReceiveDetail.AddObject(new CashBankReceiveDetail()
                    {
                        CashBankReceiveId = cbp.Id,                        
                        SubAccountId = input.Items[i].SubAccountId,
                        CurrencyId = input.Items[i].CurrencyId,
                        PayAmount = Convert.ToDecimal(input.Items[i].PayAmount),
                        Conversion = Convert.ToDecimal(input.Items[i].Conversion),
                        //EtcAccountId = input.Items[i].EtcAccountId,
                        EtcAmount = Convert.ToDecimal(input.Items[i].EtcAmount),
                        Remark = input.Items[i].Remark
                    });

                    string said = input.Items[i].SubAccountId.ToString();
                    CashBank cbd = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                    if (cbd != null)
                    {
                        cbd.Amount -= Convert.ToDecimal(input.Items[i].PayAmount);
                    }

                    //string cus = input.CustomerId.ToString();
                    //CustomerDetail cus2 = (from a in ctx.CustomerDetail where a.CustomerID == cus select a).FirstOrDefault();

                    //if (cus != null)
                    //{
                    //    cus2.Amount -= Convert.ToDecimal(input.Items[i].PayAmount);
                    //    cus2.IdrAmount -= Convert.ToDecimal(input.Items[i].PayAmount) * Convert.ToDecimal(input.Items[i].Conversion);
                    //}
                }
                else
                {
                    CashBankReceiveDetail cbpd = (from a in ctx.CashBankReceiveDetail where a.CashBankReceiveId == input.Id && a.Id == dID select a).FirstOrDefault();
                    if (cbpd != null)
                    {
                        string said = input.Items[i].SubAccountId.ToString();
                        CashBank cbdo = (from a in ctx.CashBank where a.SubAccountID == cbpd.SubAccountId select a).FirstOrDefault();
                        CashBank cbdn = (from a in ctx.CashBank where a.SubAccountID == said select a).FirstOrDefault();

                        if (cbdo != null)
                        {
                            cbdo.Amount += cbpd.PayAmount;
                        }
                        if (cbdn != null)
                        {
                            cbdn.Amount -= Convert.ToDecimal(input.Items[i].PayAmount);
                        }

                        string etcid = input.Items[i].SubAccountId;
                        CashBank cbdo2 = (from a in ctx.CashBank where a.SubAccountID == cbpd.SubAccountId select a).FirstOrDefault();
                        CashBank cbdn2 = (from a in ctx.CashBank where a.SubAccountID == etcid select a).FirstOrDefault();

                        if (cbdo2 != null)
                        {
                            cbdo2.Amount += cbpd.EtcAmount;
                        }
                        if (cbdn2 != null)
                        {
                            cbdn2.Amount -= Convert.ToDecimal(input.Items[i].EtcAmount);
                        }
                        cbpd.SubAccountId = input.Items[i].SubAccountId;
                        cbpd.CurrencyId = input.Items[i].CurrencyId;
                        cbpd.PayAmount = Convert.ToDecimal(input.Items[i].PayAmount);
                        cbpd.Conversion = Convert.ToDecimal(input.Items[i].Conversion);
                        cbpd.EtcAmount = Convert.ToDecimal(input.Items[i].EtcAmount);
                        cbpd.Remark = input.Items[i].Remark;
                        //cbpd.EtcAccountId = input.Items[i].EtcAccountId;

                    }
                }
            }
        }
        cashbank(ctx, input, oldCurrencyID, oldCashAccountID, oldCashAmount, oldBankAccountID, oldBankAmount, oldGiroAccountID, oldGiroAmount, "update");
        //giro(ctx, input, oldGiro, "update", cbrid);
        Helper.InsertLog("FIN-Transaction-CashBankReceive-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("FIN-Transaction-CashBankReceive-Update", "", ctx, input, Convert.ToInt32(user.UserID));

        ctx.SaveChanges();
    }

    protected override void Cancel(CashBankReceiveDTO input, CashBankReceiveDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);
        
        string cbpno = input.TransactionNo;
        string oldGiro = oldInput.GiroNo;
        string oldCurrencyID = oldInput.CurrencyId;
        string oldCustomerID = oldInput.CustomerId;
        string oldCashAccountID = oldInput.CashAccountId;
        decimal oldCashAmount = Convert.ToDecimal(oldInput.CashAmount);
        string oldBankAccountID = oldInput.BankAccountId;
        decimal oldBankAmount = Convert.ToDecimal(oldInput.BankAmount);
        string oldGiroAccountID = oldInput.GiroAccountId;
        decimal oldGiroAmount = Convert.ToDecimal(oldInput.GiroAmount);


        EntityKey key = new EntityKey("EasyEntities.CashBankReceives", "Id", input.Id);


        CashBankReceives cbp = (CashBankReceives)ctx.GetObjectByKey(key);
        var cbpds = from a in ctx.CashBankReceiveDetails where a.CashBankReceiveId == input.Id select a;

        cbp.Status = "Cancel";

        foreach (var cbpd in cbpds)
        {
            CashBank cbd = (from a in ctx.CashBank where a.SubAccountID == cbpd.SubAccountId select a).FirstOrDefault();

            if (cbd != null)
            {
                cbd.Amount -= cbpd.PayAmount;
            }

            CashBank cbd2 = (from a in ctx.CashBank where a.SubAccountID == cbpd.SubAccountId select a).FirstOrDefault();

            if (cbd2 != null)
            {
                cbd2.Amount -= cbpd.EtcAmount;
            }

            //CustomerDetail cus2 = (from a in ctx.CustomerDetail where a.Amount == cbpd.PayAmount select a).FirstOrDefault();

            //if (cus2 != null)
            //{
            //    cus2.Amount += cbpd.PayAmount;
            //    cus2.IdrAmount += cbpd.PayAmount;
            //}

        }

        cashbank(ctx, input, oldCurrencyID, oldCashAccountID, oldCashAmount, oldBankAccountID, oldBankAmount, oldGiroAccountID, oldGiroAmount, "cancel");
        Helper.InsertLog("FIN-Transaction-CashBankReceives-Cancel", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {

            DataTable dt = Helper.NewDataTable(@"select cbr.Id, cbr.transactionNo, CONVERT(varchar(10), cbr.transactionDate, 120) 
                                        as transactionDate,CONVERT(varchar(10), cbr.ModifiedDate, 120) as ModifiedDate, 
                                        cbr.RowVersion, cb.Name as SubAccountName,cbr.customerId, cbr.customerName, cbr.currencyId, cbr.conversion, cbr.receiveCash, 
                                        cbr.subAccountCash, cbr.receiveGiro, cbr.subAccountGiro,   
                                        cbr.receiveBank, cbr.subAccountBank, cbr.giro, CONVERT(varchar(10), cbr.DueDateGiro, 120) as dueDateGiro, 
                                        cbr.Transactiontype as type, cbr.remark, cbr.grandTotal, u.Name as createdBy, cbr.[status],
                                        cbr.ModifiedBy into #temptable1
                                        from CashBankReceives cbr inner join [User] u 
                                        on cbr.CreatedBy = u.UserID
                                        left outer join CashBankReceiveDetails cbrd
                                        on cbrd.CashBankReceiveId = cbr.Id
                                        left outer join CashBank cb
                                        on cbrd.SubAccountID = cb.SubAccountID

                                        select data.* from(select cbr.*, us.Name as ModifiedByName, ROW_NUMBER() OVER (ORDER BY transactionDate , Id DESC) as row
                                        from #temptable1 cbr left outer join
                                        [User] us 
                                        on cbr.ModifiedBy = us.UserID
                                        WHERE " + Helper.FieldFilterQuery(option, "transactionNo",
                                        "customerId", "Name", "CONVERT(varchar,TransactionDate,103)") + @") data
                                        WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"


                                        order by TransactionDate desc, [Id] desc

                                        drop table #temptable1", con);
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "items")
            {
                dt = Helper.NewDataTable(@"SELECT Id ID, subAccountId, EtcAccountId, currencyId, payAmount, conversion, etcAmount, remark 
                                                FROM CashBankReceiveDetails WHERE CashBankReceiveId='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, CashBankReceiveDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            string customerId = input.CustomerId;
            
            if (list == "customer")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT customerId, Name as customerName, ROW_NUMBER() OVER (ORDER BY CustomerId) as row
                                                              From Customer WHERE " + Helper.FieldFilterQuery(option, "CustomerID", "Name"), option), con);
            }
            if (list == "invoice")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT TransactionNo as invoiceNo, grandTotal as grandTotalInvoice,  ROW_NUMBER() OVER (ORDER BY CustomerId) as row FROM INVOICES
                                                                WHERE CustomerId = '" + customerId + @"' AND " + Helper.FieldFilterQuery(option, "TransactionNo", "grandTotal"), option), con);
            }
            else if (list == "currency")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT currencyId, Name as currencyName, conversion, 
                    ROW_NUMBER() OVER (ORDER BY CurrencyId) as row                 
                    FROM Currency  
                    WHERE " + Helper.FieldFilterQuery(option, "CurrencyId", "Name"), option), con);
            }
            else if (list == "cash" || list =="bank" || list =="giro")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"Select  subAccountId, Name as subAccountName,ROW_NUMBER() OVER (ORDER BY SubAccountId) as row From CashBank 
                     WHERE " + Helper.FieldFilterQuery(option, "SubAccountId", "Name"), option), con);
            }
            else if (list == "account")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"Select  subAccountId, Name as subAccountName,ROW_NUMBER() OVER (ORDER BY SubAccountId) as row From CashBank 
                     WHERE " + Helper.FieldFilterQuery(option, "SubAccountId", "Name"), option), con);
            }

            else if (list == "accountetc")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"Select  subAccountId, Name as subAccountName,ROW_NUMBER() OVER (ORDER BY SubAccountId) as row From CashBank 
                     WHERE " + Helper.FieldFilterQuery(option, "SubAccountId", "Name"), option), con);
            }
//            else if (list == "invoice")
//            {
//                dt = Helper.NewDataTable(Helper.PagingQuery(@"select inv.GrandTotal - ISNULL(mix.Tot,0) as remainingDebt, 
//                                       inv.Id as invoiceId, inv.CustomerId as customerId, inv.Total, CONVERT(varchar(10), 
//                                       inv.TransactionDate, 120) as transactionDate, inv.transactionNo, inv.Status, 
//                                       inv.Price, inv.Remark, inv.RemarkNote,inv.IsPpn, 
//                                       inv.InputDate, inv.CreatedBy,inv.FreightPriceDetailId,inv.BaseNumber, 
//                                       inv.GrandTotal, cus1.Name as customerName, cus.Amount, ROW_NUMBER() OVER (ORDER BY InvoiceId) as row from Invoices inv inner join 
//                                       CustomerDetail cus on cus.CustomerID = inv.CustomerId  inner join customer cus1 on cus.CustomerID = cus1.CustomerID left outer join 
//                                       (select SUM(cbrd.PayAmount+cbrd.EtcAmount) as Tot, cbrd.InvoiceId
//                                       from  CashBankReceives cbr inner join CashBankReceiveDetails cbrd on cbr.Id=cbrd.CashBankReceiveId
//                                       where cbr.Status<>'Cancel' and cbr.Status<>'Closed'
//                                       group by cbrd.InvoiceId) mix on inv.Id = mix.InvoiceId where inv.GrandTotal - ISNULL(mix.Tot,0) <> 0 and 
//                                       " + Helper.FieldFilterQuery(option, "Id"), option), con);
            //}
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

     protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (detail == "items")
            {
                dt = Helper.NewDataTable(@" SELECT Id ID, subAccountId, EtcAccountId, currencyId, payAmount, conversion, etcAmount, remark 
                                                FROM CashBankReceiveDetails WHERE CashBankReceiveId='" + Helper.EscapeSQLString(id) + "'", con);                
            }   
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    
    
    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, CashBankReceiveDTO input, CashBankReceiveDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.CashBankReceives where a.Id == oldInput.Id select a).FirstOrDefault();

        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }

    protected override void Validate(BaseHandler<CashBankReceiveDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, CashBankReceiveDTO input, CashBankReceiveDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);
        string duedate = input.GiroDueDate;

        int Id = Convert.ToInt32(input.Id);

        string transactionNo = input.TransactionNo;
        string date = input.TransactionDate;
        string transactionType = input.Type;
        string CustomerID = input.CustomerId;
        string CustomerName = input.CustomerName;
        string currencyID = input.CurrencyId;
        decimal Conversion = input.Conversion;
        string cashAccountID = input.CashAccountId;
        decimal receiveCash = input.CashAmount;
        string giroAccountID = input.GiroAccountId;
        decimal receiveGiro = input.GiroAmount;
        string giroNo = input.GiroNo;
        string bankAccountID = input.BankAccountId;
        decimal receiveBank = input.BankAmount;
        decimal total = input.GrandTotal;
        decimal summary = receiveCash + receiveGiro + receiveBank;
        
        if (transactionNo == "")
        {
            Helper.AddError(err, "TransactionNo", "Transaction No must not be empty!");
        } 
        
        if (receiveGiro != 0 && giroAccountID != "" && giroNo == "")
        {
            Helper.AddError(err, "giroNo", "Giro No must not be empty!");
        }

        if (receiveCash != 0 && cashAccountID == "")
        {
            Helper.AddError(err, "cashAccountID", "Cash Account ID must not be empty!");
        }

        if (receiveBank != 0 && bankAccountID == "")
        {
            Helper.AddError(err, "bankAccountID", "Bank Account ID must not be empty!");
        }

        if (receiveGiro != 0 && giroAccountID == "")
        {
            Helper.AddError(err, "giroAccountID", "Giro Account ID must not be empty!");
        }

        if (date == "")
        {
            Helper.AddError(err, "date", "Date must not be empty!");
        }
        else if (!Helper.IsDate(date))
        {
            Helper.AddError(err, "date", "Please enter date with correct format!");
        }

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (transactionType == "" || transactionType == "Select")
            {
                Helper.AddError(err, "transactionType", "Please choose the transaction type!");
            }
            else if (transactionType == "Invoice")
            {
                if (CustomerID == "")
                {
                    Helper.AddError(err, "CustomerID", "Customer ID must not be empty!");
                }
                else
                {
                    var sup = from a in ctx.Customer where a.CustomerID == CustomerID select a;
                    if (sup.Count() <= 0)
                    {
                        Helper.AddError(err, "towardID", "There is no Customer with ID " + CustomerID + "!");
                    }
                }
            }
            else if (transactionType == "Etc")
            {
                if (CustomerName == "")
                {
                    Helper.AddError(err, "CustomerName", "Supplier ID must not be empty!");
                }
            }

            if (Conversion == 0)
            {
                Helper.AddError(err, "Conversion", "Conversion must not be empty!");
            }

            if (currencyID == "")
            {
                Helper.AddError(err, "currencyID", "Currency must not be empty!");
            }
            else
            {
                var cur = from a in ctx.Currency where a.CurrencyID == currencyID select a;
                if (cur.Count() <= 0)
                {
                    Helper.AddError(err, "currencyID", "There is no currency with ID " + currencyID + "!");
                }

            }
            if (total <= 0)
            {
                Helper.AddError(err, "total", "Grand total must be greater than 0!");
            }
            else if (Math.Round(summary) != Math.Round(total))
            {
                Helper.AddError(err, "total", "sum cash, giro and bank must be equal to grand total! summary: " + input.CashAmount   + ", total: " + total);
            }
            else
            {
                CashBank cb = (from a in ctx.CashBank where a.SubAccountID == cashAccountID && (a.Type == "CashBank" || a.Type == "PettyCash") select a).FirstOrDefault();
                CashBank giro = (from a in ctx.CashBank where a.SubAccountID == giroAccountID && a.CurrencyID == currencyID && a.Type == "Bank" select a).FirstOrDefault();
                CashBank bank = (from a in ctx.CashBank where a.SubAccountID == bankAccountID && a.CurrencyID == currencyID && a.Type == "Bank" select a).FirstOrDefault();

                if (cb != null)
                {
                    if (mode == ValidateMode.Insert && cb.BankLoans == 0)
                    {
                    if (cb.Amount - receiveCash < 0)
                    {
                    //        Helper.AddError(err, "cashAmount", "Cash amount must not be greater than cash account's amount (" + cb.Amount.ToString("#,###.00") + ")!");
                        }
                    }
                    else if (mode == ValidateMode.Update && cb.BankLoans == 0)
                    {
                        CashBankReceives cbp = (from a in ctx.CashBankReceives where a.Id == Id select a).FirstOrDefault();

                        if (cb.SubAccountID == cbp.SubAccountCash)
                        {
                            if (cb.Amount + cbp.ReceiveCash - receiveCash < 0)
                            {
                                Helper.AddError(err, "cashAmount", "Cash amount must not be greater than cash account's amount (" + (cb.Amount + cbp.ReceiveCash).ToString("#,###.00") + ")!");
                            }
                        }
                        else
                        {
                            if (cb.Amount - receiveCash < 0)
                            {
                                Helper.AddError(err, "cashAmount", "Cash amount must not be greater than cash account's amount (" + cb.Amount.ToString("#,###.00") + ")!");
                            }
                        }
                    }
                }

                if (bank != null)
                {
                    if (mode == ValidateMode.Insert && bank.BankLoans == 0)
                    {
                        if (bank.Amount - receiveCash < 0)
                        {
                            Helper.AddError(err, "bankAmount", "First Bank amount must not be greater than bank account's amount (" + bank.Amount.ToString("#,###.00") + ")!");
                        }
                    }
                    else if (mode == ValidateMode.Update && bank.BankLoans == 0)
                    {
                        CashBankReceives bankp = (from a in ctx.CashBankReceives where a.Id == Id select a).FirstOrDefault();

                        if (bank.SubAccountID == bankp.SubAccountCash)
                        {
                            if (bank.Amount + bankp.ReceiveBank - receiveBank < 0)
                            {
                                Helper.AddError(err, "bankAmount", "First Bank amount must not be greater than bank account's amount (" + (bank.Amount + bankp.ReceiveBank).ToString("#,###.00") + ")!");
                            }
                        }
                        else
                        {
                            if (bank.Amount - receiveBank < 0)
                            {
                                Helper.AddError(err, "bankAmount", "First Bank amount must not be greater than bank account's amount (" + bank.Amount.ToString("#,###.00") + ")!");
                            }
                        }
                    }
                }
                if (giro != null)
                {
                    if (mode == ValidateMode.Insert && giro.BankLoans == 0)
                    {
                        if (giro.Amount - receiveGiro < 0)
                        {
                            Helper.AddError(err, "giroAmount", "First Giro amount must not be greater than giro account's amount (" + giro.Amount.ToString("#,###.00") + ")!");
                        }
                    }
                    else if (mode == ValidateMode.Update && giro.BankLoans == 0)
                    {
                        CashBankReceives girop = (from a in ctx.CashBankReceives where a.Id == Id select a).FirstOrDefault();

                        if (giro.SubAccountID == girop.SubAccountCash)
                        {
                            if (giro.Amount + girop.ReceiveGiro - receiveGiro < 0)
                            {
                                Helper.AddError(err, "giroAmount", "First Giro amount must not be greater than giro account's amount (" + (giro.Amount + girop.ReceiveGiro).ToString("#,###.00") + ")!");
                            }
                        }
                        else
                        {
                            if (giro.Amount - receiveGiro < 0)
                            {
                                Helper.AddError(err, "giroAmount", "First Giro amount must not be greater than giro account's amount (" + giro.Amount.ToString("#,###.00") + ")!");
                            }
                        }
                    }
                }

                if (input.Items.Count == 0)
                {
                    Helper.AddError(err, "detail", "Transaction detail must not be empty!");
                }

                for (int i = 0; i < input.Items.Count; i++)
                {
                    CashBankReceiveItemDTO d = input.Items[i];
                    int dID = Convert.ToInt32(d.DetailId);
                    string accountID = d.SubAccountId;
                    string currencyD = d.CurrencyId;
                    decimal payAmount = d.PayAmount;
                    decimal conversion = d.Conversion;
                    decimal subTotal = d.SubTotal;
                    string remark = d.Remark;
                    decimal etcAmount = d.EtcAmount;


                    if (transactionType == "Invoice" && payAmount == 0)
                    {
                        Helper.AddError(err, "detail", "Pay Amount must not be empty!");
                    }

                    if (transactionType == "Invoice" && conversion == 0)
                    {
                        Helper.AddError(err, "detail", "conversion must not be empty!");
                    }

                    if (transactionType == "Invoice" && currencyD == "")
                    {
                        Helper.AddError(err, "detail", "Currency must not be empty!");
                    }

                    if (payAmount <= 0)
                    {
                        Helper.AddError(err, "detail", "Pay amount on row: " + (i + 1) + " must be greater than 0!");
                    }

                    if (mode == ValidateMode.Update || mode == ValidateMode.Cancel)
                    {
                        var cbpolds = from a in ctx.CashBankReceiveDetails where a.CashBankReceiveId == Id select a;
                        foreach (var cbpold in cbpolds)
                        {
                            string ID = cbpold.Id.ToString();
                            string oldAccountID = cbpold.SubAccountId;
                            string oldEtcAccountID = cbpold.SubAccountId;
                            string oldtransactiontype = oldInput.Type;
                            CashBank cbold = (from a in ctx.CashBank where a.SubAccountID == oldAccountID select a).FirstOrDefault();
                            CashBank oldcbetc = (from a in ctx.CashBank where a.SubAccountID == oldEtcAccountID select a).FirstOrDefault();

                            var getdataold = from a in input.Items where (a.DetailId) == dID.ToString() select a;

                        }
                    }
                }
            }
        }
    }

    public void cashbank(EasyEntities ctx, CashBankReceiveDTO input, string oldCurrencyID, string oldCashAccountID,
        decimal oldCashAmount, string oldBankAccountID, decimal oldBankAmount,
        string oldGiroAccountID, decimal oldGiroAmount, string action)
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            string currencyID = input.CurrencyId;
            string cashAccountID = input.CashAccountId;
            decimal receiveCash = input.CashAmount;
            string bankAccountID = input.BankAccountId;
            decimal receiveBank = input.BankAmount;
            string giroAccountID = input.GiroAccountId;
            decimal receiveGiro = input.GiroAmount;

            if (action == "update" || action == "cancel")
            {

                if (oldCashAccountID.Trim() != "")
                {
                    CashBank cash = (from a in ctx.CashBank where a.SubAccountID == oldCashAccountID && a.CurrencyID == oldCurrencyID select a).FirstOrDefault();
                    cash.Amount = cash.Amount + oldCashAmount;
                }
                if (oldBankAccountID.Trim() != "")
                {
                    CashBank bank = (from a in ctx.CashBank where a.SubAccountID == oldBankAccountID && a.CurrencyID == oldCurrencyID select a).FirstOrDefault();
                    bank.Amount = bank.Amount + oldBankAmount;
                }
                if (oldGiroAccountID.Trim() != "")
                {
                    CashBank bank = (from a in ctx.CashBank where a.SubAccountID == oldGiroAccountID && a.CurrencyID == oldCurrencyID select a).FirstOrDefault();
                    bank.Amount = bank.Amount + oldGiroAmount;
                }
            }
            if (action == "insert" || action == "update")
            {
                if (cashAccountID.Trim() != "")
                {
                    CashBank cash = (from a in ctx.CashBank where a.SubAccountID == cashAccountID && a.CurrencyID == currencyID select a).FirstOrDefault();
                    cash.Amount = cash.Amount + receiveCash;

                }
                if (bankAccountID.Trim() != "")
                {
                    CashBank bank = (from a in ctx.CashBank where a.SubAccountID == bankAccountID && a.CurrencyID == currencyID select a).FirstOrDefault();
                    bank.Amount = bank.Amount + receiveBank;
                }
                if (giroAccountID.Trim() != "")
                {
                    CashBank bank = (from a in ctx.CashBank where a.SubAccountID == giroAccountID && a.CurrencyID == currencyID select a).FirstOrDefault();
                    bank.Amount = bank.Amount + receiveGiro;
                }
            }
        }
    }
}