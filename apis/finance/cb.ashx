﻿<%@ WebHandler Language="C#" Class="CashBankReceiveHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CashBankReceivesDTO
{
    //public int Id { get; set; }
    //public string TransactionNo { get; set; }
    //public string TransactionDate { get; set; }
    //public string TransactionType { get; set; }
    //public string Toward { get; set; }
    //public string CurrencyId { get; set; }
    //public string CurrencyName { get; set; }
    //public decimal Conversion { get; set; }
    //public string CashAccountId { get; set; }
    //public string CashAccountName { get; set; }
    //public decimal CashAmount { get; set; }
    //public string BankAccountId { get; set; }
    //public string BankAccountName { get; set; }
    //public decimal BankAmount { get; set; }
    //public string GiroAccountId { get; set; }
    //public string GiroAccountName { get; set; }
    //public decimal GiroAmount { get; set; }
    //public string GiroNo { get; set; }
    //public string GiroDueDate { get; set; }
    //public string Remark { get; set; }
    //public string LastModifiedBy { get; set; }
    //public string CreatedBy { get; set; }
    //public string Status { get; set; }
    //public decimal GrandTotal { get; set; }
    //public byte[] RowVersion { get; set; }
   // public List<CashBankReveiceItemDTO> Items { get; set; }
}

//class CashBankReveiceItemDTO
//{
//    public string DetailId { get; set; }
//    public string SubAccountId { get; set; }
//    public string SubAccountName { get; set; }
//    public string CurrencyId { get; set; }
//    public decimal PayAmount { get; set; }
//    public string EtcAccountId { get; set; }
//    public decimal Conversion { get; set; }
//    public string DetailType { get; set; }
//    public decimal EtcAmount { get; set; }
//    public string status { get; set; }
//    public string Remark { get; set; }
//    public decimal SubTotal { get; set; }
//}
public class CashBankReceiveHandler : BaseHandler<CashBankReceivesDTO>
{

    protected override void Insert(CashBankReceivesDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);
    }

    protected override void Update(CashBankReceivesDTO input, CashBankReceivesDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);
    }

    protected override void Cancel(CashBankReceivesDTO input, CashBankReceivesDTO oldInput, EasyEntities ctx)
    {
    }
    
    protected override void GetMasterData(ListOption option)
    {

        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;

        using (SqlConnection con = new SqlConnection(conString))
        {

            DataTable dt = Helper.NewDataTable(@"SELECT cbr.Id, cbr.Conversion, u.Name as CreatedBy, cbr.CustomerId, cus.Name as CustomerName,
                                                cbr.CurrencyId, cur.Name as CurrencyName, cur.Conversion, CONVERT(VARCHAR(10),
                                                cbr.TransactionDate,103) as TransactionDate, cbr.[Status], cbr.GrandTotal, cbr.TransactionType,
                                                CONVERT(VARCHAR(10),cbr.ModifiedDate,103) as ModifiedDate, cbr.ReceiveCash, cbr.ReceiveBank, 
                                                cbr.ReceiveGiro, cbr.SubAccountCash, cbr.SubAccountBank, cbr.SubAccountGiro, cbr.GiroNo,
                                                CONVERT(VARCHAR(10),cbr.DueDateGiro,103) as DueDateGiro, Remark into #temptable
                                                FROM CashBankReceives cbr
                                                INNER JOIN
                                                [User] u ON cbr.CreatedBy = u.UserID
                                                INNER JOIN
                                                Customer cus ON cus.CustomerID = cbr.CustomerId
                                                INNER JOIN
                                                Currency cur ON cur.CurrencyID = cbr.CurrencyId

                                                @" + Helper.PagingQuery (@"SELECT data.* FROM (select cbp.*, us.Name as ModifiedBy, ROW_NUMBER() OVER (ORDER BY Id DESC) as row
                                                from #temptable cbp left outer join
                                                [User] us 
                                                on cbp.mb = us.UserID" + Helper.FieldFilterQuery(option, "TransactionNo", "TransactionType","CONVERT(varchar,TransactionDate,103)@") + @") data
                                                WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"
                                                order by TransactionDate desc, [Id] desc
                                                ", option) + @"
                                                drop table #temptable", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    //public void ProcessRequest (HttpContext context) {
    //    context.Response.ContentType = "text/plain";
    //    context.Response.Write("Hello World");
    //}
 
    //public bool IsReusable {
    //    get {
    //        return false;
    //    }
    //}

}