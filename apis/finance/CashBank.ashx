﻿<%@ WebHandler Language="C#" Class="CashBankHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class CashBankDTO
{
    public string Id { get; set; }
    public string Type { get; set; }
    public string SubAccountId { get; set; }
    public string SubAccountName { get; set; }
    public string CurrencyId { get; set; }
    public string CurrencyName { get; set; }
    public int BankLoans { get; set; }
    public int View { get; set; }
    public decimal Amount { get; set; }
}

class CashBankHandler : BaseHandler<CashBankDTO>
{
    protected override void Insert(CashBankDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        ctx.CashBank.AddObject(new CashBank()
        {
            Type = input.Type,
            SubAccountID = input.SubAccountId,
            Name = input.SubAccountName,
            CurrencyID = input.CurrencyId,
            BankLoans = Convert.ToByte(Convert.ToBoolean(input.BankLoans)),
            ViewInReport = Convert.ToByte(Convert.ToBoolean(input.View)),
            Amount = Convert.ToDecimal(input.Amount)
        });

        Helper.InsertLog("FIN-Master-CashBank-Insert", "", ctx, input, Convert.ToInt32(user.UserID));

        ctx.SaveChanges();
    }
    protected override void Delete(CashBankDTO input, CashBankDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);
        
        byte id = Convert.ToByte(oldInput.Id);
        EntityKey key = new EntityKey("EasyEntities.CashBank", "ID", id);

        CashBank cashBank = (CashBank)ctx.GetObjectByKey(key);
        var cashbankds = from a in ctx.CashBank where a.ID == id select a;

        if (cashBank != null && cashbankds.Count() > 0)
        {
            foreach (var cashbankd in cashbankds)
            {
                ctx.DeleteObject(cashbankd);
            }
        }

        Helper.InsertLog("FIN-Master-CashBank-Delete", "", ctx, input, Convert.ToInt32(user.UserID));

        ctx.SaveChanges();
    }
    protected override void Update(CashBankDTO input, CashBankDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);
        
        byte id = Convert.ToByte(oldInput.Id);
        EntityKey key = new EntityKey("EasyEntities.CashBank", "ID", id);

        CashBank cashBank = (CashBank)ctx.GetObjectByKey(key);

        if (cashBank != null)
        {
            cashBank.Type = input.Type;
            cashBank.SubAccountID = input.SubAccountId;
            cashBank.Name = input.SubAccountName;
            cashBank.CurrencyID = input.CurrencyId;
            cashBank.BankLoans = Convert.ToByte(Convert.ToBoolean(input.BankLoans));
            cashBank.ViewInReport = Convert.ToByte(Convert.ToBoolean(input.View));
            cashBank.Amount = Convert.ToDecimal(input.Amount);
        }

        Helper.InsertLog("FIN-Master-CashBank-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("FIN-Master-CashBank-Update", "", ctx, input, Convert.ToInt32(user.UserID));

        ctx.SaveChanges();
    }
    //protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, CashBankDTO input, CashBankDTO oldInput)
    //{
    //    base.CheckConcurrency(err, ctx, input, oldInput);

    //    byte transactionID = Convert.ToByte(oldInput.Id);
    //    byte bankloans = Convert.ToByte(oldInput.BankLoans);
    //    CashBank cashbank = (from a in ctx.CashBank where a.ID == transactionID select a).FirstOrDefault();

    //    if (cashbank == null)
    //    {
    //        Helper.ConcurrencyError(err);
    //    }
    //    else if (cashbank.Type != oldInput.Type || cashbank.SubAccountID != oldInput.SubAccountId ||
    //        cashbank.Amount != Convert.ToDecimal(oldInput.Amount) || cashbank.BankLoans != Convert.ToByte(Convert.ToBoolean(oldInput.BankLoans)) ||
    //        cashbank.ViewInReport != Convert.ToByte(Convert.ToBoolean(oldInput.View)) || cashbank.CurrencyID != oldInput.CurrencyId)
    //    {
    //        Helper.ConcurrencyError(err);
    //    }
    //}
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"select cb.ID, cb.Type, cb.SubAccountID, cb.Name as SubAccountName, 
                                                                    c.Name as CurrencyName ,cb.CurrencyID, cb.BankLoans, cb.Amount, 
                                                                    cb.ViewInReport, ROW_NUMBER() OVER (ORDER BY SubAccountId) as row from CashBank cb inner join Currency c on 
                                                                    c.CurrencyID = cb.CurrencyID WHERE " + Helper.FieldFilterQuery(option, "cb.SubAccountId", "cb.Name"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    
    protected override void GetList(string list, CashBankDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "currency")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"
	                SELECT CurrencyID, Name as CurrencyName, Conversion, ROW_NUMBER() OVER 
                    (ORDER BY CurrencyID) as row from Currency where"
                    + Helper.FieldFilterQuery(option, "CurrencyID", "Name"), option), con);
            }
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void Validate(BaseHandler<CashBankDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, CashBankDTO input, CashBankDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (input.Type.Trim() == "Select")
        {
            Helper.AddError(err, "typeCashBank", "Type must be selected!");
        }

        if (input.SubAccountId == "")
        {
            Helper.AddError(err, "subAccountID", "Account ID must not be empty!");
        }

        if (input.CurrencyId == "")
        {
            Helper.AddError(err, "currencyID", "Currency must not be empty!");
        }

        if ((Convert.ToDecimal(input.Amount) < 0 && input.BankLoans !=0 && oldInput == null) || (Convert.ToDecimal(input.Amount) < 0 && Convert.ToBoolean(input.BankLoans) == false && oldInput != null))
        {
            Helper.AddError(err, "bankLoansMinus", "Amount can not below zero!");
        }

        using (EasyEntities ctz = new EasyEntities())
        {
            try
            {
                string subaccountid = input.SubAccountId;

                if (mode == null)
                    Helper.AddError(err, "action", "masuk!");

                var subaccountds = (from a in ctx.CashBank where a.SubAccountID == subaccountid select a).FirstOrDefault();

                if (subaccountds != null)
                {
                    if (subaccountid != oldInput.SubAccountId)
                    {
                        Helper.AddError(err, "subAccountIDExist", "Account ID already Exists!");
                    }
                }

                //apabila subaccount id sudah dipakai di transaksi tidak boleh edit currency, type, nilai dan bank loans
                if (mode == ValidateMode.Delete || mode == ValidateMode.Update)
                {
                    var usecashbankrcvds = (from a in ctx.CashBankReceives where a.SubAccountCash == oldInput.SubAccountId || a.SubAccountBank == oldInput.SubAccountId || a.SubAccountGiro == oldInput.SubAccountId select a).FirstOrDefault();

                    if (usecashbankrcvds != null)
                    {
                        if (mode == ValidateMode.Update)
                        {
                            if (input.SubAccountId != oldInput.SubAccountId)
                            {   
                                Helper.AddError(err, "subAccountIDChange", "Account can not be changed because already used!");
                            }

                            if (input.Type != oldInput.Type)
                            {
                                Helper.AddError(err, "typeChange", "Type can not be changed because already used!");
                            }

                            if (input.CurrencyId != oldInput.CurrencyId)
                            {
                                Helper.AddError(err, "currencyIDChange", "Currency can not be changed because already used!");
                            }

                            if (input.BankLoans != oldInput.BankLoans)
                            {
                                Helper.AddError(err, "bankLoansChange", "Bank Loans can not be changed because already used!");
                            }

                            if (input.Amount != oldInput.Amount)
                            {
                                Helper.AddError(err, "amountChange", "Amount can not be changed because already used!");
                            }
                        }

                        if (mode == ValidateMode.Delete)
                        {
                            Helper.AddError(err, "cashBankDelete", "Cash Bank Account can not be deleted because already used!");
                        }
                    }
                }
            }
            catch
            {
                Helper.ServerError(err);
            }
        }
    }
}