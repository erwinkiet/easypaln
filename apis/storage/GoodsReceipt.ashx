﻿<%@ WebHandler Language="C#" Class="GoodsReceiptHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class GoodsReceiptDTO
{
    public string TransactionId { get; set; }
    public string TransactionDate { get; set; }
    public string SupplierId { get; set; }
    public string SupplierName { get; set; }
    public string Type { get; set; }
    public string POType { get; set; }
    public string WeighId { get; set; }
    public decimal Weight { get; set; }
    public int UseVendorWeight { get; set; }
    public string Status { get; set; }
    public List<GoodsReceiptItemDTO> Items { get; set; }
}

class GoodsReceiptItemDTO
{
    public string DetailId { get; set; }
    public string POId { get; set; }
    public int POItemId { get; set; }
    public string PurchaseId { get; set; }
    public string ItemId { get; set; }
    public string ItemName { get; set; }
    public string WarehouseId { get; set; }
    public string WarehouseName { get; set; }
    public string Unit { get; set; }
    public decimal QtyReceive { get; set; }
    public decimal QtyVendor { get; set; }
    public decimal QtyReceiveEtc { get; set; }
    public decimal QtyLoss { get; set; }
    public string Remark { get; set; }
    public string Status { get; set; }
}

class GoodsReceiptHandler : BaseHandler<GoodsReceiptDTO>
{
    protected override void Insert(GoodsReceiptDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string transactionid = GetGenerateID(input, input.Items[0].POId, ctx);
        input.TransactionId = transactionid;
        ctx.Receive.AddObject(new Receive()
        {
            ReceiveID = transactionid,
            InputDate = DateTime.Now,
            Date = DateTime.Parse(input.TransactionDate + " " + DateTime.Now.ToString("HH:mm:ss")),
            UserID = user.UserID,
            PICID = user.UserID,
            VendorWeight = Convert.ToByte(input.UseVendorWeight),
            POType = input.POType,
            Type = input.Type,
            WeighID = input.WeighId,
            Status = "Open"
        });

        for (int i = 0; i < input.Items.Count; i++)
        {
            ctx.ReceiveDetail.AddObject(new ReceiveDetail()
            {
                ReceiveID = transactionid,
                ID = Convert.ToByte(i + 1),
                POID = input.Items[i].POId,
                POItemID = Convert.ToByte(input.Items[i].POItemId),
                PurchaseID = input.Items[i].PurchaseId,
                LocationID = input.Items[i].WarehouseId,
                QtyVendor = input.Items[i].QtyVendor,
                QtyReceive = input.Items[i].QtyReceive,
                QtyReceiveEtc = input.Items[i].QtyReceiveEtc,
                QtyLoss = input.Items[i].QtyLoss,
                Remark = input.Items[i].Remark,
                Status = "Open"
            });


            decimal qtydetailreceive = input.Items[i].QtyReceive + input.Items[i].QtyLoss;
            if (Convert.ToBoolean(input.UseVendorWeight))
            {
                qtydetailreceive = input.Items[i].QtyVendor + input.Items[i].QtyLoss;
            }

            UpdatePOStatus(input.Items[i].POId, Convert.ToByte(input.Items[i].POItemId), qtydetailreceive, input, ctx);
            UpdateUIStatus(input.Items[i].POId, Convert.ToByte(input.Items[i].POItemId), qtydetailreceive, input, ctx);
            UpdateStock(i, 1, input, ctx, input.Items[i].WarehouseId);
            UpdateInTransit(i, 1, input, ctx);
        }

        if (input.WeighId != "")
        {
            Update_status_Weigh("Closed", input.WeighId, ctx);
        }
        Helper.InsertLog("STR-Transaction-ProductReceive-Insert", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Update(GoodsReceiptDTO input, GoodsReceiptDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        string transactionid = oldInput.TransactionId;

        EntityKey key = new EntityKey("EasyEntities.Receive", "ReceiveID", transactionid);
        Receive receive = (Receive)ctx.GetObjectByKey(key);

        string oldweighid = receive.WeighID.Trim();

        if (receive != null)
        {
            receive.Date = DateTime.Parse(input.TransactionDate + " " + DateTime.Now.ToString("HH:mm:ss tt"));
            receive.VendorWeight = Convert.ToByte(Convert.ToBoolean(input.UseVendorWeight));
            receive.PICID = user.UserID;
            receive.POType = input.POType;
            receive.Type = input.Type;
            receive.WeighID = input.WeighId;

            for (int i = 0; i < oldInput.Items.Count(); i++)
            {
                byte detailid = Convert.ToByte(oldInput.Items[i].DetailId);
                var receivedetails = (from a in ctx.ReceiveDetail where a.ReceiveID == transactionid && a.ID == detailid orderby a.ID select a).FirstOrDefault();
                byte receivedetailid = Convert.ToByte(oldInput.Items[i].DetailId);

                int ada = 0;
                for (int j = 0; j < input.Items.Count(); j++)
                {
                    if (receivedetailid.ToString().Trim() == input.Items[j].DetailId)
                    {
                        ada = 1;
                        break;
                    }
                }
                if (ada == 0)
                {

                    UpdateInTransit(i, -1, oldInput, ctx);
                    UpdateStock(i, -1, oldInput, ctx, oldInput.Items[i].WarehouseId);
                    ctx.DeleteObject(receivedetails);
                }
            }

            for (int i = 0; i < input.Items.Count(); i++)
            {
                byte detailid = Convert.ToByte(input.Items[i].DetailId);
                var receivedetail = from a in ctx.ReceiveDetail where a.ReceiveID == transactionid && a.ID == detailid select a;
                var receivedetailfirst = receivedetail.FirstOrDefault();

                decimal qtydetailreceive = input.Items[i].QtyReceive + input.Items[i].QtyLoss;
                if (Convert.ToBoolean(input.UseVendorWeight))
                {
                    qtydetailreceive = input.Items[i].QtyVendor + input.Items[i].QtyLoss;
                }

                if (receivedetail.Count() > 0)
                {
                    receivedetailfirst.POID = input.Items[i].POId;
                    receivedetailfirst.POItemID = Convert.ToByte(input.Items[i].POItemId);
                    receivedetailfirst.PurchaseID = input.Items[i].PurchaseId;
                    receivedetailfirst.LocationID = input.Items[i].WarehouseId;
                    receivedetailfirst.QtyVendor = input.Items[i].QtyVendor;
                    receivedetailfirst.QtyReceive = input.Items[i].QtyReceive;
                    receivedetailfirst.QtyReceiveEtc = input.Items[i].QtyReceiveEtc;
                    receivedetailfirst.QtyLoss = input.Items[i].QtyLoss;
                    receivedetailfirst.Remark = input.Items[i].Remark;

                    UpdateInTransit(i, -1, oldInput, ctx);
                    UpdateStock(i, -1, oldInput, ctx, oldInput.Items[i].WarehouseId);
                    UpdateInTransit(i, 1, input, ctx);
                    UpdateStock(i, 1, input, ctx, input.Items[i].WarehouseId);

                    UpdatePOStatus(oldInput.Items[i].POId, Convert.ToByte(oldInput.Items[i].POItemId), 0, input, ctx);
                    UpdatePOStatus(input.Items[i].POId, Convert.ToByte(input.Items[i].POItemId), qtydetailreceive, input, ctx);

                    UpdateUIStatus(oldInput.Items[i].POId, Convert.ToByte(oldInput.Items[i].POItemId), 0, input, ctx);
                    UpdateUIStatus(input.Items[i].POId, Convert.ToByte(input.Items[i].POItemId), qtydetailreceive, input, ctx);
                }

                else
                {
                    ctx.ReceiveDetail.AddObject(new ReceiveDetail()
                    {
                        ReceiveID = transactionid,
                        ID = Convert.ToByte(input.Items[i].DetailId),
                        POID = input.Items[i].POId,
                        POItemID = Convert.ToByte(input.Items[i].POItemId),
                        PurchaseID = input.Items[i].PurchaseId,
                        LocationID = input.Items[i].WarehouseId,
                        QtyVendor = input.Items[i].QtyVendor,
                        QtyReceive = input.Items[i].QtyReceive,
                        QtyReceiveEtc = input.Items[i].QtyReceiveEtc,
                        QtyLoss = input.Items[i].QtyLoss,
                        Remark = input.Items[i].Remark,
                        Status = "Open"
                    });

                    UpdateInTransit(i, 1, input, ctx);
                    UpdateStock(i, 1, input, ctx, input.Items[i].WarehouseId);
                    UpdatePOStatus(input.Items[i].POId, Convert.ToByte(input.Items[i].POItemId), qtydetailreceive, input, ctx);
                    UpdateUIStatus(input.Items[i].POId, Convert.ToByte(input.Items[i].POItemId), qtydetailreceive, input, ctx);
                }
            }
        }

        if (oldweighid != "")
        {
            Update_status_Weigh("Finish", oldweighid, ctx);
        }

        if (input.WeighId != "")
        {
            Update_status_Weigh("Closed", input.WeighId, ctx);
        }

        Helper.InsertLog("STR-Transaction-ProductReceive-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("STR-Transaction-ProductReceive-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Cancel(GoodsReceiptDTO input, GoodsReceiptDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        string receiveid = oldInput.TransactionId;
        EntityKey key = new EntityKey("EasyEntities.Receive", "ReceiveID", receiveid);
        Receive receive = (Receive)ctx.GetObjectByKey(key);

        var receivedetails = from b in ctx.ReceiveDetail where b.ReceiveID == receiveid orderby b.ID select b;

        if (receive != null && receivedetails != null)
        {
            receive.Status = "Cancel";
            int i = 0;
            foreach (var receivedetail in receivedetails)
            {
                UpdateInTransit(i, -1, oldInput, ctx);
                UpdateStock(i, -1, oldInput, ctx, oldInput.Items[i].WarehouseId);
                UpdatePOStatus(oldInput.Items[i].POId, Convert.ToByte(oldInput.Items[i].POItemId), 0, input, ctx);
                UpdateUIStatus(oldInput.Items[i].POId, Convert.ToByte(oldInput.Items[i].POItemId), 0, input, ctx);
                receivedetail.Status = "Cancel";
                i++;
            }
        }

        Helper.InsertLog("STR-Transaction-ProductReceive-Cancel", "", ctx, oldInput, user.UserID);
        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT *, ROW_NUMBER() OVER (ORDER BY Date DESC, ReceiveID DESC) as row FROM (SELECT DISTINCT rcv.[ReceiveID], rcv.VendorWeight, 
                Convert(varchar(10),rcv.[Date],120) as Date, rcv.[WeighID], rcv.[Type], rcv.[PICID], rcv.[UserID], rcv.[Status], rcv.[POType],
                po.SupplierID, sp.Name as SupplierName, usr.Name as CreatedBy                 
                FROM [Receive] rcv 
                INNER JOIN ReceiveDetail rcd
                ON rcv.ReceiveID = rcd.ReceiveID
                INNER JOIN [User] usr
                ON usr.UserID = rcv.UserID
                INNER JOIN ItemLocation itl
                ON itl.LocationID = rcd.LocationID
                INNER JOIN UserWarehouse usw
                ON itl.TopLocationID=usw.LocationID OR itl.LocationID=usw.LocationID
                INNER JOIN PO po
                ON rcd.POID = po.POID
                INNER JOIN Supplier sp
                ON po.SupplierID = sp.SupplierID) receives 
                WHERE   
                receives.UserID='" + user.UserID + "' AND " + Helper.FilterQuery(option, "receives.Status", "receives.ReceiveID", 
                                   "receives.SupplierID", "receives.SupplierName", "CONVERT(varchar,receives.Date,103)"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {
            if (detail == "items")
            {
                DataTable dt = Helper.NewDataTable(@"select d.ID as DetailID, d.PurchaseID as purchaseId ,d.POID as poId,sd.ItemID as itemId, d.LocationID as warehouseId, itl.Name as warehouseName,i.name as 
                itemName,i.Unit as unit, d.QtyReceive as qtyReceive, d.QtyVendor as qtyVendor, d.QtyReceiveETC as qtyReceiveEtc, d.QtyLoss as qtyLoss,d.POItemID as poItemId, d.Remark as remark, d.Status as status from 
                ReceiveDetail d ,SPKBDetail sd, Item i, PODetail pd, ItemLocation itl where d.ReceiveID = '" + Helper.EscapeSQLString(id) + @"' 
                and d.POItemID = pd.ID and d.POID = pd.POID and pd.SPKBID = sd.SPKBID and d.LocationID = itl.LocationID and pd.SPKBItemID = sd.ID and sd.ItemID = i.ItemID 
                order by DetailID", con);

                http.Response.Write(Helper.DataTabletoJSON(dt));
            }
        }
    }

    protected override void GetListData(string list, string id)
    {
        base.GetListData(list, id);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "warehouse")
            {
                dt = Helper.NewDataTable(@"SELECT LocationID as warehouseId, Name as warehouseName 
	                    FROM ItemLocation 
                        WHERE LocationID='" + Helper.EscapeSQLString(id) + "'", con);
            }

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, GoodsReceiptDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            if (list == "purchase")
            {
                int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
                int toRow = fromRow + option.PageSize - 1;

                dt = Helper.NewDataTable(@"SELECT a.[PurchaseID], e.ItemID, f.Name as ItemName, c.Name as SupplierName, a.ReferenceNo, b.[ID], b.POID, b.POItemID, b.Qty, b.QtyEtc, 0 as QtyReceive, b.Qty as QtyAvailable, b.QtyEtc as Qty_Etc_Available, b.[Unit], b.[Remark] INTO #temppur FROM [Purchase] a, [PurchaseDetail] b, Supplier c, PODetail d, SPKBDetail e, Item f Where 
                a.PurchaseID = b.PurchaseID and a.Status <>'Cancel' and a.Type = 'Shipping Point' and a.SupplierID = c.SupplierID and b.POID=d.POID and b.POItemID=d.ID and d.SPKBID=e.SPKBID and d.SPKBItemID=e.ID and e.ItemID=f.ItemID

                select rd.POID,rd.POItemID, rd.PurchaseID ,sum(Case When rec.VendorWeight = '1' then rd.QtyReceive else rd.QtyReceive end) as Qty, 
                sum(rd.QtyReceiveEtc) as QtyEtc, sum(rd.QtyLoss) as QtyLoss INTO #tempreceive from ReceiveDetail rd inner join Receive rec on rd.ReceiveID = rec.ReceiveID where 
                rd.Status <> 'Cancel' group by rd.POID, rd.POItemID, rd.PurchaseID

                select pur.PurchaseID, pur.ItemID, pur.ItemName, pur.SupplierName, pur.ReferenceNo, pur.ID, pur.POID, pur.POItemID, pur.Qty, Pur.QtyEtc, rec.Qty as QtyReceive, (pur.Qty - rec.Qty - rec.QtyLoss) as QtyAvailable, pur.QtyEtc -
                rec.QtyEtc as Qty_Etc_Available, pur.Unit, pur.Remark into #tempsisaqty from #temppur pur inner join #tempreceive rec on pur.POID = rec.POID and pur.POItemID = rec.POItemID and
                pur.PurchaseID = rec.PurchaseID

                delete #temppur from #temppur pur inner join #tempsisaqty sisa on pur.POID = sisa.POID and pur.POItemID = sisa.POItemID
                and pur.PurchaseID = sisa.PurchaseID

                SELECT data.* FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY PurchaseID) as row FROM (select * from #temppur union all 
                select * from #tempsisaqty) purchases WHERE purchases.QtyAvailable>0 AND " + Helper.FieldFilterQuery(option, "purchases.PurchaseID",
                "purchases.ItemName", "purchases.SupplierName") + @") data 
                WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"

                drop table #temppur
                drop table #tempreceive
                drop table #tempsisaqty", con);
            }
            else if (list == "purchaseorder")
            {
                int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
                int toRow = fromRow + option.PageSize - 1;

                dt = Helper.NewDataTable(@"Select sd.ItemID, i.Name as ItemName, pd.POID, pd.ID as POItemID, p.SupplierID, sup.Name as 
                SupplierName, (pd.Qty - pd.QtyCancel) as QtyPO, (pd.Qty - pd.QtyCancel) as QtyAvailable, s.SPKBID, pd.Unit, pd.Remark, i.Location,  
                convert(varchar,s.Date,120) as SPKBDate INTO #tmpPO from PO p, Item i, SPKB s, SPKBDetail sd, Supplier sup, PODetail pd 
                where pd.POID = p.POID and sup.SupplierID = p.SupplierID and s.SPKBID = sd.SPKBID and 
                pd.SPKBID = sd.SPKBID and pd.SPKBItemID = sd.ID and sd.ItemID = i.ItemID and 
                (p.Type = 'Destination Point' AND pd.Status = 'In Process') order by pd.POID desc

                select rd.POID,rd.POItemID,Sum(Case When rec.VendorWeight = '1' then rd.QtyVendor else 
                rd.QtyReceive end) as Qty, sum(rd.QtyLoss) as QtyLoss INTO #tmpRcv from ReceiveDetail rd inner join Receive rec 
                on rd.ReceiveID = rec.ReceiveID where rd.Status <> 'Cancel' and rd.POID<>'' group by rd.POID, 
                rd.POItemID order by rd.POID desc

                SELECT po.ItemID, po.ItemName, po.POID, po.POItemID, po.SupplierID, po.SupplierName, po.QtyPO,
                (po.QtyAvailable-rcv.Qty-rcv.QtyLoss) as QtyAvailable, po.SPKBID, po.Unit, po.Remark, po.Location,
                po.SPKBDate INTO #tmpAvailable 
                FROM #tmpPO po
                INNER JOIN #tmpRcv rcv
                ON po.POID = rcv.POID and po.POItemID = rcv.POItemID

                DELETE #tmpPO FROM #tmpPO po
                INNER JOIN #tmpAvailable avb
                ON po.POID = avb.POID AND po.POItemID = avb.POItemID

                SELECT data.* FROM(SELECT *, ROW_NUMBER() OVER (ORDER BY POID) as row 
                FROM (SELECT * FROM #tmpPO union all 
                SELECT * FROM #tmpAvailable) po WHERE po.QtyAvailable > 0 AND " + Helper.FieldFilterQuery(option, "po.POID", "po.ItemID",
                "po.ItemName", "po.SPKBID", "po.SPKBDate", "po.QtyPO", "po.QtyAvailable", "po.SupplierID", "po.SupplierName") + @") data 
                WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"
            
                DROP TABLE #tmpPO
                DROP TABLE #tmpRcv
                DROP TABLE #tmpAvailable", con);
            }
            else if (list == "warehouse")
            {
                string itemId = input.Items[currentDetailIndex].ItemId;

                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT itl.LocationID as WarehouseID, itl.Name as WarehouseName, itd.Qty
                    , ROW_NUMBER() OVER (ORDER BY itl.LocationID) as row                 
                    FROM ItemLocation itl
                    INNER JOIN ItemDetail itd
                    ON itl.LocationID = itd.LocationID 
                    WHERE itd.ItemID = '" + itemId + @"' AND " + Helper.FieldFilterQuery(option, "itl.LocationID", "itl.Name"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, GoodsReceiptDTO input, GoodsReceiptDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        string transactionID = oldInput.TransactionId;
        Receive receive = (from a in ctx.Receive where a.ReceiveID == transactionID select a).FirstOrDefault();

        if (receive.WeighID != oldInput.WeighId || receive.Date.ToString("yyyy-MM-dd") != oldInput.TransactionDate ||
            receive.Type != oldInput.Type || receive.POType != oldInput.POType ||
            receive.Status != oldInput.Status)
        {
            Helper.ConcurrencyError(err);
        }

        var receivedetails = from a in ctx.ReceiveDetail where a.ReceiveID == transactionID select a;

        if (oldInput.Items.Count != receivedetails.Count())
        {
            Helper.ConcurrencyError(err);
        }

        foreach (GoodsReceiptItemDTO d in oldInput.Items)
        {
            int dID = Convert.ToInt32(d.DetailId);
            ReceiveDetail receivedetail = (from a in ctx.ReceiveDetail where a.ReceiveID == transactionID && a.ID == dID select a).FirstOrDefault();

            if (receivedetail == null)
            {
                Helper.ConcurrencyError(err);
            }

            else if (receivedetail.POID != d.POId || receivedetail.POItemID != Convert.ToByte(d.POItemId) ||
                receivedetail.PurchaseID != d.PurchaseId ||
                receivedetail.LocationID != d.WarehouseId ||
                receivedetail.QtyReceive != d.QtyReceive ||
                receivedetail.QtyReceiveEtc != d.QtyReceiveEtc ||
                receivedetail.QtyLoss != d.QtyLoss
                || receivedetail.Remark != d.Remark ||
                receivedetail.Status != d.Status)
            {
                Helper.ConcurrencyError(err);
            }
        }
    }

    protected override void Validate(BaseHandler<GoodsReceiptDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, GoodsReceiptDTO input, GoodsReceiptDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.Type == "Select")
            {
                Helper.AddError(err, "type", "Type must be selected!");
            }
            else if (input.Type == "Weight")
            {
                if (input.WeighId == "")
                {
                    Helper.AddError(err, "WeighID", "WeighID is required!");
                }
            }
            else if (input.Type == "Qty")
            {
                if (input.WeighId != "")
                {
                    Helper.AddError(err, "WeighID", "WeighID must be empty!");
                }
            }

            if (input.POType == "Select")
            {
                Helper.AddError(err, "potype", "PO Type must be selected!");
            }

            if (input.TransactionDate == "")
            {
                Helper.AddError(err, "date", "Date must not be empty!");
            }

            if (input.Items.Count == 0)
            {
                Helper.AddError(err, "detail", "Transaction detail must not be empty!");
            }

            for (int i = 0; i < input.Items.Count; i++)
            {
                string poID = input.Items[i].POId;
                var po = (from a in ctx.PO where a.POID == poID select a);
                string purID = input.Items[i].PurchaseId;
                var purchase = (from a in ctx.Purchase where a.PurchaseID == purID select a);
                if (input.Items[i].POId == "")
                {
                    Helper.AddError(err, "detail", "PO ID on row : " + (i + 1) + " is required!");
                }

                else if (mode == ValidateMode.Update)
                {
                    string pesan = CheckPPNTransactionIDAndDetail(input.Items[i].POId, input.TransactionId, ctx);
                    if (pesan != "")
                    {
                        Helper.AddError(err, "detail", "PO ID on row : " + (i + 1) + " Must be " + pesan);
                    }
                }
                //if (po.Count() > 0 && po.FirstOrDefault().Date > Convert.ToDateTime(input["date"]))
                //{
                //    Helper.AddError(err, "date", "PO Date on row : " + (i + 1) + " ( " + po.FirstOrDefault().Date.ToShortDateString() + " ) can not be greather than Receive Date (" + Convert.ToDateTime(input["date"]).ToShortDateString() + ")!");
                //}
                if (input.POType == "Destination Point")
                {
                    if (input.Items[i].PurchaseId != "")
                    {
                        Helper.AddError(err, "detail", "Purchase ID on row : " + (i + 1) + " must be empty!");
                    }
                }
                else if (input.POType == "Shipping Point")
                {
                    if (input.Items[i].PurchaseId == "")
                    {
                        Helper.AddError(err, "detail", "Purchase ID on row : " + (i + 1) + " is required!");
                    }
                    if (purchase.Count() > 0 && purchase.FirstOrDefault().Date > Convert.ToDateTime(input.TransactionDate))
                    {
                        Helper.AddError(err, "date", "Purchase Date on row : " + (i + 1) + " ( " + purchase.FirstOrDefault().Date.ToShortDateString() + " ) can not be greather than Receive Date (" + Convert.ToDateTime(input.TransactionDate).ToShortDateString() + ")!");
                    }
                }

                if (input.Items[i].WarehouseId == "")
                {
                    Helper.AddError(err, "detail", "Warehouse on row : " + (i + 1) + " is required!");
                }
                else
                {
                    string locationID = input.Items[i].WarehouseId;
                    ItemLocation loc = (from a in ctx.ItemLocation where a.LocationID == locationID select a).FirstOrDefault();
                    if (loc == null)
                    {
                        Helper.AddError(err, "detail", "Warehouse on row: " + (i + 1) + " does not exists!");
                    }
                    else if (loc.HasChild != "0")
                    {
                        Helper.AddError(err, "detail", "Warehouse on row: " + (i + 1) + " has children. You can only select lowest level warehouse that has no child!");
                    }
                    else if (loc.Status == "Closed")
                    {
                        Helper.AddError(err, "detail", "Warehouse on row: " + (i + 1) + " is already closed!");
                    }
                }

                if (input.Items[i].QtyVendor <= 0)
                {
                    Helper.AddError(err, "detail", "Qty vendor on row : " + (i + 1) + " must be greater than 0!");
                }

                if (input.Items[i].QtyReceive <= 0)
                {
                    Helper.AddError(err, "detail", "Qty on row : " + (i + 1) + " must be greater than 0!");
                }
                else if (input.Items[i].POId != "")
                {
                    string pesan = CekQtyReceiveAvailable(input, ctx, i);
                    if (pesan != "")
                    {
                        Helper.AddError(err, "detail", pesan);
                    }
                }

                if (input.Items[i].QtyReceiveEtc < 0)
                {
                    Helper.AddError(err, "detail", "Qty on row : " + (i + 1) + " must be greater or equal 0!");
                }

                if (input.Items[i].QtyLoss < 0)
                {
                    Helper.AddError(err, "detail", "Qty Loss on row : " + (i + 1) + " must be greater or equal 0!");
                }
            }

            if (input.TransactionId != "")
                CheckStatusDetailAllowEdit(input, err, input.TransactionId, ctx);

            if (!CheckPOPPNType(input, ctx))
            {
                Helper.AddError(err, "po", "Can not add PPN PO and Non PPN PO in one Product Receive Transaction");
            }

            if (!CheckDoublePOItemID(input))
            {
                Helper.AddError(err, "POItemID", "Can not add same POID and PO ItemID in one Product Receive Transaction");
            }

            if (!cek_qty_weigh(input.Weight, input))
            {
                Helper.AddError(err, "Weight", "Total Qty Receive must be equal " + input.Weight);
            }

            if (!CekSameSupplier(input, ctx))
            {
                Helper.AddError(err, "SupplierDetail", "All of transaction Detail must have same Supplier");
            }
            DateTime dateposting = Convert.ToDateTime(input.TransactionDate);
            Helper.CheckPosting(ctx, dateposting, "STR", err);
        }


        if (mode == ValidateMode.Update || mode == ValidateMode.Delete)
        {
            string transactionid = input.TransactionId;

            var receive = (from a in ctx.Receive where a.ReceiveID == transactionid select a).FirstOrDefault();
            
            string tanggal = CheckDateAllowEdit(Convert.ToDateTime(receive.Date), ctx);
            var notaBeli = from a in ctx.Purchase where a.ReceiveID == transactionid && a.Status != "Cancel" select a;

            if (notaBeli.Count() > 0 && input.POType == "Destination Point")
            {
                Helper.AddError(err, "ReceiveID", "Can not Update or delete Product Receive because already use in Purchase");
            }

            using (SqlConnection con = new SqlConnection(conString))
            {
                if (input.POType == "Shipping Point")
                {
                    DataTable dt = Helper.NewDataTable(@"select distinct 
                                                    spkbd.ItemID, 
                                                    rec.InputDate
                                                    into #temprec from [Receive]rec inner join ReceiveDetail recd on rec.ReceiveID = recd.ReceiveID inner join
                                                    podetail pod on recd.POID = pod.POID and recd.POItemID = pod.ID inner join SPKBDetail spkbd on pod.SPKBID = spkbd.SPKBID and
                                                    pod.SPKBItemID = spkbd.ID 
                                                    where rec.POType = 'Shipping Point' and rec.ReceiveID = '" + transactionid + @"' and recd.Status <> 'Cancel'

                                                    select 
                                                    usd.ItemID, 
                                                    usd.UsageID, 
                                                    us.InputDate 
                                                    into #tempusage from Usage us inner join 
                                                    Usagedetail usd on us.UsageID = usd.UsageID inner join 
                                                    #temprec rec on usd.ItemID = rec.ItemID 
                                                    where us.InputDate >= rec.InputDate and us.Status <> 'Cancel'

                                                    select distinct 
                                                    'Item ID : '+ ItemID + ' already used in Usage Transaction with TransactionID = ' + UsageID as Msg 
                                                    from #tempusage

                                                    drop table #temprec
                                                    drop table #tempusage", con);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            Helper.AddError(err, "ReceiveID", row["Msg"].ToString());
                        }
                    }
                }
            }

            var ambiltgl = (from a in ctx.Receive where a.ReceiveID == transactionid select a).FirstOrDefault();
            Helper.CheckPosting(ctx, ambiltgl.Date, "STR", err);
        }
    }

    private string GetGenerateID(GoodsReceiptDTO input, string POID, EasyEntities ctx)
    {
        var po = (from a in ctx.PO where a.POID == POID select a).FirstOrDefault();
        bool ppn = true;
        if (po.IsPPN == 0)
        {
            ppn = false;
        }
        return Helper.GenerateID("Receive", "ReceiveID", "RCV", input.TransactionDate, user.Initial, ppn);
    }

    private bool CekSameSupplier(GoodsReceiptDTO input, EasyEntities ctx)
    {
        if (input.Items.Count > 0)
        {
            if (input.Items[0].POId != "")
            {
                string po1 = input.Items[0].POId;
                EntityKey key = new EntityKey("EasyEntities.PO", "POID", po1);
                PO POSup1 = (PO)ctx.GetObjectByKey(key);

                for (int i = 1; i < input.Items.Count; i++)
                {
                    if (input.Items[i].POId != "")
                    {
                        string po2 = input.Items[i].POId;
                        EntityKey key1 = new EntityKey("EasyEntities.PO", "POID", po2);
                        PO POSup2 = (PO)ctx.GetObjectByKey(key1);

                        if (POSup1.SupplierID != POSup2.SupplierID)
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    private string CekQtyReceiveAvailable(GoodsReceiptDTO input, EasyEntities ctx, int index)
    {
        if (input.POType != "Select")
        {
            if (input.Type.ToUpper() == "QTY")
            {
                string pesan = "";
                DataTable dtQtyPOPur = null;
                DataTable dtQtyReceive = null;
                string transactionid = "";
                if (input.TransactionId != "")
                {
                    transactionid = input.TransactionId;
                }
                using (SqlConnection con = new SqlConnection(conString))
                {
                    if (input.POType == "Destination Point")
                    {
                        dtQtyPOPur = Helper.NewDataTable("select * from PODetail where POID = '" + Helper.EscapeSQLString(input.Items[index].POId) + "' and [ID] = '" + input.Items[index].POItemId.ToString() + "' order by ID", con);
                        if (dtQtyPOPur.Rows[0]["Status"].ToString() == "Cancel")
                        {
                            pesan = "Status PO ID on row : " + (index + 1) + " is already " + dtQtyPOPur.Rows[0]["Status"].ToString();
                            return pesan;
                        }

                        pesan = "Qty Receive/Qty Vendor on row : " + (index + 1) + " is greater than Qty PO";

                        dtQtyReceive = Helper.NewDataTable("select sum(case when rec.VendorWeight = '1' then recd.QtyVendor else recd.QtyReceive end) as QtyReceive, sum(recd.QtyLoss) as QtyLoss from ReceiveDetail recd inner join Receive rec on recd.ReceiveID = rec.ReceiveID where recd.POID = '" + Helper.EscapeSQLString(input.Items[index].POId) + "' and recd.[POItemID] = '" + input.Items[index].POItemId.ToString() + "' and (recd.ReceiveID <> '" + transactionid + "') and recd.Status <> 'Cancel'", con);
                    }
                    else if (input.POType == "Shipping Point")
                    {
                        if (input.Items[index].PurchaseId == "")
                        {
                            return "";
                        }
                        dtQtyPOPur = Helper.NewDataTable("select b.Qty, b.QtyEtc , a.POID, b.POItemID, a.Status from Purchase a, PurchaseDetail b where a.POID = '" + Helper.EscapeSQLString(input.Items[index].POId) + "' and b.POItemID = '" + input.Items[index].POItemId.ToString() + "' and a.PurchaseID = b.PurchaseID and a.PurchaseID = '" + Helper.EscapeSQLString(input.Items[index].PurchaseId) + "'", con);
                        if (dtQtyPOPur.Rows[0]["Status"].ToString() == "Cancel")
                        {
                            pesan = "Status Purchase ID on row : " + (index + 1) + " is already " + dtQtyPOPur.Rows[0]["Status"].ToString();
                            return pesan;
                        }
                        pesan = "Qty Receive/ Qty Vendor on row : " + (index + 1) + " is greater than Qty Purchase";

                        dtQtyReceive = Helper.NewDataTable("select sum(case when rec.VendorWeight = '1' then recd.QtyVendor else recd.QtyReceive end) as QtyReceive, sum(recd.QtyReceiveEtc) as QtyReceiveEtc, sum(recd.QtyLoss) as QtyLoss from ReceiveDetail recd inner join Receive rec on recd.ReceiveID = rec.ReceiveID where recd.POID = '" + Helper.EscapeSQLString(input.Items[index].POId) + "' and recd.[POItemID] = '" + input.Items[index].POItemId.ToString() + "' and recd.PurchaseID = '" + Helper.EscapeSQLString(input.Items[index].PurchaseId) + "' and (recd.ReceiveID <> '" + transactionid + "') and recd.Status <> 'Cancel'", con);
                    }

                    decimal qtyreceivedatabase, qtylossdatabase;
                    if (string.IsNullOrEmpty(dtQtyReceive.Rows[0]["QtyReceive"].ToString()))
                    {
                        qtyreceivedatabase = Convert.ToDecimal("0.00");
                    }
                    else
                    {
                        qtyreceivedatabase = Convert.ToDecimal(dtQtyReceive.Rows[0]["QtyReceive"].ToString());
                    }
                    if (string.IsNullOrEmpty(dtQtyReceive.Rows[0]["QtyLoss"].ToString()))
                    {
                        qtylossdatabase = Convert.ToDecimal("0.00");
                    }
                    else
                    {
                        qtylossdatabase = Convert.ToDecimal(dtQtyReceive.Rows[0]["QtyLoss"].ToString());
                    }

                    decimal qtypopur = Convert.ToDecimal(dtQtyPOPur.Rows[0]["Qty"]);
                    decimal qtyavailable = Convert.ToDecimal(dtQtyPOPur.Rows[0]["Qty"]) - qtyreceivedatabase - qtylossdatabase;
                    if (input.POType == "Destination Point")
                    {
                        qtypopur = qtypopur - Convert.ToDecimal(dtQtyPOPur.Rows[0]["QtyCancel"]);
                        qtyavailable = qtyavailable - Convert.ToDecimal(dtQtyPOPur.Rows[0]["QtyCancel"]);
                    }

                    decimal qtyrecinput = input.Items[index].QtyReceive;
                    decimal qtyveninput = input.Items[index].QtyVendor;

                    if (qtyreceivedatabase + qtyrecinput + qtylossdatabase + input.Items[index].QtyLoss > qtypopur ||
                        qtyreceivedatabase + qtyveninput + qtylossdatabase + input.Items[index].QtyLoss > qtypopur)
                    {
                        pesan = pesan + ", Qty Available = " + qtyavailable;
                        return pesan;
                    }

                    if (input.POType == "Shipping Point")
                    {
                        decimal qtyreceiveetcdatabase;
                        if (string.IsNullOrEmpty(dtQtyReceive.Rows[0]["QtyReceiveEtc"].ToString()))
                        {
                            qtyreceiveetcdatabase = Convert.ToDecimal("0.00");
                        }
                        else
                        {
                            qtyreceiveetcdatabase = Convert.ToDecimal(dtQtyReceive.Rows[0]["QtyReceiveEtc"].ToString());
                        }

                        if (qtyreceiveetcdatabase + input.Items[index].QtyReceiveEtc > Convert.ToDecimal(dtQtyPOPur.Rows[0]["QtyEtc"]))
                        {
                            decimal qtyetcavailable = Convert.ToDecimal(dtQtyPOPur.Rows[0]["QtyEtc"].ToString()) - qtyreceiveetcdatabase;
                            pesan = "Qty Etc Receive on row : " + (index + 1) + " is greater than Qty Etc Purchase , Qty Available = " + qtyetcavailable;
                            return pesan;
                        }
                    }
                }
            }
        }
        return "";
    }

    protected Boolean cek_qty_weigh(decimal weight, GoodsReceiptDTO input)
    {
        if (weight > 0)
        {
            decimal hitung = 0;
            for (int i = 0; i < input.Items.Count; i++)
            {
                if (input.Items[i].QtyReceive > 0)
                {
                    hitung = hitung + input.Items[i].QtyReceive;
                }
            }

            if (hitung != weight)
            {
                return false;
            }
        }
        return true;
    }

    protected void UpdatePOStatus(string POID, byte POItemID, decimal Qty, GoodsReceiptDTO input, EasyEntities ctx)
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            if (input.POType == "Destination Point")
            {
                string receiveid = "";
                if (input.TransactionId != "")
                {
                    receiveid = input.TransactionId;
                }
                DataTable dtPODetail = Helper.NewDataTable("SELECT * FROM PODetail WHERE POID='" + Helper.EscapeSQLString(POID) + "' and ID ='" + POItemID + "'", con);

                DataTable dtReceive = Helper.NewDataTable("SELECT sum(case when rec.VendorWeight = '1' then recd.QtyVendor else recd.QtyReceive end) as QtyReceive, sum(recd.QtyLoss) as QtyLoss, recd.POID, recd.POItemID FROM ReceiveDetail recd inner join Receive rec on recd.ReceiveID = rec.ReceiveID WHERE recd.POID='" + Helper.EscapeSQLString(POID) + "' and recd.POItemID ='" + POItemID + "' and recd.Status <> 'Cancel' and recd.ReceiveID <> '" + receiveid + "' group by recd.POID, recd.POItemID", con);

                decimal QtyPO = Convert.ToDecimal(dtPODetail.Rows[0]["Qty"].ToString());
                decimal QtyReceive, QtyLoss;
                if (dtReceive.Rows.Count > 0)
                {
                    QtyReceive = Convert.ToDecimal(dtReceive.Rows[0]["QtyReceive"].ToString());
                    QtyLoss = Convert.ToDecimal(dtReceive.Rows[0]["QtyLoss"].ToString());
                }
                else
                {
                    QtyReceive = Convert.ToDecimal("0.00");
                    QtyLoss = Convert.ToDecimal("0.00");
                }


                string status = "In Process";

                if (QtyReceive + QtyLoss + Qty >= QtyPO)
                {
                    status = "Closed";
                }

                var podetail = (from a in ctx.PODetail where a.POID == POID && a.ID == POItemID select a).FirstOrDefault();

                if (podetail != null)
                {
                    podetail.Status = status;
                }

                DataTable dt = Helper.NewDataTable("SELECT * FROM PODetail WHERE POID='" + Helper.EscapeSQLString(POID) + "'", con);

                var podetail1 = from a in ctx.PODetail where a.POID == POID select a;

                int jumlah = 0;
                string StatusMaster = "";

                foreach (var col in podetail1)
                {
                    if (col.Status == "Closed")
                    {
                        jumlah = jumlah + 1;
                    }
                }

                if (jumlah == dt.Rows.Count)
                {
                    StatusMaster = "Closed";
                }
                else
                {
                    StatusMaster = "In Process";
                }

                var po = (from a in ctx.PO where a.POID == POID select a).FirstOrDefault();

                if (po != null)
                {
                    po.Status = StatusMaster;
                }
            }
        }
    }

    protected void UpdateUIStatus(string POID, byte POItemID, decimal Qty, GoodsReceiptDTO input, EasyEntities ctx)
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            string receiveid = "";
            if (input.TransactionId != "")
            {
                receiveid = input.TransactionId;
            }
            DataTable dtPODetail = Helper.NewDataTable("SELECT * FROM PODetail WHERE POID='" + Helper.EscapeSQLString(POID) + "' and ID ='" + POItemID + "'", con);

            DataTable dtReceive = Helper.NewDataTable("SELECT sum(case when rec.VendorWeight = '1' then recd.QtyVendor else recd.QtyReceive end) as QtyReceive, sum(recd.QtyLoss) as QtyLoss, recd.POID, recd.POItemID FROM ReceiveDetail recd inner join Receive rec on recd.ReceiveID = rec.ReceiveID WHERE recd.POID='" + Helper.EscapeSQLString(POID) + "' and recd.POItemID ='" + POItemID + "' and recd.Status <> 'Cancel' and recd.ReceiveID <> '" + receiveid + "' group by recd.POID, recd.POItemID", con);

            string SPKBID = dtPODetail.Rows[0]["SPKBID"].ToString();
            string SPKBItemID = dtPODetail.Rows[0]["SPKBItemID"].ToString();

            DataTable dtSPKBDetail = Helper.NewDataTable(@"select a.SPKBID, a.SPKBItemID, c.PKBID, b.PKBItemID, b.QtyApprove-b.QtyReject as QtySPKB, SUM(a.Qty) as QtyPO, SUM(a.QtyCancel) as QtyCancelPO from PODetail a, SPKBDetail b, SPKB c
                    where a.SPKBID='" + SPKBID + "' and a.SPKBItemID='" + SPKBItemID + "' and a.Status<>'Cancel' and a.SPKBID=b.SPKBID and b.SPKBID=c.SPKBID and c.ForStock='1' and a.SPKBItemID=b.ID group by a.SPKBID,a.SPKBItemID, b.QtyApprove,b.QtyReject, c.PKBID, b.PKBItemID", con);
            //Debug.WriteLine(dtSPKBDetail.Rows.Count);
            if (dtSPKBDetail.Rows.Count > 0)
            {
                string PKBID = dtSPKBDetail.Rows[0]["PKBID"].ToString();
                byte PKBItemID = Convert.ToByte(dtSPKBDetail.Rows[0]["PKBItemID"].ToString());

                decimal QtyPO = Convert.ToDecimal(dtPODetail.Rows[0]["Qty"].ToString());
                decimal QtyReceive, QtyLoss;
                if (dtReceive.Rows.Count > 0)
                {
                    QtyReceive = Convert.ToDecimal(dtReceive.Rows[0]["QtyReceive"].ToString());
                    QtyLoss = Convert.ToDecimal(dtReceive.Rows[0]["QtyLoss"].ToString());
                }
                else
                {
                    QtyReceive = Convert.ToDecimal("0.00");
                    QtyLoss = Convert.ToDecimal("0.00");
                }

                string statusUI = "In Process";
                if (QtyReceive + QtyLoss + Qty >= QtyPO)
                {
                    statusUI = "Closed";
                }

                var uidetail = (from a in ctx.PKBDetail where a.PKBID == PKBID && a.ID == PKBItemID select a).FirstOrDefault();

                if (uidetail != null)
                {
                    uidetail.Status = statusUI;
                }

                DataTable dt = Helper.NewDataTable("SELECT * FROM PKBDetail WHERE PKBID='" + PKBID + "'", con);

                var uidetail1 = from a in ctx.PKBDetail where a.PKBID == PKBID select a;

                int jumlah = 0;
                string StatusMaster = "";

                foreach (var col in uidetail1)
                {
                    if (col.Status == "Closed")
                    {
                        jumlah = jumlah + 1;
                    }
                }

                if (jumlah == dt.Rows.Count)
                {
                    StatusMaster = "Closed";
                }
                else
                {
                    StatusMaster = "In Process";
                }

                var ui = (from a in ctx.PKB where a.PKBID == PKBID select a).FirstOrDefault();

                if (ui != null)
                {
                    ui.Status = StatusMaster;
                }
            }
        }

    }

    protected void UpdateInTransit(int index, decimal negasi, GoodsReceiptDTO input, EasyEntities ctx)
    {
        if (input.POType == "Shipping Point")
        {
            string itemid = input.Items[index].ItemId;
            var item = (from a in ctx.Item where a.ItemID == itemid select a).FirstOrDefault();

            if (item != null)
            {
                item.InTransit = item.InTransit - (input.Items[index].QtyReceive + input.Items[index].QtyLoss) * negasi;
            }
        }
    }

    protected void UpdateStock(int index, decimal negasi, GoodsReceiptDTO input, EasyEntities ctx, string loc)
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            if (input.POType == "Shipping Point")
            {
                string ItemID = input.Items[index].ItemId;
                string purchaseID = input.Items[index].PurchaseId;
                decimal qty = 0;
                decimal totalItem = 0;
                for (int i = 0; i < input.Items.Count(); i++)
                {
                    DataTable dt_po = Helper.NewDataTable("SELECT ((b.Price + b.AdditionalCost - b.Discount)*Conversion) as Price,(b.Qty + b.QtyLoss) as Qty FROM Purchase a, PurchaseDetail b WHERE b.POID ='" + Helper.EscapeSQLString(input.Items[i].POId) + "' and b.ID = '" + input.Items[i].POItemId.ToString() + "' and a.PurchaseID = '" + Helper.EscapeSQLString(input.Items[i].PurchaseId) + "' and a.PurchaseID = b.PurchaseID", con);
                    decimal qtytotal = Convert.ToDecimal(dt_po.Rows[0]["qty"]);
                    totalItem = totalItem + (Convert.ToDecimal(dt_po.Rows[0]["Price"]) * qtytotal);
                }
                decimal qty1 = input.Items[index].QtyReceive + input.Items[index].QtyReceiveEtc - input.Items[index].QtyLoss;

                DataTable dt_po1 = Helper.NewDataTable("SELECT ((b.Price + b.AdditionalCost - b.Discount)*Conversion) as Price, a.IsRounding ,(b.Qty + b.QtyLoss) as Qty FROM Purchase a, PurchaseDetail b WHERE b.POID ='" + Helper.EscapeSQLString(input.Items[index].POId) + "' and b.ID = '" + input.Items[index].POItemId.ToString() + "' and a.PurchaseID = '" + Helper.EscapeSQLString(input.Items[index].PurchaseId) + "' and a.PurchaseID = b.PurchaseID", con);
                qty = Convert.ToDecimal(dt_po1.Rows[0]["Qty"]);

                decimal rounding = Convert.ToDecimal(dt_po1.Rows[0]["Price"].ToString());

                if (dt_po1.Rows[0]["IsRounding"].ToString() == "Yes")
                {
                    rounding = Math.Round(rounding);
                }

                DataTable dtFreightInsurance = Helper.NewDataTable("SELECT * from Purchase where PurchaseID='" + Helper.EscapeSQLString(purchaseID) + "'", con);
                decimal freightInsurance = Convert.ToDecimal(dtFreightInsurance.Rows[0]["FreightCost"]) + Convert.ToDecimal(dtFreightInsurance.Rows[0]["InsuranceCost"]);
                decimal koefisien = 0;
                decimal newPrice = 0;

                decimal subtotal = qty1 * rounding;
                koefisien = subtotal / totalItem; // untuk mengikutsertakan harga freight dan insurance dalam rata-rata price
                newPrice = (subtotal + (freightInsurance * koefisien)) / qty;
                subtotal = qty * newPrice;
                Debug.WriteLine("Detail " + (index + 1) + "     " + newPrice);
                decimal price = 0;

                var itemoldvalue = from a in ctx.Item where a.ItemID == ItemID select a;
                var itemoldvaluepisah = itemoldvalue.FirstOrDefault();
                if (itemoldvalue.Count() > 0)
                {
                    decimal oldQty = itemoldvaluepisah.Qty;
                    decimal oldPrice = itemoldvaluepisah.Price;

                    decimal hasil_bagi = oldQty - qty1;
                    if (hasil_bagi <= 0)
                    {
                        hasil_bagi = 1;
                    }
                    if (negasi < 0)
                    {
                        price = ((oldPrice * oldQty) - subtotal) / hasil_bagi;
                    }
                    else
                    {
                        price = (subtotal + (oldPrice * oldQty)) / (qty1 + oldQty);
                    }
                }

                var item = (from a in ctx.Item where a.ItemID == ItemID select a).FirstOrDefault();
                var itd = (from a in ctx.ItemDetail where a.ItemID == ItemID && a.LocationID == loc select a).FirstOrDefault();

                if (item != null)
                {
                    item.Qty = item.Qty + ((qty1) * negasi);
                    item.Price = price;

                    if (item.Qty == 0)
                    {
                        item.Price = 0;
                    }
                }

                if (itd != null)
                {
                    itd.Qty = itd.Qty + ((qty1) * negasi);
                }
                else
                {
                    ctx.ItemDetail.AddObject(new ItemDetail()
                    {
                        ItemID = ItemID,
                        LocationID = loc,
                        Qty = ((qty1)),
                        Qtys = 0,
                        InTransit = 0,
                        InTransits = 0
                    });

                    ctx.ItemOBDetail.AddObject(new ItemOBDetail()
                    {
                        ItemID = ItemID,
                        LocationID = loc,
                        Qty = ((qty1)),
                        Qtys = 0,
                        InTransit = 0,
                        InTransits = 0,
                        DateUpdated = DateTime.Now
                    });
                }
            }
        }
    }

    protected void Update_status_Weigh(string Status, string WeighID, EasyEntities ctx)
    {
        //var weigh = (from a in ctx.Weigh where a.WeighID == WeighID select a).FirstOrDefault();

        //if (weigh != null)
        //{
        //    weigh.Status = Status;
        //}
    }

    private void GetNettoWeigh()
    {
        string weighid = http.Request.QueryString["weighid"];
        if (weighid.Trim() != "")
        {
            using (SqlConnection con = new SqlConnection(conString))
            {
                DataTable dtweigh = Helper.NewDataTable("Select * from Weigh where WeighID = '" + weighid + "'", con);
                if (dtweigh.Rows.Count > 0)
                {
                    http.Response.Write(dtweigh.Rows[0]["Netto"].ToString());
                }
            }
        }
        http.Response.Write("0.00");
    }

    private bool CheckPOPPNType(GoodsReceiptDTO input, EasyEntities ctx)
    {
        int same = 0;
        if (input.Items.Count() > 0)
        {
            if (input.Items[0].POId != "")
            {
                string poid1 = input.Items[0].POId;
                var po1 = (from a in ctx.PO where a.POID == poid1 select a).FirstOrDefault();
                for (int i = 1; i < input.Items.Count(); i++)
                {
                    if (input.Items[i].POId != "")
                    {
                        string poid2 = input.Items[i].POId;
                        var po2 = (from a in ctx.PO where a.POID == poid2 select a).FirstOrDefault();

                        if (po1.IsPPN != po2.IsPPN)
                        {
                            same = 1;
                            break;
                        }
                    }
                }

                if (same == 1)
                {
                    return false;
                }
            }
        }
        return true;
    }

    private string CheckPPNTransactionIDAndDetail(string poid, string transactionid, EasyEntities ctx)
    {
        string bulan = transactionid.Trim().Substring(7, 1);

        int ppn;
        bool isNumeric = int.TryParse(bulan, out ppn);

        var po = (from a in ctx.PO where a.POID == poid select a).FirstOrDefault();

        string pesan = "";
        if (Convert.ToBoolean(po.IsPPN) != isNumeric)
        {
            pesan = "PPN PO transaction";
            if (ppn == 1)
            {
                pesan = "Non PPN PO transaction";
            }
        }
        return pesan;
    }

    private string CheckDateAllowEdit(DateTime date, EasyEntities ctx)
    {
        string status = "cancel";
        DateTime tmp = date.AddDays(1);
        var receives = from a in ctx.Receive where a.Date > tmp && a.Status != status orderby a.Date descending select a;
        var receive = receives.FirstOrDefault();
        if (receives.Count() > 0)
        {
            return receive.Date.ToString("dd/MM/yyyy");
        }
        return "";
    }

    private void CheckStatusDetailAllowEdit(GoodsReceiptDTO input, Dictionary<string, List<string>> err, string TransactionID, EasyEntities ctx)
    {
        string status = "Closed";
        var receivedetails = from a in ctx.ReceiveDetail join b in ctx.PODetail on a.POID equals b.POID join c in ctx.SPKBDetail on b.SPKBID equals c.SPKBID where a.ReceiveID == TransactionID && a.Status == status && b.SPKBItemID == c.ID && a.POItemID == b.ID select new { a.POID, a.POItemID, a.QtyReceive, a.QtyVendor, a.QtyReceiveEtc, a.QtyLoss, a.Remark, c.ItemID };
        if (receivedetails.Count() > 0)
        {
            foreach (var receivedetail in receivedetails)
            {
                int found = 0;
                for (int i = 0; i < input.Items.Count; i++)
                {
                    if (input.Items[i].POId != "")
                    {
                        if (receivedetail.POID == input.Items[i].POId && receivedetail.POItemID == Convert.ToByte(input.Items[i].POItemId))
                        {
                            found = 1;
                            if (receivedetail.QtyReceive != input.Items[i].QtyReceive)
                            {
                                Helper.AddError(err, "detail", "Can not edit  Qty Receive on row : " + i + ", status has been closed!");
                            }

                            if (receivedetail.QtyVendor != input.Items[i].QtyVendor)
                            {
                                Helper.AddError(err, "detail", "Can not edit  Qty Vendor on row : " + i + ", status has been closed!");
                            }

                            if (receivedetail.QtyReceiveEtc != input.Items[i].QtyReceiveEtc)
                            {
                                Helper.AddError(err, "detail", "Can not edit Qty Receive Etc on row : " + i + ", status has been closed!");
                            }

                            if (receivedetail.QtyLoss != input.Items[i].QtyLoss)
                            {
                                Helper.AddError(err, "detail", "Can not edit Qty Loss on row : " + i + ", status has been closed!");
                            }

                            if (receivedetail.Remark.Trim() != input.Items[i].Remark)
                            {
                                Helper.AddError(err, "detail", "Can not edit Remark on row : " + i + ", status has been closed!");
                            }
                        }
                    }
                }
                if (found == 0)
                {
                    Helper.AddError(err, "edit", "Can not delete detail transaction with PO ID = " + receivedetail.POID + " and Item ID = " + receivedetail.ItemID);
                }
            }
        }
    }

    private void GetDetailWeighPO()
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            string weighid = http.Request.QueryString["WeighID"];
            if (weighid.Trim() != "")
            {
                DataTable dt = Helper.NewDataTable("select row_number() OVER (order by w.POID, w.POItemID) DetailID, w.POID, w.POItemID, w.netto as QtyReceive, w.ItemID,w.ItemName as Item_Name, p.Type from Weigh w, PO p where w.WeighID = '" + weighid + "' and p.POID = w.POID", con);
                if (dt.Rows.Count > 0)
                {
                    http.Response.Write(Helper.DataTabletoJSON(dt));
                }
            }
        }
    }

    private bool CheckDoublePOItemID(GoodsReceiptDTO input)
    {
        for (int i = 0; i < input.Items.Count(); i++)
        {
            if (input.Items[i].POId != "")
            {
                string poid1 = input.Items[i].POId;
                string poitemid1 = input.Items[i].POItemId.ToString();
                for (int j = i + 1; j < input.Items.Count(); j++)
                {
                    if (input.Items[j].POId != "")
                    {
                        string poid2 = input.Items[j].POId;
                        string poitemid2 = input.Items[j].POItemId.ToString();
                        if (poid1 == poid2 && poitemid1 == poitemid2)
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
}