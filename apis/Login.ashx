﻿<%@ WebHandler Language="C#" Class="LoginHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;
using JWT;

public class LoginHandler : IHttpHandler
{
    string conString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
    JavaScriptSerializer json = new JavaScriptSerializer();
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "application/json";
        Dictionary<string, List<string>> err = new Dictionary<string,List<string>>();

        if (context.Request.RequestType != "POST")
        {
            context.Response.StatusCode = 400;
            Helper.AddError(err, "login", "Must use POST request!");
            context.Response.Write("Bad Request!");
        }
        else
        {
            try
            {
                Login(context.Request["username"], context.Request["password"], err, context);
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 500;
                Helper.ServerError(err, e);
                context.Response.Write(json.Serialize(err));
            }
        }
    }

    private void Login(string username, string password, Dictionary<string, List<string>> err, HttpContext context)
    {
        bool passwordNeedHashing = false;

        string appName = ConfigurationManager.AppSettings["SiteName"];
        string tokenKey = ConfigurationManager.AppSettings["TokenKey"];

        using (EasyEntities ctx = new EasyEntities())
        {
            string token = "";
            
            var q = from a in ctx.User where a.UserName == username select a;

            if (q.Count() == 0)
            {
                Helper.AddError(err, "username", "User does not exist!");
            }
            else
            {
                User user = q.FirstOrDefault<User>();

                if (user.Password.ToString().Length != 70)
                {
                    if (password == user.Password.ToString())
                    {
                        passwordNeedHashing = true;
                    }
                    else
                    {
                        Helper.AddError(err, "password", "Incorrect password!");
                    }
                }
                else if (!Password.Validate(password, user.Password.ToString()))
                {
                    Helper.AddError(err, "username", "Incorrect password!");
                }
                else if (user.Status == "Closed")
                {
                    Helper.AddError(err, "username", "This user is already closed!");
                }
                else
                {                    
                    LoggedUser us = new LoggedUser();
                    us.UserID = user.UserID;
                    us.Username = user.UserName;
                    us.Name = user.Name;
                    us.Initial = user.Initial;
                    us.DepartmentID = user.Department;
                    us.Role = user.Role;
                    us.Privilege = user.Privilege;

                    token = JsonWebToken.Encode(us, tokenKey, JwtHashAlgorithm.HS256);
                }

                if (passwordNeedHashing)
                {
                    string hash = Password.Hash(password).ToString();
                    user.Password = hash;
                    ctx.SaveChanges();
                                        
                    LoggedUser us = new LoggedUser();
                    us.UserID = user.UserID;
                    us.Username = user.UserName;
                    us.Name = user.Name;
                    us.Initial = user.Initial;
                    us.DepartmentID = user.Department;
                    us.Role = user.Role;
                    us.Privilege = user.Privilege;

                    token = JsonWebToken.Encode(us, tokenKey, JwtHashAlgorithm.HS256);
                }
            }

            if (err.Count > 0)
            {
                context.Response.StatusCode = 400;
                context.Response.Write(json.Serialize(err));
            }
            else
            {
                context.Response.StatusCode = 200;
                context.Response.Write(json.Serialize(token));
            }
        }
    }
    

    public bool IsReusable {
        get {
            return false;
        }
    }
}