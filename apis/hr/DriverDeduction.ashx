﻿<%@ WebHandler Language="C#" Class="DriverDeductionHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class DriverDeductinDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public int DriverId { get; set; }
    public int VehicleId { get; set; }
    public string Destination { get; set; }
    public int IsExternal { get; set; }
    public string DriverName { get; set; }
    public decimal Amount  { get; set; }
    public string Remark { get; set; }
    public int IsClaim { get; set; }
    public int IsMinus { get; set; }
    public int IsDebtCapital { get; set; }
    public string Status { get; set; }
    public byte[] RowVersion { get; set; } 
}

class DriverDeductionHandler : BaseHandler<DriverDeductinDTO>
{
    protected override void Insert(DriverDeductinDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string status = "Open";

        ctx.DriverDeduction.AddObject(new DriverDeduction()
        {
            TransactionNo = input.TransactionNo,
            TransactionDate = DateTime.Parse(input.TransactionDate),
            DriverId = input.DriverId,
            Amount = input.Amount,
            VehicleId = input.VehicleId,
            Destination = input.Destination,
            Remark = input.Remark,
            CreatedBy = user.UserID,
            IsExternal = input.IsExternal,
            IsClaim = input.IsClaim,
            IsMinus = input.IsMinus,
            IsDebtCapital = input.IsDebtCapital,
            Status = status,
            InputDate = DateTime.Now
        });

        Helper.InsertLog("HR-Transaction-DriverDeduction-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(DriverDeductinDTO input, DriverDeductinDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int transactionid = oldInput.Id;

        EntityKey key = new EntityKey("EasyEntities.DriverDeduction", "Id", transactionid);

        DriverDeduction dd = (DriverDeduction)ctx.GetObjectByKey(key);

        string transactionno = input.TransactionNo;
        string status = "Open";

        if (dd != null)
        {
            dd.TransactionNo = transactionno;
            dd.TransactionDate = DateTime.Parse(input.TransactionDate);
            dd.DriverId = input.DriverId;
            dd.VehicleId = input.VehicleId;
            dd.Destination = input.Destination;
            dd.Amount = input.Amount;
            dd.Remark = input.Remark;
            dd.IsExternal = input.IsExternal;
            dd.IsMinus = input.IsMinus;
            dd.IsClaim = input.IsClaim;
            dd.IsDebtCapital = input.IsDebtCapital;
            dd.ModifiedBy = Convert.ToInt32(user.UserID);
            dd.ModifiedDate = DateTime.Now;
            dd.Status = status;
        }

        Helper.InsertLog("HR-Transaction-DriverDeduction-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("HR-Transaction-DriverDeduction-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Cancel(DriverDeductinDTO input, DriverDeductinDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        string transactionno = input.TransactionNo;

        EntityKey key = new EntityKey("EasyEntities.DriverDeduction", "Id", oldInput.Id);

        DriverDeduction dd = (DriverDeduction)ctx.GetObjectByKey(key);

        dd.Status = "Cancel";

        Helper.InsertLog("HR-Transaction-DriverDeduction-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"select dd.Id, dd.IsClaim, dd.IsMinus, dd.IsDebtCapital,dd.Amount, us.Name as CreatedBy, dd.DriverId, d.Name as DriverName, vc.VehicleNo, dd.VehicleId, dd.Destination,
                                        CONVERT(varchar(10), dd.InputDate, 120) as InputDate, dd.IsExternal, CONVERT(varchar(10), dd.ModifiedDate, 120) as ModifiedDate , dd.Remark,
                                        dd.RowVersion, dd.Status, CONVERT(varchar(10), dd.TransactionDate, 120) as TransactionDate, 
                                        dd.TransactionNo, dd.ModifiedBy into #temptable1 
                                        from DriverDeduction dd inner join Driver d on d.Id = dd.DriverId inner join
                                        Vehicle vc on dd.VehicleId = vc.Id inner join 
                                        [User] us on dd.CreatedBy = us.UserID

                                        "+ Helper.PagingQuery (@"select data.* from(select dd.*, us.Name as ModifiedByName, ROW_NUMBER() OVER (ORDER BY dd.TransactionDate DESC, dd.Id DESC) as row
                                        from #temptable1 dd left outer join
                                        [User] us on dd.ModifiedBy = us.UserID       
                                        WHERE " + Helper.FieldFilterQuery(option, "dd.Status", "DriverName", "CONVERT(varchar(10), dd.TransactionDate, 120)", 
                                        "dd.Destination", "dd.TransactionNo","dd.Amount", "dd.Remark") + @") data
                                        ", option) + @"                             
                                        
                                        drop table #temptable1", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, DriverDeductinDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            
            if (list == "driver")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT dr.Id as DriverId, dr.Name as DriverName, vhd.VehicleId, vhc.VehicleNo, 
                    ROW_NUMBER() OVER (ORDER BY dr.Name) as row                 
                    FROM Driver dr
                    INNER JOIN VehicleDetails vhd 
                    ON dr.Id = vhd.DriverId 
                    INNER JOIN Vehicle vhc 
                    ON vhc.Id = vhd.VehicleId                                                                                  
                    WHERE " + Helper.FieldFilterQuery(option, "dr.Id", "dr.Name"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, DriverDeductinDTO input, DriverDeductinDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.DriverDeduction where a.Id == oldInput.Id select a).FirstOrDefault();
        
        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }

    protected override void Validate(BaseHandler<DriverDeductinDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, DriverDeductinDTO input, DriverDeductinDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.TransactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. is required!");
            }
            if (input.DriverId == 0 && input.DriverName == "")
            {
                Helper.AddError(err, "driver", "Driver is required!");
            }
            if (input.Amount == 0)
            {
                Helper.AddError(err, "amount", "Amount is required!");
            }
            if (input.Remark == "")
            {
                Helper.AddError(err, "remark", "Remark is required!");
            }
        }
    }
}