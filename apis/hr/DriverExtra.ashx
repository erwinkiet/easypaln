﻿<%@ WebHandler Language="C#" Class="DriverExtraHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class DriverExtraDTO
{
    public int Id { get; set; }
    public string TransactionNo { get; set; }
    public string TransactionDate { get; set; }
    public string CrossDate { get; set; }
    public int DriverId { get; set; }
    public int VehicleId { get; set; }
    public string DriverName { get; set; }
    public string Destination { get; set; } 
    public decimal Amount  { get; set; }
    public string Remark { get; set; }
    public string Status { get; set; }
    public byte[] RowVersion { get; set; } 
}

class DriverExtraHandler : BaseHandler<DriverExtraDTO>
{
    protected override void Insert(DriverExtraDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        string status = "Open";

        ctx.DriverExtras.AddObject(new DriverExtras()
        {
            TransactionNo = input.TransactionNo,
            TransactionDate = DateTime.Parse(input.TransactionDate),
            CrossDate = DateTime.Parse(input.CrossDate),
            DriverId = input.DriverId,
            VehicleId = input.VehicleId,
            Amount = input.Amount,
            Destination = input.Destination,
            Remark = input.Remark,
            CreatedBy = user.UserID,
            Status = status,
            InputDate = DateTime.Now
        });

        Helper.InsertLog("HR-Transaction-DriverExtra-Insert", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Update(DriverExtraDTO input, DriverExtraDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int transactionid = oldInput.Id;

        EntityKey key = new EntityKey("EasyEntities.DriverExtras", "Id", transactionid);

        DriverExtras dd = (DriverExtras)ctx.GetObjectByKey(key);

        string transactionno = input.TransactionNo;
        string status = "Open";

        if (dd != null)
        {
            dd.TransactionNo = transactionno;
            dd.TransactionDate = DateTime.Parse(input.TransactionDate);
            dd.CrossDate = DateTime.Parse(input.CrossDate);
            dd.DriverId = input.DriverId;
            dd.VehicleId = input.VehicleId;
            dd.Destination = input.Destination;
            dd.Amount = input.Amount;
            dd.Remark = input.Remark;
            dd.ModifiedBy = Convert.ToInt32(user.UserID);
            dd.ModifiedDate = DateTime.Now;
            dd.Status = status;
        }

        Helper.InsertLog("HR-Transaction-DriverExtra-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("HR-Transaction-DriverExtra-Update", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void Cancel(DriverExtraDTO input, DriverExtraDTO oldInput, EasyEntities ctx)
    {
        base.Cancel(input, oldInput, ctx);

        string transactionno = input.TransactionNo;

        EntityKey key = new EntityKey("EasyEntities.DriverExtras", "Id", oldInput.Id);

        DriverExtras dd = (DriverExtras)ctx.GetObjectByKey(key);

        dd.Status = "Cancel";

        Helper.InsertLog("HR-Transaction-DriverExtra-Cancel", "", ctx, input, user.UserID);

        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        int fromRow = (option.CurrentPage - 1) * option.PageSize + 1;
        int toRow = fromRow + option.PageSize - 1;
        
        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"select dd.Id, dd.Amount, CONVERT(varchar(10), dd.CrossDate, 120) as CrossDate, us.Name as CreatedBy, dd.DriverId, d.Name as DriverName, vc.Id as VehicleId, vc.VehicleNo,
                                        CONVERT(varchar(10), dd.InputDate, 120) as InputDate, CONVERT(varchar(10), dd.ModifiedDate, 120) as ModifiedDate , dd.Remark,
                                        dd.RowVersion, dd.Status, CONVERT(varchar(10), dd.TransactionDate, 120) as TransactionDate, 
                                        dd.TransactionNo, dd.Destination ,dd.ModifiedBy into #temptable1 
                                        from DriverExtras dd inner join Driver d on d.Id = dd.DriverId inner join
                                        [User] us on dd.CreatedBy = us.UserID inner join vehicle vc on dd.VehicleId = vc.Id

                                        select data.* from(select dd.*, us.Name as ModifiedByName, ROW_NUMBER() OVER (ORDER BY TransactionDate DESC , Id DESC) as row
                                        from #temptable1 dd left outer join
                                        [User] us on dd.ModifiedBy = us.UserID       
                                        WHERE " + Helper.FieldFilterQuery(option, "TransactionNo"   ,
                                        "DriverId", "Name", "CONVERT(varchar,TransactionDate,103)") + @") data
                                        WHERE data.row>=" + fromRow + " AND data.row <= " + toRow + @"


                                        order by TransactionDate desc, [Id] desc

                                        drop table #temptable1", con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetList(string list, DriverExtraDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;
            
            if (list == "driver")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT dr.Id as DriverId, dr.Name as DriverName, vd.VehicleId, vc.VehicleNo, 
                    ROW_NUMBER() OVER (ORDER BY dr.Name) as row                 
                    FROM Driver dr
                    INNER JOIN VehicleDetails vd
                    ON dr.Id = vd.DriverId
                    INNER JOIN Vehicle vc
                    ON vd.VehicleId = vc.Id                                                                                  
                    WHERE " + Helper.FieldFilterQuery(option, "dr.Id", "dr.Name"), option), con);
            }
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as Date", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, DriverExtraDTO input, DriverExtraDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        var tran = (from a in ctx.DriverExtras where a.Id == oldInput.Id select a).FirstOrDefault();
        
        if (Helper.CheckConcurrency(err, tran.RowVersion, oldInput.RowVersion))
        {
            Helper.ConcurrencyError(err);
        }
    }

    protected override void Validate(BaseHandler<DriverExtraDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, DriverExtraDTO input, DriverExtraDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.CrossDate == "")
            {
                Helper.AddError(err, "crossdate", "Cross Date must not be empty!");
            }
            if (input.TransactionNo == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. is required!");
            }
            if (input.DriverId == 0 && input.DriverName == "")
            {
                Helper.AddError(err, "driver", "Driver is required!");
            }
            if (input.Amount == 0)
            {
                Helper.AddError(err, "amount", "Amount is required!");
            }
            if (input.Remark == "")
            {
                Helper.AddError(err, "remark", "Remark is required!");
            }
        }
    }
}