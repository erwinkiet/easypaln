﻿<%@ WebHandler Language="C#" Class="RptDriverHonorHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class RptDriverHonorDTO
{
    public int DriverId { get; set; }
    public string DriverName { get; set; }
    public int VehicleId { get; set; }
    public string VehicleNo { get; set; }
}

class RptDriverHonorHandler : BaseHandler<RptDriverHonorDTO>
{

    protected override void GetList(string list, RptDriverHonorDTO input, ListOption option, int currentDetailIndex)
    {
        base.GetList(list, input, option, currentDetailIndex);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = null;

            if (list == "driver")
            {
                dt = Helper.NewDataTable(Helper.PagingQuery(@"SELECT d.Id as DriverId, d.Name as DriverName, v.VehicleNo, 
                                        ROW_NUMBER() OVER (ORDER BY d.Id) as row 
                                        FROM Driver d
                                        INNER JOIN VehicleDetails vd
                                        on vd.DriverId = d.Id
                                        INNER JOIN Vehicle v
                                        on v.Id = vd.VehicleId     
                                        WHERE " + Helper.FieldFilterQuery(option, "d.Id", "d.Name", "v.VehicleNo"), option), con);
            }
           

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    
    protected override void GetDefaultData()
    {
        base.GetDefaultData();

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(@"
                select convert(varchar(10),GETDATE(),120) as fromDate, convert(varchar(10),GETDATE(),120) as toDate", con);
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
}

