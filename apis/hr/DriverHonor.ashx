﻿<%@ WebHandler Language="C#" Class="DriverHonorHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;

class DriverHonorDTO
{
    public string Id { get; set; }
    public string Destination { get; set; }
    public decimal Honor { get; set; }
}

class DriverHonorHandler : BaseHandler<DriverHonorDTO>
{
    protected override void Insert(DriverHonorDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);
        ctx.DriverHonors.AddObject(new DriverHonors()
        {
            Destination = input.Destination,
            Honor = Convert.ToDecimal(input.Honor)

        });
        Helper.InsertLog("HR-Master-DriverHonors-Insert", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    protected override void Delete(DriverHonorDTO input, DriverHonorDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);
        string destination = oldInput.Destination;
        EntityKey key = new EntityKey("EasyEntities.DriverHonors", "Destination", destination);
        DriverHonors driverhonors = (DriverHonors)ctx.GetObjectByKey(key);

        if (driverhonors != null)
        {
            ctx.DeleteObject(driverhonors);
        }
        Helper.InsertLog("HR-Master-DriverHonors-Delete", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    protected override void Update(DriverHonorDTO input, DriverHonorDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);
        string destination = oldInput.Destination;
        EntityKey key = new EntityKey("EasyEntities.DriverHonors", "Destination", destination);
        DriverHonors driverhonors = (DriverHonors)ctx.GetObjectByKey(key);

        driverhonors.Destination = input.Destination;
        driverhonors.Honor = Convert.ToDecimal(input.Honor);
        Helper.InsertLog("HR-Master-DriverHonors-Update", "Old", ctx, oldInput, Convert.ToInt32(user.UserID));
        Helper.InsertLog("HR-Master-DriverHonors-Update", "", ctx, input, Convert.ToInt32(user.UserID));
        ctx.SaveChanges();
    }
    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, DriverHonorDTO input, DriverHonorDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);
        
        string destination = oldInput.Destination;
        DriverHonors driverhonor = (from a in ctx.DriverHonors where a.Destination == destination select a).FirstOrDefault();

        if (driverhonor == null)
        {
            Helper.ConcurrencyError(err);
        }
        else if (driverhonor.Destination != oldInput.Destination || driverhonor.Honor != Convert.ToDecimal(oldInput.Honor))
        {
            Helper.ConcurrencyError(err);
        }
    }
    
    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(@"
                        select Id, Destination, Honor, 
                        ROW_NUMBER() OVER (ORDER BY Id) as row from DriverHonors                   
                         WHERE " + Helper.FieldFilterQuery(option,
                        "Destination", "Honor"), option), con);

            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }
    protected override void Validate(BaseHandler<DriverHonorDTO>.ValidateMode mode, Dictionary<string, List<string>> err, EasyEntities ctx, DriverHonorDTO input, DriverHonorDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.Destination == "")
            {
                Helper.AddError(err, "transactionNo", "Transaction No. is required!");
            }
            if (input.Honor == 0)
            {
                Helper.AddError(err, "Honor", "Remark is required!");
            }
        }
        if (mode == ValidateMode.Update || mode == ValidateMode.Delete)
        {
            string id = input.Destination;
            var usecurrency = from a in ctx.DriverHonors where a.Destination == id select a;
            var cur = (from a in ctx.DriverHonors where a.Destination == id select a).FirstOrDefault();

            if (usecurrency.Count() > 0)
            {
                if (mode == ValidateMode.Update)
                {
                    if (input.Destination != cur.Destination)
                    {
                        Helper.AddError(err, "Destination", "Destination can not be changed because already used!");
                    }
                }

                if (mode == ValidateMode.Delete)
                {
                    Helper.AddError(err, "DestinationDelete", "Destination can not be deleted because already used!");
                }
            }
        }
    }
}