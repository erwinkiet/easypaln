﻿<%@ WebHandler Language="C#" Class="UserHandler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using EasyModel;
using System.Linq;
using System.Text.RegularExpressions;

class UserDTO
{
    public int UserId { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string PasswordConfirm { get; set; }
    public string DepartmentId { get; set; }
    public string Role { get; set; }
    public string Name { get; set; }
    public string Initial { get; set; }
    public string Status { get; set; }
    public string Privilege { get; set; }
    public List<int> PermissionIds { get; set; }
    public List<string> WarehouseIds { get; set; }
}

class UserHandler : BaseHandler<UserDTO>
{
    protected override void Insert(UserDTO input, EasyEntities ctx)
    {
        base.Insert(input, ctx);

        var users = from a in ctx.User orderby a.UserID descending select a;
        int uid = users.FirstOrDefault().UserID + 1;
        string pw = Password.Hash(input.Password);

        string depName = (from a in ctx.Department where a.DepartmentID == input.DepartmentId select a.Name).FirstOrDefault();

        ctx.User.AddObject(new User()
        {
            UserID = uid,
            UserName = input.Username,
            Password = pw,
            Department = input.DepartmentId,
            DepartmentName = depName,
            Role = input.Role,
            Name = input.Name,
            Initial = input.Initial.ToUpper(),
            Status = "Open",
            Privilege = input.Privilege
        });

        foreach (int i in input.PermissionIds)
        {
            ctx.UserPermission.AddObject(new UserPermission()
            {
                UserID = uid,
                PermissionID = i
            });
        }

        foreach (string i in input.WarehouseIds)
        {
            ctx.UserWarehouse.AddObject(new UserWarehouse()
            {
                UserID = uid,
                LocationID = i
            });
        }

        Helper.InsertLog("General-Master-UserProfile-Insert", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Update(UserDTO input, UserDTO oldInput, EasyEntities ctx)
    {
        base.Update(input, oldInput, ctx);

        int uid = oldInput.UserId;
        EntityKey key = new EntityKey("EasyEntities.User", "UserID", input.UserId);

        User u = (User)ctx.GetObjectByKey(key);
        var ups = from a in ctx.UserPermission where a.UserID == uid select a;
        var uws = from a in ctx.UserWarehouse where a.UserID == uid select a;

        if (u != null)
        {
            foreach (var up in ups)
            {
                ctx.DeleteObject(up);
            }

            foreach (var uw in uws)
            {
                ctx.DeleteObject(uw);
            }

            string pw = input.Password;
            if (pw != "")
            {
                pw = Password.Hash(pw);
            }

            u.UserName = input.Username;
            if (pw != "")
            {
                u.Password = pw;
            }
            
            string depName = (from a in ctx.Department where a.DepartmentID == input.DepartmentId select a.Name).FirstOrDefault();

            u.Department = input.DepartmentId;
            //u.DepartmentName = depName;
            u.Role = input.Role;
            u.Name = input.Name;
            u.Initial = input.Initial.ToUpper();
            u.Privilege = input.Privilege;

            foreach (int i in input.PermissionIds)
            {
                ctx.UserPermission.AddObject(new UserPermission()
                {
                    UserID = uid,
                    PermissionID = i
                });
            }

            foreach (string i in input.WarehouseIds)
            {
                ctx.UserWarehouse.AddObject(new UserWarehouse()
                {
                    UserID = uid,
                    LocationID = i
                });
            }
        }

        Helper.InsertLog("General-Master-UserProfile-Update", "Old", ctx, oldInput, user.UserID);
        Helper.InsertLog("General-Master-UserProfile-Update", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Delete(UserDTO input, UserDTO oldInput, EasyEntities ctx)
    {
        base.Delete(input, oldInput, ctx);

        int uid = oldInput.UserId;
        EntityKey key = new EntityKey("EasyEntities.User", "UserID", uid);

        User u = (User)ctx.GetObjectByKey(key);
        var ups = from a in ctx.UserPermission where a.UserID == uid select a;
        var uws = from a in ctx.UserWarehouse where a.UserID == uid select a;

        if (u != null)
        {
            foreach (var up in ups)
            {
                ctx.DeleteObject(up);
            }

            foreach (var uw in uws)
            {
                ctx.DeleteObject(uw);
            }

            ctx.DeleteObject(u);
        }

        Helper.InsertLog("General-Master-UserProfile-Delete", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Open(UserDTO input, UserDTO oldInput, EasyEntities ctx)
    {
        base.Open(input, oldInput, ctx);

        int uid = oldInput.UserId;
        EntityKey key = new EntityKey("EasyEntities.User", "UserID", uid);

        User u = (User)ctx.GetObjectByKey(key);

        if (u != null)
        {
            u.Status = "Open";
        }

        Helper.InsertLog("General-Master-UserProfile-Open", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void Close(UserDTO input, UserDTO oldInput, EasyEntities ctx)
    {
        base.Close(input, oldInput, ctx);

        int uid = oldInput.UserId;
        EntityKey key = new EntityKey("EasyEntities.User", "UserID", uid);

        User u = (User)ctx.GetObjectByKey(key);

        if (u != null)
        {
            u.Status = "Closed";
        }

        Helper.InsertLog("General-Master-UserProfile-Close", "", ctx, input, user.UserID);
        ctx.SaveChanges();
    }

    protected override void GetMasterData(ListOption option)
    {
        base.GetMasterData(option);

        using (SqlConnection con = new SqlConnection(conString))
        {
            string query = "";

            if (user.Role == "Admin")
            {
                query = "SELECT *, ROW_NUMBER() OVER (ORDER BY UserID) as row FROM [User] WHERE ";
            }
            else if (user.Role == "Manager")
            {
                query = "SELECT *, ROW_NUMBER() OVER (ORDER BY UserID) as row FROM [User] WHERE Department='" + user.DepartmentID + "' AND Role<>'Admin' AND ";
            }
            else if (user.Role == "Supervisor")
            {
                query = "SELECT *, ROW_NUMBER() OVER (ORDER BY UserID) as row FROM [User] WHERE Department='" + user.DepartmentID + "' AND Role<>'Admin' AND Role<>'Manager' AND ";
            }
            else if (user.Role == "Standard User")
            {
                query = "SELECT *, ROW_NUMBER() OVER (ORDER BY UserID) as row FROM [User] WHERE UserID=" + user.UserID + " AND ";
            }

            query += Helper.FilterQuery(
                    option, "Status",
                    "UserID", "UserName", "Name",
                    "Initial", "Department", "DepartmentName", "Role");

            DataTable dt = Helper.NewDataTable(Helper.PagingQuery(query, option), con);
            
            http.Response.Write(Helper.DataTabletoJSON(dt));
        }
    }

    protected override void GetDetailData(string id, string detail)
    {
        base.GetDetailData(id, detail);

        using (SqlConnection con = new SqlConnection(conString))
        {            
            if (detail == "warehouses")
            {
                DataTable dt = Helper.NewDataTable(@"
                    SELECT * FROM UserWarehouse WHERE UserID=" + Helper.EscapeSQLString(id), con);
                    
                List<string> warehouseIds = new List<string>();

                foreach (DataRow dr in dt.Rows)
                {
                    warehouseIds.Add(dr["LocationID"].ToString());
                }

                http.Response.Write(json.Serialize(warehouseIds));
            }
            else if (detail == "permissions")
            {
                DataTable dt = Helper.NewDataTable(@"
                    SELECT * FROM UserPermission WHERE UserID=" + Helper.EscapeSQLString(id), con);

                List<int> permissionIds = new List<int>();

                foreach (DataRow dr in dt.Rows)
                {
                    permissionIds.Add(Convert.ToInt32(dr["PermissionID"]));
                }

                http.Response.Write(json.Serialize(permissionIds));
            }
        }
    }

    void GetChoiceData()
    {
        using (SqlConnection con = new SqlConnection(conString))
        {
            List<DataTable> dts = new List<DataTable>();

            DataTable dt;

            if (user.Role == "Admin")
            {
                dt = Helper.NewDataTable("SELECT * FROM Department", con);
            }
            else if (user.Role == "Supervisor" || user.Role == "Manager")
            {
                dt = Helper.NewDataTable(@"WITH DepartmentsCTE AS
                    ( 
                        SELECT *
                        FROM Department
                        WHERE DepartmentID = '" + user.DepartmentID + @"'
                        UNION ALL
                        SELECT a.*
                        FROM Department a 
                        INNER JOIN DepartmentsCTE s ON a.ParentDepartmentID = s.DepartmentID 
                    ) 
                    SELECT * FROM DepartmentsCTE", con);
            }
            else
            {
                dt = Helper.NewDataTable("SELECT * FROM Department WHERE DepartmentID='" + user.DepartmentID + "'", con);
            }
            dts.Add(dt);

            if (user.Role == "Standard User")
            {
                dts.Add(new DataTable());
            }
            else if (user.Role == "Supervisor" || user.Role == "Manager")
            {
                dt = Helper.NewDataTable("SELECT * FROM Permission ORDER BY Module, [Type], Form, Action WHERE Module='" + user.Privilege + "'", con);
                dts.Add(dt);
            }
            else
            {
                dt = Helper.NewDataTable("SELECT * FROM Permission ORDER BY Module, [Type], Form, Action", con);
                dts.Add(dt);
            }

            if (user.Role != "Standard User")
            {
                dt = Helper.NewDataTable("SELECT * FROM ItemLocation WHERE HasChild='0' and Status='Open' ORDER BY LocationID", con);
                dts.Add(dt);
            }
            else
            {
                dts.Add(new DataTable());
            }

            dt = new DataTable();
            dt.Columns.Add("DepartmentID");
            dt.Columns.Add("Name");
            if (user.Role == "Admin")
            {
                dt.Rows.Add("ACC", "Accounting");
                dt.Rows.Add("STR", "Storage");
                dt.Rows.Add("PUR", "Purchasing");
                dt.Rows.Add("FIN", "Finance");
                dt.Rows.Add("SAL", "Sales");
                dt.Rows.Add("LOG", "Logistic");
                dt.Rows.Add("MNF", "Manufacture");
                dt.Rows.Add("LAB", "Lab");
            }
            else
            {
                DataTable dttmp = Helper.NewDataTable("SELECT Privilege from [User] where UserID='" + user.UserID + "'", con);
                if (dttmp.Rows.Count > 0)
                {
                    switch (dttmp.Rows[0]["Privilege"].ToString())
                    {
                        case "ACC":
                            dt.Rows.Add("ACC", "Accounting");
                            break;
                        case "STR":
                            dt.Rows.Add("STR", "Storage");
                            break;
                        case "PUR":
                            dt.Rows.Add("PUR", "Purchasing");
                            break;
                        case "FIN":
                            dt.Rows.Add("FIN", "Finance");
                            break;
                        case "SAL":
                            dt.Rows.Add("SAL", "Sales");
                            break;
                        case "LOG":
                            dt.Rows.Add("LOG", "Logistic");
                            break;
                        case "MNF":
                            dt.Rows.Add("MNF", "Manufacture");
                            break;
                        case "LAB":
                            dt.Rows.Add("LAB", "Lab");
                            break;
                    }
                }
            }
            dts.Add(dt);

            dt = new DataTable();
            dt.Columns.Add("Role");
            dt.Columns.Add("Name");

            dt.Rows.Add("Standard User", "Standard User");

            if (user.Role == "Supervisor" || user.Role == "Manager" || user.Role == "Admin")
            {
                dt.Rows.Add("Supervisor", "Supervisor");
            }

            if (user.Role == "Manager" || user.Role == "Admin")
            {
                dt.Rows.Add("Manager", "Manager");
            }

            if (user.Role == "Admin")
            {
                dt.Rows.Add("Admin", "Admin");
            }
            
            
            dts.Add(dt);

            http.Response.Write(Helper.DataTablestoJSON(dts));
        }
    }

    protected override void CheckConcurrency(Dictionary<string, List<string>> err, EasyEntities ctx, UserDTO input, UserDTO oldInput)
    {
        base.CheckConcurrency(err, ctx, input, oldInput);

        int uid = oldInput.UserId;
        User user = (from a in ctx.User where a.UserID == uid select a).FirstOrDefault();

        if (user == null)
        {
            Helper.ConcurrencyError(err);
        }
        else if (user.UserName != oldInput.Username || user.Name != oldInput.Name ||
            user.Department != oldInput.DepartmentId ||
            user.Role != oldInput.Role || user.Initial != oldInput.Initial ||
            user.Status != oldInput.Status ||
            user.Privilege != oldInput.Privilege)
        {
            Helper.ConcurrencyError(err);
        }

        var ups = from a in ctx.UserPermission where a.UserID == uid select a;
        if (ups.Count() != oldInput.PermissionIds.Count)
        {
            Helper.ConcurrencyError(err);
        }

        foreach (int p in oldInput.PermissionIds)
        {
            UserPermission up = (from a in ctx.UserPermission where a.UserID == uid && a.PermissionID == p select a).FirstOrDefault();

            if (up == null)
            {
                Helper.ConcurrencyError(err);
            }
        }

        foreach (string l in oldInput.WarehouseIds)
        {
            UserWarehouse ul = (from a in ctx.UserWarehouse where a.UserID == uid && a.LocationID == l select a).FirstOrDefault();

            if (ul == null)
            {
                Helper.ConcurrencyError(err);
            }
        }
    }

    protected override void Validate(ValidateMode mode, Dictionary<string, List<string>> err,
        EasyEntities ctx, UserDTO input, UserDTO oldInput)
    {
        base.Validate(mode, err, ctx, input, oldInput);
        
        if (mode == ValidateMode.Delete)
        {
            var u = from a in ctx.ApplicationLog where a.UserID == oldInput.UserId select a;

            if (u.Count() > 0)
            {
                Helper.AddError(err, "username", "This user can not be deleted because it already has activities!");
            }
        }

        if (mode == ValidateMode.Insert || mode == ValidateMode.Update)
        {
            if (input.Username == "")
            {
                Helper.AddError(err, "username", "Username must not be empty!");
            }
            else
            {
                Regex r = new Regex(@"^[a-zA-Z][a-zA-Z0-9]{2,20}$");
                if (!r.IsMatch(input.Username))
                {
                    Helper.AddError(err, "username", "Username must start with a letter, may only contain letters and numbers, and must be between 3 and 20 characters long!");
                }
                else
                {
                    var u = from a in ctx.User where a.UserName == input.Username && a.UserID != input.UserId select a;

                    if (u.Count() > 0)
                    {
                        Helper.AddError(err, "username", "Username already exists!");
                    }
                }
            }

            if (mode == ValidateMode.Insert && input.Password == "")
            {
                Helper.AddError(err, "password", "Password must not be empty!");
            }
            else if (input.Password != input.PasswordConfirm)
            {
                Helper.AddError(err, "passwordConfirm", "Confirm password does not match with password!");
            }

            if (input.DepartmentId == "" || input.DepartmentId == "Select")
            {
                Helper.AddError(err, "department", "Department must not be empty!");
            }

            if (input.Role == "" || input.Role == "Select")
            {
                Helper.AddError(err, "role", "Role must not be empty!");
            }

            if (input.Name == "")
            {
                Helper.AddError(err, "name", "Name must not be empty!");
            }

            if (input.Initial == "")
            {
                Helper.AddError(err, "initial", "Initial must not be empty!");
            }
        }
    }

    public override void ProcessRequest(HttpContext context)
    {
        this.http = context;

        context.Response.ContentType = "application/json";
        context.Response.TrySkipIisCustomErrors = true;

        Dictionary<string, List<string>> err = new Dictionary<string, List<string>>();

        try
        {
            user = Helper.GetCurrentUser(context);

            if (http.Request.RequestType == "GET" &&
                http.Request.QueryString["action"] == "getChoices")
            {
                GetChoiceData();
            }
            else
            {
                BeginProcess(err);
            }
        }
        catch (Exception e)
        {
            context.Response.StatusCode = 500;
            Helper.ServerError(err, e);
            context.Response.Write(json.Serialize(err));
        }
    }
}