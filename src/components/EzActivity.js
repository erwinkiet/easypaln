import { 
  initInput, updateTextInput, updateNumberInput,
  pushArrayInput, removeArrayInput, showList,
  submitActivity, initList, initMasterList, selectAndHideList,
  showMasterList, selectMasterList, getListData,
  refreshActivity, printActivity, changeActivitySchema,
  updateDateInput, initSelectedDetails, selectAllDetails,
  updateSelectedDetails, removeSelectedDetails, calculateTotal,
  clearActivitySchema, initServerInput, clearList,
  clearSelectedMaster, copySelectedDetails, pasteDetails
} from '../vuex/actions'
import EzMasterGrid from './EzMasterGrid'
import EzListGrid from './EzListGrid'
import EzActionModal from './EzActionModal'
import EzYesNoModal from './EzYesNoModal'
import * as helper from '../helper'
import { 
  input, oldInput, list, activitySchema, master, selectedMasterData, selectedDetails,
  user, permission
} from '../vuex/getters'
import { numberDisplay, dateDisplay } from '../filters'
import { dateBox, copyButton } from '../directives'

const Decimal = require('decimal.js')

export default {
  components: { EzMasterGrid, EzListGrid, EzActionModal, EzYesNoModal },
  filters: { numberDisplay, dateDisplay },
  directives: { dateBox, copyButton },
  data: function () {
    return {
      showActionModal: false,
      showPromptModal: false,
      promptMsg: '',
      promptAction: '',
      isOnAction: false,
      actionErrors: { 
        status: 0,
        data: null
      }
    }
  },
  ready: function () {
    if (this.activitySchema.hasOwnProperty('watch')) {
      let toDate103 = helper.toDate103

      for (let k in this.activitySchema.watch) {
        if (this.activitySchema.watch.hasOwnProperty(k)) {
          let v = this.activitySchema.watch[k]
          let tmp = { }


          this.$watch(`input.${k}`, function (val) {
            //if (v.hasOwnProperty('disableOnUpdate') && v.disableOnUpdate &&
            //  !helper.isEmptyObject(this.oldInput)) return
            
            if (v.hasOwnProperty('start')) {
              for (let i = 0; i < v.start.length; i++) {
                eval(v.start[i])
              }
            }
                  
            if (v.hasOwnProperty('detail')) {
              for (let i = 0; i < this.input[k].length; i++) {
                let detail = this.input[k][i]
                
                for (let i = 0; i < v.detail.length; i++) {
                  eval(v.detail[i])
                }
              }
            }
            
            if (v.hasOwnProperty('end')) {
              for (let i = 0; i < v.end.length; i++) {
                eval(v.end[i])
              }
            }     
          }, { deep: true })
        }
      }
    }
  },
  methods: {
    selectNextRow (gridDOMId, col, row, maxCol, maxRow, direction) {
      if ((direction < 0 && row <= 0) || (direction > 0 && row >= maxRow - 1)) {
        return
      }
      
      let detailGrid = document.getElementById(gridDOMId)
      let cellClass = 'cell' + col + (row + direction)
      let td = detailGrid.getElementsByClassName(cellClass)[0]
      let input = td.getElementsByTagName('input')[0]
      input.select()
    },
    prompt (action) {
      this.promptMsg = 'Are you sure you want to ' + action + '?'
      this.promptAction = action
      this.showPromptModal = true
    },
    onActivitySuccess (action, data) {    
      if (this.activitySchema.action[action].hasOwnProperty('bindOnSuccess')) {
        for (let k in this.activitySchema.action[action].bindOnSuccess) {
          if (this.activitySchema.action[action].bindOnSuccess.hasOwnProperty(k)) {
            let v = this.activitySchema.action[action].bindOnSuccess[k]

            if (v === 'data') {
              this.input[k] = data
            } else {
              this.updateTextInput(this.input, k, data[v])
            }
          }
        }
      }  

      if (this.activitySchema.action[action].hasOwnProperty('printOnSuccess') &&
        this.activitySchema.action[action].printOnSuccess) {
        this.openReport()
      }
    },
    onActivityFail (action) {
      window.setTimeout(function () {
        this.isOnAction = false
      })
    },
    onModalClose () {
      this.isOnAction = false
    },
    onPromptYes () {
      this.showActionModal = true
      this.isOnAction = true
      this.submitActivity(this.promptAction, this.actionErrors, this.onActivitySuccess, this.onActivityFail)      
    },
    onActivityClick (action) {
      this.showActionModal = true
      this.isOnAction = true
      this.submitActivity(action, this.actionErrors, this.onActivitySuccess, this.onActivityFail)
    },
    detailToTabString (detail, selectedDetails) {
      return helper.detailToTabString(detail, selectedDetails)
    },
    checkAllCheckboxes(id, check) {
      let checkboxes = new Array(); 
      checkboxes = document.getElementById(id).getElementsByTagName('input');
    
      for (var i = 0; i < checkboxes.length; i++)  {
        if (checkboxes[i].type == 'checkbox')   {
          checkboxes[i].checked = checktoggle;
        }
      }
    },
    openReport () {
      if (this.activitySchema.action.print.hasOwnProperty('method') && 
        this.activitySchema.action.print.method === 'POST') {
        if (this.activitySchema.action.print.hasOwnProperty('conditionalLinks')) {
          for (let k in this.activitySchema.action.print.conditionalLinks) {
            if (this.activitySchema.action.print.conditionalLinks.hasOwnProperty(k)) {
              let v = this.activitySchema.action.print.conditionalLinks[k]

              if (this.input[k]) {
                this.openPostReport(this.activitySchema.action.print, v)
              }
            }
          }
        } else {
          this.openPostReport(this.activitySchema.action.print, this.activitySchema.action.print.link)
        }
      } else {
        if (this.activitySchema.action.print.hasOwnProperty('links')) {          
          for (let i = 0; i < this.activitySchema.action.print.links.length; i++) {
            window.open(this.generateReportLink(this.activitySchema.action.print,
              this.activitySchema.action.print.links[i]), '_blank')
          }
        } else if (this.activitySchema.action.print.hasOwnProperty('conditionalLinks')) {
          for (let k in this.activitySchema.action.print.conditionalLinks) {
            if (this.activitySchema.action.print.conditionalLinks.hasOwnProperty(k)) {
              let v = this.activitySchema.action.print.conditionalLinks[k]

              if (this.input[k]) {
                window.open(this.generateReportLink(this.activitySchema.action.print,
                  v), '_blank')
              }
            }
          }
        } else {
          window.open(this.generateReportLink(this.activitySchema.action.print,
            this.activitySchema.action.print.link), '_blank')
        }
      }
    },
    openPostReport (r, link) {
      let form = document.createElement("form")

      form.action = link
      form.method = 'POST'
      form.target = "_blank"

      let input = document.createElement("textarea")
      input.name = r.masterParam
      input.value = JSON.stringify(this.selectedMasterData)
      form.appendChild(input);

      if (r.hasOwnProperty('inputParams')) {
        for(let k in r.inputParams) {
          if (r.inputParams.hasOwnProperty(k)) {
            let v = r.inputParams[k]

            input = document.createElement("textarea")
            input.name = k
            input.value = this.input[v]
            form.appendChild(input);
          }
        }
      }
  
      form.style.display = 'none';
      document.body.appendChild(form);
      form.submit();
    },
    generateReportLink (r, link) {
      let res = link
      
      if (r.hasOwnProperty('textParams') || r.hasOwnProperty('inputParams')) {
        res += '?'
        
        let tmp = []      
      
        if (r.hasOwnProperty('textParams')) {
          for (let k in r.textParams) {
            if (r.textParams.hasOwnProperty(k)) {
              let v = r.textParams[k]
              
              tmp.push(k + '=' + v)
            }
          }
        }
        if (r.hasOwnProperty('inputParams')) {
          for (let k in r.inputParams) {
            if (r.inputParams.hasOwnProperty(k)) {
              let v = r.inputParams[k]
              
              tmp.push(k + '=' + this.input[v])
            }
          }
        }
        
        res += tmp.join('&')
      }
      
      return res
    },
    noop () {
      this.isOnAction = false
    },
    initialize () {
      this.isOnAction = false

      this.clearSelectedMaster()
      
      if (this.activitySchema.hasOwnProperty('master')) {
        this.initMasterList(this.activitySchema.master)
      } else {
        this.initMasterList({ })
      }
      
      if (this.activitySchema.hasOwnProperty('list')) {
        this.initList(this.activitySchema.list)
      } else {
        this.initList({ })
      }
      
      if (this.activitySchema.hasOwnProperty('input')) {
        this.initInput(this.activitySchema.input)
      } else {
        this.initInput({ })
      }
      
      if (this.activitySchema.hasOwnProperty('master')) {
        this.showMasterList()
      }
      
      if (this.activitySchema.hasOwnProperty('initServerInput')) {
        this.initServerInput(this.activitySchema.initServerInput)
      }
    },
    getDefaultDetail (detailName) {
      return Object.assign({ }, this.activitySchema.defaultDetailInput[detailName])
    },
    generateDetailControl (k, v, inputField) {      
      let template = ''
      
      template += `<div class="input-group">`
      
      if (v.type === 'text' || v.type === 'textarea' || v.type === 'number' || v.type === 'date') {
        template += `
          ${v.type === 'textarea' ? '<textarea' : '<input'} 
          ${v.type === 'date' ? `v-date-box="'DD/MM/YYYY'"` : ''}
          v-bind:value="${v.value}${
            v.type === 'number' ? ` | numberDisplay${
              v.hasOwnProperty('numberFormat') ? ` '${v.numberFormat}'` : ''
            }` : 
            (v.type === 'date' ? ' | dateDisplay' : '')
          }" 
          class="ez-${v.type}-input"
          v-bind:disabled="${v.disabled ? v.disabled : false}"          
          ${v.type === 'number' ? 'v-on:focus="$event.target.select()"' : ''}
          v-on:change="${
            v.hasOwnProperty('list') ? `getListData(detail, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
            (v.type === 'number' ? `updateNumberInput(detail, '${k}', $event.target.value)` : 
            (v.type === 'date' ? `updateDateInput(detail, '${k}', $event.target.value)` : 
              `updateTextInput(detail, '${k}', $event.target.value)`))
          }"          
          ${
            v.type === 'number' && v.hasOwnProperty('list') ?
              `style="padding-right: 2.5em;"` : ''
          }
          ${v.type === 'textarea' ? '></textarea>' : ' />'}`
      } else if (v.type === 'checkbox') {
        template += `
          <input type="checkbox" 
            v-bind:true-value="${v.trueValue}" 
            v-bind:false-value="${v.falseValue}"
            v-model="${v.value}" 
            class="ez-${v.type}-input"
            v-bind:disabled="${v.disabled ? v.disabled : false}" />
            ${v.label}`
      } else if (v.type === 'dropdown') {
        template += `
          <select v-bind:value="${v.value}"
            class="ez-${v.type}-input"
            v-bind:disabled="${v.disabled ? v.disabled : false}" 
            v-on:change="${
              v.hasOwnProperty('list') ? `getListData(detail, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
                `updateTextInput(detail, '${k}', $event.target.value)`
            }">`
        
        for (let i = 0; i < v.options.length; i++) {
          let opt = v.options[i]
          
          template += `<option value="${opt.value}">${opt.label}</option>`
        }
        
        template += `</select>`
      }
      
      template += `${
            v.hasOwnProperty('list') ? 
            `<button class="list-button" type="button" v-on:keyup.enter.stop="" v-on:click="showList('${v.list}', list.${v.list}, detail, $index)"
                v-bind:disabled="${
                  v.hasOwnProperty('listDisabled') ? v.listDisabled : (v.hasOwnProperty('disabled') ? v.disabled : false)
                }">. . .</button>` : ''
          }
        </div>`
      
      return template
    },
    generateControl (k, v) {
      let template = ''
      
      if (v.type === 'detail') {
        this.initSelectedDetails(k)
        
        let gridId = 'detail-' + k
        
        template += `<div id="${gridId}" class="detail-control">
            <span class="detail-label">${v.label}</span>`
            
        if (v.hasOwnProperty('action')) {
          let l = v.action
          
          if (l.hasOwnProperty('add')) {
            template += ` 
              <button type="button" 
                v-on:click="pushArrayInput(input, '${k}', getDefaultDetail('${k}'))"
                ${l.add.hasOwnProperty('disabled') ? `v-bind:disabled="${l.add.disabled}"` : '' }>
                Add Row
              </button>`
          }
          
          if (l.hasOwnProperty('delete')) {
            template += ` 
              <button type="button" 
                v-on:click="removeSelectedDetails(input, '${k}')"
                ${l['delete'].hasOwnProperty('disabled') ? `v-bind:disabled="${l['delete'].disabled}"` : '' }>
                Delete Rows
              </button>`
          }            

          if (l.hasOwnProperty('copy')) {
            template += ` 
              <button type="button" 
                v-on:click="copySelectedDetails(input, '${k}', getDefaultDetail('${k}'), 
                  [${
                    l.copy.hasOwnProperty('fields') ? 
                    l.copy.fields.map(x => `'${x}'`) : ''
                  }])">
                Copy Rows
              </button>`
          } 

          if (l.hasOwnProperty('paste')) {
            template += ` 
              <button type="button" 
                v-on:click="pasteDetails(input, '${k}')">
                Paste Rows
              </button>`
          } 
        }
     
        // <button type="button" v-copy-button="detailToTabString(input.${k}, selectedDetails.${k})" >Copy Rows</button>
        template += ` <button type="button" v-on:click="selectAllDetails(input, '${k}')">Select All</button>
            
            <div class="grid-container">
              <table>
                <thead>
                  <tr><th class="select-box"></th>`
                  
        let maxCol = 0
        for (let l in v.control) {
          if (v.control.hasOwnProperty(l)) {
            template += `<th v-bind:style="{
                width: '${v.control[l].width ? v.control[l].width : 100}px'
              }">`
            template += v.control[l].label
            template += '</th>'
            
            maxCol++
          }    
        }
        
        template += `
                  </tr>
                </thead>
                
                <tbody>
                  <tr v-for="detail of ${v.value}" track-by="$index">
                    <td><input type="checkbox" v-model="selectedDetails.${k}" v-bind:value="$index" /></td>`
                  
        let i = 0
        for(let l in v.control) {
          if (v.control.hasOwnProperty(l)) {
            template += `<td v-bind:class="['cell${i}' + $index]" 
              v-on:keyup.enter.down="selectNextRow('${gridId}', ${i}, $index, ${maxCol}, ${v.value}.length, 1)"
              v-on:keyup.up="selectNextRow('${gridId}', ${i}, $index, ${maxCol}, ${v.value}.length, -1)">`
            template += this.generateDetailControl(l, v.control[l], k)
            template += '</td>'
            i++
          }    
        }
        
        template += `
                  </tr>
                </tbody>
              </table>
            </div>`        
                        
        template += `</div>`
      } else {
        template += `<div class="form-control${
          v.hasOwnProperty("inline") && v.inline ? ' inline-control' : ` grid-${
            v.hasOwnProperty('width') ? v.width : 6
          }`
        }" ${
            v.hasOwnProperty('show') ? `v-show="${v.show}"` : '' 
          }><label>${v.type !== 'checkbox' ? v.label : '&nbsp;'}
          <div class="input-group${v.hasOwnProperty ? ' with-list' : ''}">`
        
        if (v.type === 'text' || v.type === 'textarea' || v.type === 'number' || v.type === 'date') {
          template += `
            ${v.type === 'textarea' ? '<textarea' : '<input'} 
            type="${v.type === 'checkbox' ? 'checkbox' : 'text'}"
            ${v.type === 'date' ? `v-date-box="'DD/MM/YYYY'"` : ''}
            v-bind:value="${v.value}${
              v.type === 'number' ? ` | numberDisplay${
                v.hasOwnProperty('numberFormat') ? ` '${v.numberFormat}'` : ''
              }` : 
              (v.type === 'date' ? ' | dateDisplay' : '')
            }"
            class="ez-${v.type}-input"
            ${v.type === 'number' ? 'v-on:focus="$event.target.select()"' : ''}
            v-bind:disabled="${v.disabled ? v.disabled : false}"
            v-on:change="${
              v.hasOwnProperty('list') ? `getListData(input, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
              (v.type === 'number' ? `updateNumberInput(input, '${k}', $event.target.value)` : 
              (v.type === 'date' ? `updateDateInput(input, '${k}', $event.target.value)` : 
                `updateTextInput(input, '${k}', $event.target.value)`))
            }"
            ${
              v.type === 'number' && v.hasOwnProperty('list') ?
                `style="padding-right: 2.5em;"` : ''
            }
            ${v.type === 'textarea' ? '></textarea>' : ' />'}`
        } else if (v.type === 'checkbox') {
          template += `
            <input type="checkbox" 
              class="ez-${v.type}-input"
              v-bind:disabled="${v.disabled ? v.disabled : false}" 
              v-bind:true-value="${v.trueValue}" 
              v-bind:false-value="${v.falseValue}"
              v-model="${v.value}" />
              ${v.label}`
        } else if (v.type === 'radio') {
          for (let i = 0; i < v.options.length; i++) {
            let opt = v.options[i]
            template += `
              <label class="radio-option">
                <input type="radio" 
                  class="ez-${v.type}-input"
                  v-bind:disabled="${v.disabled ? v.disabled : false}" 
                  v-bind:value="${opt.value}" 
                  v-model="${v.value}" />
                  ${opt.label}
              </label>`
          }
        } else if (v.type === 'dropdown') {
          template += `
            <select v-bind:value="${v.value}"
              class="ez-${v.type}-input"
              v-bind:disabled="${v.disabled ? v.disabled : false}" 
              v-on:change="${
                v.hasOwnProperty('list') ? `getListData(input, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
                  `updateTextInput(input, '${k}', $event.target.value)`
              }">`
          
          for (let i = 0; i < v.options.length; i++) {
            let opt = v.options[i]
            
            if (opt.range) {
              for (let j = opt.range[0]; j <= opt.range[1]; j++) {
                template += `<option value="${j}">${j}</option>`
              }
            } else {
              template += `<option value="${opt.value}">${opt.label}</option>`
            }
          }
          
          template += `</select>`
        }
        
        template += `${
                v.hasOwnProperty('list') ? 
                `<button class="list-button" type="button" v-on:click="showList('${v.list}', list.${v.list}, input)"
                  v-bind:disabled="${
                    v.hasOwnProperty('listDisabled') ? v.listDisabled : (v.hasOwnProperty('disabled') ? v.disabled : false)
                  }">. . .</button>` : ''
              }
            </div>
          </label></div>`
      }

      return template
    }
  },
  created: function () {    
    this.initialize()
    
    let template = `<div class="ez-activity"><h1>${this.activitySchema.title}</h1><hr />`
    
    if (this.activitySchema.hasOwnProperty('debug') && this.activitySchema.debug) {
      template += `<div><pre>{{ selectedMasterData | json }}</pre></div><div><pre>{{ input | json }}</pre></div>`
    }

    if (this.activitySchema.hasOwnProperty('master')) {
      template += `<ez-master-grid v-bind:list="master" 
        v-bind:multiselect="${
          this.activitySchema.master.hasOwnProperty('multiselect') ? 'activitySchema.master.multiselect' : 'false'
        }" 
        v-bind:selection-var="selectedMasterData"></ez-master-grid>`
    }
    
    if (this.activitySchema.hasOwnProperty('list')) {
      for (let k in this.list) {
        if (this.list.hasOwnProperty(k)) {
          let v = this.list[k]
          
          template += `<ez-list-grid v-bind:list="list.${k}" v-bind:type="'${k}'"></ez-list-grid>`
        }
      }
    }
    
    template += `<form novalidate>`
    for (let k in this.activitySchema.control) {
      if (this.activitySchema.control.hasOwnProperty(k)) {
        let v = this.activitySchema.control[k]
        
        template += this.generateControl(k, v)
      }
    }
    template += `</form>`
    
    if (this.activitySchema.hasOwnProperty('action')) {
      let action = this.activitySchema.action
      template += `<br /><ez-action-modal v-bind:show.sync="showActionModal"
          v-bind:error-schema.sync="actionErrors" v-bind:on-success="${
            action.hasOwnProperty("reload") && !action.reload ?
              'noop' :
              'initialize'
          }"
          v-on:close="onModalClose()"></ez-action-modal>
        <ez-yes-no-modal v-bind:show.sync="showPromptModal" v-on:yes="onPromptYes(${
            action.hasOwnProperty("reload") && !action.reload ?
              'noop' :
              'initialize'
          })"
          v-bind:msg="promptMsg"></ez-yes-no-modal>`
      for (let k in this.activitySchema.action) {
        if (this.activitySchema.action.hasOwnProperty(k)) {
          let v = this.activitySchema.action[k]

          if (k === 'reload') { continue }
          
          template += `<button type="button" class="action-button"
            ${
             v.hasOwnProperty('show') ? `v-show="${v.show}"` : '' 
            }
            v-bind:disabled="isOnAction${
              v.hasOwnProperty('disabled') ? ` || ${v.disabled}` : '' 
            }"
            v-on:click="${
              k === 'reset' ? `initialize` : 
              (k === 'print' ? `openReport` : 
              (v.prompt ? `prompt('${k}')` : `onActivityClick('${k}')`))
            }">${helper.capitalize(k)}</button> `
        }
      }
    }
    
    template += `</div>`
    this.$options.template = template
  },
  vuex: {
    actions: { 
      initInput, updateTextInput, updateNumberInput,
      pushArrayInput, removeArrayInput, showList,
      submitActivity, initList, initMasterList, selectAndHideList,
      showMasterList, selectMasterList, getListData,
      refreshActivity, printActivity, changeActivitySchema,
      updateDateInput, initSelectedDetails, selectAllDetails,
      updateSelectedDetails, removeSelectedDetails, calculateTotal,
      clearActivitySchema, initServerInput, clearList,
      clearSelectedMaster, copySelectedDetails, pasteDetails
    },
    getters: { 
      input, oldInput, list, activitySchema, master, selectedDetails, selectedMasterData,
      user, permission
    }
  }
}
