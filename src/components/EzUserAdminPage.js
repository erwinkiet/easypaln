import { 
  initInput, updateTextInput, updateNumberInput,
  pushArrayInput, removeArrayInput, showList,
  submitActivity, initList, initMasterList, selectAndHideList,
  showMasterList, selectMasterList, getListData,
  refreshActivity, printActivity, changeActivitySchema,
  updateDateInput, initSelectedDetails, selectAllDetails,
  updateSelectedDetails, removeSelectedDetails, calculateTotal,
  clearActivitySchema, initServerInput, clearList,
  setActivitySchema
} from '../vuex/actions'
import EzHeader from './EzHeader'
import EzFooter from './EzFooter'
import EzMasterGrid from './EzMasterGrid'
import EzListGrid from './EzListGrid'
import EzActionModal from './EzActionModal'
import EzYesNoModal from './EzYesNoModal'
import * as helper from '../helper'
import { 
  input, oldInput, list, activitySchema, master, selectedDetails,
  user, permission, selectedMasterData
} from '../vuex/getters'
import { numberDisplay, dateDisplay } from '../filters'
import { dateBox, copyButton } from '../directives'
import localStorage from 'store'

const schema = {
  "input": {
    "userId": 0,
    "username": "",
    "password": "",
    "passwordConfirm": "",
    "departmentId": "",
    "role": "",
    "name": "",
    "initial": "",
    "privilege": "",
    "status": "",
    "permissionIds": [],
    "warehouseIds": []
  },
  "master": {
    "data": [],
    "isLoading": false,
    "option": {
      "statusFilters": ["Open", "Closed"],
      "currentPage": 1,
      "pageSize": 10,
      "filters": ['']
    },
    "idField": "UserID",
    "binding": {
      "userId": "UserID",
      "username": "UserName",
      "departmentId": "Department",
      "role": "Role",
      "name": "Name",
      "initial": "Initial",
      "privilege": "Privilege",
      "status": "Status",
      "permissionIds": "permissions",
      "warehouseIds": "warehouses"
    },
    "display": {
      "UserID": {
        "label": "ID",
        "type": "text",
        "width": 100
      },
      "UserName": {
        "label": "Username",
        "type": "text",
        "width": 200
      },
      "Name": {
        "label": "Name",
        "type": "text",
        "width": 300
      },
      "Initial": {
        "label": "Initial",
        "type": "text",
        "width": 100
      },
      "Department": {
        "label": "Department",
        "type": "text",
        "width": 100
      },
      "DepartmentName": {
        "label": "DepartmentName",
        "type": "text",
        "width": 200
      },
      "Role": {
        "label": "Role",
        "type": "text",
        "width": 100
      },
      "Status": {
        "label": "Status",
        "type": "text",
        "width": 100
      }    
    }
  }
}

const Decimal = require('decimal.js')

export default {
  components: { EzMasterGrid, EzListGrid, EzActionModal, EzYesNoModal,
    EzHeader, EzFooter },
  filters: { numberDisplay, dateDisplay },
  directives: { dateBox, copyButton },
  data: function () {
    return {
      ashxFile: './apis/config/User.ashx',
      departmentData: [],
      roleData: [],
      warehouseData: [],
      rawPermissionData: [],
      permissionData: { },
      currentPermissionTab: 'General',
      privilegeData: [],
      showActionModal: false,
      showPromptModal: false,
      promptMsg: '',
      promptAction: '',
      isOnAction: false,
      actionErrors: { 
        status: 0,
        data: null
      }
    }
  },
  ready: function () {
    let file = './apis/config/User.ashx'

    this.$http.get(file, {
      action: 'getChoices'
    }).then(function (response) {
      this.departmentData = response.data[0]
      this.rawPermissionData = response.data[1]
      this.permissionData = { }
      this.warehouseData = response.data[2]
      this.privilegeData = response.data[3]
      this.roleData = response.data[4]
      
      let oldModule = ''
      let oldType = ''
      let oldForm = ''

      for (let k = 0; k < response.data[1].length; k++) {
        let v = response.data[1][k]

        if (v.Module !== oldModule) {
          this.permissionData[v.Module] = { }

          oldType = ''
          oldForm = ''
          oldModule = v.Module
        }

        if (v.Type !== oldType) {
          this.permissionData[v.Module][v.Type] = { }

          oldForm = ''
          oldType = v.Type
        }

        if (v.Form !== oldForm) {
          this.permissionData[v.Module][v.Type][v.Form] = []
          this.permissionData[v.Module][v.Type][v.Form].push(response.data[1][k])

          oldForm = v.Form
        } else {
          this.permissionData[v.Module][v.Type][v.Form].push(response.data[1][k])
        }
      }
    }, function (response) { })
  },
  methods: {
    changePermissionTab (tab) {
      this.currentPermissionTab = tab
    },
    checkAllPermission (mod = 'all') {
      if (mod === 'all') {
        this.input.permissionIds = []

        for (let i = 0; i < this.rawPermissionData.length; i++) {
          this.input.permissionIds.push(this.rawPermissionData[i].ID)
        }
      } else if (mod === 'none') {
        this.input.permissionIds = []
      } else {
        for (let i = 0; i < this.rawPermissionData.length; i++) {
          let v = this.rawPermissionData[i]          

          if (v.Module === mod) {
            let index = this.input.permissionIds.indexOf(v.ID)

            if (index <= -1) {
              this.input.permissionIds.push(v.ID)
            }
          }
        }
      }
    },
    prompt (action) {
      this.promptMsg = 'Are you sure you want to ' + action + '?'
      this.promptAction = action
      this.showPromptModal = true
    },
    onActivitySuccess () {
    },
    onActivityFail () {
      window.setTimeout(function () {
        this.isOnAction = false
      })
    },
    onModalClose () {
      this.isOnAction = false
    },
    onPromptYes (initialize) {
      this.showActionModal = true
      this.isOnAction = true
      this.submitActivity(this.promptAction, this.actionErrors, this.onActivitySuccess, this.onActivityFail, this.ashxFile)      
    },
    onActivityClick (action, initialize) {
      this.showActionModal = true
      this.isOnAction = true
      this.submitActivity(action, this.actionErrors, this.onActivitySuccess, this.onActivityFail, this.ashxFile)
    },
    initialize () {
      this.isOnAction = false
      
      if (this.activitySchema.hasOwnProperty('master')) {
        this.initMasterList(this.activitySchema.master)
      } else {
        this.initMasterList({ })
      }
      
      if (this.activitySchema.hasOwnProperty('list')) {
        this.initList(this.activitySchema.list)
      } else {
        this.initList({ })
      }
      
      if (this.activitySchema.hasOwnProperty('input')) {
        this.initInput(this.activitySchema.input)
      } else {
        this.initInput({ })
      }
      
      if (this.activitySchema.hasOwnProperty('master')) {
        this.showMasterList(this.ashxFile)
      }
    }
  },
  created: function () {    
    if (!localStorage.get('userToken')) {
      this.$route.router.go('./login')
    } else {
      // Start of temporary bug fix
      if (!localStorage.get('bugTmpFix')) {
        localStorage.set('bugTmpFix', 1)
      } else {
        let btf = localStorage.get('bugTmpFix')
        
        if (btf === 1) {
          localStorage.set('bugTmpFix', 0)
          location.reload()
          return
        } else {
          localStorage.set('bugTmpFix', 1)
        }
      }
    }

    this.setActivitySchema(schema)
    this.initialize()
  },  
  template:`
    <div class="ez-user-admin page">
      <ez-header></ez-header>

      <div class="content">
        <h1>Manage User</h1><hr />
        
        <ez-master-grid v-bind:list="master" v-bind:ashx-file="ashxFile"
          v-bind:multiselect="false" 
          v-bind:selection-var="selectedMasterData">
        </ez-master-grid>

        <form novalidate>
          <div class="form-control grid-6">
            <label>Username
              <div class="input-group">
                <input type="text" class="ez-text-input" v-model="input.username" />
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Department
              <div class="input-group">
                <select class="ez-dropdown-input" v-model="input.departmentId">
                  <option value="">Select</option>
                  <option v-for="dep in departmentData" v-bind:value="dep.DepartmentID">{{ dep.Name }}</option>               
                </select>
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Password
              <div class="input-group">
                <input type="password" class="ez-text-input" v-model="input.password" />
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Confirm Password
              <div class="input-group">
                <input type="password" class="ez-text-input" v-model="input.passwordConfirm" />
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Role
              <div class="input-group">
                <select class="ez-dropdown-input" v-model="input.role">
                  <option value="">Select</option>
                  <option v-for="role in roleData" v-bind:value="role.Role">{{ role.Name }}</option>                  
                </select>
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Privilege
              <div class="input-group">
                <select class="ez-dropdown-input" v-model="input.privilege">
                  <option value="">Select</option>
                  <option v-for="priv in privilegeData" v-bind:value="priv.DepartmentID">{{ priv.Name }}</option>            
                </select>
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Initial
              <div class="input-group">
                <input type="text" class="ez-text-input" v-model="input.initial" />
              </div>
            </label>
          </div>

          <div class="form-control grid-6">
            <label>Name
              <div class="input-group">
                <input type="text" class="ez-text-input" v-model="input.name" />
              </div>
            </label>
          </div>
        </form><br />

        <div class="warehouse-input">
          <h3>Warehouse</h3>
          <label v-for="war of warehouseData">
            <input type="checkbox" class="ez-checkbox-input" 
              v-bind:value="war.LocationID"
              v-model="input.warehouseIds" />
              {{ war.LocationID }} - {{ war.Name }}
          </label>
        </div><br />

        <div class="permission-input">
          <h3>Permission</h3>
          <ul class="ez-tabs">
            <li v-for="(mod, types) of permissionData" class="tab-item">
              <button type="button" class="tab-link" 
                v-on:click="changePermissionTab(mod)"
                v-bind:class="{ 'is-active': currentPermissionTab === mod }">{{ mod }}</button>
              <div class="tab-content" v-show="currentPermissionTab === mod">
                <button class="check-button" type="button" v-on:click="checkAllPermission()">Check All</button>
                <button class="check-button" type="button" v-on:click="checkAllPermission(mod)">Check All {{ mod }}</button>
                <button class="check-button" type="button" v-on:click="checkAllPermission('none')">Uncheck All</button>
                <div v-for="(type, forms) of types"><br />
                  <h5>{{ type }}</h5>
                  <div v-for="(form, perms) of forms">
                    <b class="form-name">{{ perms[0].Name }}</b>
                    <label v-for="perm of perms">
                      <input type="checkbox" class="ez-checkbox-input" 
                        v-bind:value="perm.ID"
                        v-model="input.permissionIds" />{{ perm.Action }}
                    </label>
                  </div>                
                </div>
              </div>
            </li>
          </ul>
        </div>

        <br />
        <ez-action-modal v-bind:show.sync="showActionModal"
          v-bind:error-schema.sync="actionErrors" v-bind:on-success="initialize"
          v-on:close="onModalClose()"></ez-action-modal>
        <ez-yes-no-modal v-bind:show.sync="showPromptModal" v-on:yes="onPromptYes(initialize)"
          v-bind:msg="promptMsg"></ez-yes-no-modal>

        <button type="button" class="action-button"
          v-bind:disabled="isOnAction" 
          v-show="input.status === ''"
          v-on:click="onActivityClick('insert', initialize)">Insert</button>        

        <button type="button" class="action-button"
          v-bind:disabled="isOnAction" 
          v-show="input.status !== ''"
          v-on:click="onActivityClick('update', initialize)">Update</button>        

        <button type="button" class="action-button"
          v-bind:disabled="isOnAction" 
          v-show="input.status !== ''"
          v-on:click="onActivityClick('delete', initialize)">Delete</button>

        <button type="button" class="action-button"
          v-bind:disabled="isOnAction" 
          v-show="input.status === 'Closed'"
          v-on:click="onActivityClick('open', initialize)">Open</button>

        <button type="button" class="action-button"
          v-bind:disabled="isOnAction" 
          v-show="input.status === 'Open'"
          v-on:click="onActivityClick('close', initialize)">Close</button>        

        <button type="button" class="action-button"
          v-bind:disabled="isOnAction" 
          v-on:click="initialize">Reset</button>
      </div>
      
      <ez-footer></ez-footer>
    </div>`,
  vuex: {
    actions: { 
      initInput, updateTextInput, updateNumberInput,
      pushArrayInput, removeArrayInput, showList,
      submitActivity, initList, initMasterList, selectAndHideList,
      showMasterList, selectMasterList, getListData,
      refreshActivity, printActivity, changeActivitySchema,
      updateDateInput, initSelectedDetails, selectAllDetails,
      updateSelectedDetails, removeSelectedDetails, calculateTotal,
      clearActivitySchema, initServerInput, clearList,
      setActivitySchema
    },
    getters: { 
      input, oldInput, list, activitySchema, master, selectedDetails,
      user, permission, selectedMasterData
    }
  }
}
