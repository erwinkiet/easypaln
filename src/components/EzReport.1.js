import { 
  initInput, updateTextInput, updateNumberInput,
  pushArrayInput, removeArrayInput, showList,
  submitActivity, initList, initMasterList, selectAndHideList,
  showMasterList, selectMasterList, getListData,
  refreshActivity, printActivity, changeActivitySchema,
  updateDateInput, initSelectedDetails, selectAllDetails,
  updateSelectedDetails, removeSelectedDetails, calculateTotal,
  clearActivitySchema, initServerInput, clearList,
  clearSelectedMaster
} from '../vuex/actions'
import * as helper from '../helper'
import { 
  input, oldInput, activitySchema, master,
  user, permission
} from '../vuex/getters'
import { numberDisplay, dateDisplay } from '../filters'
import { dateBox, copyButton } from '../directives'

const Decimal = require('decimal.js')

export default {
  filters: { numberDisplay, dateDisplay },
  directives: { dateBox, copyButton },
  data: function () {
    return { }
  },
  methods: {
    initialize () {
      if (this.activitySchema.hasOwnProperty('master')) {
        this.initMasterList(this.activitySchema.master)
      } else {
        this.initMasterList({ })
      }
      
      if (this.activitySchema.hasOwnProperty('input')) {
        this.initInput(this.activitySchema.input)
      } else {
        this.initInput({ })
      }      
    },
    generateDetailControl (k, v, inputField) {      
      let template = ''
      
      template += `<div class="input-group">`
      
      if (v.type === 'text' || v.type === 'textarea' || v.type === 'number' || v.type === 'date') {
        template += `
          ${v.type === 'textarea' ? '<textarea' : '<input'} 
          ${v.type === 'date' ? `v-date-box="'DD/MM/YYYY'"` : ''}
          v-bind:value="${v.value}${
            v.type === 'number' ? ` | numberDisplay${
              v.hasOwnProperty('numberFormat') ? ` '${v.numberFormat}'` : ''
            }` : 
            (v.type === 'date' ? ' | dateDisplay' : '')
          }" 
          class="ez-${v.type}-input"
          v-bind:disabled="${v.disabled ? v.disabled : false}"          
          ${v.type === 'number' ? 'v-on:focus="$event.target.select()"' : ''}
          v-on:change="${
            v.hasOwnProperty('list') ? `getListData(detail, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
            (v.type === 'number' ? `updateNumberInput(detail, '${k}', $event.target.value)` : 
            (v.type === 'date' ? `updateDateInput(detail, '${k}', $event.target.value)` : 
              `updateTextInput(detail, '${k}', $event.target.value)`))
          }"          
          ${
            v.type === 'number' && v.hasOwnProperty('list') ?
              `style="padding-right: 2.5em;"` : ''
          }
          ${v.type === 'textarea' ? '></textarea>' : ' />'}`
      } else if (v.type === 'checkbox') {
        template += `
          <input type="checkbox" 
            v-bind:true-value="${v.trueValue}" 
            v-bind:false-value="${v.falseValue}"
            v-model="${v.value}" 
            class="ez-${v.type}-input"
            v-bind:disabled="${v.disabled ? v.disabled : false}" />
            ${v.label}`
      } else if (v.type === 'dropdown') {
        template += `
          <select v-bind:value="${v.value}"
            class="ez-${v.type}-input"
            v-bind:disabled="${v.disabled ? v.disabled : false}" 
            v-on:change="${
              v.hasOwnProperty('list') ? `getListData(detail, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
                `updateTextInput(detail, '${k}', $event.target.value)`
            }">`
        
        for (let i = 0; i < v.options.length; i++) {
          let opt = v.options[i]
          
          template += `<option value="${opt.value}">${opt.label}</option>`
        }
        
        template += `</select>`
      }
      
      template += `${
            v.hasOwnProperty('list') ? 
            `<button class="list-button" type="button" v-on:keyup.enter.stop="" v-on:click="showList('${v.list}', list.${v.list}, detail, $index)"
                v-bind:disabled="${
                  v.hasOwnProperty('listDisabled') ? v.listDisabled : (v.hasOwnProperty('disabled') ? v.disabled : false)
                }">. . .</button>` : ''
          }
        </div>`
      
      return template
    },
    generateControl (k, v) {
      let template = ''
      
      if (v.type === 'detail') {
        this.initSelectedDetails(k)
        
        let gridId = 'detail-' + k
        
        template += `<div id="${gridId}" class="detail-control">
            <span class="detail-label">${v.label}</span>`
            
        if (v.hasOwnProperty('action')) {
          let l = v.action
          
          if (l.hasOwnProperty('add')) {
            template += ` 
              <button type="button" 
                v-on:click="pushArrayInput(input, '${k}', getDefaultDetail('${k}'))"
                v-on:click="pushArrayInput(input, '${k}', getDefaultDetail('${k}'))" 
                ${l.hasOwnProperty('disabled') ? `v-bind:disabled="${l.disabled}"` : '' }>
                Add Row
              </button>`
          }
          
          if (l.hasOwnProperty('delete')) {
            template += ` 
              <button type="button" 
                v-on:click="removeSelectedDetails(input, '${k}')"
                v-on:click="removeSelectedDetails(input, '${k}')" 
                ${l.hasOwnProperty('disabled') ? `v-bind:disabled="${l.disabled}"` : '' }>
                Delete Rows
              </button>`
          }            
        }
     
        // <button type="button" v-copy-button="detailToTabString(input.${k}, selectedDetails.${k})" >Copy Rows</button>
        template += ` <button type="button" v-on:click="selectAllDetails(input, '${k}')">Select All</button>
            
            <div class="grid-container">
              <table>
                <thead>
                  <tr><th class="select-box"></th>`
                  
        let maxCol = 0
        for (let l in v.control) {
          if (v.control.hasOwnProperty(l)) {
            template += `<th v-bind:style="{
                width: '${v.control[l].width ? v.control[l].width : 100}px'
              }">`
            template += v.control[l].label
            template += '</th>'
            
            maxCol++
          }    
        }
        
        template += `
                  </tr>
                </thead>
                
                <tbody>
                  <tr v-for="detail of ${v.value}" track-by="$index">
                    <td><input type="checkbox" v-model="selectedDetails.${k}" v-bind:value="$index" /></td>`
                  
        let i = 0
        for(let l in v.control) {
          if (v.control.hasOwnProperty(l)) {
            template += `<td v-bind:class="['cell${i}' + $index]" 
              v-on:keyup.enter.down="selectNextRow('${gridId}', ${i}, $index, ${maxCol}, ${v.value}.length, 1)"
              v-on:keyup.up="selectNextRow('${gridId}', ${i}, $index, ${maxCol}, ${v.value}.length, -1)">`
            template += this.generateDetailControl(l, v.control[l], k)
            template += '</td>'
            i++
          }    
        }
        
        template += `
                  </tr>
                </tbody>
              </table>
            </div>`        
                        
        template += `</div>`
      } else {
        template += `<div class="form-control${
          v.hasOwnProperty("inline") && v.inline ? ' inline-control' : ` grid-${
            v.hasOwnProperty('width') ? v.width : 6
          }`
        }" ${
            v.hasOwnProperty('show') ? `v-show="${v.show}"` : '' 
          }><label>${v.type !== 'checkbox' ? v.label : '&nbsp;'}
          <div class="input-group${v.hasOwnProperty ? ' with-list' : ''}">`
        
        if (v.type === 'text' || v.type === 'textarea' || v.type === 'number' || v.type === 'date') {
          template += `
            ${v.type === 'textarea' ? '<textarea' : '<input'} 
            type="${v.type === 'checkbox' ? 'checkbox' : 'text'}"
            ${v.type === 'date' ? `v-date-box="'DD/MM/YYYY'"` : ''}
            v-bind:value="${v.value}${
              v.type === 'number' ? ` | numberDisplay${
                v.hasOwnProperty('numberFormat') ? ` '${v.numberFormat}'` : ''
              }` : 
              (v.type === 'date' ? ' | dateDisplay' : '')
            }"
            class="ez-${v.type}-input"
            ${v.type === 'number' ? 'v-on:focus="$event.target.select()"' : ''}
            v-bind:disabled="${v.disabled ? v.disabled : false}"
            v-on:change="${
              v.hasOwnProperty('list') ? `getListData(input, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
              (v.type === 'number' ? `updateNumberInput(input, '${k}', $event.target.value)` : 
              (v.type === 'date' ? `updateDateInput(input, '${k}', $event.target.value)` : 
                `updateTextInput(input, '${k}', $event.target.value)`))
            }"
            ${
              v.type === 'number' && v.hasOwnProperty('list') ?
                `style="padding-right: 2.5em;"` : ''
            }
            ${v.type === 'textarea' ? '></textarea>' : ' />'}`
        } else if (v.type === 'checkbox') {
          template += `
            <input type="checkbox" 
              class="ez-${v.type}-input"
              v-bind:disabled="${v.disabled ? v.disabled : false}" 
              v-bind:true-value="${v.trueValue}" 
              v-bind:false-value="${v.falseValue}"
              v-model="${v.value}" />
              ${v.label}`
        } else if (v.type === 'radio') {
          for (let i = 0; i < v.options.length; i++) {
            let opt = v.options[i]
            template += `
              <label class="radio-option">
                <input type="radio" 
                  class="ez-${v.type}-input"
                  v-bind:disabled="${v.disabled ? v.disabled : false}" 
                  v-bind:value="${opt.value}" 
                  v-model="${v.value}" />
                  ${opt.label}
              </label>`
          }
        } else if (v.type === 'dropdown') {
          template += `
            <select v-bind:value="${v.value}"
              class="ez-${v.type}-input"
              v-bind:disabled="${v.disabled ? v.disabled : false}" 
              v-on:change="${
                v.hasOwnProperty('list') ? `getListData(input, '${k}', $event.target.value, '${v.list}', list.${v.list})` :
                  `updateTextInput(input, '${k}', $event.target.value)`
              }">`
          
          for (let i = 0; i < v.options.length; i++) {
            let opt = v.options[i]
            
            template += `<option value="${opt.value}">${opt.label}</option>`
          }
          
          template += `</select>`
        }
        
        template += `${
                v.hasOwnProperty('list') ? 
                `<button class="list-button" type="button" v-on:click="showList('${v.list}', list.${v.list}, input)"
                  v-bind:disabled="${
                    v.hasOwnProperty('listDisabled') ? v.listDisabled : (v.hasOwnProperty('disabled') ? v.disabled : false)
                  }">. . .</button>` : ''
              }
            </div>
          </label></div>`
      }

      return template
    }
  },
  created: function () {    
    this.initialize()
    
    let template = `<div class="ez-report"><h1>${this.activitySchema.title}</h1><hr />`
    
    if (this.activitySchema.hasOwnProperty('debug') && this.activitySchema.debug) {
      template += `<div><pre>{{ input | json }}</pre></div>`
    }
    
    template += `<div>`
    for (let k in this.activitySchema.control) {
      if (this.activitySchema.control.hasOwnProperty(k)) {
        let v = this.activitySchema.control[k]
        
        template += this.generateControl(k, v)
      }
    }
    template += `</div></div>`
    this.$options.template = template
  },
  vuex: {
    actions: { 
      initInput, updateTextInput, updateNumberInput,
      pushArrayInput, removeArrayInput, showList,
      submitActivity, initList, initMasterList, selectAndHideList,
      showMasterList, selectMasterList, getListData,
      refreshActivity, printActivity, changeActivitySchema,
      updateDateInput, initSelectedDetails, selectAllDetails,
      updateSelectedDetails, removeSelectedDetails, calculateTotal,
      clearActivitySchema, initServerInput, clearList,
      clearSelectedMaster
    },
    getters: { 
      input, oldInput, list, activitySchema, master, selectedDetails, selectedMasterData,
      user, permission
    }
  }
}
