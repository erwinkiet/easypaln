import { 
  initInput, updateTextInput, updateNumberInput,
  pushArrayInput, removeArrayInput, showList,
  submitActivity, initList, selectAndHideList,
  showMasterList, selectMasterList, getListData,
  refreshActivity, printActivity
} from '../vuex/actions'
import { input, list } from '../vuex/getters'
import numberDisplay from '../filters'

const insertIcon = require('../images/icon-insert.png')
const updateIcon = require('../images/icon-update.png')
const deleteIcon = require('../images/icon-delete.png')
const printIcon = require('../images/icon-print.png')
const refreshIcon = require('../images/icon-refresh.png')

export default{
  filters: {
    numberDisplay
  },
  methods: {
    initialize () {
      this.initList({
        master: {
          data: [],
          option: {
            currentPage:1,
            pageSize: 20
          },
          idField: 'PKBID',
          binding: {
            transactionId: "PKBID",
            requestedDate: "Date",
            approvedByName: "ApprovedName",
            approvedDate: "ApprovedDate",
            rejectedByName: "RejectedName",
            rejectedDate: "RejectedDate",
            remark: "Remark",
            items: "items"
          }
        },
        item: {
          data: [],
          option: {
            currentPage:1,
            pageSize: 20
          },
          idField: 'Name',
          binding: {
            itemName: "Name",
            unit: "Unit",
            status: "Availability"
          }
        }
      })
      this.initInput({
        transactionId: '',
        requestedDate: '',
        approvedByName: '',
        approvedDate: '',
        rejectedByName: '',
        rejectedDate: '',
        remark: '',
        items: []
      })
      this.showMasterList(this.list.master)
    },
    getDefaultItem () {
      return {
        detailId: 0,
        itemName: '',
        qty: '0',
        unit: '',
        neededDate: '',
        remark: '',
        requestedByName: '',
        status: ''
      }
    }
  },
  created: function () {
    this.initialize()
  },
  vuex: {
    actions: { 
      initInput, updateTextInput, updateNumberInput,
      pushArrayInput, removeArrayInput, showList,
      submitActivity, initList, selectAndHideList,
      showMasterList, selectMasterList, getListData,
      refreshActivity, printActivity
    },
    getters: { input, list }
  },
  template: `
    <ul v-show="list.master.data.length > 0">
      <li v-for="itemData in list.master.data">
        <button v-on:click="selectMasterList(list.master, itemData)">Select</button>
        {{itemData.PKBID}} - {{itemData.Date}} - {{itemData.Remark}} - {{itemData.Status}}
      </li>
    </ul>
    <form novalidate>
      <label>
        Transaction No
        <input type="text" v-bind:value="input.transactionId" disabled
          v-on:change="updateTextInput(input, 'transactionId', $event.target.value)" />
      </label>
      <label>
        Requested Date
        <input type="text" v-bind:value="input.requestedDate" autofocus
          v-on:change="updateTextInput(input, 'requestedDate', $event.target.value)" />
      </label>
      <label>
        Approved By
        <input type="text" v-bind:value="input.approvedByName" disabled
          v-on:change="updateTextInput(input, 'approvedByName', $event.target.value)" />
      </label>
      <label>
        Approved Date
        <input type="text" v-bind:value="input.approvedDate" disabled
          v-on:change="updateTextInput(input, 'approvedDate', $event.target.value)" />
      </label>
      <label>
        Rejected By
        <input type="text" v-bind:value="input.rejectedByName" disabled
          v-on:change="updateTextInput(input, 'rejectedByName', $event.target.value)" />
      </label>
      <label>
        Rejected Date
        <input type="text" v-bind:value="input.rejectedDate" disabled
          v-on:change="updateTextInput(input, 'rejectedDate', $event.target.value)" />
      </label>
      <label>
        Remark
        <input type="text" v-bind:value="input.remark"
          v-on:change="updateTextInput(input, 'remark', $event.target.value)" />
      </label>
      
      <div>
        Items
        <button v-on:click="pushArrayInput(input, 'items', getDefaultItem())">Add Item</button>
        
        <div v-show="list.item.data.length > 0">   
          <label>
            Page:
            <input type="text" v-model="list.item.option.currentPage" />
          </label>       
          <ul>
            <li v-for="itemData in list.item.data">
              <button v-on:click="selectAndHideList(list.item, itemData)">Select</button>
              {{itemData.Name}} - {{itemData.Unit}} - {{itemData.Availability}}
            </li>
          </ul>
        </div>
        
        <div v-for="item in input.items">
          <label>
            Item Name
            <input type="text" v-bind:value="item.itemName"
              v-on:change="getListData(item, 'itemName', $event.target.value, 'item', list.item)" />
            <button v-on:click="showList('item', list.item, item)">...</button>
          </label>
          
          <label>
            Qty
            <input type="text" v-bind:value="item.qty | numberDisplay"
              v-on:change="updateNumberInput(item, 'qty', $event.target.value)" />
          </label>
          <label>
            Unit
            <input type="text" v-bind:value="item.unit"
              v-on:change="updateTextInput(item, 'unit', $event.target.value)" />
          </label>
          <label>
            Needed Date
            <input type="text" v-bind:value="item.neededDate"
              v-on:change="updateTextInput(item, 'neededDate', $event.target.value)" />
          </label>
          <label>
            Remark
            <input type="text" v-bind:value="item.remark"
              v-on:change="updateTextInput(item, 'remark', $event.target.value)" />
          </label>
          <label>
            Requested By
            <input type="text" v-bind:value="item.requestedByName"
              v-on:change="updateTextInput(item, 'requestedByName', $event.target.value)" />
          </label>
          <label>
            Status
            <input type="text" v-bind:value="item.status" disabled
              v-on:change="updateTextInput(item, 'status', $event.target.value)" />
          </label>
          <button v-on:click="removeArrayInput(input, 'items', $index)">Delete</button>
        </div>
      </div>
    </form>

    <input type="image" src="${insertIcon}" class="imagebutton" title="Insert"
      v-on:click="submitActivity('insert')" v-bind:disabled="input.transactionId != ''" />
    <input type="image" src="${updateIcon}" class="imagebutton" title="Update"
      v-on:click="submitActivity('update')" v-bind:disabled="input.transactionId == ''" />
    <input type="image" src="${deleteIcon}" class="imagebutton" title="Delete"
      v-on:click="submitActivity('delete')" v-bind:disabled="input.transactionId == ''" />
    <input type="image" src="${printIcon}" class="imagebutton" title="Print"
      v-on:click="printActivity('')" v-bind:disabled="input.transactionId == ''" />
    <input type="image" src="${refreshIcon}" class="imagebutton" title="Refresh"
      v-on:click="refreshActivity()" />
    <div><pre>{{ input | json }}</pre></div>`
}
