import EzReport from './EzReport'
import { 
  changeActivitySchema, getCurrentUserAndPermission, resetState,
  setCurrentUser, setCurrentPermission
} from '../vuex/actions'
import { activitySchema } from '../vuex/getters'
import { getReportJsonPath, getReportAshxPath } from '../helper'
import localStorage from 'store'

export default {
  components: { 
    EzReport,
    TmpLoading: {
      template: '<div>Loading report...</div>'
    }
  },
  data: function () {
    return {
      currentComponent: 'TmpLoading'
    }
  },
  route: {
    data: function (transition) {
      if (!localStorage.get('userToken')) {
        transition.abort()
        this.$route.router.go('./login')
      } else {
        // Start of temporary bug fix
        if (!localStorage.get('bugTmpFix')) {
          localStorage.set('bugTmpFix', 1)
        } else {
          let btf = localStorage.get('bugTmpFix')
          
          if (btf === 1) {
            localStorage.set('bugTmpFix', 0)
            location.reload()
            return
          } else {
            localStorage.set('bugTmpFix', 1)
          }
        }
        // End of temporary bug fix
        
        const file = getReportJsonPath(this.$route.params.path, this.$route.params.form)
        
        this.resetState()
        this.currentComponent = 'TmpLoading'
        
        this.$http.get(file).then(function (response) {
          this.changeActivitySchema(response.data)
                    
          if (response.data.hasOwnProperty('permission')) {
            const ashxFile = getReportAshxPath(this.$route.params.path, this.$route.params.form)
            
            this.$http.get(ashxFile, {
              formName: response.data.permission
            }).then(function (response) {
              if (Object.keys(response.data.permission).length === 0 && 
                response.data.permission.constructor === Object) {
                alert('You do not have permission to view this page.')
                transition.abort()
              } else {
                this.setCurrentUser(response.data.user)
                this.setCurrentPermission(response.data.permission)
                
                transition.next({
                  currentComponent: 'EzReport'
                })
              }
            }, function (response) {
              alert('Internal server error. Please try again. ' +
                'If the problem persists, contact IT Support!')
              transition.abort()
            })
          } else {            
            //this.currentComponent = 'EzActivity'
            transition.next({
              currentComponent: 'EzReport'
            })
          }
        }, function (response) { 
          transition.abort()
          //alert('Internal server error. Please try again. ' +
          //  'If the problem persists, contact IT Support!')
        })
      }
    }
  },
  template: `
    <div class="report">
      <component v-bind:is="currentComponent"></component>
    </div>`,
  vuex: {
    actions: { 
      changeActivitySchema, getCurrentUserAndPermission, resetState,
      setCurrentUser, setCurrentPermission
    },
    getters: { activitySchema }
  }
}
