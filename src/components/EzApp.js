import store from '../vuex/store'

export default {
  template: '<router-view></router-view>',
  store
}
