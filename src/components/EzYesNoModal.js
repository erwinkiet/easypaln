import EzModal from './EzModal'
import * as helper from '../helper'

export default {
  props: {
    show: {
      type: Boolean,
      required: true,
      twoWay: true
    },
    msg: {
      type: String,
      required: true
    }
  },
  components: { EzModal },
  methods: {
    onYes () {
      this.show = false
      
      this.$dispatch('yes')
    },
    
    onNo () {
      this.show = false
    }
  },
  template: `
    <ez-modal v-bind:show="show" class="ez-action-modal">
      <div slot="header">
        <h3>Confirmation</h3>
      </div>
      
      <div slot="body">        
        {{msg}}
      </div>
      <div slot="footer">
        The action cannot be undone
        <button class="modal-default-button"
          v-on:click="onYes">Yes
        </button>
        <button class="modal-default-button"
          v-on:click="onNo">No
        </button>
      </div>
    </ez-modal>`
}
