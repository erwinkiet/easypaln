import EzHeader from './EzHeader'
import EzFooter from './EzFooter'
import EzActivity from './EzActivity'
import { 
  changeActivitySchema, getCurrentUserAndPermission, resetState,
  setCurrentUser, setCurrentPermission
} from '../vuex/actions'
import { activitySchema } from '../vuex/getters'
import { getActivityPath, getAshxPath } from '../helper'
import localStorage from 'store'

export default {
  components: { 
    EzActivity, EzHeader, EzFooter,
    TmpLoading: {
      template: '<div>Loading activity...</div>'
    }
  },
  data: function () {
    return {
      currentComponent: 'TmpLoading'
    }
  },
  route: {
    data: function (transition) {
      if (!localStorage.get('userToken')) {
        transition.abort()
        this.$route.router.go('./login')
      } else {
        // Start of temporary bug fix
        if (!localStorage.get('bugTmpFix')) {
          localStorage.set('bugTmpFix', 1)
        } else {
          let btf = localStorage.get('bugTmpFix')
          
          if (btf === 1) {
            localStorage.set('bugTmpFix', 0)
            location.reload()
            return
          } else {
            localStorage.set('bugTmpFix', 1)
          }
        }
        // End of temporary bug fix
        
        const file = getActivityPath(this.$route.params.path, this.$route.params.form)
        
        this.resetState()
        this.currentComponent = 'TmpLoading'
        
        this.$http.get(file).then(function (response) {
          this.changeActivitySchema(response.data)
                    
          if (response.data.hasOwnProperty('permission')) {
            const ashxFile = getAshxPath(this.$route.params.path, this.$route.params.form)
            
            this.$http.get(ashxFile, {
              formName: response.data.permission
            }).then(function (response) {
              if (Object.keys(response.data.permission).length === 0 && 
                response.data.permission.constructor === Object) {
                alert('You do not have permission to view this page.')
                transition.abort()
              } else {
                this.setCurrentUser(response.data.user)
                this.setCurrentPermission(response.data.permission)
                
                transition.next({
                  currentComponent: 'EzActivity'
                })
              }
            }, function (response) {
              alert('Internal server error. Please try again. ' +
                'If the problem persists, contact IT Support!')
              transition.abort()
            })
          } else {            
            //this.currentComponent = 'EzActivity'
            transition.next({
              currentComponent: 'EzActivity'
            })
          }
        }, function (response) { 
          transition.abort()
          //alert('Internal server error. Please try again. ' +
          //  'If the problem persists, contact IT Support!')
        })
      }
    }
  },
  template: `
    <div class="page">
      <ez-header></ez-header>
      <div class="content">
        <component v-bind:is="currentComponent"></component>
      </div>
      <ez-footer></ez-footer>
    </div>`,
  vuex: {
    actions: { 
      changeActivitySchema, getCurrentUserAndPermission, resetState,
      setCurrentUser, setCurrentPermission
    },
    getters: { activitySchema }
  }
}
