export default {
  props: ['progress'],
  template: `
    <div class="ez-loading-bar">
      <span class="meter" v-bind:style="{ width: progress + '%' }">
        <p>Loading...</p>
      </span>
    </div>`
}
