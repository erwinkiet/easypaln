const config = require('../config.json')
const logoImage = require('../images/logo.png')

import { logout } from '../vuex/actions'

function createMenu (obj) {
  let tmp = ''
  for (let k in obj) {
    if (obj.hasOwnProperty(k)) {
      let v = obj[k]
      tmp += createMenuItem(k, v, true)
    }
  }
  return tmp
}

function createMenuItem (key, val, isRoot) {
  if (typeof val === 'object') {
    let tmp = `<li class="${isRoot ? 'nav-link ' : ''}more"><a href="javascript:void(0)">${key}</a>
      <ul class="submenu">`
    for (let k in val) {
      if (val.hasOwnProperty(k)) {
        let v = val[k]
        tmp += createMenuItem(k, v, false)
      }
    }
    tmp += `</ul></li>`
    return tmp
  } else {
    return `<li${isRoot ? ' class="nav-link"' : ''}><a v-link="'${val}'">${key}</a></li>`
  }
}

export default {
  vuex: {
    actions: { logout }
  },
  template: `
    <header class="navigation" role="banner">
      <div class="navigation-wrapper">
        <a v-link="'/'" class="logo">
          <img src="${logoImage}" alt="Easy">
        </a>
        <a href="javascript:void(0)" class="navigation-menu-button" id="js-mobile-menu">MENU</a>
        <nav role="navigation">
          <ul id="js-navigation-menu" class="navigation-menu show">
            ${createMenu(config.menu)}
          </ul>
        </nav>
        <div class="navigation-tools">
          <ul class="navigation-menu show">
            <li class="nav-link"><a v-link="'/login'" v-on:click="logout">Log out</a></li>
          </ul>
        </div>
      </div>
    </header>
    `
}