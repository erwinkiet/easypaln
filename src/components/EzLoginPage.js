import EzFooter from './EzFooter'
import EzLoadingBar from './EzLoadingBar'
import { login } from '../vuex/actions'
import * as helper from '../helper'

export default {
  vuex: {
    actions: { login }
  },
  data: function () {
    return {
      logoImage: require('../images/logoe.png'),
      username: '',
      password: '',
      isLoading: false,
      errors: { },
      helper: helper
    }
  },
  methods: {
    onLogin: function () {
      this.isLoading = true
      this.login(this.username, this.password, this.onSuccess, this.onError)
    },
    onSuccess: function () {

    },
    onError: function (errs) {
      this.isLoading = false
      this.errors = errs
    }
  },
  components: { EzFooter, EzLoadingBar },
  template: `
    <div class="login-box">
      <form novalidate>
        <div class="header">
          <img v-bind:src="logoImage" />
          <span>Login to your account</span>
        </div>

        <div class="body">
          <div v-show="isLoading">
            <ez-loading-bar progress="100"></ez-loading-bar>
          </div>

          <div v-show="!isLoading && !helper.isEmptyObject(errors)" class="error">
            <ul v-for="errs of errors">
              <li v-for="err in errs">{{ err }}</li>
            </ul>
          </div>

          <label>
            Username
            <input type="text" placeholder="Username" autofocus
              v-model="username" v-on:keyup.enter="onLogin()"
              v-bind:disabled="isLoading" />
          </label>
          
          <label>
            Password
            <input type="password" placeholder="Password"
              v-model="password" v-on:keyup.enter="onLogin()"
              v-bind:disabled="isLoading" />
          </label>
          
          <div>
            <button type="button"
              v-on:click="onLogin()"
              v-bind:disabled="isLoading">Submit</button>
          </div>
        </div>
      </form> 
      <ez-footer></ez-footer>
    </div>`
}
