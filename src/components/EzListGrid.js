import { 
  selectAndHideList, showList, addListFilter, removeListFilter
} from '../vuex/actions'
import EzModal from './EzModal'
import EzLoadingBar from './EzLoadingBar'
import * as helper from '../helper'
import moment from 'moment'
import numeral from 'numeral'

export default {
  props: ['list', 'type'],
  data: function () {
    return {
      moment: moment,
      numeral: numeral
    }
  },
  template: `
    <div class="ez-list-grid">
      <ez-modal v-bind:show="list.show">
        <div slot="header">
          <span class="modal-title">{{ list.title }}</span>
          <button class="modal-default-button"
            v-on:click="list.show = false">
            Close
          </button>
        </div>
        <div slot="body">
          <div class="field-search" v-for="filter of list.option.filters" track-by="$index">
            <input type="search" placeholder="Enter Search" v-model="filter"
              v-on:keyup.enter="applySearch" />
            <div>
              <button type="button" v-on:click="applySearch">Search</button>
              <button type="button" v-on:click="addFilterRow($index)"> + </button>
              <button type="button" v-on:click="removeFilterRow($index)"> - </button>
            </div>
          </div>
          
          <div class="grid-container">
            <table>
              <thead>
                <tr>
                  <th v-for="(field, display) of list.display" v-bind:class="{ 
                      'ez-number-input': (display.type === 'number'), 
                      'headerSortUp': isSorted(field, 'asc'),
                      'headerSortDown': isSorted(field, 'desc')
                    }" 
                    v-on:click="setSort(field)"
                    v-bind:style="{
                      width: (display.width ? display.width : 200) + 'px'
                    }">
                    {{ display.label }}
                  </th>
                </tr>
              </thead>
              
              <tbody>
                <tr v-for="itemData of list.data" v-on:click="selectAndHideList(list, itemData)">
                  <td v-for="(field, display) of list.display" v-bind:class="{ 'ez-number-input': (display.type === 'number') }">
                    {{ display.type === 'date' ? (itemData[field] === '' ? '' : moment(itemData[field]).format('DD/MM/YYYY')) : 
                      (display.type === 'number' ? numeral(itemData[field]).format('0,0.00[00000000]') : 
                      itemData[field]) }}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          
          <div v-show="list.isLoading">
            <ez-loading-bar progress="100"></ez-loading-bar>
          </div>

          <div v-show="!list.isLoading && list.data.length === 0" class="nodata">
            No data
          </div>

          <div class="pagination">
            <button type="button" v-on:click="toFirstPage()">First</button>
            <button type="button" v-on:click="changePage(-1)">Prev</button>
            <input type="text" v-model="list.option.currentPage"
              v-on:keyup.enter="filterAndShowList"
              v-on:change="filterAndShowList" />
            <button type="button" v-on:click="changePage(1)">Next</button>
            <button type="button" v-on:click="toLastPage()">Last</button>
          </div>
        </div>
      </ez-modal>
    </div>`,
  components: { EzModal, EzLoadingBar },
  vuex: {
    actions: { 
      selectAndHideList, showList, addListFilter, removeListFilter
    }
  },
  methods: {
    isSorted (key, direction) {
      return this.list.option.sortings.filter(x => x.field === key && x.direction === direction).length > 0
    },
    setSort (key) {
      let exist = false

      for (let i = 0; i < this.list.option.sortings.length; i++) {
        let tmp = this.list.option.sortings[i]
        
        if (tmp.field === key) {
          exist = true

          if (tmp.direction === 'asc') {
            tmp.direction = 'desc'
          } else if (tmp.direction === 'desc') {
            this.list.option.sortings.splice(i, 1)
            break
          }
        }
      }

      if (!exist) {
        this.list.option.sortings.push({ field: key, direction: 'asc' })
      }
      
      this.applySearch()
    },
    addFilterRow (index) {
      this.addListFilter(this.list, index)
    },
    removeFilterRow (index) {
      this.removeListFilter(this.list, index)
      this.applySearch()
    },
    applySearch() {
      this.list.option.currentPage = 1
      this.showList(this.type, this.list)
    },
    filterAndShowList () {
      let dataCount = this.list.dataCount
      let pageSize = this.list.option.pageSize
      let lastPage = Math.ceil(dataCount / pageSize)

      if (this.list.option.currentPage === '' || this.list.option.currentPage < 1) {
        this.list.option.currentPage = 1
      } else if (this.list.option.currentPage > lastPage) {
        this.list.option.currentPage = lastPage
      }
      
      this.showList(this.type, this.list)
    },
    changePage (inc) {
      let tmp = parseInt(this.list.option.currentPage) + inc
      this.list.option.currentPage = tmp > 0 ? tmp : 1

      let dataCount = this.list.dataCount
      let pageSize = this.list.option.pageSize
      let lastPage = Math.ceil(dataCount / pageSize)
      
      if (this.list.option.currentPage > lastPage) {
        this.list.option.currentPage = lastPage
      }
      
      this.showList(this.type, this.list);
    },
    toFirstPage () {
      this.list.option.currentPage = 1
      
      this.showList(this.type, this.list);
    },
    toLastPage () {
      let dataCount = this.list.dataCount
      let pageSize = this.list.option.pageSize

      this.list.option.currentPage = Math.ceil(dataCount / pageSize)
      
      this.showList(this.type, this.list);
    }
  }
}
