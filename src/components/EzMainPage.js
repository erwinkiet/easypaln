import EzHeader from './EzHeader'
import EzFooter from './EzFooter'
import localStorage from 'store'

export default {
  route: {
    data: function (transition) {
      if (!localStorage.get('userToken')) {
        transition.abort()
        this.$route.router.go('./login')
      } else {        
        transition.next()
      }
    }
  },
  template: `
    <div class="page">
      <ez-header></ez-header>
      <div class="content">Main page</div>
      <ez-footer></ez-footer>
    </div>`,
  components: { EzHeader, EzFooter }
}
