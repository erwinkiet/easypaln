import { 
  selectMasterList, showMasterList, addListFilter, removeListFilter 
} from '../vuex/actions'
import moment from 'moment'
import numeral from 'numeral'
import EzLoadingBar from './EzLoadingBar'

export default {
  components: { EzLoadingBar },
  props: ['list', 'ashxFile', 'multiselect', 'selectionVar'],
  data: function () {
    let tmp = []

    for (let k in this.list.display) {
      if (this.list.display.hasOwnProperty(k)) {
        tmp.push(k)
      }
    }

    return {
      statusOptions: this.list.option.statusFilters.slice(0, this.list.option.statusFilters.length),
      showDisplay: tmp,
      allFields: tmp.slice(0, tmp.length),
      moment: moment,
      numeral: numeral,
      prevSelectedRow: null
    }
  },
  template: `
    <div class="ez-master-grid">
      <div class="field-search" v-for="filter of list.option.filters" track-by="$index">
        <input type="search" placeholder="Enter Search" v-model="filter"
          v-on:keyup.enter="applySearch" />
        <div>
          <button type="button" v-on:click="applySearch">Search</button>
          <button type="button" v-on:click="addFilterRow($index)"> + </button>
          <button type="button" v-on:click="removeFilterRow($index)"> - </button>
        </div>
      </div>
      
      <div class="status-search" v-show="statusOptions.length > 0">
        <label><b>Show Status: </b></label>
        <label v-for="status of statusOptions">
          <input type="checkbox" name="status" v-bind:value="status" v-model="list.option.statusFilters"
            v-on:change="applySearch" /> {{status}}
        </label>
      </div>
      
      <div class="field-filter">
        <label><b>Show Field: </b></label>
        <label v-for="field of allFields">
          <input type="checkbox" name="field" v-bind:value="field" v-model="showDisplay" /> {{ list.display[field].label }}
        </label>
      </div>
      
      <div class="grid-container">
        <table>
          <thead>
            <tr>
              <th v-for="(field, display) of list.display" v-bind:class="{ 
                  'ez-number-input': (display.type === 'number'),
                  'headerSortUp': isSorted(field, 'asc'),
                  'headerSortDown': isSorted(field, 'desc')
                }"
                v-on:click="setSort(field)"
                v-if="showDisplay.indexOf(field) > -1"
                v-bind:style="{
                  width: (display.width ? display.width : 200) + 'px'
                }">
                {{ display.label }}
              </th>
            </tr>
          </thead>
          
          <tbody v-show="!list.isLoading">
            <tr v-for="itemData of list.data" v-on:click="selectListItem(itemData, $event.target)"
              v-bind:class="{ selected: isSelected(itemData) }">
              <td v-for="(field, display) of list.display" 
                v-if="showDisplay.indexOf(field) > -1"
                v-bind:class="{ 'ez-number-input': (display.type === 'number') }">
                {{ display.type === 'date' ? (itemData[field] === '' ? '' : moment(itemData[field]).format('DD/MM/YYYY')) : 
                  (display.type === 'number' ? 
                  (display.numberFormat ? 
                    numeral(itemData[field]).format(display.numberFormat) :
                    numeral(itemData[field]).format('0,0.00[00000000]')) : 
                  itemData[field]) }}
              </td>
            </tr>
          </tbody>
        </table>
        
        <div v-show="list.isLoading">
          <ez-loading-bar progress="100"></ez-loading-bar>
        </div>

        <div v-show="!list.isLoading && list.data.length === 0" class="nodata">
          No data
        </div>
      </div>
      
      <div class="pagination">
        <button type="button" v-on:click="toFirstPage()">First</button>
        <button type="button" v-on:click="changePage(-1)">Prev</button>
        <input type="text" v-model="list.option.currentPage"
          v-on:keyup.enter="filterAndShowList"
            v-on:change="filterAndShowList" />
        <button type="button" v-on:click="changePage(1)">Next</button>
        <button type="button" v-on:click="toLastPage()">Last</button>

        <label>Page size:
          <select v-bind:value="list.option.pageSize" v-on:change="changePageSize($event.target.value)">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </select>
        </label>
      </div>
    </div>`,
  vuex: {
    actions: { 
      selectMasterList, showMasterList, addListFilter, removeListFilter
    }
  },
  methods: {
    isSelected (itemData) {
      return this.selectionVar.filter(x => x[this.list.idField] === itemData[this.list.idField]).length !== 0
    },
    isSorted (key, direction) {
      return this.list.option.sortings.filter(x => x.field === key && x.direction === direction).length > 0
    },
    setSort (key) {
      let exist = false

      for (let i = 0; i < this.list.option.sortings.length; i++) {
        let tmp = this.list.option.sortings[i]
        
        if (tmp.field === key) {
          exist = true

          if (tmp.direction === 'asc') {
            tmp.direction = 'desc'
          } else if (tmp.direction === 'desc') {
            this.list.option.sortings.splice(i, 1)
            break
          }
        }
      }

      if (!exist) {
        this.list.option.sortings.push({ field: key, direction: 'asc' })
      }
      
      this.applySearch()
    },
    addFilterRow (index) {
      this.addListFilter(this.list, index)
    },
    removeFilterRow (index) {
      this.removeListFilter(this.list, index)
      this.applySearch()
    },
    applySearch () {
      this.list.option.currentPage = 1
      this.showMasterList(this.ashxFile)
    },
    selectListItem (itemData, dom) {
      /*dom.parentNode.className = 'selected'
      
      if (this.prevSelectedRow && this.prevSelectedRow !== dom.parentNode) {
        this.prevSelectedRow.className = ''
      }
      
      this.prevSelectedRow = dom.parentNode*/
      this.selectMasterList(itemData, this.multiselect, this.ashxFile)
    },
    filterAndShowList () {
      let dataCount = this.list.dataCount
      let pageSize = this.list.option.pageSize
      let lastPage = Math.ceil(dataCount / pageSize)

      if (this.list.option.currentPage === '' || this.list.option.currentPage < 1) {
        this.list.option.currentPage = 1
      } else if (this.list.option.currentPage > lastPage) {
        this.list.option.currentPage = lastPage
      }

      this.showMasterList(this.ashxFile)
    },
    changePageSize (newSize) {
      this.list.option.pageSize = newSize

      this.showMasterList(this.ashxFile)
    },
    changePage (inc) {
      let tmp = parseInt(this.list.option.currentPage) + inc
      this.list.option.currentPage = tmp > 0 ? tmp : 1

      let dataCount = this.list.dataCount
      let pageSize = this.list.option.pageSize
      let lastPage = Math.ceil(dataCount / pageSize)

      if (this.list.option.currentPage > lastPage) {
        this.list.option.currentPage = lastPage
      }

      this.showMasterList(this.ashxFile)
    },
    toFirstPage () {
      this.list.option.currentPage = 1

      this.showMasterList(this.ashxFile)
    },
    toLastPage () {
      let dataCount = this.list.dataCount
      let pageSize = this.list.option.pageSize

      this.list.option.currentPage = Math.ceil(dataCount / pageSize)
      
      this.showMasterList(this.ashxFile)
    }
  }
}
