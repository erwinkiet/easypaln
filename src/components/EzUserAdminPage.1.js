import EzHeader from './EzHeader'
import EzFooter from './EzFooter'
import EzMasterGrid from './EzMasterGrid'

export default {
  components: {
    EzFooter, EzHeader, EzMasterGrid
  },
  data: function () {
    return {
      input: {
        "userId": "",
        "username": "",
        "password": "",
        "passwordConfirm": "",
        "departmentId": "",
        "departmentName": "",
        "role": "",
        "name": "",
        "initial": "",
        "privilege": "",
        "status": "",
        "permissionIds": [],
        "warehouseIds": []
      },
      master: {
        "data": [],
        "isLoading": false,
        "option": {
          "statusFilters": ["Open", "Closed"],
          "currentPage": 1,
          "pageSize": 10,
          "filters": ['']
        },
        "idField": "UserID",
        "binding": {
          "userId": "UserID",
          "username": "UserName",
          "departmentId": "Department",
          "departmentName": "DepartmentName",
          "role": "Role",
          "name": "Name",
          "initial": "Initial",
          "privilege": "Privilege",
          "status": "Status",
          "permissionIds": "permissions",
          "warehouseIds": "warehouses"
        },
        "display": {
          "UserID": {
            "label": "ID",
            "type": "text",
            "width": 100
          },
          "UserName": {
            "label": "Username",
            "type": "text",
            "width": 200
          },
          "Name": {
            "label": "Name",
            "type": "text",
            "width": 300
          },
          "Initial": {
            "label": "Initial",
            "type": "text",
            "width": 100
          },
          "Department": {
            "label": "Department",
            "type": "text",
            "width": 100
          },
          "DepartmentName": {
            "label": "DepartmentName",
            "type": "text",
            "width": 200
          },
          "Role": {
            "label": "Role",
            "type": "text",
            "width": 100
          },
          "Status": {
            "label": "Status",
            "type": "text",
            "width": 100
          }    
        }
      }
    }
  },
  template: `
    <div class="user-admin page">
      <ez-header></ez-header>
      <div class="content">
        <h1>Manage User</h1><hr />
        
        <ez-master-grid v-bind:list="master" v-bind:ashx-file="'./apis/config/User.ashx'"></ez-master-grid>
      </div>
      <ez-footer></ez-footer>
    </div>`
}