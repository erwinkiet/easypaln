import { 
  initInput, updateInput, submitActivity
} from '../vuex/actions'
import { input } from '../vuex/getters'

const insertIcon = require('../images/icon-insert.png')
const updateIcon = require('../images/icon-update.png')
const deleteIcon = require('../images/icon-delete.png')

export default{
  ready: function () {
    this.initInput({
      currencyID: '',
      currencyName: '',
      conversion: 0
    })
  },
  vuex: {
    actions: { 
      initInput, updateInput, submitActivity
    },
    getters: { input }
  },
  template: `
    <form novalidate>
      <label> Currency ID
        <input type="text" autofocus v-bind:value="input.currencyID"
          v-on:change="updateInput('currencyID', $event.target.value)" />
      </label>
      <label> Currency Name
        <input type="text" v-bind:value="input.currencyName"
          v-on:change="updateInput('currencyName', $event.target.value)" />
      </label>
      <label> Conversion
        <input type="text" v-bind:value="input.conversion"
          v-on:change="updateInput('conversion', $event.target.value)" />
      </label>
    </form>

    <input type="image" src="${insertIcon}" class="imagebutton" title="Insert"
      v-on:click="submitActivity('insert')" />
    <input type="image" src="${updateIcon}" class="imagebutton" title="Update"
      v-on:click="submitActivity('update')" />
    <input type="image" src="${deleteIcon}" class="imagebutton" title="Delete"
      v-on:click="submitActivity('delete')" />
    <div>{{ input | json }}</div>`
}
