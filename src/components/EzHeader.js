import EzNavigation from './EzNavigation'

const logoImage = require('../images/logo.png')

export default {
  components: { EzNavigation },
  template: `
    <div class="header">
      <ez-navigation></ez-navigation>
    </div>`
  /*
      <div class="logo-image">
        <a v-link="'/'"><img src="${logoImage}" /></a>
      </div>
  */
}
