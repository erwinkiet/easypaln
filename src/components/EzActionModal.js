import EzModal from './EzModal'
import EzLoadingBar from './EzLoadingBar'
import * as helper from '../helper'

export default {
  props: {
    show: {
      type: Boolean,
      required: true,
      twoWay: true
    },
    errorSchema: {
      type: Object,
      required: true,
      twoWay: true
    },
    onSuccess: {
      type: Function,
      required: true
    }
  },
  components: { EzLoadingBar, EzModal },
  methods: {
    close (withCallback) {
      this.show = false
      this.errorSchema.status = 0
      this.errorSchema.data = null

      this.$dispatch('close')
      
      if (withCallback) {
        this.onSuccess()
      }
    }
  },
  template: `
    <ez-modal v-bind:show="show" class="ez-action-modal">
      <div slot="header">
      </div>
      
      <div slot="body">        
        <div v-if="errorSchema.data !== null">
          <div v-if="errorSchema.data.success">
            <strong>Success!</strong>
          </div>
          <div v-else>
            <strong>The following error(s) were found:</strong>
            <ul v-for="errs of errorSchema.data">
                <li v-for="err in errs">{{ err }}</li>
            </ul>
          </div>
          <br />
        </div>
        <div v-else>
          <ez-loading-bar progress="100"></ez-loading-bar>
        </div>
      </div>
      <div slot="footer" v-if="errorSchema.data !== null">
        <div v-if="errorSchema.data.success">
          Click OK to continue
          <button class="modal-default-button"
            v-on:click="close(true)">
            OK
          </button>
        </div>
        <div v-else>
          Please fix and try again.
          <button class="modal-default-button"
            v-on:click="close(false)">
            OK
          </button>
        </div>
      </div>
    </ez-modal>`
}
