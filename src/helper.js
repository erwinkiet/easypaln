import moment from 'moment'

export function getDefaultValue (val) {
  return isNaN(val) ? '' : '0'
}

export function isEmptyObject (obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object
}

export function toDate103 (date120) {
  const format = ['DD/MM/YYYY', 'YYYY-MM-DD']
  let m = moment(date120, format)
  return m.isValid() ? m.format('DD/MM/YYYY') : ''
}

export function detailToTabString (detail, selectedDetail) {
  let tmp = []
  
  for (let i = 0; i < selectedDetail.length; i++) {
    tmp.push(detail[selectedDetail[i]])
  }
  
  tmp.sort()
  
  let r = []
    
  for (let i = 0; i < tmp.length; i++) {
    let tmp2 = []
    for (let k in tmp[i]) {
      if (tmp[i].hasOwnProperty(k)) {
        tmp2.push(tmp[i][k])
      }
    }
    r.push(tmp2.join('\t'))
  }
  
  return r.join('\n')
}

export function capitalize(val) {
  let tmp = val.split(' ')
  
  let res = tmp.map(function (v) {
    return v.charAt(0).toUpperCase() + v.slice(1);
  })
  
  return res.join('')
}

export function removeHyphens(val) {
  let tmp = val.replace('-', '')
}

export function jsToCsFilename(filename) {
  let tmp = filename.split('-')
  
  let res = tmp.map(function (v) {
    return v.charAt(0).toUpperCase() + v.slice(1);
  })
  
  return res.join('')
}

export function getReportAshxPath(path, jsFilename) {
  return '.reports/' + path + '/' + jsToCsFilename(jsFilename) + '.ashx'
}

export function getReportJsonPath(path, jsonFileName) {
  return './reports/' + path + '/' + jsonFileName + '.json'
}

export function getAshxPath(path, jsFilename) {
  return './apis/' + path + '/' + jsToCsFilename(jsFilename) + '.ashx'
}

export function getActivityPath(path, jsonFileName) {
  return './activities/' + path + '/' + jsonFileName + '.json'
}
