import mutationType from './mutation-types'
import numeral from 'numeral'
import moment from 'moment'

export default {
  [mutationType.CALCULATE_TOTAL] (state, member, field, inputField, totalField) {
    state.input[totalField] = state.input[inputField].reduce(function (prev, curr) {
      return prev + numeral().unformat(curr[field])
    }, 0)
  },
  [mutationType.INIT_INPUT] (state, input) {
    state.input = JSON.parse(JSON.stringify(input))
    state.oldInput = { }
  },
  [mutationType.INIT_MASTER] (state, list) {
    state.master = list
  },
  [mutationType.INIT_LIST] (state, list) {
    state.list = Object.assign({}, list)
  },
  [mutationType.INIT_SELECTED_DETAILS] (state, field) {
    state.selectedDetails[field] = []
  },
  [mutationType.SELECT_ALL_DETAILS] (state, member, field) {    
    state.selectedDetails[field].splice(0, state.selectedDetails[field].length)
    for (let i = 0; i < member[field].length; i++) {
      state.selectedDetails[field].push(i)
    }
  },
  [mutationType.UPDATE_SELECTED_DETAILS] (state, field, checked, value) {
    if (checked) {
      if (!state.selectedDetails[field]) {
        state.selectedDetails[field] = []
      }
      state.selectedDetails[field].push(value)
    } else {
      let tmp = state.selectedDetails[field].indexOf(value)
      state.selectedDetails[field].splice(tmp, 1)
    }
  },
  [mutationType.REMOVE_SELECTED_DETAILS] (state, member, field) {
    if (!state.selectedDetails[field]) {
      return
    }
    
    state.selectedDetails[field].sort()
    for (let i = state.selectedDetails[field].length - 1; i >= 0; i--) {
      let tmp = parseInt(state.selectedDetails[field][i])
      member[field].splice(tmp, 1)
    }
    state.selectedDetails[field].splice(0, state.selectedDetails[field].length)
  },
  [mutationType.UPDATE_TEXT_INPUT] (state, member, field, value) {
    member[field] = value
  },
  [mutationType.UPDATE_NUMBER_INPUT] (state, member, field, value) {
    member[field] = numeral().unformat(value).toString()
  },
  [mutationType.UPDATE_DATE_INPUT] (state, member, field, value) {
    const format = ['DD/MM/YYYY', 'YYYY-MM-DD']
    let m = moment(value, format)
    member[field] = m.isValid() ? m.format('YYYY-MM-DD') : ''
  },
  [mutationType.PUSH_ARRAY_INPUT] (state, member, field, value) {
    member[field].push(value)
  },
  [mutationType.REMOVE_ARRAY_INPUT] (state, member, field, index) {
    member[field].splice(index, 1)
  },  
  [mutationType.BIND_LIST] (state, list, selectedItem) {
    let boundMember = list.currentBind
    let binding = list.binding
    
    for(let k in binding) {
      if(binding.hasOwnProperty(k)) {
        boundMember[k] = selectedItem[binding[k]]
      }
    }
  }
}
