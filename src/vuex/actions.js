import localStorage from 'store'

import mutationType from './mutation-types'
import { getAshxPath } from '../helper'

export function addListFilter (store, list, index) {
  list.option.filters.splice(index + 1, 0, '')
}

export function removeListFilter (store, list, index) {
  if (list.option.filters.length > 1) {
    list.option.filters.splice(index, 1)
  }
}

export function initServerInput (store, binding) {
  const file = getAshxPath(this.$route.params.path, this.$route.params.form)
  
  this.$http.get(file, {
    action: 'getDefaults'
  }).then(function (response) {
    let tmp = response.data[0]
    
    for (let k in binding) {
      if (binding.hasOwnProperty(k)) {
        if (typeof binding[k] === 'object') {
          for (let l in binding[k]) {
            if (binding[k].hasOwnProperty(l)) {
              store.state.activitySchema.defaultDetailInput[k][l] = tmp[binding[k][l]]
            }
          }
        } else {
          store.state.input[k] = tmp[binding[k]]
        }
      }
    }
  }, function (response) {
    // alert('Internal server error. Please try again. ' +
    //   'If the problem persists, contact IT Support!')
  })  
}

export function setActivitySchema (store, schema) {
  store.state.activitySchema = schema
}

export function resetState (store) {
  store.state.master = { }
  store.state.input = { }
  store.state.oldInput = { }
  store.state.list = { }
  store.state.activitySchema = { }
  store.state.selectedDetails = { }
}

export function clearList (store) {
  store.state.list = { }
}

export function clearActivitySchema (store) {
  store.state.activitySchema = { }
}

export function setCurrentUser (store, user) {
  store.state.user = user
}

export function setCurrentPermission (store, permission) {
  store.state.permission = permission
}

export function getCurrentUserAndPermission (store, transition) {
  const file = getAshxPath(this.$route.params.path, this.$route.params.form)
  const formName = this.$route.params.form
  
  this.$http.get(file, {
    formName: formName
  }).then(function (response) {
    transition.next()
  }, function (response) {
    alert('Internal server error. Please try again. ' +
      'If the problem persists, contact IT Support!')
    transition.abort()
  })
}

export function logout (store) {
  localStorage.remove('userToken')
  store.router.go('./login')
}

export function login (store, username, password, onSuccess, onError) {
  this.$http.post('./apis/Login.ashx', {
    username: username,
    password: password
  }).then(function (response) {
    onSuccess()
    localStorage.set('userToken', response.data)
    store.router.go('./')
  }, function (response) {
    onError(response.data)
    // if (response.status >= 400 && response.status < 500) {
    //  alert('Input error! ' + JSON.stringify(response.data))
    // } else if (response.status > 500 || response.status < 600) {
    //  alert('Internal server error. Please try again. ' +
    //    'If the problem persists, contact IT Support!')
    // }
  })
}

export function initInput (store, input) {
  store.dispatch(mutationType.INIT_INPUT, input)
}

export function initList (store, list) {
  let tmpList = { }
  
  for (let k in list) {
    if (list.hasOwnProperty(k)) {
      let v = list[k]

      tmpList[k] = { }

      tmpList[k].show = false
      tmpList[k].isLoading = false
      tmpList[k].dataCount = 0
      tmpList[k].title = v.hasOwnProperty('title') ? v.title : 'List'
      tmpList[k].idField = v.hasOwnProperty('idField') ? v.idField : ''
      tmpList[k].data = v.hasOwnProperty('data') ? v.data : []      
      tmpList[k].binding = v.hasOwnProperty('binding') ? Object.assign({},v.binding) : { }
      tmpList[k].display = v.hasOwnProperty('display') ? Object.assign({},v.display) : { }
      
      tmpList[k].option = { }
      if (v.hasOwnProperty('option')) {
        tmpList[k].option = Object.assign({ }, v.option)
        tmpList[k].option.filters =
          v.option.hasOwnProperty('filters') ? v.option.filters : ['']
        tmpList[k].option.currentPage = 
          v.option.hasOwnProperty('currentPage') ? v.option.currentPage : 1
        tmpList[k].option.pageSize = 
          v.option.hasOwnProperty('pageSize') ? v.option.pageSize : 20        
        tmpList[k].option.sortings = 
          v.option.hasOwnProperty('sortings') ? v.option.sortings : []        
      }
    }
  }

  store.dispatch(mutationType.INIT_LIST, tmpList)
}

export function initMasterList (store, list) {
  let tmpList = { }

  tmpList.data = list.hasOwnProperty('data') ? list.data : []
  tmpList.idField = list.hasOwnProperty('idField') ? list.idField : ''
  tmpList.binding = list.hasOwnProperty('binding') ? list.binding : { }
  tmpList.display = list.hasOwnProperty('display') ? list.display : { }
  tmpList.isLoading = false
  tmpList.dataCount = 0

  tmpList.option = { }
  if (list.hasOwnProperty('option')) {
    tmpList.option = Object.assign({}, list.option)
    
    tmpList.option.filters =
      list.option.hasOwnProperty('filters') ? list.option.filters : ['']

    tmpList.option.statusFilters = 
      list.option.hasOwnProperty('statusFilters') ? 
      list.option.statusFilters : []

    tmpList.option.currentPage = 
      list.option.hasOwnProperty('currentPage') ? list.option.currentPage : 1

    tmpList.option.pageSize = 
      list.option.hasOwnProperty('pageSize') ? list.option.pageSize : 20

    tmpList.option.sortings = 
      list.option.hasOwnProperty('sortings') ? list.option.sortings : []
  }

  store.dispatch(mutationType.INIT_MASTER, tmpList)
}

export function initSelectedDetails (store, field) {
  store.dispatch(mutationType.INIT_SELECTED_DETAILS, field)
}

export function selectAllDetails(store, member, field) {
  store.dispatch(mutationType.SELECT_ALL_DETAILS, member, field)
  
  let checkboxes = new Array(); 
  checkboxes = document.getElementById('detail-' + field).getElementsByTagName('input');

  for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i].type == 'checkbox') {
      checkboxes[i].checked = true;
    }
  }
}

export function updateSelectedDetails (store, field, checked, value) {
  store.dispatch(mutationType.UPDATE_SELECTED_DETAILS, field, checked, value)  
}

export function copySelectedDetails (store, member, field, defaultField, fields) {
  let tmp = []
  let tmp55 = store.state.selectedDetails[field]

  for (let i = 0; i < tmp55.length; i++) {
    tmp.push(member[field][tmp55[i]])
  }

  let res = []

  for (let i = 0; i < tmp.length; i++) {
    let tmp2 = Object.assign({}, defaultField)
    let tmp3 = tmp[i]

    for (let k in tmp3) {
      if (tmp3.hasOwnProperty(k)) {
        let v = tmp3[k]

        if (fields.indexOf(k) > -1) {
          tmp2[k] = v
        }
      }
    }
    res.push(tmp2)
  }

  localStorage.set('copiedDetail', res)
}

export function pasteDetails (store, member, field) {
  let tmp = localStorage.get('copiedDetail')

  member[field] = tmp
}

export function removeSelectedDetails (store, member, field) {
  store.dispatch(mutationType.REMOVE_SELECTED_DETAILS, member, field)  

  let checkboxes = new Array(); 
  checkboxes = document.getElementById('detail-' + field).getElementsByTagName('input');

  for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i].type == 'checkbox') {
      checkboxes[i].checked = false;
    }
  }
}

export function updateTextInput (store, member, field, value) {
  store.dispatch(mutationType.UPDATE_TEXT_INPUT, member, field, value)
}

export function updateNumberInput (store, member, field, value, inputField = '', totalField = '') {
  store.dispatch(mutationType.UPDATE_NUMBER_INPUT, member, field, value)
}

export function calculateTotal (store, member, field, inputField, totalField) {
  store.dispatch(mutationType.CALCULATE_TOTAL, member, field, inputField, totalField)
}

export function updateDateInput (store, member, field, value) {
  store.dispatch(mutationType.UPDATE_DATE_INPUT, member, field, value)
}

export function pushArrayInput (store, member, field, value) {
  store.dispatch(mutationType.PUSH_ARRAY_INPUT, member, field, value)
}

export function removeArrayInput (store, member, field, index) {
  store.dispatch(mutationType.REMOVE_ARRAY_INPUT, member, field, index)
}

export function printActivity (store, link) { }

export function refreshActivity (store) {
  this.initialize()
}

export function showMasterList (store, ashxFile) {
  const file = ashxFile || getAshxPath(this.$route.params.path, this.$route.params.form)
  let list = store.state.master
  list.isLoading = true
  
  this.$http.get(file, {
    option: JSON.stringify(list.option)
  }).then(function (response) {
    list.data = response.data
    list.currentBind = store.state.input
    if (response.data.length > 0) {
      list.dataCount = response.data[0].dataCount
    } else {
      list.dataCount = 0
    }
    list.isLoading = false
  }, function (response) {
    // alert('Internal server error. Please try again. ' +
    //   'If the problem persists, contact IT Support!')
  })
}

export function showList (store, listType, list, member, currentIndex = -1) {
  const file = getAshxPath(this.$route.params.path, this.$route.params.form)

  list.show = true
  list.isLoading = true

  this.$http.post(file, {
    list: listType, input: JSON.stringify(store.state.input),
    option: JSON.stringify(list.option),
    detailIndex: currentIndex
  }).then(function (response) {
    list.data = response.data
    list.isLoading = false
    if (response.data.length > 0) {
      list.dataCount = response.data[0].dataCount
    } else {
      list.dataCount = 0
    }
    if (member) {
      list.currentBind = member
    }
  }, function (response) {
    // alert('Internal server error. Please try again. ' +
    //  'If the problem persists, contact IT Support!')
  })
}

export function clearSelectedMaster (store) {
  store.state.selectedMasterData = []
}

export function selectMasterList(store, selectedItem, multiselect, ashxFile) {
  const file = ashxFile || getAshxPath(this.$route.params.path, this.$route.params.form)
  
  store.state.oldInput = Object.assign({ }, store.state.input)

  let list = store.state.master  
  let boundMember = store.state.input
  let boundMember2 = store.state.oldInput
  let binding = list.binding

  let id = selectedItem[list.idField]

  if (multiselect) {
    let tmp = store.state.selectedMasterData.filter(x => x[list.idField] === id)

    if (tmp.length === 0) {
      store.state.selectedMasterData.push(selectedItem)
    } else {
      store.state.selectedMasterData = store.state.selectedMasterData.filter(x => x[list.idField] !== id)
    }
  } else {
    store.state.selectedMasterData = []
    store.state.selectedMasterData.push(selectedItem)
  }
  
  for (let k in binding) {
    if (binding.hasOwnProperty(k)) {
      if (boundMember[k] && boundMember[k].constructor === Array && k !== 'rowVersion') {
        boundMember[k] = []
        boundMember2[k] = []
        this.$http.get(file, {
          id: selectedItem[list.idField],
          detail: binding[k]
        }).then(function (response) {
          boundMember[k] = response.data
          boundMember2[k] = JSON.parse(JSON.stringify(response.data))
        }, function (response) {
          alert('Internal server error. Please try again. ' +
            'If the problem persists, contact IT Support!')
        })
      } else {
        boundMember[k] = selectedItem[binding[k]]
        boundMember2[k] = selectedItem[binding[k]]
      }
    }
  }
}

export function selectAndHideList(store, list, selectedItem) {  
  let boundMember = list.currentBind
  let binding = list.binding  
  
  for (let k in binding) {
    if (binding.hasOwnProperty(k)) {
      if (boundMember[k] && boundMember[k].constructor === Array) {
        const file = getAshxPath(this.$route.params.path, this.$route.params.form)
        
        boundMember[k] = []
        this.$http.get(file, {
          id: selectedItem[list.idField],
          detail: binding[k]
        }).then(function (response) {
          boundMember[k] = response.data
        }, function (response) {
          // alert('Internal server error. Please try again. ' +
          //   'If the problem persists, contact IT Support!')
        })
      } else {
        boundMember[k] = selectedItem[binding[k]]
      }
    }
  }
  
  list.show = false
  list.data = []
  list.currentBind = { }
}

export function getListData(store, member, field, value, listType, list) {  
  store.dispatch(mutationType.UPDATE_TEXT_INPUT, member, field, value)
  
  const file = getAshxPath(this.$route.params.path, this.$route.params.form)
  
  let boundMember = member
  let binding = list.binding
  
  this.$http.get(file, {
    list: listType,
    id: value
  }).then(function (response) {
    if (response.data.length > 0) {
      let tmpData = response.data[0]
      for(let k in binding) {
        if(binding.hasOwnProperty(k)) {
          if (boundMember[k] && boundMember[k].constructor === Array) {
            const file = getAshxPath(this.$route.params.path, this.$route.params.form)
            
            boundMember[k] = []
            this.$http.get(file, {
              id: tmpData[list.idField],
              detail: binding[k]
            }).then(function (response) {
              boundMember[k] = response.data
            }, function (response) {
              // alert('Internal server error. Please try again. ' +
              //   'If the problem persists, contact IT Support!')
            })
          } else {
            boundMember[k] = tmpData[binding[k]]
          }
        }
      }
    } else {
      for(let k in binding) {
        if(binding.hasOwnProperty(k)) {
          if (k !== field) {
            if (boundMember[k] && boundMember[k].constructor === Array) {
              boundMember[k] = []
            } else {
              boundMember[k] = isNaN(boundMember[k]) || boundMember[k] === '' ? '' : '0'
            }
          }
        }
      }
    }
  }, function (response) {
    // alert('Internal server error. Please try again. ' +
    //   'If the problem persists, contact IT Support!')
  })
}

export function submitActivity (store, action, errors, onSuccess, onFail, ashxFile) {
  const file = ashxFile || getAshxPath(this.$route.params.path, this.$route.params.form)
  
  this.$http.post(file, {
    action: action, 
    input: JSON.stringify(store.state.input),
    oldInput: JSON.stringify(store.state.oldInput)
  }).then(function (response) {
    errors.status = response.status
    errors.data = { success: true }
    onSuccess(action, response.data)
  }, function (response) {
    errors.status = response.status
    errors.data = response.data
    onFail(action)
  })
}

export function changeActivitySchema (store, schema) {
  store.state.activitySchema = schema
}
