export function master (state) {
  return state.master
}

export function input (state) {
  return state.input
}

export function oldInput (state) {
  return state.oldInput
}

export function list (state) {
  return state.list
}

export function activitySchema (state) {
  return state.activitySchema
}

export function selectedMasterData (state) {
  return state.selectedMasterData
}

export function selectedDetails (state) {
  return state.selectedDetails
}

export function user (state) {
  return state.user
}

export function permission (state) {
  return state.permission
}
