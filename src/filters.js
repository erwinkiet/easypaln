import numeral from 'numeral'
import moment from 'moment'

export function numberDisplay (val, format) {
  format = typeof format !== 'undefined' ? format : '0,0.00'
  return numeral(val).format(format)
}

export function dateDisplay (val, format) {
  format = typeof format !== 'undefined' ? format : 'DD/MM/YYYY'
  let m = moment(val, 'YYYY-MM-DD')
  let mf = m.isValid() ? m.format(format) : ''
  return mf
}
