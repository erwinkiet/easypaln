import EzLoginPage from '../components/EzLoginPage'
import EzUserAdminPage from '../components/EzUserAdminPage'
import EzMainPage from '../components/EzMainPage'
import EzActivityPage from '../components/EzActivityPage'
import EzReportPage from '../components/EzReportPage'

export default {
  '/login': {
    component: EzLoginPage
  },
  '/useradmin': {
    component: EzUserAdminPage
  },
  '/activity/:path/:form': {
    component: EzActivityPage
  },
  '/report/:path/:form': {
    component: EzReportPage
  },
  '/': {
    component: EzMainPage
  }
}
