require('../node_modules/normalize.css/normalize.css')
require('./styles/app.scss')
require('../node_modules/pikaday/css/pikaday.css')

import Vue from 'vue'
import VueResource from 'vue-resource'
import { sync } from 'vuex-router-sync'
import EzApp from './components/EzApp'
import store from './vuex/store'
import router from './router/router'
import localStorage from 'store'

const path = require('path')

Vue.config.debug = true
Vue.config.devtools = true
Vue.use(VueResource)
//Vue.http.options.root = './'
Vue.http.options.emulateJSON = true
Vue.http.options.emulateHTTP = true
Vue.http.interceptors.push({
  request: function (request) {
    let token = localStorage.get('userToken')
    
    if (token) {
      request.headers = { userToken: token }
    }
    
    return request;
  },

  response: function (response) {
      return response;
  }
});

sync(store, router)

router.start(EzApp, 'ez-app')
/* eslint-disable no-new *
new Vue({
  el: 'body',
  components: { EzApp },
  store
})*/
