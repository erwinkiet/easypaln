import moment from 'moment'
import Pikaday from 'pikaday'
import Clipboard from 'clipboard'
//require('script!../node_modules/colResizable/node_modules/jquery/dist/jquery.js')
//require('script!../node_modules/colresizable/colResizable-1.6.min.js')

/*export let resizableTable = {
  bind: function () { },
  update: function (newValue, oldValue) {
    window.setTimeout(function () {
      $('table').colResizable({
        resizeMode: 'overflow'
      })
    })
  },
  unbind: function () { }
}*/

export let dateBox = {
  bind: function () { },
  update: function (newValue, oldValue) {
    if (this.picker) {
      this.picker.destroy()
    }
    
    this.picker = new Pikaday({
      field: this.el,
      format: newValue
    })
  },
  unbind: function () {
    if (this.picker) {
      this.picker.destroy()
    }
  }
}

export let copyButton = {
  bind: function () {
    this.clipboard = new Clipboard(this.el)
  },
  update: function (newValue, oldValue) {
    if (this.clipboard) {
      this.clipboard.destroy()
    }
    
    this.clipboard = new Clipboard(this.el, {
      text: function (trigger) {
        return newValue
      }
    })
  },
  unbind: function () {
    if (this.clipboard) {
      this.clipboard.destroy()
    }
  }
}
